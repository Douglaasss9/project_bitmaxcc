package bit.minisys.minicc.icgen;

import java.io.File;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.parser.ast.ASTCompilationUnit;
import bit.minisys.minicc.parser.ast.ASTCompoundStatement;
import bit.minisys.minicc.parser.ast.ASTFunctionDefine;
import bit.minisys.minicc.parser.ast.ASTNode;

enum IC_OP_TYPE{
	IC_OP_ADD,
	IC_OP_LOAD,
	IC_LABEL
}

class Quadruple{
	public IC_OP_TYPE op;
	public String arg1;
	public String arg2;
	public String result;
}

class TempVar{
	static public int num = 0;
	public int id;
	
	static public TempVar getNewTempVar() {
		TempVar tv = new TempVar();
		tv.id = TempVar.num;
		TempVar.num++;
		
		return tv; 
	}
}

//This is a demo implementation of IC generator
public class ExampleICGener implements IMiniCCICGen{
	private ArrayList<Quadruple> iclist;
	
	public ExampleICGener() {
		this.iclist = new ArrayList<Quadruple>();
	}
	
	@Override
	public String run(String iFile) throws Exception {
		String oFile = MiniCCUtil.remove2Ext(iFile) + MiniCCCfg.MINICC_ICGEN_OUTPUT_EXT;;
		
		// Step 1: read in AST in the JSON file
		ObjectMapper mapper = new ObjectMapper();
		ASTCompilationUnit program = (ASTCompilationUnit)mapper.readValue(new File(iFile), ASTCompilationUnit.class);
	    
		// Step 2: build symbol table or reuse the table built before
		//TODO:
		
		// Step 3: generate intermediate code
		//TODO: change null to your symbol table object
		ICBuilder icb = new ICBuilder(null);
		program.accept(icb);
		
		// Step 4: output intermediate code
		//TODO:
		System.out.println("5. Icgen finished!");
		return oFile;
	}
}
