package bit.minisys.minicc.icgen;

import java.util.Stack;

import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.IRBrfalse;
import bit.minisys.minicc.internal.ir.IRBrtrue;
import bit.minisys.minicc.internal.ir.IRConstval;
import bit.minisys.minicc.internal.ir.IRDassign;
import bit.minisys.minicc.internal.ir.IRExpression;
import bit.minisys.minicc.internal.ir.IRGotoStatement;
import bit.minisys.minicc.internal.ir.IRIassign;
import bit.minisys.minicc.internal.ir.IRIassignfpoff;
import bit.minisys.minicc.internal.ir.IRIread;
import bit.minisys.minicc.internal.ir.IRIreadfpoff;
import bit.minisys.minicc.internal.ir.IRLabel;
import bit.minisys.minicc.internal.ir.VirtualReg;
import bit.minisys.minicc.internal.symbol.ArrayType;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;
import bit.minisys.minicc.parser.ast.ASTArrayAccess;
import bit.minisys.minicc.parser.ast.ASTArrayDeclarator;
import bit.minisys.minicc.parser.ast.ASTBinaryExpression;
import bit.minisys.minicc.parser.ast.ASTBreakStatement;
import bit.minisys.minicc.parser.ast.ASTCastExpression;
import bit.minisys.minicc.parser.ast.ASTCharConstant;
import bit.minisys.minicc.parser.ast.ASTCompilationUnit;
import bit.minisys.minicc.parser.ast.ASTCompoundStatement;
import bit.minisys.minicc.parser.ast.ASTConditionExpression;
import bit.minisys.minicc.parser.ast.ASTContinueStatement;
import bit.minisys.minicc.parser.ast.ASTDeclaration;
import bit.minisys.minicc.parser.ast.ASTDeclarator;
import bit.minisys.minicc.parser.ast.ASTExpression;
import bit.minisys.minicc.parser.ast.ASTExpressionStatement;
import bit.minisys.minicc.parser.ast.ASTFloatConstant;
import bit.minisys.minicc.parser.ast.ASTFunctionCall;
import bit.minisys.minicc.parser.ast.ASTFunctionDeclarator;
import bit.minisys.minicc.parser.ast.ASTFunctionDefine;
import bit.minisys.minicc.parser.ast.ASTGotoStatement;
import bit.minisys.minicc.parser.ast.ASTIdentifier;
import bit.minisys.minicc.parser.ast.ASTInitList;
import bit.minisys.minicc.parser.ast.ASTIntegerConstant;
import bit.minisys.minicc.parser.ast.ASTIterationDeclaredStatement;
import bit.minisys.minicc.parser.ast.ASTIterationStatement;
import bit.minisys.minicc.parser.ast.ASTLabeledStatement;
import bit.minisys.minicc.parser.ast.ASTMemberAccess;
import bit.minisys.minicc.parser.ast.ASTNode;
import bit.minisys.minicc.parser.ast.ASTParamsDeclarator;
import bit.minisys.minicc.parser.ast.ASTPostfixExpression;
import bit.minisys.minicc.parser.ast.ASTReturnStatement;
import bit.minisys.minicc.parser.ast.ASTSelectionStatement;
import bit.minisys.minicc.parser.ast.ASTStatement;
import bit.minisys.minicc.parser.ast.ASTStringConstant;
import bit.minisys.minicc.parser.ast.ASTToken;
import bit.minisys.minicc.parser.ast.ASTTypename;
import bit.minisys.minicc.parser.ast.ASTUnaryExpression;
import bit.minisys.minicc.parser.ast.ASTUnaryTypename;
import bit.minisys.minicc.parser.ast.ASTVariableDeclarator;
import bit.minisys.minicc.parser.ast.ASTVisitor;

public class ICBuilder  implements ASTVisitor{
	//TODO: change object to your symbol table class
	private Stack<Object> symTblStack ;

	public ICBuilder(Object tbl) {
		this.symTblStack = new Stack<Object>();
		this.symTblStack.push(tbl);
	}
	
	@Override
	public void visit(ASTCompilationUnit program) throws Exception {
		// TODO Auto-generated method stub
		for(ASTNode n: program.items) {
			if(n instanceof ASTFunctionDefine)
				visit((ASTFunctionDefine)n);
		}
	}

	@Override
	public void visit(ASTDeclaration declaration) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTArrayDeclarator arrayDeclarator) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTVariableDeclarator variableDeclarator) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTFunctionDeclarator functionDeclarator) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTParamsDeclarator paramsDeclarator) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTArrayAccess arrayAccess) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTBinaryExpression binaryExpression) throws Exception {
		if(binaryExpression == null) return;
		
		String opString = binaryExpression.op.value;
		ASTExpression realExpr = binaryExpression.expr1;
		if (opString.equals("+") || opString.equals("-")) {
			TempVar tv = TempVar.getNewTempVar();
			
			Quadruple q = new Quadruple();
			q.op = IC_OP_TYPE.IC_OP_ADD;
			//q.result = tv;
			binaryExpression.iclist.add(q);
		}
		
	}

	@Override
	public void visit(ASTBreakStatement breakStat) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTContinueStatement continueStatement) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTCastExpression castExpression) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTCharConstant charConst) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTCompoundStatement compoundStat) throws Exception {
		for(ASTNode n: compoundStat.blockItems) {
			 if (n instanceof ASTStatement) {
					visit((ASTStatement)n);
				}
		}
	}

	@Override
	public void visit(ASTConditionExpression conditionExpression) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTExpression expression) throws Exception {
		if(expression == null)
			return;
		if(expression instanceof ASTBinaryExpression) {
			visit((ASTBinaryExpression)expression);
		}
	}

	@Override
	public void visit(ASTExpressionStatement expressionStat) throws Exception {
		if(expressionStat == null)return;
		for (ASTExpression e : expressionStat.exprs) {
			visit(e);
		}		
	}

	@Override
	public void visit(ASTFloatConstant floatConst) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTFunctionCall funcCall) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTGotoStatement gotoStat) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTIdentifier identifier) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTInitList initList) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTIntegerConstant intConst) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTIterationDeclaredStatement iterationDeclaredStat) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTIterationStatement iterationStat) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTLabeledStatement labeledStat) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTMemberAccess memberAccess) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTPostfixExpression postfixExpression) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTReturnStatement returnStat) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTSelectionStatement selectionStat) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTStringConstant stringConst) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTTypename typename) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTUnaryExpression unaryExpression) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTUnaryTypename unaryTypename) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTFunctionDefine functionDefine) throws Exception {
		if(functionDefine.body != null) {
			visit(functionDefine.body);
		}
		
	}

	@Override
	public void visit(ASTDeclarator declarator) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ASTStatement statement) throws Exception {
		if(statement == null)
			return;
		if(statement instanceof ASTExpressionStatement) {
			visit((ASTExpressionStatement)statement);
		}
	}

	@Override
	public void visit(ASTToken token) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
