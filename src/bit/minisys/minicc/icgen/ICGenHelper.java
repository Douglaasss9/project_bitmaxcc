package bit.minisys.minicc.icgen;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.icgen.IMiniCCICGen;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.parser.ast.*;
import bit.minisys.minicc.symbol.FunctionSymbol;
import bit.minisys.minicc.symbol.SymbolTableStack;

import com.fasterxml.jackson.databind.ObjectMapper;
import bit.minisys.minicc.ir.*;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ICGenHelper {
	
	private StringBuilder codes = new StringBuilder();
	private SymbolTableStack symbolTableStack;
	private int nextVarId = 0;
	private int nextIfId = 0;
	private int nextForId = 0;
	private HashMap<IRExpression, String> types = new HashMap<>();
	private HashMap<IRExpression, Integer> tempVarId = new HashMap<>();
	
    public ICGenHelper() {
        symbolTableStack = new SymbolTableStack();
        symbolTableStack.appendSymbolTable("global");
    }
	
    private int insertTempVar(IRExpression expression) {
        tempVarId.put(expression, nextVarId);
        nextVarId++;
        return nextVarId - 1;
    }
    
    private int getTempId(IRExpression expression) {
        return tempVarId.get(expression);
    }
    
    private String type2LLVMType(String type) {
        if (type.equals("int")) {
            return "i32";
        } else if (type.equals("char")) {
            return "i8";
        } else if (type.equals("bool")) {
            return "i1";
        } else if (type.equals("void")) {
            return "void";
        }
        
        return "error";
    }
    
	public StringBuilder getCodes() {
		
		return codes;
	}
	public void compilationUnit(IRCompilationUnit cu) {
		
				for(int i=0;i<cu.variableNames.size();i++) {
			
			IRVariableDeclaration var = cu.variableNames.get(i);
			
			String varType = var.varType;
			String llvmType = type2LLVMType(varType);
			String varName = var.varName;
			int initValue = var.value;
			symbolTableStack.addVariable(varName, varType, "global", 0);
			
			codes.append("@").append(varName).append(" = global ")
            	 .append(llvmType).append(" ").append(initValue).append("\n");
		}
		
				for (int i=0;i<cu.functionNames.size();i++) {
			IRFunction func = cu.functionNames.get(i);
			
			String funcName = func.funcName;
			String retType = func.returnType;
			String llvmRetType = type2LLVMType(retType);
			
			ArrayList<String> paramsType = new ArrayList<>();
	        for (IRVariableDeclaration par: func.paramList) {
	        		        	paramsType.add(func.paramList.get(i).varType);
	        }
			
	     	        symbolTableStack.addFunction(funcName, retType, paramsType);
	        
	     	        symbolTableStack.appendSymbolTable(funcName);
	        
	        StringBuilder params = new StringBuilder();
	        
	       
	        boolean flag = true;		        	        for (IRVariableDeclaration par: func.paramList) {
	            String type = par.varType;
	            String llvmType = type2LLVMType(type);
	            String name = par.varName;
	            
	            	            symbolTableStack.addVariable(name, type, "param", 0);
	            String fullName = symbolTableStack.getFullVariableName(name);
	            if (flag) {
	            	flag = false;
	                params.append(llvmType).append(" %").append(fullName);
	            } else {
	                params.append(", ").append(llvmType).append(" %").append(fullName);
	            }

	        } 
	        
	        	        codes.append("define ").append(llvmRetType).append(" @").append(funcName)
	                .append("(").append(params).append(") {\nentry:\n");
	        
	        	        scope(func.body);
	        
	        codes.append("}\n\n");

	        symbolTableStack.pop();
		}	
	}
	
	public void scope(IRScope sc) {
		for(int i=0;i<sc.variableNames.size();i++) {
			
			IRVariableDeclaration var = sc.variableNames.get(i);
			
			String type = var.varType;
            String llvmType = type2LLVMType(type);
            String varName = var.varName;
            
                        symbolTableStack.addVariable(varName, type, "local", 0);
            String fullName = symbolTableStack.getFullVariableName(varName);
            
            codes.append("    %").append(fullName).append(" = alloca ").append(llvmType).append("\n");
            
            if(var.initExpr != null) {
            	expression(var.initExpr);
            	
            	codes.append("    store ").append(llvmType).append(" %")
                	 .append(getTempId(var.initExpr))
                	 .append(", ").append(llvmType).append("* %")
                	 .append(fullName).append("\n");
            }
		}
		
		for(int i=0;i<sc.statements.size();i++) {
			IRStatement state = sc.statements.get(i);
			
			if(state.statType.equals("return")) {
				
				expression(state.returnStat.expr);
				
	            String type = types.get(state.returnStat.expr);
	            String llvmType = type2LLVMType(type);
	            int id = getTempId(state.returnStat.expr);

	            codes.append(String.format("    ret %s %%%d\n", llvmType, id));
			}
			else if(state.statType.contentEquals("expr")) {
				
				expression(state.exprStat.expr);
			}
			else if(state.statType.equals("select")) {
				
				IRExpression condition = state.selectStat.cond;
		        expression(condition);
		        int conditionId = getTempId(condition);

		        codes.append(String.format("    br i1 %%%d, label %%if_%d_true, label %%if_%d_false\n",
		                conditionId, nextIfId, nextIfId));

		        codes.append(String.format("if_%d_true:\n", nextIfId));
		        scope(state.selectStat.trueScope);
		        codes.append(String.format("    br label %%if_%d_end\n", nextIfId));

		        codes.append(String.format("if_%d_false:\n", nextIfId));
		        if (state.selectStat.hasFalse) {
		            scope(state.selectStat.falseScope);
		        }
		        codes.append(String.format("    br label %%if_%d_end\n", nextIfId));

		        codes.append(String.format("if_%d_end:\n", nextIfId));
		        nextIfId++;
			}
			else if(state.statType.equals("iterate")) {
				symbolTableStack.appendSymbolTable("for_" + nextForId);
				
				String initType = state.iterateStat.varNames.get(0).varType;
		        String llvmType = type2LLVMType(initType);
		        
		        for(int j=0;j<state.iterateStat.varNames.size();j++) {
		        	
		        	IRVariableDeclaration var = state.iterateStat.varNames.get(j);
		        	String varName = var.varName;
		        	symbolTableStack.addVariable(varName, initType, "local", 0);
		        	String fullVarName = symbolTableStack.getFullVariableName(varName);
		        	codes.append(String.format("    %%%s = alloca %s\n", fullVarName, llvmType));
		        	
		        	if(state.iterateStat.varNames.get(j).initExpr != null) {
		        		expression(var.initExpr);
		        		codes.append("    store ").append(llvmType).append(" %")
                        	 .append(getTempId(var.initExpr))
                        	 .append(", ").append(llvmType).append("* %")
                             .append(fullVarName).append("\n");
		        	}
		        		        			        	
		        }
		        		           
		        codes.append(String.format("    br label %%iteration_%d_cond\n", nextForId));

		        codes.append(String.format("iteration_%d_cond:\n", nextForId));
		        expression(state.iterateStat.cond);
		        
		        int conditionId = getTempId(state.iterateStat.cond);

		        codes.append(String.format("    br i1 %%%d, label %%iteration_%d_body, label %%iteration_%d_end\n",
		                conditionId, nextForId, nextForId));

		        codes.append(String.format("iteration_%d_step:\n", nextForId));
		        expression(state.iterateStat.step);

		        codes.append(String.format("    br label %%iteration_%d_cond\n", nextForId));

		        codes.append(String.format("iteration_%d_body:\n", nextForId));

		        int back = nextForId;
		        nextForId++;
		        scope(state.iterateStat.body);
		        codes.append(String.format("    br label %%iteration_%d_step\n", back));

		        codes.append(String.format("iteration_%d_end:\n", back));

			}
		}
	}
	
	public void expression(IRExpression expr) {
		
		if(expr.exprType.equals("const")) {
			int id = insertTempVar(expr);
			int value = expr.constval.value;
            codes.append(String.format("    %%%d = add i32 0, %d\n", id, value));
            types.put(expr, "int");
		}
		else if(expr.exprType.equals("var")) {
			String varName = expr.var.varName;
            String fullName = symbolTableStack.getFullVariableName(varName);
            String type = symbolTableStack.variableType(varName);
            String llvmType = type2LLVMType(type);
            String varFlag = symbolTableStack.getVariableFlag(varName);
            
            expr.var.flag = varFlag;
            expr.var.varType = type;
            
            types.put(expr, type);
            int id = insertTempVar(expr);

            if (varFlag.equals("local")) {
                codes.append("    %").append(id).append(" = load ").append(llvmType)
                        .append(", ").append(llvmType).append("* %").append(fullName).append("\n");
            } else if (varFlag.equals("param")) {
                codes.append(String.format("    %%%d = add %s 0, %%%s\n", id, llvmType, fullName));
            } else if (varFlag.equals("global")) {
                codes.append(String.format("    %%%d = add %s 0, @%s\n", id, llvmType, fullName));
            }
		}
		else if(expr.exprType.equals("binary")) {
			
			String op = expr.op;
	        if (op.equals("=")) {
	            
	        	expression(expr.opnd1);
	        	
	            int id2 = getTempId(expr.opnd1);

	            String type2 = types.get(expr.opnd1);
	            String llvmType2 = type2LLVMType(type2);
	            
	            
	            String name = expr.opnd0.var.varName;
	            String fullName = symbolTableStack.getFullVariableName(name);
	            codes.append("    store ").append(llvmType2).append(" %").append(id2)
	                        .append(", ").append(llvmType2).append("* %").append(fullName).append("\n");
	   

	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = add %s 0, %%%d\n", id, llvmType2, id2));

	            types.put(expr, type2);
	            return;
	        }

	        expression(expr.opnd0);
	        expression(expr.opnd1);

	        int id1 = tempVarId.containsKey(expr.opnd0) ? getTempId(expr.opnd0) : -1;
	        int id2 = tempVarId.containsKey(expr.opnd1) ? getTempId(expr.opnd1) : -1;

	        String type1 = types.get(expr.opnd0);
	        String llvmType1 = type2LLVMType(type1);
	        String type2 = types.get(expr.opnd1);
	        String llvmType2 = type2LLVMType(type2);
	        
	        if (op.equals("+")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = add %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals("-")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = sub %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals(">")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = icmp sgt %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals("<")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = icmp slt %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals(">=")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = icmp sge %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals("<=")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = icmp sle %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals("&&")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = and i1 %%%d, %%%d\n", id, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals("||")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = or i1 %%%d, %%%d\n", id, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals("!=")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = icmp ne %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals("==")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = icmp eq %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, "bool");
	        }
	        else if (op.equals("*")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = mul %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals("/")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = sdiv %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals("%")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = srem %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals("<<")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = shl %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals(">>")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = ashr %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals("&")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = and %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals("|")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = or %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
	        else if (op.equals("^")) {
	            int id = insertTempVar(expr);
	            codes.append(String.format("    %%%d = xor %s %%%d, %%%d\n", id, llvmType1, id1, id2));

	            types.put(expr, type1);
	        }
		}
		else if(expr.exprType.equals("postfix")) {
			 
			 String op = expr.op;
			 
		     if (op.equals("++")) {
		    	 
		    	 String varName = expr.var.varName;
		    	 String fullName = symbolTableStack.getFullVariableName(varName);
		    	 String type = symbolTableStack.variableType(varName);
	             String llvmType = type2LLVMType(type);
	             int id = insertTempVar(expr);		                		                

		         codes.append(String.format("    %%%d = load %s, %s* %%%s\n", id, llvmType, llvmType, fullName));
		         codes.append(String.format("    %%%d = add %s %%%d, 1\n", id + 1, llvmType, id));
		         codes.append(String.format("    store %s %%%d, %s* %%%s\n", llvmType, id + 1, llvmType, fullName));
		         nextVarId++;

		         types.put(expr, type);
		            
		     }
		     else if (op.equals("--")) {
		    
		    	 String name = expr.var.varName;
		         String fullName = symbolTableStack.getFullVariableName(name);
		         String type = symbolTableStack.variableType(name);
		         String llvmType = type2LLVMType(type);
		         int id = insertTempVar(expr);

		         codes.append(String.format("    %%%d = load %s, %s* %%%s\n", id, llvmType, llvmType, fullName));
		         codes.append(String.format("    %%%d = sub %s %%%d, 1\n", id + 1, llvmType, id));
		         codes.append(String.format("    store %s %%%d, %s* %%%s\n", llvmType, id + 1, llvmType, fullName));
		         nextVarId++;

		         types.put(expr, type);
		     }
		        
		}
		else if(expr.exprType.equals("call")) {
			
			for(int j =0;j<expr.call.paramList.size();j++) {
				expression(expr.call.paramList.get(j));
			}
			
	        String name = expr.call.funcName;
	        FunctionSymbol function = symbolTableStack.getFunction(name);
	       
	        String retType = function.returnType;
	        	        String llvmRetType = type2LLVMType(retType);

	        StringBuilder params = new StringBuilder();
	        boolean flag = true;

	        for (int i = 0; i < expr.call.paramList.size(); i++) {
	            String paramType = function.paramTypes.get(i);
	            String llvmParamType = type2LLVMType(paramType);
	            int paramId = getTempId(expr.call.paramList.get(i));
	            if (flag) {
	                flag = false;
	                params.append(String.format("%s %%%d", llvmParamType, paramId));
	            } else {
	                params.append(String.format(", %s %%%d", llvmParamType, paramId));
	            }
	        }

	        int id = insertTempVar(expr);

	        codes.append(String.format("    %%%d = call %s @%s(%s)\n", id, llvmRetType, name, params));

	        types.put(expr, retType);
		}
	}
                	
}
