package bit.minisys.minicc.icgen.internal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import bit.minisys.minicc.internal.ir.*;
import bit.minisys.minicc.internal.symbol.*;
import bit.minisys.minicc.parser.ast.*;
import bit.minisys.minicc.semantic.internal.SymbolTableBuilder;



public class IRBuilder implements ASTVisitor{
	
	private IRFile irFile;							// 返回生成的Ir文件（包括符号表+指令序列）
	private GlobalSymbolTable globalSymbolTable;	// 全局符号表
	private IRScope currentScope;					// 当前IR节点所处的作用域
	
	// 普通变量（包括数组元素）类型Declaration相关:
	// 全局变量对应的伪寄存器及其属性(在内存中的offset)
	private Map<VirtualReg, VirtualRegProperty> globalVarPropMap = new HashMap<VirtualReg, VirtualRegProperty>();
	private Map<VirtualReg, VirtualRegProperty> localVarPropMap = new HashMap<VirtualReg, VirtualRegProperty>();
	private Set<VirtualReg> ramVarLoaded = new HashSet<VirtualReg>(); // 记录全局变量是否被加载到vReg(每个函数体开始都要clear)
	private Integer farOffset = 0;									  // 记录每个函数内部使用最大的stack空间(byte)
	private LinkedList<IRScope> allScopes = new LinkedList<IRScope>();// 记录每个函数内新建的所有scope，用于在函数最后计算达到的farOffset
	private Map<String, VirtualReg> paramRegMap = new HashMap<String, VirtualReg>();	// 每个函数参数对应的vReg列表
	private List<IRInstruction> tmpScopeInstrs = new LinkedList<IRInstruction>();       // 每个函数体生成的IR指令序列
	private List<IRInstruction> paramInitInstrs = new LinkedList<IRInstruction>();	// 函数开始把参数值放到栈槽中的指令
	private Map<String, IRInstruction> labelMap = new LinkedHashMap<String, IRInstruction>(); // 每个函数体内部的label集合
	
	// 用于存储临时表达式对应的IRExpression(这里Object均为VirtualReg)
	private Map<ASTNode, Object> map = new IdentityHashMap<ASTNode, Object>();
	// 用于直接从文本读取的同学获取字符串常量...... （一个特殊处理）
	private Map<IRInstruction, String> constStrMap = new HashMap<IRInstruction, String>();
	//临时寄存器分配
	private Integer virtualRegId = 0;	
	// 为控制流 新建label id
	private Integer labelId = 0;
	// 循环控制流相关:
	private Integer whileLabelId = 0;
	private Stack<Integer> whileLabelStack = new Stack<Integer>();
	// 条件语句控制流相关:
	private Integer ifLabelId = 0;
	// 记录当前处理的节点是否为lvalue
	private Integer isLvalue = 0;
	// 用于处理( ++ ++a)这类的情况，遍历++a时用vReg返回++a的值，同时记录该vReg对应的Expression是a;
	// 在遍历外层++的时候取出vReg对应的Expression再次进行++操作
	private Map<VirtualReg, ASTExpression> selfChangeMap = new IdentityHashMap<VirtualReg, ASTExpression>();
	// 用于存放全局变量的首地址的寄存器
	//private VirtualReg ramRegAddrHolder = new VirtualReg(++virtualRegId,GlobalSymbolTable.intType);
	
	public IRBuilder(GlobalSymbolTable globalSymbolTable) {
		this.globalSymbolTable = globalSymbolTable;
	}
	public IRFile getIrFile() {
		irFile.globalSymTab.pushInstructions(tmpScopeInstrs);
		return irFile;
	}
	public Map<VirtualReg, VirtualRegProperty> getGlobalVarPropMap() {
		return globalVarPropMap;
	}
	public Map<IRInstruction, String> getConstStrMap() {
		return constStrMap;
	}
	
	// Program
	@Override
	public void visit(ASTCompilationUnit program) {
		if(program == null) return;
		// 全局符号表建立
		irFile = new IRFile();
		irFile.setGlobalSymTab(globalSymbolTable.globalSymbolTable);
		currentScope = irFile.globalSymTab;
		// 对于函数定义，需要递归得到执行语句(在函数定义里处理)
		for (ASTNode item : program.items) {
			if(item instanceof ASTFunctionDefine)
				visit((ASTFunctionDefine)item);
			else if (item instanceof ASTDeclaration) {
				visit((ASTDeclaration)item);
			}
		}
	}
	// FunctionDefine
	@Override
	public void visit(ASTFunctionDefine functionDefine) {
		if(functionDefine == null) return;
		// 和函数相关的信息需重置:
		localVarPropMap = new HashMap<VirtualReg, VirtualRegProperty>();	
		farOffset = 0;				// 当前函数的栈大小清0
		ramVarLoaded.clear();		// 所有全局变量在当前函数尚未load过
		labelMap = new HashMap<String, IRInstruction>();
		allScopes.clear();			// allScope用于存储每个函数体内部所有定义的scope,用来获取函数最终的局部变量占用的栈offset大小
		paramRegMap.clear();		// 存储每个函数的参数对应分配的伪寄存器
		paramInitInstrs.clear();	
		
		ASTDeclarator tmpDeclarator = functionDefine.declarator;
		if(tmpDeclarator instanceof ASTFunctionDeclarator) {
			tmpDeclarator = ((ASTFunctionDeclarator)tmpDeclarator).declarator;
		}
		String funcname = ((ASTVariableDeclarator)tmpDeclarator).identifier.value;
		// 访问functionDeclarator 为了获取参数，每个参数关联一个寄存器, 设置map记录vReg对应的参数属性（包括参数是第几个以及是什么类别）
		Map<VirtualReg, ParamInfo> paramsInfo = new HashMap<VirtualReg, ParamInfo>();
		ASTFunctionDeclarator fDecl = (ASTFunctionDeclarator)functionDefine.declarator;
		FunctionType functionType = (FunctionType)globalSymbolTable.globalSymbolTable.get(funcname).type;
		for(int i = 0; i<fDecl.params.size(); i++) {
			ASTParamsDeclarator paramsDeclarator = fDecl.params.get(i);
			ParamInfo paramInfo = new ParamInfo(i+1,functionType.paramsType.get(i));
			String paramName = ((ASTVariableDeclarator)paramsDeclarator.declarator).getName();
			// 新建一个vReg，关联参数名
			VirtualReg vReg = new VirtualReg(++virtualRegId, paramInfo.type);
			paramsInfo.put(vReg, paramInfo);
			paramRegMap.put(paramName, vReg);	// 这个map记录参数对应的vReg
			
			Symbol paramSymbol = paramsDeclarator.scope.get(paramName);
			Integer offsize = paramSymbol.offsize;
			VirtualRegProperty vrp = new VirtualRegProperty();
			vrp.setOffset(offsize);
			vrp.setVarName(paramName);
			localVarPropMap.put(vReg, vrp);
			
			Integer paramId = i+1;
			// 增加IR指令，将传递的参数加载到参数对应的栈槽中
			IRInstruction loadInstr = new IRDassign(vReg, new ParamVirtualReg(paramId,fDecl.params.size()));
			paramInitInstrs.add(loadInstr);
		}
		
		// 访问函数体部分(instrs)
		visit(functionDefine.body);
		irFile.setFuncBody(funcname,(IRScope)map.get(functionDefine.body));
		
		// 设置IRFunctionDeclaration用于后续阶段的一些属性
		for (IRScope irScope : allScopes) {
			// 获取当前函数内部栈空间最大达到的大小
			farOffset = (farOffset>irScope.offset)?farOffset:irScope.offset;
		}
		IRFunctionDeclaration irfuncDecl = irFile.getFuncDecl(funcname);
		irfuncDecl.setFarOffset(farOffset);
		irfuncDecl.setLocalVarPropMap(localVarPropMap);
		irfuncDecl.setParamsInfo(paramsInfo);
		irfuncDecl.setLabelMap(labelMap);
		
		currentScope = irFile.globalSymTab;
		tmpScopeInstrs = currentScope.chaseInstructions();
	}
	// Declaration
	@Override
	public void visit(ASTDeclaration declaration) {
		// 这个要访问初始化列表的赋值信息，然后添加到语句列表里面
		for (ASTInitList initList : declaration.initLists) {
			visit(initList);
		}
		//declaration.initLists.stream().forEachOrdered(this::visit);
	}
	@Override
	public void visit(ASTDeclarator declarator) {
		// TODO Auto-generated method stub
	}
	@Override
	public void visit(ASTArrayDeclarator arrayDeclarator) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void visit(ASTVariableDeclarator variableDeclarator) {
		// TODO Auto-generated method stub
	}
	@Override
	public void visit(ASTFunctionDeclarator functionDeclarator) {
		// TODO Auto-generated method stub
	}
	@Override
	public void visit(ASTParamsDeclarator paramsDeclarator) {
		// TODO Auto-generated method stub
	}
	@Override
	public void visit(ASTInitList initList) {
		String name = initList.declarator.getName();
		Type type = currentScope.getSymbolType(name);
		if(initList.exprs != null && initList.exprs.size()>0) {
			// 如果声明有初始化内容 : 
				// 1. 函数声明不可能有初始化，已经在语义检查阶段排除
				// 2. 对于普通变量的初始化: 
					// 1) 关联一个唯一的伪寄存器; 
					// 2) 如果是全局变量 ,设置IRDeclaration中的init
					// 3) 如果是局部变量，需要增加赋值IR语句
				// 3. 对于数组变量的初始化: 
					// 无须关联寄存器，如果是局部数组变量：直接计算对应的存储位置，增加赋值语句
									// 如果是全局数组变量,设置IRDeclaration中的init
			if(type instanceof ArrayType) {
				//if(currentScope == irFile.globalSymTab) {
					// 如果是全局作用域，说明为全局数组初始化，需要设置对应IRDeclaration的init值
					// 否则为函数内部的局部数组初始化
					LinkedList<IRExpression> initValues = new LinkedList<IRExpression>();
					Integer regSize = ((ArrayType)type).elementType.getRegisiterSize();
					// vReg计算数组元素地址
					VirtualReg vReg = new VirtualReg(++virtualRegId, ((ArrayType)type).elementType);
					Integer offsize = currentScope.getSymbolOffsize(name);
					VirtualReg consReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.intType);
					for(int i = 0; i<initList.exprs.size(); i++) {
						tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.intType, offsize)));
						// 处理成顺序赋值
						visit(initList.exprs.get(i));
						initValues.add((IRExpression)map.get(initList.exprs.get(i)));
						//if(currentScope!=irFile.globalSymTab) {
							// 对于数组顺序赋值：
							tmpScopeInstrs.add(new IRDassign(consReg, new IRConstval(GlobalSymbolTable.intType, regSize*i)));
							tmpScopeInstrs.add(new IRDassign(vReg, new IRAdd(vReg,consReg, GlobalSymbolTable.intType)));
							if(currentScope == irFile.globalSymTab) {
								tmpScopeInstrs.add(new IRIassign(type, vReg, (IRExpression)map.get(initList.exprs.get(i))));
							}
							else {
								tmpScopeInstrs.add(new IRIassignfpoff(type, vReg, (IRExpression)map.get(initList.exprs.get(i))));
							}
						//}
					}

			}else if (type instanceof NormalType) {
				Integer offset = currentScope.getSymbolOffsize(name);
				VirtualRegProperty vrp = new VirtualRegProperty();
				vrp.setOffset(offset);
				vrp.setVarName(name);
				VirtualReg vReg = new VirtualReg(++virtualRegId, type);
				ASTExpression expr = initList.exprs.get(initList.exprs.size()-1);
				visit(expr);
				if(currentScope == irFile.globalSymTab) {
					// 如果是全局变量，需设置对应IRDeclaration的init值
					currentScope.setDeclarationInit(name, (IRExpression)map.get(expr));
					tmpScopeInstrs.add(new IRIassign(type, new IRConstval(GlobalSymbolTable.intType, offset), (IRExpression)map.get(expr)));
					globalVarPropMap.put(vReg, vrp);
				}else {
					// 如果是局部变量
					tmpScopeInstrs.add(new IRDassign(vReg, (IRExpression)map.get(expr)));
					localVarPropMap.put(vReg, vrp);
				}
				currentScope.setVariableReg(name, vReg);
			}
		}else {
			// 如果声明没有初始化内容，那么对于普通变量只关联寄存器，同时设置属性
			if (type instanceof NormalType) {
				Integer offset = currentScope.getSymbolOffsize(name);
				VirtualReg vReg = new VirtualReg(++virtualRegId, type);
				VirtualRegProperty vrp = new VirtualRegProperty();
				vrp.setVarName(name);
				vrp.setOffset(offset);
				currentScope.setVariableReg(name, vReg);
				if(currentScope != irFile.globalSymTab) {
					localVarPropMap.put(vReg, vrp);
				}else {
					globalVarPropMap.put(vReg, vrp);
				}
			}
		}
	}
	
	@Override
	public void visit(ASTExpression expression) {
		if(expression == null)
			return;
		if(expression instanceof ASTArrayAccess) {
			visit((ASTArrayAccess)expression);
		}else if(expression instanceof ASTBinaryExpression) {
			visit((ASTBinaryExpression)expression);
		}else if(expression instanceof ASTCastExpression) {
			visit((ASTCastExpression)expression);
		}else if(expression instanceof ASTCharConstant) {
			visit((ASTCharConstant)expression);
		}else if(expression instanceof ASTConditionExpression) {
			visit((ASTConditionExpression)expression);
		}else if(expression instanceof ASTFloatConstant) {
			visit((ASTFloatConstant)expression);
		}else if(expression instanceof ASTFunctionCall) {
			visit((ASTFunctionCall)expression);
		}else if(expression instanceof ASTIdentifier) {
			visit((ASTIdentifier)expression);
		}else if(expression instanceof ASTIntegerConstant) {
			visit((ASTIntegerConstant)expression);
		}else if(expression instanceof ASTMemberAccess) {
			visit((ASTMemberAccess)expression);
		}else if(expression instanceof ASTPostfixExpression) {
			visit((ASTPostfixExpression)expression);
		}else if(expression instanceof ASTStringConstant) {
			visit((ASTStringConstant)expression);
		}else if(expression instanceof ASTUnaryExpression) {
			visit((ASTUnaryExpression)expression);
		}else if(expression instanceof ASTUnaryTypename){
			visit((ASTUnaryTypename)expression);
		}
	}
	@Override
	public void visit(ASTArrayAccess arrayAccess) {
		if(arrayAccess == null) return;
		Stack<IRExpression> irStack = new Stack<IRExpression>();
		// 获取a[i]中的i,如果a[i,j]这种不规范形式则是获取j
		ASTExpression expression = arrayAccess.elements.get(arrayAccess.elements.size()-1);
		for (ASTExpression expr : arrayAccess.elements) {
			visit(expr);
		}
		//arrayAccess.elements.stream().forEachOrdered(this::visit);
		irStack.add((IRExpression)map.get(expression));
		// 用栈处理多维数组的情况
		ASTArrayAccess tmpAccess = arrayAccess;
		while(tmpAccess.arrayName instanceof ASTArrayAccess) {
			tmpAccess = (ASTArrayAccess)tmpAccess.arrayName;
			expression = tmpAccess.elements.get(tmpAccess.elements.size()-1);
			for (ASTExpression expr : tmpAccess.elements) {
				visit(expr);
			}
			//tmpAccess.elements.stream().forEachOrdered(this::visit);
			irStack.add((IRExpression)map.get(expression));
		}
		// 根据数组名获取数组类别
		String arrayName = ((ASTIdentifier)tmpAccess.arrayName).value;
		ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
		// 把栈中内容依次弹出到list中，这样a[i][j][k]在list中即i j k
		LinkedList<IRExpression> lists = new LinkedList<IRExpression>();
		while(!irStack.isEmpty()) {
			lists.add(irStack.pop());
		}
		// vReg存储a[i][j]的相对a的偏移字节
		VirtualReg vReg = new VirtualReg(++virtualRegId, arrayType.elementType);
		tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.intType, 0)));
		int i = 1;
		for (IRExpression irExpression : lists) {
			tmpScopeInstrs.add(new IRDassign(vReg, new IRAdd(vReg, irExpression, GlobalSymbolTable.intType)));
			Integer lastDimens = arrayType.getLastRegSize(i++);
			if(lastDimens!=0) {
				tmpScopeInstrs.add(new IRDassign(vReg, new IRMul(vReg, new IRConstval(GlobalSymbolTable.intType, lastDimens), GlobalSymbolTable.intType)));
			}
		}
		tmpScopeInstrs.add(new IRDassign(vReg, new IRMul(vReg, new IRConstval(GlobalSymbolTable.intType, arrayType.elementType.getRegisiterSize()), GlobalSymbolTable.intType)));
		VirtualReg tmpReg = new VirtualReg(++virtualRegId, arrayType.elementType);
		Integer offsize = currentScope.getSymbolOffsize(arrayName);
		tmpScopeInstrs.add(new IRDassign(tmpReg, new IRConstval(GlobalSymbolTable.intType, offsize)));
		tmpScopeInstrs.add(new IRDassign(vReg, new IRAdd(vReg, tmpReg, GlobalSymbolTable.intType)));
		if(isLvalue == 0) {
			if(currentScope.isGlobal(arrayName)) {
				// 如果是全局数组，从内存iread
				tmpScopeInstrs.add(new IRDassign(vReg, new IRIread(arrayType.elementType, GlobalSymbolTable.pointerType, vReg)));
			}else {
				// 局部数组元素变量，从fp+offset读取
				tmpScopeInstrs.add(new IRDassign(vReg, new IRIreadfpoff(arrayType.elementType,vReg)));
			}
		}
		map.put(arrayAccess, vReg);
	}
	@Override
	public void visit(ASTBinaryExpression binaryExpression) {
		if(binaryExpression == null) return;
		// 为了数组访问 设置是否为左值
		if(isLvalueOp(binaryExpression.op.value)) isLvalue++;
		visit(binaryExpression.expr1);
		if(isLvalueOp(binaryExpression.op.value)) isLvalue--;
		IRExpression expr1 = (IRExpression)map.get(binaryExpression.expr1);
		IRExpression finalExpr1 = expr1;
		if(binaryExpression.expr1 instanceof ASTArrayAccess && isLvalueOp(binaryExpression.op.value) && !binaryExpression.op.value.equals("=")) {
			// 如果是arrayAccess,返回的是数组的地址,对于 += 这一类，需要增加load操作
			String arrayName = getArrayName((ASTArrayAccess)binaryExpression.expr1);
			ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
			VirtualReg vReg = new VirtualReg(++virtualRegId,arrayType.elementType);
			if(currentScope.isGlobal(arrayName)) {
				tmpScopeInstrs.add(new IRDassign(vReg, expr1));
				tmpScopeInstrs.add(new IRDassign(vReg, new IRIread(arrayType.elementType, arrayType, vReg)));
			}else {
				tmpScopeInstrs.add(new IRDassign(vReg, expr1));
				tmpScopeInstrs.add(new IRDassign(vReg, new IRIreadfpoff(arrayType.elementType, vReg)));
			}
			finalExpr1 = vReg;
		}else if (binaryExpression.expr1 instanceof ASTIdentifier && isLvalueOp(binaryExpression.op.value) && !binaryExpression.op.value.equals("=")) {
			
			String idName = ((ASTIdentifier)binaryExpression.expr1).value;
			VirtualReg vReg = getVarVReg(idName);
			if(globalVarPropMap.containsKey(vReg)) {
				VirtualRegProperty vrp = globalVarPropMap.get(vReg);
				tmpScopeInstrs.add(new IRDassign(vReg, new IRIread(vReg.getType(), GlobalSymbolTable.pointerType, new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()))));
				finalExpr1 = vReg;
			}
			
		}
		// 处理短路求值
		if(isLogicBinaryExpr(binaryExpression)) {
			// 如果是 需要额外插入一条jump
			if(binaryExpression.op.value.equals("||")){
				tmpScopeInstrs.add(new IRBrtrue("@"+ ++labelId + "shortwaytrue",(IRExpression)map.get(binaryExpression.expr1)));
			}else {
				tmpScopeInstrs.add(new IRBrfalse("@"+ ++labelId + "shortwaytrue",(IRExpression)map.get(binaryExpression.expr1)));
			}
		}
		Integer tmpLabelId = labelId;
		visit(binaryExpression.expr2);
		IRExpression expr2 = (IRExpression)map.get(binaryExpression.expr2);
		IRExpression tmpRes = getBinaryResult(binaryExpression.op.value,finalExpr1,expr2,binaryExpression.type);
		String opString = binaryExpression.op.value;
		ASTExpression realExpr = binaryExpression.expr1;
		if (opString.equals("=")) {
			if(!(realExpr instanceof ASTIdentifier || realExpr instanceof ASTArrayAccess) && realExpr.canAssigned) {
				realExpr = selfChangeMap.get(map.get(realExpr));
			}
			if(realExpr instanceof ASTIdentifier) {
				String name = ((ASTIdentifier)realExpr).value;
				VirtualReg vReg = getVarVReg(name);
				tmpScopeInstrs.add(new IRDassign(vReg, expr2));
				if(globalVarPropMap.containsKey(vReg)) {
					VirtualRegProperty vrp = globalVarPropMap.get(vReg);
					tmpScopeInstrs.add(new IRIassign(vReg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vReg));
				}
				map.put(binaryExpression,vReg);
			}else if (realExpr instanceof ASTArrayAccess) {
				String arrayName = getArrayName((ASTArrayAccess)realExpr);
				ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
				if(currentScope.isGlobal(arrayName)) {
				//	tmpScopeInstrs.add(new IRIassign(arrayType.elementType,(IRExpression)map.get(realExpr), expr2));
					tmpScopeInstrs.add(new IRIassign(arrayType,(IRExpression)map.get(realExpr), expr2));
				}else {
				//	tmpScopeInstrs.add(new IRIassignfpoff(arrayType.elementType, (IRExpression)map.get(realExpr), expr2));
					tmpScopeInstrs.add(new IRIassignfpoff(arrayType, (IRExpression)map.get(realExpr), expr2));
				}
				map.put(binaryExpression, map.get(realExpr));
			}
		}else if (opString.equals("*=") ||opString.equals("/=") ||opString.equals("%=") ||opString.equals("+=") ||
				opString.equals("-=") ||opString.equals("<<=") ||opString.equals(">>=") ||opString.equals("&=") ||
				opString.equals("^=") ||opString.equals("|=")) {
			realExpr = binaryExpression.expr1;
			if(!(realExpr instanceof ASTIdentifier || realExpr instanceof ASTArrayAccess) && realExpr.canAssigned) {
				realExpr = selfChangeMap.get(map.get(realExpr));
			}
			
			if(realExpr instanceof ASTIdentifier) {
				String name = ((ASTIdentifier)realExpr).value;
				VirtualReg vReg =  getVarVReg(name);
				tmpScopeInstrs.add(new IRDassign(vReg, tmpRes));
				if(globalVarPropMap.containsKey(vReg)) {
					VirtualRegProperty vrp = globalVarPropMap.get(vReg);
					tmpScopeInstrs.add(new IRIassign(vReg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vReg));
				}
				map.put(binaryExpression,vReg);
			}else if (realExpr instanceof ASTArrayAccess) {
				String arrayName = getArrayName((ASTArrayAccess)realExpr);
				ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
				// 因为是+=此类，lvalue还是数组，(IRExpression)map.get(realExpr)返回的是地址
				if(currentScope.isGlobal(arrayName)) {
					VirtualReg tmpReg = getTmpRegForSavedAddr(arrayType.elementType,(IRExpression)map.get(realExpr));
				//	tmpScopeInstrs.add(new IRIassign(arrayType.elementType,tmpReg, tmpRes));
					tmpScopeInstrs.add(new IRIassign(arrayType,tmpReg, tmpRes));
				}else {
					VirtualReg tmpReg = getTmpRegForSavedAddr(arrayType.elementType,(IRExpression)map.get(realExpr));
					tmpScopeInstrs.add(new IRIassignfpoff(arrayType, tmpReg, tmpRes));
				//	tmpScopeInstrs.add(new IRIassignfpoff(arrayType.elementType, tmpReg, tmpRes));
				}
				map.put(binaryExpression,(IRExpression)map.get(realExpr));
			}
		}else if (opString.equals("+") ||opString.equals("-") ||opString.equals("*") ||
				opString.equals("/") ||opString.equals("%") ||opString.equals("<<") ||
				opString.equals(">>") ||opString.equals("<=") ||opString.equals(">=") ||
				opString.equals("<") ||opString.equals(">") ||opString.equals("==") ||
				opString.equals("!=") ||opString.equals("&") ||opString.equals("^") ||opString.equals("|")) {
			VirtualReg vReg = new VirtualReg(++virtualRegId, binaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(vReg, tmpRes));
			map.put(binaryExpression,vReg);
		}else if (opString.equals("&&")) {
			tmpScopeInstrs.add(new IRGotoStatement("@"+tmpLabelId+"shortwayfalse"));
			IRLabel shorwayTrue = new IRLabel(tmpLabelId+"shortwaytrue");
			tmpScopeInstrs.add(shorwayTrue);
			labelMap.put(shorwayTrue.label, shorwayTrue);
			VirtualReg vReg = new VirtualReg(++virtualRegId, binaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.intType, 0)));
			tmpScopeInstrs.add(new IRGotoStatement("@"+tmpLabelId+"shortwayend"));
			IRLabel shorwayfalse = new IRLabel(tmpLabelId+"shortwayfalse");
			tmpScopeInstrs.add(shorwayfalse);
			labelMap.put(shorwayfalse.label, shorwayfalse);
			tmpScopeInstrs.add(new IRDassign(vReg, tmpRes));
			IRLabel shorwayEnd = new IRLabel(tmpLabelId+"shortwayend");
			tmpScopeInstrs.add(shorwayEnd);
			labelMap.put(shorwayEnd.label, shorwayEnd);
			map.put(binaryExpression,vReg);
		}else if (opString.equals("||")) {
			tmpScopeInstrs.add(new IRGotoStatement("@"+tmpLabelId+"shortwayfalse"));
			IRLabel shorwayTrue = new IRLabel(tmpLabelId+"shortwaytrue");
			tmpScopeInstrs.add(shorwayTrue);
			labelMap.put(shorwayTrue.label, shorwayTrue);
			VirtualReg vReg = new VirtualReg(++virtualRegId, binaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.intType, 1)));
			tmpScopeInstrs.add(new IRGotoStatement("@"+tmpLabelId+"shortwayend"));
			IRLabel shorwayfalse = new IRLabel(tmpLabelId+"shortwayfalse");
			tmpScopeInstrs.add(shorwayfalse);
			labelMap.put(shorwayfalse.label, shorwayfalse);
			tmpScopeInstrs.add(new IRDassign(vReg, tmpRes));
			IRLabel shorwayEnd = new IRLabel(tmpLabelId+"shortwayend");
			tmpScopeInstrs.add(shorwayEnd);
			labelMap.put(shorwayEnd.label, shorwayEnd);
			map.put(binaryExpression,vReg);
		}
	}
	@Override
	public void visit(ASTCastExpression castExpression) {
		if(castExpression == null) return;
		Type casttType = castExpression.type;
		Type oldType = castExpression.expr.type;
		visit(castExpression.expr);
		
		VirtualReg tmpReg = new VirtualReg(++virtualRegId, casttType);
		tmpScopeInstrs.add(new IRDassign(tmpReg, new IRCvt(oldType, casttType, (IRExpression)map.get(castExpression.expr))));
		map.put(castExpression, tmpReg);
	}
	@Override
	public void visit(ASTConditionExpression conditionExpression) {
		if(conditionExpression == null) return;
		Type resType = conditionExpression.type;
		
		visit(conditionExpression.condExpr);
		IRExpression opnd0 = (IRExpression)map.get(conditionExpression.condExpr);
		tmpScopeInstrs.add(new IRBrfalse("@"+ ++labelId +"ternayCond", opnd0));
		
		ASTExpression lastExpr = conditionExpression.trueExpr.getLast();
		for (ASTExpression expr : conditionExpression.trueExpr) {
			visit(expr);
		}
		//conditionExpression.trueExpr.stream().forEachOrdered(this::visit);
		IRExpression opnd1 = (IRExpression)map.get(lastExpr);
		VirtualReg tmpReg = new VirtualReg(++virtualRegId, resType);
		tmpScopeInstrs.add(new IRDassign(tmpReg, opnd1));
		tmpScopeInstrs.add(new IRGotoStatement("@"+ labelId +"ternayCondEnd"));
		

		IRLabel condFalse = new IRLabel(labelId+"ternayCond");
		tmpScopeInstrs.add(condFalse);
		labelMap.put(condFalse.label, condFalse);
		
		visit(conditionExpression.falseExpr);
		IRExpression opnd2 = (IRExpression)map.get(conditionExpression.falseExpr);
		
		tmpScopeInstrs.add(new IRDassign(tmpReg, opnd2));
		
		IRLabel condEnd = new IRLabel(labelId+"ternayCondEnd");
		tmpScopeInstrs.add(condEnd);
		labelMap.put(condEnd.label, condEnd);
		
		map.put(conditionExpression, tmpReg);
	}
	@Override
	public void visit(ASTMemberAccess memberAccess) {
		if(memberAccess == null) return;
		// to be done
	}
	@Override
	public void visit(ASTPostfixExpression postfixExpression) {
		if(postfixExpression == null)return;
		isLvalue++;
		visit(postfixExpression.expr);
		isLvalue--;
		String opString = postfixExpression.op.value;
		if(opString.equals("++")||opString.equals("--")) {
			if(postfixExpression.expr instanceof ASTIdentifier || postfixExpression.expr instanceof ASTArrayAccess) {
				VirtualReg vReg = new VirtualReg(++virtualRegId, postfixExpression.expr.type);
				if(postfixExpression.expr instanceof ASTIdentifier) {
					tmpScopeInstrs.add(new IRDassign(vReg, (IRExpression)map.get(postfixExpression.expr)));
				}else {
					String arrayName = getArrayName((ASTArrayAccess)postfixExpression.expr);
					ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
					tmpScopeInstrs.add(new IRDassign(vReg, (IRExpression)map.get(postfixExpression.expr)));
					if(currentScope.isGlobal(arrayName)) {
						tmpScopeInstrs.add(new IRDassign(vReg, new IRIread(arrayType.elementType, arrayType, vReg)));
					}else {
						tmpScopeInstrs.add(new IRDassign(vReg, new IRIreadfpoff(arrayType.elementType, vReg)));
					}
				}
				selfChange(postfixExpression.expr,postfixExpression.op.value);
				selfChangeMap.put(vReg, postfixExpression.expr);
				map.put(postfixExpression, vReg);
			}else {
				ASTExpression realLvalue = selfChangeMap.get(map.get(postfixExpression.expr));
				selfChange(realLvalue,postfixExpression.op.value);
				map.put(postfixExpression, map.get(postfixExpression.expr));
			}
		}
	}
	@Override
	public void visit(ASTTypename typename) {
		if(typename == null)return;
	}
	@Override
	public void visit(ASTUnaryExpression unaryExpression) {
		if(unaryExpression == null)return;
		String opString = unaryExpression.op.value;
		if(opString.equals("++") || opString.equals("--"))isLvalue ++;
		visit(unaryExpression.expr);
		if(opString.equals("++") || opString.equals("--"))isLvalue--;
		
		if (opString.equals("++")) {
			if(unaryExpression.expr instanceof ASTIdentifier || unaryExpression.expr instanceof ASTArrayAccess) {
				selfChange(unaryExpression.expr,unaryExpression.op.value);
				VirtualReg vReg = new VirtualReg(++virtualRegId, unaryExpression.expr.type);
				if(unaryExpression.expr instanceof ASTIdentifier)
					tmpScopeInstrs.add(new IRDassign(vReg, (IRExpression)map.get(unaryExpression.expr)));
				else {
					String arrayName = getArrayName((ASTArrayAccess)unaryExpression.expr);
					ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
					tmpScopeInstrs.add(new IRDassign(vReg, (IRExpression)map.get(unaryExpression.expr)));
					if(currentScope.isGlobal(arrayName)) {
						tmpScopeInstrs.add(new IRDassign(vReg,new IRIread(arrayType.elementType, arrayType, vReg)));
					}else {
						tmpScopeInstrs.add(new IRDassign(vReg,new IRIreadfpoff(arrayType.elementType, vReg)));
					}
					
				}
				selfChangeMap.put(vReg, unaryExpression.expr);
				map.put(unaryExpression, vReg);
			}else {
				ASTExpression realLvalue = selfChangeMap.get(map.get(unaryExpression.expr));
				selfChange(realLvalue,unaryExpression.op.value);
				VirtualReg vReg = new VirtualReg(++virtualRegId, unaryExpression.expr.type);
				VirtualReg constReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.intType);
				tmpScopeInstrs.add(new IRDassign(constReg, new IRConstval(GlobalSymbolTable.intType, 1)));
				tmpScopeInstrs.add(new IRDassign(vReg, 
						new IRAdd(
								(IRExpression)map.get(unaryExpression.expr),
								constReg,
								GlobalSymbolTable.intType
								)));
				selfChangeMap.put(vReg, realLvalue);
				map.put(unaryExpression, vReg);
			}
		}else if (opString.equals("--")) {
			if(unaryExpression.expr instanceof ASTIdentifier || unaryExpression.expr instanceof ASTArrayAccess) {
				selfChange(unaryExpression.expr,unaryExpression.op.value);
				VirtualReg vReg = new VirtualReg(++virtualRegId, unaryExpression.expr.type);
				if(unaryExpression.expr instanceof ASTIdentifier) {
					tmpScopeInstrs.add(new IRDassign(vReg, (IRExpression)map.get(unaryExpression.expr)));
				}
				else {
					String arrayName = getArrayName((ASTArrayAccess)unaryExpression.expr);
					ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
					if(currentScope.isGlobal(arrayName)) {
						tmpScopeInstrs.add(new IRDassign(vReg,new IRIread(arrayType.elementType, arrayType, (IRExpression)map.get(unaryExpression.expr))));
					}else {
						tmpScopeInstrs.add(new IRDassign(vReg,new IRIreadfpoff(arrayType.elementType, (IRExpression)map.get(unaryExpression.expr))));
					}
				}
				
				selfChangeMap.put(vReg, unaryExpression.expr);
				map.put(unaryExpression, vReg);
			}else {
				ASTExpression realLvalue = selfChangeMap.get(map.get(unaryExpression.expr));
				selfChange(realLvalue,unaryExpression.op.value);
				VirtualReg vReg = new VirtualReg(++virtualRegId, unaryExpression.expr.type);
				VirtualReg constReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.intType);
				tmpScopeInstrs.add(new IRDassign(constReg, new IRConstval(GlobalSymbolTable.intType, 1)));
				tmpScopeInstrs.add(new IRDassign(vReg, 
						new IRSub(
								(IRExpression)map.get(unaryExpression.expr),
								constReg,
								GlobalSymbolTable.intType
								)));
				selfChangeMap.put(vReg, realLvalue);
				map.put(unaryExpression, vReg);
			}
		}else if (opString.equals("sizeof")) {
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, unaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(tmpReg, new IRSizeoftype(
					unaryExpression.expr.type
					)));
			map.put(unaryExpression, tmpReg);
		}else if (opString.equals("&")) {
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, unaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(tmpReg, new IRAddrof(
					GlobalSymbolTable.intType,
					((ASTIdentifier)unaryExpression.expr).value)));
			map.put(unaryExpression, tmpReg);
		}else if (opString.equals("*")) {
			
		}else if (opString.equals("+")) {
			map.put(unaryExpression, (IRExpression)map.get(unaryExpression.expr));
		}else if (opString.equals("-")) {
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, unaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(tmpReg, new IRConstval(GlobalSymbolTable.intType, 0)));
			tmpScopeInstrs.add(new IRDassign(tmpReg, new IRSub(
					tmpReg,
					(IRExpression)map.get(unaryExpression.expr),
					unaryExpression.type
					)));
			map.put(unaryExpression, tmpReg);
		}else if (opString.equals("~")) {
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, unaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(tmpReg, new IRNeg(
					(IRExpression)map.get(unaryExpression.expr),
					unaryExpression.type
					)));
			map.put(unaryExpression,tmpReg);
		}else if (opString.equals("!")) {
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, unaryExpression.type);
			tmpScopeInstrs.add(new IRDassign(tmpReg, new IRInot(
					(IRExpression)map.get(unaryExpression.expr),
					unaryExpression.type
					)));
			map.put(unaryExpression, tmpReg);
		}
	}
	@Override
	public void visit(ASTUnaryTypename unaryTypename) {
		if(unaryTypename == null)return;
		visit(unaryTypename.typename);
		String opString = unaryTypename.op.value;
		if (opString.equals("sizeof")) {
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.intType);
			tmpScopeInstrs.add(new IRDassign(tmpReg, new IRSizeoftype(
					SymbolTableBuilder.getSpecifierType(unaryTypename.typename.specfiers))));
			map.put(unaryTypename, tmpReg);
		}else if (opString.equals("_Alignof")) {
			// 暂时搁置
		}
		
	}
	@Override
	public void visit(ASTFunctionCall funcCall) {
		if(funcCall == null) return;
		// 需要保证identifier
		String funcname = ((ASTIdentifier)funcCall.funcname).value;
		Type type = currentScope.getSymbolType(funcname);	// 这里是返回值类型
		if(GlobalSymbolTable.isMarsPrintStr(funcname)) {
			// 对于Mars_PrintStr
			ASTExpression printStr = funcCall.argList.get(0);
			String idStr = irFile.addStringConst(((ASTStringConstant)printStr).value);
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.pointerType);
			IRInstruction irDassign = new IRDassign(tmpReg, new IRAddrof(GlobalSymbolTable.pointerType,idStr));
			constStrMap.put(irDassign, ((ASTStringConstant)printStr).value);
			tmpScopeInstrs.add(irDassign);
			tmpScopeInstrs.add(new IRCall(funcname, tmpReg));
		}else {
			List<IRExpression> args = new LinkedList<IRExpression>();
			for (ASTExpression expr : funcCall.argList) {
				visit(expr);
				args.add((IRExpression)map.get(expr));
			}

			if(type instanceof NormalType && type == GlobalSymbolTable.voidType) {
				// 如果是void类型
				tmpScopeInstrs.add(new IRCall(funcname, args));
			}else {
				tmpScopeInstrs.add(new IRCall(funcname, args));
				VirtualReg tmpReg = new VirtualReg(++virtualRegId, type);
			//	tmpScopeInstrs.add(new IRDassign(tmpReg, new IRCall(funcname, args)));
				tmpScopeInstrs.add(new IRDassign(tmpReg, new ParamVirtualReg(-1, 0)));
				map.put(funcCall, tmpReg);
			}
		}
		
	}

	

	// statement
	@Override
	public void visit(ASTStatement statement) {
		if(statement == null)
			return;
		if(statement instanceof ASTIterationDeclaredStatement) {
			visit((ASTIterationDeclaredStatement)statement);
		}else if(statement instanceof ASTIterationStatement) {
			visit((ASTIterationStatement)statement);
		}else if(statement instanceof ASTCompoundStatement) {
			visit((ASTCompoundStatement)statement);
		}else if(statement instanceof ASTSelectionStatement) {
			visit((ASTSelectionStatement)statement);
		}else if(statement instanceof ASTExpressionStatement) {
			visit((ASTExpressionStatement)statement);
		}else if(statement instanceof ASTBreakStatement) {
			visit((ASTBreakStatement)statement);
		}else if(statement instanceof ASTContinueStatement) {
			visit((ASTContinueStatement)statement);
		}else if(statement instanceof ASTReturnStatement) {
			visit((ASTReturnStatement)statement);
		}else if(statement instanceof ASTGotoStatement) {
			visit((ASTGotoStatement)statement);
		}else if(statement instanceof ASTLabeledStatement) {
			visit((ASTLabeledStatement)statement);
		}
		
	}
	
	@Override
	public void visit(ASTBreakStatement breakStat) {
		if(breakStat == null) return;
		Integer labelId = whileLabelStack.lastElement();
		tmpScopeInstrs.add(new IRGotoStatement("@"+labelId+"LoopEndLabel"));
	}
	@Override
	public void visit(ASTContinueStatement continueStatement) {
		if(continueStatement == null) return;
		Integer labelId = whileLabelStack.lastElement();
		tmpScopeInstrs.add(new IRGotoStatement("@"+labelId+"LoopStepLabel"));
	}
	@Override
	public void visit(ASTCompoundStatement compoundStat) {
		if(compoundStat == null)return;
		// 保存目前为止的指令
		IRScope irScope = new IRScope(currentScope);
		allScopes.add(irScope);
		currentScope.pushInstructions(tmpScopeInstrs);
		currentScope = irScope;
		
		// 获取scope的待填充IR指令列表
		tmpScopeInstrs = irScope.chaseInstructions();

		if(paramInitInstrs.size() > 0) {
			// 如果是刚进入函数体，把参数初始化的指令加入，随后清空
			tmpScopeInstrs.addAll(paramInitInstrs);
			paramInitInstrs.clear();
		}
		
		for (ASTNode item : compoundStat.blockItems) {
			if(!irScope.symTabBuilt()) {
				irScope.assignLocalSymTab(item.scope);
			}

			if(item instanceof ASTDeclaration) {
				visit((ASTDeclaration)item);
			}else if (item instanceof ASTStatement) {
				visit((ASTStatement)item);
			}
		}
		irScope.pushInstructions(tmpScopeInstrs);
		map.put(compoundStat, irScope);
		currentScope = irScope.getFatherScope();
		// 返回上一级的，填上这个scope后 ，继续填充
		tmpScopeInstrs = currentScope.chaseInstructions();
		if(currentScope != irFile.globalSymTab)
			tmpScopeInstrs.add(irScope);
	}
	@Override
	public void visit(ASTExpressionStatement expressionStat) {
		if(expressionStat == null)return;
		for (ASTExpression expr : expressionStat.exprs) {
			visit(expr);
			if(map.get(expr) instanceof VirtualReg)
				continue;
			tmpScopeInstrs.add((IRExpression) map.get(expr));
		}		
	}
	@Override
	public void visit(ASTGotoStatement gotoStat) {
		if(gotoStat == null) return;
		tmpScopeInstrs.add(new IRGotoStatement("@"+gotoStat.label.value));
	}

	@Override
	public void visit(ASTIterationDeclaredStatement iterationDeclaredStat) {
		if(iterationDeclaredStat == null) return;
		// irscope
		// 访问cond
		Integer labelId = ++whileLabelId;
		whileLabelStack.push(labelId);		// 为了解决for中的break和continue问题
		// 保存当前指令序列
		currentScope.pushInstructions(tmpScopeInstrs);
		// 进入新的子scope
		IRScope irScope = new IRScope(currentScope);
		allScopes.add(irScope);
		currentScope = irScope;
		// tmp清空
		tmpScopeInstrs = irScope.chaseInstructions();
		// 遍历声明部分，设置局部符号表
		irScope.assignLocalSymTab(iterationDeclaredStat.stat.scope);
		visit(iterationDeclaredStat.init);
		// 插入check-label
		IRLabel irLabel = new IRLabel(labelId+"LoopCheckLabel");
		tmpScopeInstrs.add(irLabel);
		labelMap.put(irLabel.getLabel(), irLabel);
		// 遍历cond表达式，取最后一个
		//List<IRExpression> condList = new LinkedList<IRExpression>();
		if(iterationDeclaredStat.cond != null) {
			IRExpression last = null;
			for (ASTExpression condExpr : iterationDeclaredStat.cond) {
				visit(condExpr);
				last = (IRExpression) map.get(condExpr);
			}
			// 插入goto end-for-label
			tmpScopeInstrs.add(new IRBrfalse("@"+labelId+"LoopEndLabel", last));
		}
		
		// 遍历语句，这里注意特判一下compound
		if(iterationDeclaredStat.stat instanceof ASTCompoundStatement) {
			for (ASTNode item : ((ASTCompoundStatement)iterationDeclaredStat.stat).blockItems) {
				if(!irScope.symTabBuilt())
					irScope.assignLocalSymTab(item.scope);
				 if (item instanceof ASTStatement) {
					visit((ASTStatement)item);
				}else if(item instanceof ASTDeclaration){
					visit((ASTDeclaration)item);
				}
			}
		}else {
			visit(iterationDeclaredStat.stat);
		}
		// 插入continue的label
		irLabel = new IRLabel(labelId+"LoopStepLabel");
		tmpScopeInstrs.add(irLabel);
		labelMap.put(irLabel.getLabel(), irLabel);
		// 遍历step, 截取step部分语句
		//List<IRExpression> stepList = new LinkedList<IRExpression>();
		if(iterationDeclaredStat.step != null) {
			for (ASTExpression stepExpr : iterationDeclaredStat.step) {
				visit(stepExpr);
				//stepList.add((IRExpression)map.get(stepExpr));
			}
		}
		// 插入goto check-label
		tmpScopeInstrs.add(new IRGotoStatement("@"+labelId+"LoopCheckLabel"));
		
		irScope.pushInstructions(tmpScopeInstrs);
		
		// 回到for的scope
		currentScope = currentScope.getFatherScope();
		tmpScopeInstrs = currentScope.chaseInstructions();
		//System.out.println("1232433"+":"+tmpScopeInstrs.size());
		tmpScopeInstrs.add(irScope);
		irLabel = new IRLabel(labelId+"LoopEndLabel");
		tmpScopeInstrs.add(irLabel);
		labelMap.put(irLabel.getLabel(), irLabel);
		
		whileLabelStack.pop();
	}

	@Override
	public void visit(ASTIterationStatement iterationStat) {
		if(iterationStat == null) return;
		// irscope
		// 访问cond
		Integer labelId = ++whileLabelId;
		whileLabelStack.push(labelId);		// 为了解决for中的break和continue问题
		// 保存当前指令序列
		currentScope.pushInstructions(tmpScopeInstrs);
		// 进入新的子scope
		IRScope irScope = new IRScope(currentScope);
		allScopes.add(irScope);
		
		currentScope = irScope;
		// tmp清空
		tmpScopeInstrs = irScope.chaseInstructions();
		// 设置局部符号表
		irScope.assignLocalSymTab(iterationStat.stat.scope);
		//List<IRExpression> startExpr = null;
		if(iterationStat.init != null) {
			for (ASTExpression expr : iterationStat.init) {
				visit(expr);
			}
		}
		
		// 插入check-label
		IRLabel irLabel = new IRLabel(labelId+"LoopCheckLabel");
		tmpScopeInstrs.add(irLabel);
		labelMap.put(irLabel.getLabel(), irLabel);
		// 遍历cond表达式，取最后一个作为判断
		//List<IRExpression> condList = new LinkedList<IRExpression>();
		if(iterationStat.cond != null) {
			IRExpression last = null;
			for (ASTExpression condExpr : iterationStat.cond) {
				visit(condExpr);
				last = (IRExpression) map.get(condExpr);
			//	condList.add((IRExpression)map.get(condExpr));
			}
			tmpScopeInstrs.add(new IRBrfalse("@"+labelId+"LoopEndLabel", last));
		}
		// 插入goto end-for-label
			
		// 遍历语句，这里注意特判一下compound
		if(iterationStat.stat instanceof ASTCompoundStatement) {
			for (ASTNode item : ((ASTCompoundStatement)iterationStat.stat).blockItems) {
				if(!irScope.symTabBuilt())
					irScope.assignLocalSymTab(item.scope);
				 if (item instanceof ASTStatement) {
					visit((ASTStatement)item);
				}else if(item instanceof ASTDeclaration){
					visit((ASTDeclaration)item);
				}
			}
		}else {
			visit(iterationStat.stat);
		}
		// 插入continue的label
		irLabel = new IRLabel(labelId+"LoopStepLabel");
		tmpScopeInstrs.add(irLabel);
		labelMap.put(irLabel.getLabel(), irLabel);
		// 遍历step
		//List<IRExpression> stepList = new LinkedList<IRExpression>();
		if(iterationStat.step != null) {
			for (ASTExpression stepExpr : iterationStat.step) {
				visit(stepExpr);
				//stepList.add((IRExpression)map.get(stepExpr));
			}
		}
		// 插入goto check-label
		tmpScopeInstrs.add(new IRGotoStatement("@"+labelId+"LoopCheckLabel"));
		
		irScope.pushInstructions(tmpScopeInstrs);
		// 回到for的scope
		currentScope = currentScope.getFatherScope();
		tmpScopeInstrs = currentScope.chaseInstructions();
		// 没有声明，设置为null
		tmpScopeInstrs.add(irScope);
		
		irLabel = new IRLabel(labelId+"LoopEndLabel");
		tmpScopeInstrs.add(irLabel);
		labelMap.put(irLabel.getLabel(), irLabel);
		
		whileLabelStack.pop();
	}

	@Override
	public void visit(ASTLabeledStatement labeledStat) {
		if(labeledStat == null) return;
		// 新建label
		IRLabel irLabel = new IRLabel(labeledStat.label.value);
		tmpScopeInstrs.add(irLabel);
		labelMap.put(irLabel.getLabel(), irLabel);
		visit(labeledStat.stat);
	}
	@Override
	public void visit(ASTReturnStatement returnStat) {
		if(returnStat == null)return;
		if(returnStat.expr != null && returnStat.expr.size() > 0) {
			ASTExpression lastExpr = returnStat.expr.getLast();
			for (ASTExpression expr : returnStat.expr) {
				visit(expr);
			}
			//returnStat.expr.stream().forEachOrdered(this::visit);
			tmpScopeInstrs.add(new IRReturnStatement((IRExpression)map.get(lastExpr)));
		}else {
			tmpScopeInstrs.add(new IRReturnStatement());
		}
	}
	@Override
	public void visit(ASTSelectionStatement selectionStat) {
		if(selectionStat == null)return;
		ASTExpression lastCond = selectionStat.cond.getLast();
		//selectionStat.cond.stream().forEachOrdered(this::visit);
		for (ASTExpression expr : selectionStat.cond) {
			visit(expr);
		}
		IRExpression irCond = (IRExpression)map.get(lastCond);
		// then
		Integer nowLabelId = ++ifLabelId;
		Integer elseIflabel = 0;
		tmpScopeInstrs.add(new IRBrfalse("@"+ nowLabelId+"otherwise"+ ++elseIflabel, irCond));
		
		currentScope.pushInstructions(tmpScopeInstrs);
		IRScope thenScope = new IRScope(currentScope);
		allScopes.add(thenScope);
		
		currentScope = thenScope;
		tmpScopeInstrs = currentScope.instructions;
		
		if(selectionStat.then instanceof ASTCompoundStatement) {
			for (ASTNode item : ((ASTCompoundStatement)selectionStat.then).blockItems) {
				if(!thenScope.symTabBuilt())
					thenScope.assignLocalSymTab(item.scope);
				if(item instanceof ASTDeclaration ) {
					visit((ASTDeclaration)item);
				}else if (item instanceof ASTStatement) {
					visit((ASTStatement)item);
				}
			}
		}else {
			visit(selectionStat.then);
		}
		thenScope.pushInstructions(tmpScopeInstrs);
		
		currentScope = currentScope.getFatherScope();
		tmpScopeInstrs = currentScope.chaseInstructions();
		tmpScopeInstrs.add(thenScope);
		tmpScopeInstrs.add(new IRGotoStatement("@"+nowLabelId+"endif"));
		// elseif list

		//LinkedList<IRScope> elseIfList = new LinkedList<IRScope>();
		LinkedList<IRExpression> condIrExpr = null;
		ASTStatement tmpStat = selectionStat.otherwise;
		while(tmpStat != null && tmpStat instanceof ASTSelectionStatement) {
			IRLabel elseLabel = new IRLabel(nowLabelId+"otherwise"+elseIflabel);
			tmpScopeInstrs.add(elseLabel);
			labelMap.put(elseLabel.getLabel(), elseLabel);
			
			// 对于cond部分，取最后一个
			LinkedList<ASTExpression> condExpr = ((ASTSelectionStatement)tmpStat).cond;
			condIrExpr = new LinkedList<IRExpression>();
			for (ASTExpression expression : condExpr) {
				visit(expression);
				condIrExpr.add((IRExpression)map.get(expression));
			}

			
			tmpScopeInstrs.add(new IRBrfalse("@"+nowLabelId+"otherwise"+ ++elseIflabel, condIrExpr.getLast()));
			currentScope.pushInstructions(tmpScopeInstrs);
			// 对每个都新建一个scope
			IRScope tmpScope = new IRScope(currentScope);
			allScopes.add(tmpScope);
			currentScope = tmpScope;
			tmpScopeInstrs = currentScope.chaseInstructions();
			
			if(((ASTSelectionStatement)tmpStat).then instanceof ASTCompoundStatement) {
				ASTCompoundStatement tmpComStat = (ASTCompoundStatement)((ASTSelectionStatement)tmpStat).then;
				for (ASTNode item : tmpComStat.blockItems) {
					if(!tmpScope.symTabBuilt())
						tmpScope.assignLocalSymTab(item.scope);
					if(item instanceof ASTDeclaration ) {
						visit((ASTDeclaration)item);
					}else if (item instanceof ASTStatement) {
						visit((ASTStatement)item);
					}
				}
			}else {
				visit(((ASTSelectionStatement)tmpStat).then);
			}
			currentScope = currentScope.getFatherScope();
			tmpScope.pushInstructions(tmpScopeInstrs);
			tmpScopeInstrs = currentScope.chaseInstructions();
			tmpScopeInstrs.add(tmpScope);
			tmpScopeInstrs.add(new IRGotoStatement("@"+nowLabelId+"endif"));
			// 取可能的下一个elseif
			tmpStat = ((ASTSelectionStatement)tmpStat).otherwise;
		}
		
		IRLabel elseLabel = new IRLabel(nowLabelId+"otherwise"+elseIflabel);
		tmpScopeInstrs.add(elseLabel);
		labelMap.put(elseLabel.getLabel(), elseLabel);
		// else
		IRScope elseScope = new IRScope(currentScope);
		allScopes.add(elseScope);
		
		if(tmpStat != null) {
			currentScope = elseScope;
			tmpScopeInstrs = currentScope.instructions;
			if(tmpStat instanceof ASTCompoundStatement) {
				for (ASTNode item : ((ASTCompoundStatement)tmpStat).blockItems) {
					if(!elseScope.symTabBuilt())
						elseScope.assignLocalSymTab(item.scope);
					if(item instanceof ASTDeclaration && !elseScope.symTabBuilt()) {
						visit((ASTDeclaration)item);
					}else if (item instanceof ASTStatement) {
						visit((ASTStatement)item);
					}
				}
			}else {
				visit(tmpStat);
			}
			elseScope.pushInstructions(tmpScopeInstrs);
			currentScope = currentScope.getFatherScope();
		}
		
		tmpScopeInstrs = currentScope.instructions;
		tmpScopeInstrs.add(elseScope);
		IRLabel endLabel = new IRLabel(nowLabelId+"endif");
		tmpScopeInstrs.add(endLabel);
		labelMap.put(endLabel.getLabel(), endLabel);
		
		//tmpScopeInstrs.add(new IRIfStatement(irCond,thenScope,condIrExpr,elseIfList,elseScope));
	}

	
	@Override
	public void visit(ASTCharConstant charConst) {
		if(charConst == null) return;
		VirtualReg vReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.intType);
		tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.intType, charConst.value)));
		map.put(charConst, vReg);
	}
	@Override
	public void visit(ASTIdentifier identifier) {
		if(identifier == null) return;
		VirtualReg virtualReg = currentScope.getVariableReg(identifier.value);
		
		if(virtualReg == null) {
			// 获取这个参数分配的offset，添加到stackmap中
			String paramName = identifier.value;
			virtualReg = paramRegMap.get(paramName);
		}
		
		if(currentScope.isGlobal(identifier.value)) {
			// 如果是全局变量，每次都 load进来
			VirtualRegProperty vrp = globalVarPropMap.get(virtualReg);
			Integer offset = vrp.getOffset();
			tmpScopeInstrs.add(new IRDassign(virtualReg, new IRIread(virtualReg.getType(), GlobalSymbolTable.pointerType, new IRConstval(GlobalSymbolTable.intType, offset))));
		}
		map.put(identifier, virtualReg);
	}
	@Override
	public void visit(ASTFloatConstant floatConst) {
		if(floatConst == null) return;
		VirtualReg vReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.floatType);
		tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.floatType, floatConst.value)));
		map.put(floatConst, vReg);
	}
	@Override
	public void visit(ASTIntegerConstant intConst) {
		if(intConst == null) return;
		VirtualReg vReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.intType);
		tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.intType, intConst.value)));
		map.put(intConst, vReg);
		//map.put(intConst, new IRConstval(GlobalSymbolTable.intType, intConst.value));
	}
	@Override
	public void visit(ASTStringConstant stringConst) {
		if(stringConst == null)return;
		VirtualReg vReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.stringType);
		tmpScopeInstrs.add(new IRDassign(vReg, new IRConstval(GlobalSymbolTable.stringType, stringConst.value)));
		map.put(stringConst, vReg);
	}

	@Override
	public void visit(ASTToken token) {
		// nothing
	}
	
	
	private IRExpression getBinaryResult(String op,IRExpression expr1,IRExpression expr2,Type type) {
		if (op.equals("=")) {
			return null;
		}
		VirtualReg vReg = new VirtualReg(++virtualRegId, type);
		IRExpression tmpRes = null;
		if (op.equals("*=")||op.equals("*")) {
			tmpRes = new IRMul(expr1, expr2, type);
		}else if(op.equals("/=")||op.equals("/")) {
			tmpRes =  new IRDiv(expr1, expr2, type);
		}else if(op.equals("%=")||op.equals("%")) {
			tmpRes =  new IRRem(expr1, expr2, type);
		}else if(op.equals("+=")||op.equals("+")) {
			tmpRes =  new IRAdd(expr1, expr2, type);
		}else if(op.equals("-=")||op.equals("-")) {
			tmpRes =  new IRSub(expr1, expr2, type);
		}else if(op.equals("<<=")||op.equals("<<")) {
			tmpRes =  new IRShl(expr1, expr2, type);
		}else if(op.equals(">>=")||op.equals(">>")) {
			tmpRes =  new IRLshr(expr1, expr2, type);
		}else if(op.equals("&=")||op.equals("&")) {
			tmpRes =  new IRBand(expr1, expr2, type);
		}else if(op.equals("^=")||op.equals("^")) {
			tmpRes =  new IRBxor(expr1, expr2, type);
		}else if(op.equals("|=")||op.equals("|")) {
			tmpRes =  new IRBior(expr1, expr2, type);
		}else if(op.equals("<")) {
			tmpRes = new IRLt(expr1, expr2, type);
		}else if(op.equals(">")) {
			tmpRes =  new IRGt(expr1, expr2, type);
		}else if(op.equals("<=")) {
			tmpRes =  new IRLe(expr1, expr2, type);
		}else if(op.equals(">=")) {
			tmpRes =  new IRGe(expr1, expr2, type);
		}else if(op.equals("==")) {
			tmpRes =  new IREq(expr1, expr2, type);
		}else if(op.equals("!=")) {
			tmpRes =  new IRNe(expr1, expr2, type);
		}else if(op.equals("&&")) {
			tmpRes =  new IRLand(expr1, expr2, type);
		}else if(op.equals("||")) {
			tmpRes =  new IRLior(expr1, expr2, type);
		}
		tmpScopeInstrs.add(new IRDassign(vReg, tmpRes));
		return vReg;
	}

	
	private boolean isLogicBinaryExpr(ASTExpression expr) {
		if(expr instanceof ASTBinaryExpression) {
			if(((ASTBinaryExpression)expr).op.value.equals("||") || ((ASTBinaryExpression)expr).op.value.equals("&&")) {
				return true;
			}
		}
		return false;
	}
	
	
	private String getArrayName(ASTArrayAccess arrayAccess) {
		if(arrayAccess.arrayName instanceof ASTIdentifier)
			return ((ASTIdentifier)arrayAccess.arrayName).value;
		return getArrayName((ASTArrayAccess)arrayAccess.arrayName);
	}
	
	private void selfChange(ASTExpression expr,String type) {
		String varName;
		Type varType;
	    VirtualReg constReg = new VirtualReg(++virtualRegId, GlobalSymbolTable.intType);
	   
		if(expr instanceof ASTIdentifier) {
			varName = ((ASTIdentifier)expr).value;
		    varType = ((ASTIdentifier)expr).type;
		   // VirtualReg vReg = currentScope.getVariableReg(varName);
		    VirtualReg vReg = getVarVReg(varName);
		    //xxx
		    tmpScopeInstrs.add(new IRDassign(constReg, new IRConstval(GlobalSymbolTable.intType, 1)));
		    if(type.equals("++")) {
		    	tmpScopeInstrs.add(new IRDassign(vReg,
						new IRAdd(
								vReg,
								constReg,
								varType
								)
						));
		    }else {
		    	tmpScopeInstrs.add(new IRDassign(vReg,
						new IRSub(
								vReg,
								constReg,
								varType
								)
						));
		    }
		    if(globalVarPropMap.containsKey(vReg)) {
		    	VirtualRegProperty vrp = globalVarPropMap.get(vReg);
		    	Integer offset = vrp.getOffset();
		    	// store 到内存
		    	tmpScopeInstrs.add(new IRIassign(vReg.getType(), new IRConstval(GlobalSymbolTable.intType, offset), vReg));
		    }
			
		}else {
			varType = ((ASTArrayAccess)expr).type;
			String arrayName = getArrayName((ASTArrayAccess)expr);
			ArrayType arrayType = (ArrayType)currentScope.getSymbolType(arrayName);
			tmpScopeInstrs.add(new IRDassign(constReg, new IRConstval(GlobalSymbolTable.intType, 1)));
			// 此时expr已经visit过，map中取出的是vReg内容是地址，需要增加一个load,值存在一个临时寄存器里，用作下面加减法的参数
			VirtualReg arrVreg = (VirtualReg)map.get(expr);
			VirtualReg tmpReg = new VirtualReg(++virtualRegId, arrayType.elementType);
			tmpScopeInstrs.add(new IRDassign(tmpReg, arrVreg));
			if(currentScope.isGlobal(arrayName)) {
				tmpScopeInstrs.add(new IRDassign(tmpReg, new IRIread(arrayType.elementType, GlobalSymbolTable.pointerType, tmpReg)));
			}else {
				tmpScopeInstrs.add(new IRDassign(tmpReg, new IRIreadfpoff(arrayType.elementType, tmpReg)));
			}
			
			if(type.equals("++")) {
				tmpScopeInstrs.add(new IRDassign(tmpReg, new IRAdd(tmpReg,constReg,varType)));
			}else {
				tmpScopeInstrs.add(new IRDassign(tmpReg, new IRSub(tmpReg,constReg,varType)));
			}
			
			VirtualReg tmpReg2 = new VirtualReg(++virtualRegId, arrayType.elementType);
			tmpScopeInstrs.add(new IRDassign(tmpReg2, arrVreg));
			if(currentScope.isGlobal(arrayName)) {
				tmpScopeInstrs.add(new IRIassign(arrayType,tmpReg2, tmpReg));
			}else {
				tmpScopeInstrs.add(new IRIassignfpoff(arrayType, tmpReg2, tmpReg));
			}
			
		}
	}
	
	private VirtualReg getVarVReg(String name) {
		VirtualReg res = currentScope.getVariableReg(name);
		if(res == null) {
			// 说明是参数，因为在关联参数的vreg时还没进入currentScope
			// 从map中取出
			res = paramRegMap.get(name);
		}
		return res;
	}
	
	private boolean isLvalueOp(String op) {
		if (op.equals("=") || op.equals("*=") ||op.equals("/=") ||
			op.equals("%=") ||op.equals("+=") ||op.equals("-=") ||
			op.equals("<<=") ||op.equals(">>=") ||op.equals("&=") ||
			op.equals("^=") ||op.equals("|=")) {
			return true;
		}
		return false;
	}
	
	private VirtualReg getTmpRegForSavedAddr(Type type,IRExpression savedAddrReg) {
		VirtualReg tmpVReg = new VirtualReg(++virtualRegId, type);
		tmpScopeInstrs.add(new IRDassign(tmpVReg, savedAddrReg));
		return tmpVReg;
	}
}
