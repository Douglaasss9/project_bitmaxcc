package bit.minisys.minicc.icgen.internal;

import java.util.LinkedList;

import bit.minisys.minicc.internal.ir.*;

public class IROpVisitor implements IRVisitor{
	
	private VirtualReg res;
	private VirtualReg op1;
	private VirtualReg op2;
	private LinkedList<VirtualReg> callArgs;
	private Integer paramId = -1;
	private Integer paramNum = 0;
	public Integer getParamId() {
		return paramId;
	}
	public Integer getParamNum() {
		return paramNum;
	}
	public VirtualReg getOp1() {
		return op1;
	}
	public VirtualReg getOp2() {
		return op2;
	}
	public VirtualReg getRes() {
		return res;
	}
	public LinkedList<VirtualReg> getCallArgs() {
		return callArgs;
	}
	@Override
	public void visit(IRDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRFunctionDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRVariableDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRInstruction node) {
		if(node == null) return;
		if(node instanceof IRStatement) {
			visit((IRStatement)node);
		}else if (node instanceof IRExpression) {
			visit((IRExpression)node);
		}
		
	}

	@Override
	public void visit(IRStatement node) {
		// TODO Auto-generated method stub
		if(node == null) return;
		if(node instanceof IRBrfalse) {
			visit((IRBrfalse)node);
		}else if (node instanceof IRBrtrue) {
			visit((IRBrtrue)node);
		}else if (node instanceof IRGotoStatement) {
			visit((IRGotoStatement)node);
		}else if (node instanceof IRReturnStatement) {
			visit((IRReturnStatement)node);
		}
	}

	@Override
	public void visit(IRBrfalse node) {
		this.op1 = (VirtualReg)node.opnd0;
	}

	@Override
	public void visit(IRBrtrue node) {
		this.op1 = (VirtualReg)node.opnd0;
	}


	@Override
	public void visit(IRGotoStatement node) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(IRLabel node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRReturnStatement node) {
		// TODO Auto-generated method stub
		this.op1 = (VirtualReg)node.opnd;
	}

	@Override
	public void visit(IRScope node) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(IRExpression node) {
		// TODO Auto-generated method stub
		if(node instanceof IRUnaryOperator) {
			visit((IRUnaryOperator)node);
		}else if (node instanceof IRBinaryOperator) {
			visit((IRBinaryOperator)node);
		}else if (node instanceof IRTernaryOperator) {
			visit((IRTernaryOperator)node);
		}else if (node instanceof IRCastOperator) {
			visit((IRCastOperator)node);
		}else if (node instanceof IRCall) {
			visit((IRCall)node);
		}else if (node instanceof IRLeaf) {
			visit((IRLeaf)node);
		}else if (node instanceof IRIread) {
			visit((IRIread)node);
		}else if (node instanceof IRIreadfpoff) {
			visit((IRIreadfpoff)node);
		}else if (node instanceof IRDassign) {
			visit((IRDassign)node);
		}else if (node instanceof IRIassign) {
			visit((IRIassign)node);
		}else if (node instanceof IRIassignfpoff) {
			visit((IRIassignfpoff)node);
		}
	}

	@Override
	public void visit(IRUnaryOperator node) {
		// TODO Auto-generated method stub
		this.op1 = (VirtualReg)node.opnd0;
	}

	@Override
	public void visit(IRInot node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNeg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBinaryOperator node) {
		// TODO Auto-generated method stub
		this.op1 = (VirtualReg)node.opnd0;
		if(node.opnd1 instanceof IRConstval) {
			return;
		}
		else if (node.opnd1 instanceof VirtualReg) {
			this.op2 = (VirtualReg)node.opnd1;
		}

	}

	@Override
	public void visit(IRAdd node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBxor node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCmp node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRDiv node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IREq node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRMul node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRRem node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRShl node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSub node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRTernaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCastOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCvt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCall node) {
		callArgs = new LinkedList<VirtualReg>();
		if(node.args != null && node.args.size() > 0) {
			for (IRExpression arg : node.args) {
				if(arg instanceof VirtualReg) {
					callArgs.add((VirtualReg)arg);
				}
			}
		}
	
	}

	@Override
	public void visit(IRLeaf node) {
		// TODO Auto-generated method stub
		if(node instanceof IRConstval) {
			visit((IRConstval)node);
		}else if (node instanceof IRSizeoftype) {
			visit((IRSizeoftype)node);
		}else if (node instanceof IRAddrof) {
			visit((IRAddrof)node);
		}else if (node instanceof VirtualReg) {
			visit((VirtualReg)node);
		}
	}

	@Override
	public void visit(IRConstval node) {
		// TODO Auto-generated method stub
	}

	@Override
	public void visit(IRSizeoftype node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAddrof node) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(IRIread node) {
		// TODO Auto-generated method stub
		if(node.addrExpr instanceof VirtualReg)
			this.op1 = (VirtualReg) node.addrExpr;
	}


	@Override
	public void visit(IRDassign node) {
		this.res = (VirtualReg) node.varName;
		if(node.rhsExpr instanceof ParamVirtualReg) {
			// 如果是load参数
			paramId = ((ParamVirtualReg)node.rhsExpr).getParamId();
			paramNum = ((ParamVirtualReg)node.rhsExpr).getParamNum();
		}else {
			visit(node.rhsExpr);
		}

	}

	@Override
	public void visit(IRIassign node) {
		// TODO Auto-generated method stub
		this.op1 = (VirtualReg) node.rhsExpr;
		if(node.addrExpr instanceof VirtualReg)
			this.op2 = (VirtualReg) node.addrExpr;
	}


	@Override
	public void visit(VirtualReg node) {
		if(node instanceof ParamVirtualReg) {
			return;
		}else {
			this.op1 = node;
		}
	}
	@Override
	public void visit(IRIassignfpoff node) {
		if (node.offsetExpr instanceof VirtualReg ) {
			this.op1 = (VirtualReg) node.offsetExpr;
		}
		if (node.rhsExpr instanceof VirtualReg) {
			this.op2 = (VirtualReg) node.rhsExpr;
		}
	}
	@Override
	public void visit(IRIreadfpoff node) {
		if(node.offsetExpr instanceof VirtualReg)
			this.op1 = (VirtualReg)node.offsetExpr;
	}
	@Override
	public void visit(IRIassignoff node) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void visit(IRIreadoff node) {
		// TODO Auto-generated method stub
		
	}


}
