package bit.minisys.minicc.icgen.internal;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.python.antlr.PythonParser.else_clause_return;
import org.python.indexer.ast.NSequence;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;

import bit.minisys.minicc.internal.ir.IRAdd;
import bit.minisys.minicc.internal.ir.IRAddrof;
import bit.minisys.minicc.internal.ir.IRAshr;
import bit.minisys.minicc.internal.ir.IRBand;
import bit.minisys.minicc.internal.ir.IRBinaryOperator;
import bit.minisys.minicc.internal.ir.IRBior;
import bit.minisys.minicc.internal.ir.IRBrfalse;
import bit.minisys.minicc.internal.ir.IRBrtrue;
import bit.minisys.minicc.internal.ir.IRBxor;
import bit.minisys.minicc.internal.ir.IRCall;
import bit.minisys.minicc.internal.ir.IRCand;
import bit.minisys.minicc.internal.ir.IRCastOperator;
import bit.minisys.minicc.internal.ir.IRCior;
import bit.minisys.minicc.internal.ir.IRCmp;
import bit.minisys.minicc.internal.ir.IRConstval;
import bit.minisys.minicc.internal.ir.IRCvt;
import bit.minisys.minicc.internal.ir.IRDassign;
import bit.minisys.minicc.internal.ir.IRDeclaration;
import bit.minisys.minicc.internal.ir.IRDiv;
import bit.minisys.minicc.internal.ir.IREq;
import bit.minisys.minicc.internal.ir.IRExpression;
import bit.minisys.minicc.internal.ir.IRFunctionDeclaration;
import bit.minisys.minicc.internal.ir.IRGe;
import bit.minisys.minicc.internal.ir.IRGotoStatement;
import bit.minisys.minicc.internal.ir.IRGt;
import bit.minisys.minicc.internal.ir.IRIassign;
import bit.minisys.minicc.internal.ir.IRIassignfpoff;
import bit.minisys.minicc.internal.ir.IRIassignoff;
import bit.minisys.minicc.internal.ir.IRInot;
import bit.minisys.minicc.internal.ir.IRInstruction;
import bit.minisys.minicc.internal.ir.IRIread;
import bit.minisys.minicc.internal.ir.IRIreadfpoff;
import bit.minisys.minicc.internal.ir.IRIreadoff;
import bit.minisys.minicc.internal.ir.IRLabel;
import bit.minisys.minicc.internal.ir.IRLand;
import bit.minisys.minicc.internal.ir.IRLe;
import bit.minisys.minicc.internal.ir.IRLeaf;
import bit.minisys.minicc.internal.ir.IRLior;
import bit.minisys.minicc.internal.ir.IRLshr;
import bit.minisys.minicc.internal.ir.IRLt;
import bit.minisys.minicc.internal.ir.IRMul;
import bit.minisys.minicc.internal.ir.IRNe;
import bit.minisys.minicc.internal.ir.IRNeg;
import bit.minisys.minicc.internal.ir.IRRem;
import bit.minisys.minicc.internal.ir.IRReturnStatement;
import bit.minisys.minicc.internal.ir.IRScope;
import bit.minisys.minicc.internal.ir.IRShl;
import bit.minisys.minicc.internal.ir.IRSizeoftype;
import bit.minisys.minicc.internal.ir.IRStatement;
import bit.minisys.minicc.internal.ir.IRSub;
import bit.minisys.minicc.internal.ir.IRTernaryOperator;
import bit.minisys.minicc.internal.ir.IRUnaryOperator;
import bit.minisys.minicc.internal.ir.IRVariableDeclaration;
import bit.minisys.minicc.internal.ir.VirtualReg;
import bit.minisys.minicc.internal.symbol.ArrayType;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;
import bit.minisys.minicc.internal.symbol.NormalType;
import bit.minisys.minicc.internal.symbol.Type;
import bit.minisys.minicc.parser.ast.ASTIntegerConstant;

public class IRPrinter implements IRVisitor{
	private boolean isGlobal = true;	// 记录是否为全局作用域
	private boolean isParam = false;	// 记录是否为函数参数
	private boolean isLvalue = false;	// 记录是否为左值
	private boolean isCall = false;     // 区分x = fun()还是fun();
	private Map<VirtualReg, VirtualRegProperty> globalVarPropMap = new HashMap<VirtualReg, VirtualRegProperty>();
	private Map<VirtualReg, VirtualRegProperty> localVarPropMap = new HashMap<VirtualReg, VirtualRegProperty>();
	private Set<String> params = new HashSet<String>();
	Map<IRInstruction, String> constStrMap = new HashMap<IRInstruction, String>();
	public void setConstStrMap(Map<IRInstruction, String> constStrMap) {
		this.constStrMap = constStrMap;
	}
	StringBuilder sb = new StringBuilder();
	StringBuilder tab = new StringBuilder();
	private void append() {
		tab.append("\t");
	}
	private void delete() {
		tab.delete(tab.length()-1, tab.length());
	}
	public void write(String filename) {
		try {
			FileWriter fileWriter = new FileWriter(new File(filename));
			fileWriter.write(sb.toString());
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setGlobalVarPropMap(Map<VirtualReg, VirtualRegProperty> globalVarPropMap) {
		this.globalVarPropMap = globalVarPropMap;
	}
	@Override
	public void visit(IRDeclaration node) {
		if (node instanceof IRFunctionDeclaration) {
			visit((IRFunctionDeclaration)node);
		}else if (node instanceof IRVariableDeclaration) {
			visit((IRVariableDeclaration)node);
		}
	}

	@Override
	public void visit(IRFunctionDeclaration node) {
		localVarPropMap = node.getLocalVarPropMap();
		isGlobal = false;
		String funcname = node.funcname;
		sb.append("func &"+funcname);
		sb.append("(");
		// 参数部分
		isParam = true;
		for (int i = 0; i<node.argsSymTab.size(); i++) {
			visit(node.argsSymTab.get(i));
			if (i!=node.argsSymTab.size()-1) {
				sb.append(", ");
			}
		}
		isParam = false;
		sb.append(")");
		String resType = node.returnType.toIRString();
		sb.append(" "+resType);
		sb.append("{\n");
		// 函数体
		visit(node.body);
		sb.append("}\n\n");
		isGlobal = true;
		params.clear();
	}

	private String getTypeStr(Type type) {
		if (type instanceof NormalType) {
			return type.toIRString();
		}else if (type instanceof ArrayType) {
			String res = "<";
			Integer ds = ((ArrayType)type).dimension;
			for (int i = 0; i < ds; i++) {
				res+="[";
				Integer num = ((ASTIntegerConstant)((ArrayType)type).dimens.get(i)).value;
				res+=num;
				res+="]";
			}
			res+="> ";
			res+=((ArrayType)type).elementType.toIRString();
			return res;
		}
		return null;
	}
	private String getAddrTypeStr(Type type) {
		if (type instanceof NormalType) {
			return "<*"+type.toIRString()+">";
		}else if (type instanceof ArrayType) {
			String res = "<*";
			Integer ds = ((ArrayType)type).dimension;
			for (int i = 0; i < ds; i++) {
				res+="[";
				Integer num = ((ASTIntegerConstant)((ArrayType)type).dimens.get(i)).value;
				res+=num;
				res+="]";
			}
			res+="> ";
			res+=((ArrayType)type).elementType.toIRString();
			return res;
		}
		return null;
	}
	@Override
	public void visit(IRVariableDeclaration node) {

		String label = isGlobal == true ? "$" : "%";
		String name = node.name;
		String type = getTypeStr(node.type);
		
		if (params.contains(name)) {
			return;
		}
		
		if (type!=null) {
			sb.append("var "+label+name+" "+type);
			if (!isParam) {
				sb.append("\n");
			}else {
				params.add(name);
			}
		}else {
			System.out.println("IRPrinter ERROR.");
		}
		
	}

	@Override
	public void visit(IRInstruction node) {
		if (node instanceof IRExpression) {
			visit((IRExpression)node);
		}else if (node instanceof IRStatement) {
			visit((IRStatement)node);
		}
	}

	@Override
	public void visit(IRStatement node) {
		if (node instanceof IRBrfalse) {
			visit((IRBrfalse)node);
		}else if (node instanceof IRBrtrue) {
			visit((IRBrtrue)node);
		}else if (node instanceof IRGotoStatement) {
			visit((IRGotoStatement)node);
		}else if (node instanceof IRLabel) {
			visit((IRLabel)node);
		}else if (node instanceof IRReturnStatement) {
			visit((IRReturnStatement)node);
		}else if (node instanceof IRScope) {
			visit((IRScope)node);
		}
	}

	@Override
	public void visit(IRBrfalse node) {
		sb.append(tab.toString());
		sb.append("brfalse <"+node.label+">");
		sb.append("(");
		if (node.opnd0 instanceof IRLeaf) {
			visit(node.opnd0);
		}else {
			append();
			visit(node.opnd0);
			delete();
		}
		sb.append(")");
		appendEnter();
	}

	@Override
	public void visit(IRBrtrue node) {
		sb.append(tab.toString());
		sb.append("brtrue <"+node.label+">");
		sb.append("(");
		if (node.opnd0 instanceof IRLeaf) {
			visit(node.opnd0);
		}else {
			append();
			visit(node.opnd0);
			delete();
		}
		sb.append(")");
		appendEnter();
	}

	@Override
	public void visit(IRGotoStatement node) {
		sb.append(tab.toString());
		sb.append("goto ");
		sb.append("<"+node.label+">");
		appendEnter();
	}

	@Override
	public void visit(IRLabel node) {
		sb.append(tab.toString());
		sb.append(node.getLabel()+":");
		appendEnter();
	}

	@Override
	public void visit(IRReturnStatement node) {
		sb.append("return ");
		sb.append("(");
		if (node.opnd instanceof IRLeaf) {
			visit(node.opnd);
		}else {
			append();
			visit(node.opnd);
			delete();
		}
		sb.append(")");
		appendEnter();
	}

	private Integer scopeCnt = 0;
	private Boolean enterFlag = false;
	private void appendEnter() {
		if (enterFlag) {
			sb.append("\n");
		}
	}
	private void dealWithStrConst() {
		
	}
	@Override
	public void visit(IRScope node) {
		scopeCnt ++;
		for (IRDeclaration decl : node.localSymTab) {
			if (decl instanceof IRVariableDeclaration) {
				visit((IRVariableDeclaration)decl);
			}
		}
		if (scopeCnt == 1) {
			// 说明是全局作用域，还需要把那些字符串常量特殊输出
			dealWithStrConst();
		}
		for (IRDeclaration decl : node.localSymTab) {
			if (decl instanceof IRFunctionDeclaration) {
				visit((IRFunctionDeclaration)decl);
			}
		}
		if (isGlobal != true) {
			for (int i = 0; i < node.instructions.size(); i++) {
				if (scopeCnt == 2 && i == node.instructions.size()-1) {
					enterFlag = false;
				}else {
					enterFlag = true;
				}
				visit(node.instructions.get(i));
			}
		}
		scopeCnt --;
	}

	@Override
	public void visit(IRExpression node) {
		if(node instanceof IRUnaryOperator) {
			visit((IRUnaryOperator)node);
		}else if (node instanceof IRBinaryOperator) {
			visit((IRBinaryOperator)node);
		}else if (node instanceof IRTernaryOperator) {
			visit((IRTernaryOperator)node);
		}else if (node instanceof IRCastOperator) {
			visit((IRCastOperator)node);
		}else if (node instanceof IRCall) {
			visit((IRCall)node);
		}else if (node instanceof IRLeaf) {
			visit((IRLeaf)node);
		}else if (node instanceof IRIread) {
			visit((IRIread)node);
		}else if (node instanceof IRIreadfpoff) {
			visit((IRIreadfpoff)node);
		}else if (node instanceof IRIreadoff) {
			visit((IRIreadfpoff)node);
		}else if (node instanceof IRDassign) {
			visit((IRDassign)node);
		}else if (node instanceof IRIassign) {
			visit((IRIassign)node);
		}else if (node instanceof IRIassignoff) {
			visit((IRIassignoff)node);
		}else if (node instanceof IRIassignfpoff) {
			visit((IRIassignfpoff)node);
		}
	}

	@Override
	public void visit(IRUnaryOperator node) {
		sb.append(tab.toString());
		if (node instanceof IRInot) {
			visit((IRInot)node);
		}else if (node instanceof IRNeg) {
			visit((IRNeg)node);
		}
	}

	@Override
	public void visit(IRInot node) {
		String resType = node.type.toIRString();
		String op = "lnot";
		sb.append(op+" "+resType);
		sb.append("(");
		if (node.opnd0 instanceof IRLeaf) {
			visit(node.opnd0);
		}else {
			sb.append("\n");
			append();
			visit(node.opnd0);
			delete();
		}
		sb.append(")");
	}

	@Override
	public void visit(IRNeg node) {
		String resType = node.type.toIRString();
		String op = "neg";
		sb.append(op+" "+resType);
		sb.append("(");
		if (node.opnd0 instanceof IRLeaf) {
			visit(node.opnd0);
		}else {
			sb.append("\n");
			append();
			visit(node.opnd0);
			delete();
		}
		sb.append(")");
	}

	@Override
	public void visit(IRBinaryOperator node) {
		sb.append(tab.toString());
		String resType = node.resType.toIRString();
		String op = "";
		if (node instanceof IRAdd) {
			op = "add";
		}else if (node instanceof IRAshr) {
			op = "ashr";
		}else if (node instanceof IRBand) {
			op = "band";
		}else if (node instanceof IRBior) {
			op = "bior";
		}else if (node instanceof IRBxor) {
			op = "bxor";
		}else if (node instanceof IRCand) {
			op = "cand";
		}else if (node instanceof IRCior) {
			op = "cior";
		}else if (node instanceof IRCmp) {
			op = "cmp";
		}else if (node instanceof IRDiv) {
			op = "div";
		}else if (node instanceof IREq) {
			op = "eq";
		}else if (node instanceof IRGe) {
			op = "ge";
		}else if (node instanceof IRGt) {
			op = "gt";
		}else if (node instanceof IRLand) {
			op = "land";
		}else if (node instanceof IRLior) {
			op = "lior";
		}else if (node instanceof IRLe) {
			op = "le";
		}else if (node instanceof IRLt) {
			op = "lt";
		}else if (node instanceof IRLshr) {
			op = "lshr";
		}else if (node instanceof IRMul) {
			op = "mul";
		}else if (node instanceof IRNe) {
			op = "ne";
		}else if (node instanceof IRRem) {
			op = "rem";
		}else if (node instanceof IRShl) {
			op = "shl";
		}else if (node instanceof IRSub) {
			op = "sub";
		}
		sb.append(op+" "+resType);
		sb.append("(");
		if (node.opnd0 instanceof IRLeaf && node.opnd1 instanceof IRLeaf) {
			visit(node.opnd0);
			sb.append(",");
			visit(node.opnd1);
		}else {
			sb.append("\n");
			append();
			visit(node.opnd0);
			sb.append(",\n");
			visit(node.opnd1);
			delete();
		}
		sb.append(")");
	}

	@Override
	public void visit(IRAdd node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBxor node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCmp node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRDiv node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IREq node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRMul node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRRem node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRShl node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSub node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRTernaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCastOperator node) {
		if (node instanceof IRCvt) {
			visit((IRCvt)node);
		}
	}

	@Override
	public void visit(IRCvt node) {
		sb.append(tab.toString());
		String toType = getTypeStr(node.toType);
		String fromType = getTypeStr(node.fromType);
		sb.append("cvt "+toType+" "+fromType);
		sb.append("(");
		if (node.opnd0 instanceof IRLeaf) {
			visit(node.opnd0);
		}else {
			sb.append("\n");
			append();
			visit(node.opnd0);
			delete();
		}
		sb.append(")");
	}

	@Override
	public void visit(IRCall node) {
		if (!isCall) {
			// fun()
			tab.delete(0, tab.length());
		}
		sb.append(tab.toString());
		sb.append("call "+node.funcname);
		sb.append("(");
		Boolean flag = true;
		for (IRExpression arg : node.args) {
			if (!(arg instanceof IRLeaf)) {
				flag = false;
				break;
			}
		}
		if (!flag) {
			sb.append("\n");
			for (int i = 0; i<node.args.size(); i++) {
				visit(node.args.get(i));
				if (i != node.args.size()-1) {
					sb.append(",\n");
				}
			}
		}else {
			for (int i = 0; i<node.args.size(); i++) {
				visit(node.args.get(i));
				if (i != node.args.size()-1) {
					sb.append(",");
				}
			}
		}
		sb.append(")");
		if (!isCall) {
			// fun()
			appendEnter();
		}
	}

	@Override
	public void visit(IRLeaf node) {
		if (node instanceof IRConstval) {
			visit((IRConstval)node);
		}else if (node instanceof IRSizeoftype) {
			visit((IRSizeoftype)node);
		}else if (node instanceof VirtualReg) {
			visit((VirtualReg)node);
		}else if (node instanceof IRAddrof) {
			visit((IRAddrof)node);
		}
	}

	@Override
	public void visit(IRConstval node) {
		sb.append("constval ");
		if (isIntType(node.type)) {
			sb.append("i32 ");
			Integer value = (Integer)node.value;
			sb.append(value);
		}else {
			// 这里是除了i32其他类型的常量，to be done
		}
	}

	@Override
	public void visit(IRSizeoftype node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAddrof node) {
		String type = getTypeStr(node.resType);
		sb.append("addrof "+type+" "+node.varName);
	}

	@Override
	public void visit(VirtualReg node) {
		if (node instanceof ParamVirtualReg) {
			ParamVirtualReg pReg = (ParamVirtualReg)node;
			if (pReg.getParamId() == -1) {
				sb.append("regread "+pReg.getType().toIRString()+" %retval0");
			}
		}else {
			String idName = "";
			String suf = "";
			if (globalVarPropMap.containsKey(node)) {
				idName = "$"+globalVarPropMap.get(node).getVarName();
				if (!isLvalue) {
					suf = "dread "+node.getType().toIRString()+" ";
				}
			}else if (localVarPropMap.containsKey(node)) {
				idName = "%"+localVarPropMap.get(node).getVarName();
				if (!isLvalue) {
					suf = "dread "+node.getType().toIRString()+" ";
				}
			}else {
				idName = "%"+node.getId();
				if (!isLvalue) {
					suf = "regread "+node.getType().toIRString()+" ";
				}
			}
			sb.append(suf+idName);
		}
	}

	@Override
	public void visit(IRDassign node) {
		tab.delete(0, tab.length());
		if (node.rhsExpr instanceof ParamVirtualReg) {
			ParamVirtualReg preg = (ParamVirtualReg)node.rhsExpr;
			if (preg.getParamId() != -1) {
				return;
			}
		}
		
		sb.append("dassign ");
		isLvalue = true;
		visit(node.varName);
		isLvalue = false;
		if (constStrMap.containsKey(node)) {
			String conStr = constStrMap.get(node);
			sb.append("(conststr a32 "+conStr+")");
			appendEnter();
			return;
		}
		if (node.rhsExpr instanceof IRCall) {
			isCall = true;
		}
		sb.append("(");
		if (node.rhsExpr instanceof IRLeaf) {
			visit((IRLeaf)node.rhsExpr);
		}else {
			sb.append("\n");
			append();
			visit(node.rhsExpr);
			delete();
		}
		sb.append(")");
		appendEnter();
		
		if (node.rhsExpr instanceof IRCall) {
			isCall = false;
		}
	}

	@Override
	public void visit(IRIassign node) {
		tab.delete(0, tab.length());
		String type = getAddrTypeStr(node.type);
		sb.append("iassign "+type);
		sb.append("(");
		if (node.addrExpr instanceof IRLeaf && node.rhsExpr instanceof IRLeaf) {
			visit(node.addrExpr);
			sb.append(",");
			visit(node.rhsExpr);
		}else {
			sb.append("\n");
			append();
			visit(node.addrExpr);
			sb.append(",\n");
			visit(node.rhsExpr);
			delete();
		}
		sb.append(")");
		appendEnter();
	}

	@Override
	public void visit(IRIassignoff node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIassignfpoff node) {
		tab.delete(0, tab.length());
		String type = getAddrTypeStr(node.type);
		sb.append("iassignfpoff "+type);
		sb.append("(");
		if (node.offsetExpr instanceof IRLeaf && node.rhsExpr instanceof IRLeaf) {
			visit(node.offsetExpr);
			sb.append(",");
			visit(node.rhsExpr);
		}else {
			sb.append("\n");
			append();
			visit(node.offsetExpr);
			sb.append(",\n");
			visit(node.rhsExpr);
			delete();
		}
		sb.append(")");
		appendEnter();
	}

	@Override
	public void visit(IRIreadfpoff node) {
		String type = node.primType.toIRString();
		sb.append(tab.toString());
		sb.append("ireadfpoff "+type);
		sb.append("(");
		if (node.offsetExpr instanceof IRLeaf) {
			visit(node.offsetExpr);
		}else {
			sb.append("\n");
			append();
			visit(node.offsetExpr);
			delete();
		}
		sb.append(")");
	}

	@Override
	public void visit(IRIread node) {
		String type = node.primType.toIRString();
		sb.append(tab.toString());
		sb.append("iread "+type);
		sb.append("(");
		if (node.addrExpr instanceof IRLeaf) {
			visit(node.addrExpr);
		}else {
			sb.append("\n");
			append();
			visit(node.addrExpr);
			delete();
		}
		sb.append(")");
	}

	@Override
	public void visit(IRIreadoff node) {
		// TODO Auto-generated method stub
		
	}
	
	private Boolean isIntType(Type type) {
		if (type == GlobalSymbolTable.intType) {
			return true;
		}
		return false;
	}

}
