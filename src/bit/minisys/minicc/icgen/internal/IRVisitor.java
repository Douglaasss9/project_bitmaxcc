package bit.minisys.minicc.icgen.internal;

import bit.minisys.minicc.internal.ir.*;

public interface IRVisitor {
	
	// Declaration
	void visit(IRDeclaration node);
	
	void visit(IRFunctionDeclaration node);
	void visit(IRVariableDeclaration node);
	
	// Instruction
	void visit(IRInstruction node);
	// Statement
	void visit(IRStatement node);
	
	void visit(IRBrfalse node);
	void visit(IRBrtrue node);
	void visit(IRGotoStatement node);
	void visit(IRLabel node);
	void visit(IRReturnStatement node);
	void visit(IRScope node);
	// Expression
	void visit(IRExpression node);
	
	void visit(IRUnaryOperator node);
		void visit(IRInot node);
		void visit(IRNeg node);
	void visit(IRBinaryOperator node);
		void visit(IRAdd node);
		void visit(IRAshr node);
		void visit(IRBand node);
		void visit(IRBior node);
		void visit(IRBxor node);
		void visit(IRCand node);
		void visit(IRCior node);
		void visit(IRCmp node);
		void visit(IRDiv node);
		void visit(IREq node);
		void visit(IRGe node);
		void visit(IRGt node);
		void visit(IRLand node);
		void visit(IRLior node);
		void visit(IRLe node);
		void visit(IRLshr node);
		void visit(IRLt node);
		void visit(IRMul node);
		void visit(IRNe node);
		void visit(IRRem node);
		void visit(IRShl node);
		void visit(IRSub node);
	void visit(IRTernaryOperator node);
	void visit(IRCastOperator node);
		void visit(IRCvt node);
	void visit(IRCall node);
	void visit(IRLeaf node);
		void visit(IRConstval node);
		void visit(IRSizeoftype node);
		void visit(IRAddrof node);
		void visit(VirtualReg node);
	void visit(IRDassign node);
	void visit(IRIassign node);
	void visit(IRIassignoff node);
	void visit(IRIassignfpoff node);
	void visit(IRIreadfpoff node);
	void visit(IRIread node);
	void visit(IRIreadoff node);
}
