package bit.minisys.minicc.icgen.internal;

import java.util.Map;

import bit.minisys.minicc.internal.ir.*;

public class LIRBuilder implements IRVisitor{

	private BasicBlock curBB = null;
	private IRScope curScope = null;
	
	private Map<String, IRInstruction> labelMap;
	private IRFunctionDeclaration curFuncFather;
	private BasicBlock endBB = null;
	private Integer bbid = 0;
	public LIRBuilder(Map<String, IRInstruction> labelMap) {
		this.labelMap = labelMap;
	}
	
	IRInstruction lastIr;
	Integer loopLabelId = 0;
	Integer nowLoopCnt = 0;
	Integer IfLabelid = 0;
	
	@Override
	public void visit(IRInstruction node) {
		if(node == null) return;
		if(node instanceof IRStatement) {
			visit((IRStatement)node);
		}else if (node instanceof IRExpression) {
			visit((IRExpression)node);
		}
	}

	@Override
	public void visit(IRDeclaration node) {
		if(node == null) return;
		if(node instanceof IRVariableDeclaration) {
			visit((IRDeclaration)node);
		}else if (node instanceof IRFunctionDeclaration) {
			visit((IRFunctionDeclaration)node);
		}
	}

	@Override
	public void visit(IRFunctionDeclaration node) {
		// TODO Auto-generated method stub
		if(node.body != null) {
			endBB = new BasicBlock(++bbid);
			
			curBB =  new BasicBlock(++bbid);
			lastIr = new IRLabel(node.funcname);
			lastIr.setBb(curBB);
			curBB.setFirst(lastIr);
			curBB.setFatherFunc(node);
			node.setStartBB(curBB);
			curFuncFather = node;
			
			// 这里的scope单独处理
			// 还有局部声明没有搞
			for (IRInstruction instr : node.body.instructions) {
				visit(instr);
			}
		}else {
			// 否则就只是一个没有实现的声明
			// 这里要怎么处理 ?
		}
	}

	@Override
	public void visit(IRVariableDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRStatement node) {
		if(node == null) return;
		if(node instanceof IRBrfalse) {
			visit((IRBrfalse)node);
		}else if (node instanceof IRBrtrue) {
			visit((IRBrtrue)node);
		}else if (node instanceof IRGotoStatement) {
			visit((IRGotoStatement)node);
		}else if (node instanceof IRLabel) {
			visit((IRLabel)node);
		}else if (node instanceof IRReturnStatement) {
			visit((IRReturnStatement)node);
		}else if (node instanceof IRScope) {
			visit((IRScope)node);
		}
	}

	@Override
	public void visit(IRBrfalse node) {
		if(curBB.getEnd() != null) {
			// 建立新的BB
			curBB = new BasicBlock(++bbid);
			curBB.setFatherFunc(curFuncFather);
			curBB.setFirst(node);
		}
		// 和上一个Instr
		node._prev(lastIr);
		// 找到label表中对应的label指令并设置
		IRInstruction falseNext = labelMap.get(node.label);
		node.setFalseNext(falseNext);
		// 作为当前bb的end
		curBB.setEnd(node);
		node.setBb(curBB);
		// 更新lastIr，进行下一条指令处理
		lastIr = node;
	}

	@Override
	public void visit(IRBrtrue node) {
		if(curBB.getEnd() != null) {
			// 建立新的BB
			curBB = new BasicBlock(++bbid);
			curBB.setFatherFunc(curFuncFather);
			curBB.setFirst(node);
		}
		// 和上一个Instr
		node._prev(lastIr);
		// 找到label表中对应的label指令并设置
		IRInstruction trueNext = labelMap.get(node.label);
		node.setTrueNext(trueNext);
		// 作为当前bb的end
		curBB.setEnd(node);
		node.setBb(curBB);
		// 更新lastIr，进行下一条指令处理
		lastIr = node;
	}


	@Override
	public void visit(IRGotoStatement node) {
		if(curBB.getEnd() != null) {
			// 建立新的BB
			curBB = new BasicBlock(++bbid);
			curBB.setFatherFunc(curFuncFather);
			curBB.setFirst(node);
		}
		// 和上一个Instr
		node._prev(lastIr);
		// 找到label表中对应的label指令并设置
		IRInstruction next = labelMap.get(node.label);
		
		node.setAbsNext(next);
		// 作为当前bb的end
		curBB.setEnd(node);
		node.setBb(curBB);
		// 更新lastIr，进行下一条指令处理
		lastIr = node;
	}

	@Override
	public void visit(IRLabel node) {
		if(curBB.getEnd() != null) {
			
		}else if(curBB.getEnd() == null) {
			curBB.setEnd(lastIr);
		}
		// 和上一个Instr
		node._prev(lastIr);
		// 建立新的BB
		curBB = new BasicBlock(++bbid);
		curBB.setFatherFunc(curFuncFather);
		curBB.setFirst(node);
		// 更新lastIr，进行下一条指令处理
		node.setBb(curBB);
		lastIr = node;
	}

	@Override
	public void visit(IRReturnStatement node) {
		if(curBB.getEnd() != null) {
			// 建立新的BB
			curBB = new BasicBlock(++bbid);
			curBB.setFatherFunc(curFuncFather);
			curBB.setFirst(node);
		}
		node._prev(lastIr);
		curBB.setEnd(node);
		node.setBb(curBB);
		node.setEndBlock(endBB);
		lastIr = node;
	}

	@Override
	public void visit(IRScope node) {
		if(node.getFatherScope() == null) {
			for (IRDeclaration irdecl : node.localSymTab) {
				if(irdecl instanceof IRFunctionDeclaration) {
					visit((IRFunctionDeclaration)irdecl);
				}
			}
		}else {
			// 此时已经进入到函数的body部分  开始按序处理指令
			for (IRInstruction instr : node.instructions) {
				visit(instr);
			}
		}
	}

	@Override
	public void visit(IRExpression node) {
		if(node == null) return;
		if(node instanceof VirtualReg) return;
		if(curBB.getEnd() != null) {
			// 建立新的BB
			curBB = new BasicBlock(++bbid);
			curBB.setFatherFunc(curFuncFather);
			curBB.setFirst(node);
		}
		node._prev(lastIr);
		node.setBb(curBB);
		lastIr = node;
		/*
		if(node instanceof IRUnaryOperator) {
			visit((IRUnaryOperator)node);
		}else if (node instanceof IRBinaryOperator) {
			visit((IRBinaryOperator)node);
		}else if (node instanceof IRTernaryOperator) {
			visit((IRTernaryOperator)node);
		}else if (node instanceof IRCastOperator) {
			visit((IRCastOperator)node);
		}else if (node instanceof IRCall) {
			visit((IRCall)node);
		}else if (node instanceof IRArray) {
			visit((IRArray)node);
		}else if (node instanceof IRLeaf) {
			visit((IRLeaf)node);
		}else if (node instanceof IRDread) {
			visit((IRDread)node);
		}else if (node instanceof IRIread) {
			visit((IRIread)node);
		}else if (node instanceof IRRegread) {
			visit((IRRegread)node);
		}else if (node instanceof IRDassign) {
			visit((IRDassign)node);
		}else if (node instanceof IRIassign) {
			visit((IRIassign)node);
		}else if (node instanceof IRIassignoff) {
			visit((IRIassignoff)node);
		}else if (node instanceof IRSelfInc) {
			visit((IRSelfInc)node);
		}else if (node instanceof IRSelfDec) {
			visit((IRSelfDec)node);
		}else if (node instanceof IRIncSelf) {
			visit((IRIncSelf)node);
		}else if (node instanceof IRDecSelf) {
			visit((IRDecSelf)node);
		}
		*/
	}

	@Override
	public void visit(IRUnaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRInot node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNeg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBinaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAdd node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBxor node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCmp node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRDiv node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IREq node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRMul node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRRem node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRShl node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSub node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRTernaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCastOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCvt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCall node) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(IRLeaf node) {
		if(node instanceof IRConstval) {
			visit((IRConstval)node);
		}else if (node instanceof IRSizeoftype) {
			visit((IRSizeoftype)node);
		}else if (node instanceof IRAddrof) {
			visit((IRAddrof)node);
		}
	}

	@Override
	public void visit(IRConstval node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSizeoftype node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAddrof node) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(IRIread node) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(IRDassign node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIassign node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(VirtualReg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIassignfpoff node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIreadfpoff node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIassignoff node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIreadoff node) {
		// TODO Auto-generated method stub
		
	}
	
	
}
