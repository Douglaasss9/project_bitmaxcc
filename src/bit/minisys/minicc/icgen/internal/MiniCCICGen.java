package bit.minisys.minicc.icgen.internal;

import java.io.File;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.icgen.IMiniCCICGen;
import bit.minisys.minicc.internal.ir.IRDeclaration;
import bit.minisys.minicc.internal.ir.IRFile;
import bit.minisys.minicc.internal.ir.IRFunctionDeclaration;
import bit.minisys.minicc.internal.ir.IRInstruction;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.parser.ast.ASTCompilationUnit;
import bit.minisys.minicc.semantic.internal.ErrorMessage;
import bit.minisys.minicc.semantic.internal.SymbolTableBuilder;


public class MiniCCICGen implements IMiniCCICGen{
	private static  IRBuilder irBuilder = null;
	public static IRBuilder getIrBuilder() {
		return irBuilder;
	}
	@Override
	public String run(String iFile) throws Exception {
		
		String oFile = MiniCCUtil.remove2Ext(iFile) + MiniCCCfg.MINICC_ICGEN_OUTPUT_EXT;
		
		// 1. ����json��ʽ��AST������AST��
		//	  ��ֱ�Ӵӿ���﷨�����׶εõ�AST
		ObjectMapper mapper = new ObjectMapper();
		ASTCompilationUnit program = (ASTCompilationUnit)mapper.readValue(new File(iFile), ASTCompilationUnit.class);
	    
		GlobalSymbolTable globalSym = new GlobalSymbolTable();
		ErrorMessage errors = new ErrorMessage();
		SymbolTableBuilder symbolTableBuilder = new SymbolTableBuilder(globalSym,errors);
		program.accept(symbolTableBuilder);
		
		// 2. ����AST�õ�High Level MapleIR
		irBuilder = new IRBuilder(globalSym);
		program.accept(irBuilder);
		IRFile irfile = irBuilder.getIrFile();

		// 3. ����High Level MapleIR���õ��ײ�����IR (���ֻ����飬����CFG)
		for (IRDeclaration decl : irfile.globalSymTab.localSymTab) {
			if(decl instanceof IRFunctionDeclaration) {
				Map<String, IRInstruction> labelMap = ((IRFunctionDeclaration)decl).getLabelMap();
				LIRBuilder lirBuilder = new LIRBuilder(labelMap);
				((IRFunctionDeclaration)decl).accept(lirBuilder);
			}
		}
		
		// ������������ı�ir
		IRPrinter irPrinter = new IRPrinter();
		irPrinter.setGlobalVarPropMap(irBuilder.getGlobalVarPropMap());
		irPrinter.setConstStrMap(irBuilder.getConstStrMap());
		irfile.globalSymTab.accept(irPrinter);
		irPrinter.write(oFile);
		
		System.out.println("5. Icgen finished!");
		return oFile;
	}


}
