package bit.minisys.minicc.icgen.internal;

import bit.minisys.minicc.internal.symbol.VariableType;

public class ParamInfo {
	public Integer id;	// 第几个参数
	public VariableType type;	// 目前只处理了int一种情况
	public ParamInfo(Integer id,VariableType type) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.type = type;
	}
}
