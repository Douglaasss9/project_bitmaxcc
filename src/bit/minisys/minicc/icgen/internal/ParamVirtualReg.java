package bit.minisys.minicc.icgen.internal;

import bit.minisys.minicc.internal.ir.VirtualReg;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;

public class ParamVirtualReg extends VirtualReg{

	private Integer paramId;
	private Integer paramNum;
	public ParamVirtualReg(Integer paramId,Integer paramNum) {
		this.paramId = paramId;
		this.paramNum = paramNum;
		super.setType(GlobalSymbolTable.intType);
	}
	public Integer getParamNum() {
		return paramNum;
	}
	public Integer getParamId() {
		return paramId;
	}
}
