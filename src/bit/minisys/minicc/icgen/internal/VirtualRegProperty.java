package bit.minisys.minicc.icgen.internal;

import bit.minisys.minicc.internal.ir.VirtualReg;

// 记录每个伪寄存器对应的属性
public class VirtualRegProperty {
	private Integer offset;		// 在对应存储位置(ram或者Stack)上的offset
	private VirtualReg offsetReg;	// 对于不确定具体offset数值的数组元素，用来表示其offset表达式的伪寄存器
	private String VarName;		// 全局变量根据名字获取地址
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public void setOffsetReg(VirtualReg offsetReg) {
		this.offsetReg = offsetReg;
	}
	public Integer getOffset() {
		return offset;
	}
	public VirtualReg getOffsetReg() {
		return offsetReg;
	}
	public void setVarName(String varName) {
		VarName = varName;
	}
	public String getVarName() {
		return VarName;
	}
	public boolean addrIsSure() {
		if(offset != null) {
			return true;
		}
		return false;
	}
}
