package bit.minisys.minicc.internal.ir;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.icgen.internal.IROpVisitor;


public class BasicBlock {
	private IRInstruction first;
	private IRInstruction end;
	private IRDeclaration fatherFunc;
	
	private Integer bbiD;
	
	public List<BasicBlock> prevs = new LinkedList<BasicBlock>();
	public List<BasicBlock> succs = new LinkedList<BasicBlock>();
	
	// for liveness String:tmpReg's name 
	private Set<VirtualReg> use ;
	private Set<VirtualReg> def ;
	private Set<VirtualReg> liveIn = new HashSet<VirtualReg>();
	private Set<VirtualReg> liveOut = new HashSet<VirtualReg>();	
	
	public Set<VirtualReg> getLiveIn() {
		return liveIn;
	}
	public Set<VirtualReg> getLiveOut() {
		return liveOut;
	}
	public Set<VirtualReg> getUse() {
		return use;
	}
	public Set<VirtualReg> getDef() {
		return def;
	}
	public void setLiveIn(Set<VirtualReg> liveIn) {
		this.liveIn = liveIn;
	}
	public void setLiveOut(Set<VirtualReg> liveOut) {
		this.liveOut = liveOut;
	}
	
	public void addSucc(BasicBlock bb) {
		succs.add(bb);
		bb.prevs.add(this);
	}
	
	public boolean isExit() {
		if(first == null)
			return true;
		return false;
	}
	public Integer getBbiD() {
		return bbiD;
	}
	// for liveness 
	
	public BasicBlock(Integer bbiD) {
		this.bbiD = bbiD;
	}
	public BasicBlock(IRInstruction first,IRInstruction end,IRDeclaration fatherFunc) {
		this.first = first;
		this.end = end;
		this.fatherFunc = fatherFunc;
	}
	public void setEnd(IRInstruction end) {
		this.end = end;
	}
	public void setFirst(IRInstruction first) {
		this.first = first;
	}
	public void setFatherFunc(IRDeclaration fatherFunc) {
		this.fatherFunc = fatherFunc;
	}
	public IRInstruction getEnd() {
		return end;
	}
	public IRDeclaration getFatherFunc() {
		return fatherFunc;
	}
	public IRInstruction getFirst() {
		return first;
	}
	
	// 初始化use(uevar)和def(varkill)集合
	public void init() {
		use = new HashSet<VirtualReg>();
		def = new HashSet<VirtualReg>();
		if(first == null) {
			return;
		}
		IRInstruction start = first;
		
		while(start != null && start.getBb() == this) {
			IROpVisitor irOpVisitor = new IROpVisitor();
			irOpVisitor.visit(start);

			// x = y op z;
			VirtualReg x = irOpVisitor.getRes();
			VirtualReg y = irOpVisitor.getOp1();
			VirtualReg z = irOpVisitor.getOp2();
			LinkedList<VirtualReg> args = irOpVisitor.getCallArgs();
			if(y != null) {
				if(!def.contains(y)) {
					use.add(y);
				}
			}
			if(z != null) {
				if(!def.contains(z)) {
					use.add(z);
				}
			}
			if (args!=null && args.size() >0) {
				for (VirtualReg arg : args) {
					if (!def.contains(arg)) {
						use.add(arg);
					}
				}
			}
			if (x != null) {
				def.add(x);
			}
			start = start.getNext();
		}
	}
	// 更新out和in
	public boolean updateOutAndIn() {
		boolean flag = updateOut();
		//flag = 
				updateIn();
		//if(!flag) updateOut();
		return flag;
	}
	// 更新out
	public boolean updateOut() {
		boolean changed = false;
		Set<VirtualReg> newLiveOut = new HashSet<VirtualReg>();
		for (BasicBlock succ : succs) {
			Set<VirtualReg> succIns = succ.getLiveIn();
			for (VirtualReg virtualReg : succIns) {
				if(!liveOut.contains(virtualReg)) {
					//System.out.println("bbid"+this.bbiD+virtualReg.getName());
					changed = true;
				}
				if(!newLiveOut.contains(virtualReg)) {
					newLiveOut.add(virtualReg);
				}
			}
		}
		if(newLiveOut.size() != liveOut.size()) {
			changed = true;
		}
		if(changed) {
			//liveOut.clear();
			//liveOut.addAll(newLiveOut);
			//System.out.println("111"+newLiveOut.size());
			this.liveOut = newLiveOut;
		}
		return changed;
	}
	// 更新in
	private boolean updateIn() {
		Set<VirtualReg> newLiveIn = new HashSet<VirtualReg>();
		newLiveIn.addAll(liveOut);
		boolean changed = false;
		for (VirtualReg virtualReg : def) {
			if(newLiveIn.contains(virtualReg)) {
				newLiveIn.remove(virtualReg);
			}
		}
		for (VirtualReg virtualReg : use) {
			if(!newLiveIn.contains(virtualReg)) {
				newLiveIn.add(virtualReg);
			}
		}
		if(liveIn.size() != newLiveIn.size()) {
			changed = true;
		}else {
			for (VirtualReg virtualReg : liveIn) {
				if(!newLiveIn.contains(virtualReg)) {
					changed = true;
				}
			}
		}
		liveIn = newLiveIn;
		return changed;
		
	}
	// 打印
	public void print() throws JsonProcessingException {
		System.out.println(" -------------- BasicBlock"+this.bbiD+"------------------ ");
		if(first == null) {
			System.out.println("END BasicBlock");
		}
		IRInstruction next = first;
		ObjectMapper mapper = new ObjectMapper();
		while(next != end) {
			System.out.println(mapper.writeValueAsString(next));
			next = next.getNext();
		}
		if(next == null) {
			System.out.println(" -------------------------------------------- \n");
			return;
		}

		System.out.println(mapper.writeValueAsString(next));
		if(next instanceof IRBrfalse) {
			System.out.println("(false->)"+((IRBrfalse)next).getFalseNext().getBb().bbiD);
		}else if (next instanceof IRBrtrue) {
			System.out.println("(true->)"+((IRBrtrue)next).getTrueNext().getBb().bbiD);
		}else if (next instanceof IRGotoStatement) {
			System.out.println("(goto->)"+((IRGotoStatement)next).getAbsNext().getBb().bbiD);
		}else if(next instanceof IRReturnStatement) {
			System.out.println("(return->)"+((IRReturnStatement)next).getEndBlock().bbiD);
		}
		System.out.println(" -------------------------------------------- \n");
		if(next.getNext() != null) {
			BasicBlock nextBB = next.getNext().getBb();
			if(nextBB!=null)
				nextBB.print();
		}
		if(next instanceof IRReturnStatement) {
			((IRReturnStatement)next).getEndBlock().print();
		}
	}
}
