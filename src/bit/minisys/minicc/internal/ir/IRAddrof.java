package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.NormalType;


@JsonTypeName("addrof")
public class IRAddrof extends IRLeaf{
	@JsonIgnore
	public NormalType resType;
	public String varName;
	@JsonIgnore
	public Integer fieldId;
	
	public IRAddrof(NormalType resType,String varName,Integer fieldId) {
		this.resType = resType;
		this.varName = varName;
		this.fieldId = fieldId;
	}
	
	public IRAddrof(NormalType resType,String varName) {
		this.resType = resType;
		this.varName = varName;
		this.fieldId = 0;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
