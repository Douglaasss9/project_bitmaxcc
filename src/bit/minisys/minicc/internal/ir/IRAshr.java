package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;


@JsonTypeName("ashr")
public class IRAshr extends IRBinaryOperator{

	public IRAshr(IRExpression opnd0, IRExpression opnd1, Type resType) {
		super(opnd0, opnd1, resType);
	}

	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}

}
