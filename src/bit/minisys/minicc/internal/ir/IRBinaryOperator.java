package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRAdd.class,name = "add"),
	@JsonSubTypes.Type(value = IRAshr.class,name = "ashr"),
	@JsonSubTypes.Type(value = IRBand.class,name = "band"),
	@JsonSubTypes.Type(value = IRBior.class,name="bior"),
	@JsonSubTypes.Type(value = IRBxor.class,name = "bxor"),
	@JsonSubTypes.Type(value = IRCand.class,name = "cand"),
	@JsonSubTypes.Type(value = IRCior.class,name = "cior"),
	@JsonSubTypes.Type(value = IRCmp.class,name = "cmp"),
	@JsonSubTypes.Type(value = IRDiv.class,name = "div"),
	@JsonSubTypes.Type(value = IREq.class,name = "eq"),
	@JsonSubTypes.Type(value = IRGe.class,name = "ge"),
	@JsonSubTypes.Type(value = IRGt.class,name = "gt"),
	@JsonSubTypes.Type(value = IRLand.class,name = "land"),
	@JsonSubTypes.Type(value = IRLior.class,name = "lior"),
	@JsonSubTypes.Type(value = IRLe.class,name = "le"),
	@JsonSubTypes.Type(value = IRLshr.class,name = "lshr"),
	@JsonSubTypes.Type(value = IRLt.class,name = "lt"),
	@JsonSubTypes.Type(value = IRMul.class,name = "mul"),
	@JsonSubTypes.Type(value = IRNe.class,name = "ne"),
	@JsonSubTypes.Type(value = IRRem.class,name = "rem"),
	@JsonSubTypes.Type(value = IRShl.class,name = "shl"),
	@JsonSubTypes.Type(value = IRSub.class,name = "sub"),
	
})
@JsonTypeName("binaryOperator")
public class IRBinaryOperator extends IRExpression{
	public IRExpression opnd0;
	public IRExpression opnd1;
	@JsonIgnore
	public Type resType;
	
	public IRBinaryOperator(IRExpression opnd0,IRExpression opnd1,Type resType) {
		this.opnd0 = opnd0;
		this.opnd1 = opnd1;
		this.resType = resType;
	}
	
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
