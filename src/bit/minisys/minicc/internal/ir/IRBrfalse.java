package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeName("brfalse")
public class IRBrfalse extends IRStatement{
	public String label;
	public IRExpression opnd0;
	
	@JsonIgnore
	private IRInstruction trueNext;
	@JsonIgnore
	private IRInstruction falseNext;
	
	public IRBrfalse() {
		// TODO Auto-generated constructor stub
	}
	
	public IRBrfalse(String label,IRExpression opnd0) {
		this.label = label;
		this.opnd0 = opnd0;
	}
	
	@JsonIgnore
	public void setTrueNext(IRInstruction trueNext) {
		this.trueNext = trueNext;
	}
	@JsonIgnore
	public void setFalseNext(IRInstruction falseNext) {
		this.falseNext = falseNext;
	}
	
	@JsonIgnore
	public IRInstruction getTrueNext() {
		return trueNext;
	}
	@JsonIgnore
	public IRInstruction getFalseNext() {
		return falseNext;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}

}
