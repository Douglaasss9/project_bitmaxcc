package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
@JsonTypeName("brtrue")
public class IRBrtrue extends IRStatement {
	public String label;
	public IRExpression opnd0;
	
	@JsonIgnore
	private IRInstruction trueNext;
	@JsonIgnore
	private IRInstruction falseNext;
	
	public IRBrtrue() {
		// TODO Auto-generated constructor stub
	}
	
	
	@JsonIgnore
	public void setTrueNext(IRInstruction trueNext) {
		this.trueNext = trueNext;
	}
	@JsonIgnore
	public void setFalseNext(IRInstruction falseNext) {
		this.falseNext = falseNext;
	}
	
	@JsonIgnore
	public IRInstruction getTrueNext() {
		return trueNext;
	}
	@JsonIgnore
	public IRInstruction getFalseNext() {
		return falseNext;
	}
	
	public IRBrtrue(String label,IRExpression opnd0) {
		this.label = label;
		this.opnd0 = opnd0;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
