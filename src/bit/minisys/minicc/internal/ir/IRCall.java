package bit.minisys.minicc.internal.ir;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeName("call")
public class IRCall extends IRExpression{
	public String funcname;
	public List<IRExpression> args;
	
	public IRCall(String funcname,List<IRExpression> args) {
		this.funcname = funcname;
		this.args = args;
	}
	
	public IRCall(String funcname,IRExpression arg) {
		this.funcname = funcname;
		this.args = new LinkedList<IRExpression>();
		this.args.add(arg);
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
