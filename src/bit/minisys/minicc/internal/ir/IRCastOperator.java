package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRCvt.class,name = "cvt"),
})
@JsonTypeName("castOperator")
public class IRCastOperator extends IRExpression{

	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
