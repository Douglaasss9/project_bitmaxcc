package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.NormalType;

@JsonTypeName("constval")
public class IRConstval extends IRLeaf{
	@JsonIgnore
	public NormalType type;
	public Object value;
	
	public IRConstval(NormalType type,Object value) {
		this.type = type;
		this.value = value;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
	
	public boolean isStr() {
		if (value instanceof String) {
			return true;
		}
		return false;
	}
}
