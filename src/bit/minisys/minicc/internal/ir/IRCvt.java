package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("cvt")
public class IRCvt extends IRCastOperator{
	public Type fromType;
	public Type toType;
	public IRInstruction opnd0;
	
	public IRCvt(Type fromType,Type toType,IRInstruction opnd0) {
		this.fromType = fromType;
		this.toType = toType;
		this.opnd0 = opnd0;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
