package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
@JsonTypeName("dassign")
public class IRDassign extends IRExpression{
	//public String varName;
	public IRExpression varName;
	public Integer fieldId;
	public IRExpression rhsExpr;
	
	public IRDassign() {
		
	}
	
	public IRDassign(IRExpression varName, IRExpression rhsExpr) {
		this.varName = varName;
		this.fieldId = 0;
		this.rhsExpr = rhsExpr;
	}
	
	public IRDassign(IRExpression varName,Integer fieldId, IRExpression rhsExpr) {
		this.varName = varName;
		this.fieldId = fieldId;
		this.rhsExpr = rhsExpr;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
