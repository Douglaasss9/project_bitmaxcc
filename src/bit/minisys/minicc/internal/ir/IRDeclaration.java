package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRFunctionDeclaration.class,name = "functionDeclaration"),
	@JsonSubTypes.Type(value = IRVariableDeclaration.class,name = "variableDeclaration"),
	
})
@JsonTypeName("declaration")
public abstract class IRDeclaration{
	@JsonIgnore
	public abstract String returnName();
	@JsonIgnore
	public abstract void accept(IRVisitor visitor);
	//@JsonIgnore
	//public abstract LinkedList<IRExpression> initToIRExpression();
	
}
