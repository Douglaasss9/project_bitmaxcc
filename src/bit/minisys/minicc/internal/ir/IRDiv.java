package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;
@JsonTypeName("div")
public class IRDiv extends IRBinaryOperator{

	public IRDiv(IRExpression opnd0, IRExpression opnd1, Type resType) {
		super(opnd0, opnd1, resType);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
