package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRBinaryOperator.class,name = "binaryOperator"),
	@JsonSubTypes.Type(value = IRUnaryOperator.class,name = "unaryOperator"),
	@JsonSubTypes.Type(value = IRTernaryOperator.class,name = "ternaryOperator"),
	@JsonSubTypes.Type(value = IRCall.class,name = "call"),
	@JsonSubTypes.Type(value = IRCastOperator.class,name = "castOperator"),
	@JsonSubTypes.Type(value = IRLeaf.class,name="leaf"),
	@JsonSubTypes.Type(value = IRIread.class,name="iread"),
	@JsonSubTypes.Type(value = IRIreadfpoff.class,name="ireadfpoff"),
	@JsonSubTypes.Type(value = IRIreadoff.class,name="ireadoff"),
	@JsonSubTypes.Type(value = IRDassign.class,name="dassign"),
	@JsonSubTypes.Type(value = IRIassign.class,name="iassign"),
	@JsonSubTypes.Type(value = IRIassignoff.class,name="iassignoff"),
	@JsonSubTypes.Type(value = IRIassignfpoff.class,name="iassignfpoff"),
	
})
@JsonTypeName("expression")
public class IRExpression extends IRInstruction{
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
