package bit.minisys.minicc.internal.ir;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnore;
import bit.minisys.minicc.internal.symbol.SymbolTable;

public class IRFile {
	
	// 属性
	
	// 符号表
	public IRScope globalSymTab;
	@JsonIgnore
	private Map<String, String> globalStringConstMap;	// 用于记录输出的符号串在.data中的名字
	@JsonIgnore
	private Integer strConstId = 0;
	public String addStringConst(String printStr) {
		if (globalStringConstMap == null) {
			globalStringConstMap = new HashMap<String, String>();
		}
		if (!globalStringConstMap.containsKey(printStr)) {
			globalStringConstMap.put(printStr, "_"+ ++strConstId + "sc");
		}
		return globalStringConstMap.get(printStr);
	}
	public Map<String, String> getGlobalStringConstMap() {
		if (globalStringConstMap == null) {
			globalStringConstMap = new HashMap<String, String>();
		}
		return globalStringConstMap;
	}
	public IRFile(){
		//globalSymTab = new LinkedList<IRDeclaration>();
		globalSymTab = new IRScope(null);
	}
	
	// 设置全局符号表
	public void setGlobalSymTab(SymbolTable symTab) {
		globalSymTab.localSymTab = symTab.changeToIR();
	}
	
	
	// 设置函数定义的body部分
	public void setFuncBody(String name,IRScope irScope) {
		for (IRDeclaration irDeclaration : globalSymTab.localSymTab) {
			if(irDeclaration instanceof IRFunctionDeclaration) {
				if (name.equals(((IRFunctionDeclaration) irDeclaration).funcname)) {
					((IRFunctionDeclaration) irDeclaration).setBody(irScope);
				}
			}
		}
	}
	
	public IRFunctionDeclaration getFuncDecl(String funcname) {
		for (IRDeclaration irDeclaration : globalSymTab.localSymTab) {
			if(irDeclaration instanceof IRFunctionDeclaration) {
				if (funcname.equals(((IRFunctionDeclaration) irDeclaration).funcname)) {
					return (IRFunctionDeclaration)irDeclaration;
				}
			}
		}
		return null;
	}
	
	public IRScope getGlobalSymTab() {
		return globalSymTab;
	}
	public void print() {
		System.out.println("-------------------------IRFILE------------------------");
		//IRPrinter irPrinter=new IRPrinter(System.out);
		//globalSymTab.accept(irPrinter);
		System.out.println("-------------------------------------------------------");
	}
}
