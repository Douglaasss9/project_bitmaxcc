package bit.minisys.minicc.internal.ir;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.icgen.internal.ParamInfo;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.symbol.FunctionType;
import bit.minisys.minicc.internal.symbol.NormalType;
import bit.minisys.minicc.internal.symbol.VariableType;


@JsonTypeName("functionDeclaration")
public class IRFunctionDeclaration extends IRDeclaration{
	public String funcname;						// 函数名
	public NormalType returnType;				// 函数返回值类型
	public List<IRVariableDeclaration> argsSymTab;	// 函数参数声明
	public IRScope body;						// 函数体

	// for reg alloc
	@JsonIgnore
	Map<VirtualReg, VirtualRegProperty> localVarPropMap;		// 函数体内部局部变量(vReg)对应的栈空间记录
	@JsonIgnore
	Integer farOffset = 0;										// 函数体内部最大的栈空间使用量
	@JsonIgnore
	Map<VirtualReg, ParamInfo> paramsInfo;						// 函数参数信息
	@JsonIgnore
	Map<String, IRInstruction> labelMap;						// 函数体内部全部label对应的IRinstruction
	
	public void setParamsInfo(Map<VirtualReg, ParamInfo> paramsInfo) {
		this.paramsInfo = paramsInfo;
	}
	public void setLocalVarPropMap(Map<VirtualReg, VirtualRegProperty> localVarPropMap) {
		this.localVarPropMap = localVarPropMap;
	}
	public void setFarOffset(Integer farOffset) {
		this.farOffset = farOffset;
	}
	public void setLabelMap(Map<String, IRInstruction> labelMap) {
		this.labelMap = labelMap;
	}
	public Map<String, IRInstruction> getLabelMap() {
		return labelMap;
	}
	public Map<VirtualReg, VirtualRegProperty> getLocalVarPropMap() {
		return localVarPropMap;
	}
	public Map<VirtualReg, ParamInfo> getParamsInfo() {
		return paramsInfo;
	}
	public Integer getFarOffset() {
		return farOffset;
	}

	// for cfg
	@JsonIgnore
	private BasicBlock startBB;
	@JsonIgnore
	public void setStartBB(BasicBlock startBB) {
		this.startBB = startBB;
	}
	@JsonIgnore
	public BasicBlock getStartBB() {
		return startBB;
	}
	
	public IRFunctionDeclaration() {

	}
	public NormalType getReturnType() {
		return returnType;
	}
	public IRFunctionDeclaration(String funcname) {
		this.funcname = funcname;
	}
	public void setBody(IRScope body) {
		this.body = body;
	}
	@JsonIgnore
	public void setArgs(FunctionType type) {
		if(type == null) return;
		// args是建立符号表时已经设置好的参数对应的scope
		argsSymTab = new LinkedList<IRVariableDeclaration>();
		for(int i = 0; i<type.paramsType.size(); i++) {
			VariableType argType = type.paramsType.get(i);
			String argName = type.paramsNames.get(i);
			if(argName == "") {
				argName = ""+UUID.randomUUID().toString().substring(24);
			}
			// 这个参数表 作用是？
			argsSymTab.add(new IRVariableDeclaration(argName, argType,null));
		}
	}
	
	public void setReturnType(NormalType returnType2) {
		this.returnType = returnType2;
	}

	@Override
	public String returnName() {
		return funcname;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
