package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeName("goto")
public class IRGotoStatement extends IRStatement{
	public String label;
	
	@JsonIgnore
	private IRInstruction absNext;
	
	@JsonIgnore
	public void setAbsNext(IRInstruction absNext) {
		this.absNext = absNext;
	}
	@JsonIgnore
	public IRInstruction getAbsNext() {
		return absNext;
	}
	public IRGotoStatement(String label) {
		this.label = label;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
