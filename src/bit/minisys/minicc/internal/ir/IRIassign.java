package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("iassign")
public class IRIassign extends IRExpression{
	@JsonIgnore
	public Type type;
	@JsonIgnore
	public Integer fieldId;
	public IRExpression addrExpr;
	public IRExpression rhsExpr;

	public IRIassign() {
		// TODO Auto-generated constructor stub
	}
	public IRIassign(Type type,IRExpression addrExpr,IRExpression rhsExpr) {
		this.type = type;
		this.fieldId = 0;
		this.addrExpr = addrExpr;
		this.rhsExpr = rhsExpr;
	}
	
	public IRIassign(Type type,Integer fieldId, IRExpression addrExpr,IRExpression rhsExpr) {
		this.type = type;
		this.fieldId = fieldId;
		this.addrExpr = addrExpr;
		this.rhsExpr = rhsExpr;
	}
}
