package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("iassignfpoff")
public class IRIassignfpoff extends IRExpression{
	@JsonIgnore
	public Type type;
	public IRExpression offsetExpr;
	public IRExpression rhsExpr;

	public IRIassignfpoff(Type type, IRExpression offsetExpr,IRExpression rhsExpr) {
		this.type = type;
		this.offsetExpr = offsetExpr;
		this.rhsExpr = rhsExpr;
	}
}
