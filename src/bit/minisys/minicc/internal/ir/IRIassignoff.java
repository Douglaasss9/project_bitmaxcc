package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.internal.symbol.Type;
@JsonTypeName("iassignoff")
public class IRIassignoff extends IRExpression{
	@JsonIgnore
	public Type type;
	public IRExpression offsetExpr;
	public IRExpression addrExpr;
	public IRExpression rhsExpr;
	
	public IRIassignoff(Type type, IRExpression offsetExpr,IRExpression addrExpr,IRExpression rhsExpr) {
		this.type = type;
		this.offsetExpr = offsetExpr;
		this.addrExpr = addrExpr;
		this.rhsExpr = rhsExpr;
	}
}
