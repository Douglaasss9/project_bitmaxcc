package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("inot")
public class IRInot extends IRUnaryOperator{
	public Type type;
	public IRInot(IRExpression opnd0) {
		super(opnd0);
	}

	public IRInot(IRExpression opnd0,Type type) {
		super(opnd0);
		this.type = type;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
