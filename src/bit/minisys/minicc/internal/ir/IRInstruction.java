package bit.minisys.minicc.internal.ir;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import bit.minisys.minicc.icgen.internal.IRVisitor;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRStatement.class,name = "statement"),

	@JsonSubTypes.Type(value = IRExpression.class,name = "expression"),
})
public abstract class IRInstruction {
	@JsonIgnore
	public IRScope scope;
	@JsonIgnore
	private IRInstruction prev = null;
	@JsonIgnore
	private IRInstruction next = null;
	@JsonIgnore
	private BasicBlock bb;
	@JsonIgnore
	private Set<VirtualReg> liveIn;
	@JsonIgnore
	private Set<VirtualReg> liveOut;
	public Set<VirtualReg> getLiveIn() {
		return liveIn;
	}
	public Set<VirtualReg> getLiveOut() {
		return liveOut;
	}
	public void setLiveIn(Set<VirtualReg> liveIn) {
		this.liveIn = liveIn;
	}
	public void setLiveOut(Set<VirtualReg> liveOut) {
		this.liveOut = liveOut;
	}
	
	private Integer instrId;
	
	public void setInstrId(Integer instrId) {
		this.instrId = instrId;
	}
	public Integer getInstrId() {
		return instrId;
	}
	
	public void setBb(BasicBlock bb) {
		this.bb = bb;
	}
	public void setNext(IRInstruction next) {
		this.next = next;
	}
	public void setPrev(IRInstruction prev) {
		this.prev = prev;
	}
	public BasicBlock getBb() {
		return bb;
	}
	public IRInstruction getNext() {
		return next;
	}
	public IRInstruction getPrev() {
		return prev;
	}
	
	
	public void _prev(IRInstruction prev) {
		this.prev = prev;
		if(prev instanceof IRBrfalse) {
			((IRBrfalse)prev).setTrueNext(this);
		}else if(prev instanceof IRBrtrue) {
			((IRBrtrue)prev).setFalseNext(this);
		}
		
		prev.next = this;
		
	}
	
	public boolean isJump() {
		if(this instanceof IRGotoStatement || this instanceof IRBrfalse || this instanceof IRBrtrue || this instanceof IRReturnStatement) {
			return true;
		}
		return false;
	}
	public abstract void accept(IRVisitor visitor);
	
}
