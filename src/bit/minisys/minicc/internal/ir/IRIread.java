package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("iread")
public class IRIread extends IRExpression{
	@JsonIgnore
	public Type primType;
	@JsonIgnore
	public Type type;
	@JsonIgnore
	public Integer fieldId;
	public IRExpression addrExpr;
	
	public IRIread() {
		// TODO Auto-generated constructor stub
	}
	
	public IRIread(Type primType,Type type ,IRExpression addrExpr) {
		this.primType = primType;
		this.type = type;
		this.addrExpr = addrExpr;
		this.fieldId = 0;
		
	}
	
	public IRIread(Type primType,Type type ,Integer fieldId,IRExpression addrExpr) {
		this.primType = primType;
		this.type = type;
		this.addrExpr = addrExpr;
		this.fieldId = fieldId;
		
	}
	
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
