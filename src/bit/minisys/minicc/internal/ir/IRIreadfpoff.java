package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("ireadfpoff")
public class IRIreadfpoff extends IRExpression{
	public Type primType;
	public IRExpression offsetExpr;
	
	public IRIreadfpoff() {
		// TODO Auto-generated constructor stub
	}
	
	public IRIreadfpoff(Type primType,IRExpression offsetExpr) {
		this.primType = primType;
		this.offsetExpr = offsetExpr;		
	}
}
