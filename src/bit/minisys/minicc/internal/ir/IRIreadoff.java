package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("ireadoff")
public class IRIreadoff extends IRExpression{
	public Type primType;
	public IRExpression addrExpr;
	public IRExpression offsetExpr;
	
	public IRIreadoff() {
		// TODO Auto-generated constructor stub
	}
	
	public IRIreadoff(Type primType,IRExpression addrExpr,IRExpression offsetExpr) {
		this.primType = primType;
		this.addrExpr = addrExpr;
		this.offsetExpr = offsetExpr;		
	}
}
