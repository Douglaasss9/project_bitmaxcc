package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeName("label")
public class IRLabel extends IRStatement{
	
	public String label;
	public IRLabel(String label) {
		this.label = "@"+label;
	}
	public IRLabel(Integer label) {
		this.label = "@"+label;
	}
	
	public String getLabel() {
		return label;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
