package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRConstval.class,name = "constval"),
	@JsonSubTypes.Type(value = IRSizeoftype.class,name = "sizeoftype"),
	@JsonSubTypes.Type(value = IRAddrof.class,name = "addrof"),
	@JsonSubTypes.Type(value = VirtualReg.class,name = "virtualreg"),
})
@JsonTypeName("leaf")
public abstract class IRLeaf extends IRExpression{

}
