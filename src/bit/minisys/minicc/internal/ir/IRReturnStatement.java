package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;


@JsonTypeName("return")
public class IRReturnStatement extends IRStatement{
	
	// Maple IR中多返回，C只支持单返回 所以此处不用列表
	public IRExpression opnd;
	@JsonIgnore
	private BasicBlock endBlock;
	
	public void setEndBlock(BasicBlock endBlock) {
		this.endBlock = endBlock;
	}
	public BasicBlock getEndBlock() {
		return endBlock;
	}
	public IRReturnStatement() {
		// TODO Auto-generated constructor stub
	}
	public IRReturnStatement(IRExpression opnd) {
		this.opnd = opnd;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
