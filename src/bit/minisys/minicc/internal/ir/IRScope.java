package bit.minisys.minicc.internal.ir;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;
import bit.minisys.minicc.internal.symbol.SymbolTable;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("scope")
public class IRScope extends IRStatement{
	
	public List<IRDeclaration> localSymTab = new LinkedList<IRDeclaration>();
	public List<IRInstruction> instructions = new LinkedList<IRInstruction>();
	
	@JsonIgnore
	SymbolTable symbolTab;
	@JsonIgnore
	public Integer offset;
	@JsonIgnore
	IRScope fatherScope;
	@JsonIgnore
	boolean isBuilt =false;
	public IRScope() {
		// TODO Auto-generated constructor stub
	}
	
	public IRScope(IRScope father) {
		this.offset = 0;
		this.fatherScope = father;
	}
	
	
	@JsonIgnore
	public void pushInstructions(List<IRInstruction> instructions) {
		this.instructions = instructions;
	}

	@JsonIgnore
	public List<IRInstruction> chaseInstructions() {
		return instructions;
	}
	
	
	@JsonIgnore
	public IRScope getFatherScope() {
		return fatherScope;
	}

	@JsonIgnore
	public void assignLocalSymTab(SymbolTable st) {
		this.localSymTab = st.changeToIR();
		this.symbolTab = st;
		this.offset = st.getNowOffset();
		this.isBuilt = true;
	}
	
	@JsonIgnore
	public boolean symTabBuilt() {
		return isBuilt;
	}
	
	@JsonIgnore
	public void addInstruction(IRInstruction instr) {
		if(instructions == null)
			instructions = new LinkedList<IRInstruction>();
		instructions.add(instr);
	}
	
	@JsonIgnore
	// 为名称对应的declaration添加初始化值
	public void setDeclarationInit(String name,LinkedList<IRExpression> exprs) {
		for (IRDeclaration irdecl : localSymTab) {
			if(irdecl instanceof IRVariableDeclaration && ((IRVariableDeclaration)irdecl).name.equals(name)) {
				((IRVariableDeclaration)irdecl).setInitValues(exprs);
				return;
			}
		}
	}
	@JsonIgnore
	public void setDeclarationInit(String name,IRExpression exprs) {
		for (IRDeclaration irdecl : localSymTab) {
			if(irdecl instanceof IRVariableDeclaration && ((IRVariableDeclaration)irdecl).name.equals(name)) {
				((IRVariableDeclaration)irdecl).assignInitValues(exprs);
				return;
			}
		}
	}
	@JsonIgnore
	public void setVariableReg(String name,VirtualReg vReg) {
		for (IRDeclaration irdecl : localSymTab) {
			if(irdecl instanceof IRVariableDeclaration && ((IRVariableDeclaration)irdecl).name.equals(name)) {
				((IRVariableDeclaration)irdecl).setReg(vReg);
				return;
			}
		}
		if(fatherScope != null) {
			fatherScope.setVariableReg(name, vReg);
		}
	}
	@JsonIgnore
	public VirtualReg getVariableReg(String name) {
		for (IRDeclaration irdecl : localSymTab) {
			if(irdecl instanceof IRVariableDeclaration && ((IRVariableDeclaration)irdecl).name.equals(name)) {
				return ((IRVariableDeclaration)irdecl).getReg();
			}
		}
		if(fatherScope != null) {
			return fatherScope.getVariableReg(name);
		}
		return null;
	}
	@JsonIgnore
	// 从符号表向上逐级查找name对应的Type
	public Type getSymbolType(String name) {
		if(GlobalSymbolTable.isBuiltInFunc(name)) {
			// 如果是BuiltInFunc
			return GlobalSymbolTable.getBuiltFuncType(name).returnType;
		}
		if(localSymTab != null) {
			for (IRDeclaration irdecl : localSymTab) {
				if(irdecl instanceof IRVariableDeclaration && ((IRVariableDeclaration)irdecl).name.equals(name)) {
					return ((IRVariableDeclaration)irdecl).getType();
				}else if (irdecl instanceof IRFunctionDeclaration  && ((IRFunctionDeclaration)irdecl).funcname.equals(name)) {
					return ((IRFunctionDeclaration)irdecl).getReturnType();
				}
			}
		}
		if(fatherScope != null) {
			return fatherScope.getSymbolType(name);
		}
		return null;
	}

	@JsonIgnore
	public Integer getSymbolOffsize(String name) {
		if(localSymTab != null) {
			for (IRDeclaration irdecl : localSymTab) {
				if(irdecl instanceof IRVariableDeclaration && ((IRVariableDeclaration)irdecl).name.equals(name)) {
					return ((IRVariableDeclaration)irdecl).offsize;
				}else if (irdecl instanceof IRFunctionDeclaration  && ((IRFunctionDeclaration)irdecl).funcname.equals(name)) {
					return null;
				}
			}
		}
		if(fatherScope != null) {
			return fatherScope.getSymbolOffsize(name);
		}
		return null;
	}
	
	@JsonIgnore
	public boolean isGlobal(String name) {
		if(localSymTab != null) {
			for (IRDeclaration irdecl : localSymTab) {
				if(irdecl instanceof IRVariableDeclaration && ((IRVariableDeclaration)irdecl).name.equals(name)) {
					if(fatherScope == null) return true;
					return false;
				}
			}
		}
		if(fatherScope != null) {
			return fatherScope.isGlobal(name);
		}
		return false;
	}
	
	@JsonIgnore
	//根据名字返回对应的irdecls
	public List<IRDeclaration> searchDeclsByNames(List<String> name){
		List<IRDeclaration> list = new LinkedList<IRDeclaration>();
		for (String nm : name) {
			for (IRDeclaration irDeclaration : localSymTab) {
				if(irDeclaration.returnName().equals(nm)) {
					list.add(irDeclaration);
					break;
				}
			}
		}
		return list;
	}
	
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
