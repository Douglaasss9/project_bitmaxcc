package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;
import bit.minisys.minicc.internal.symbol.NormalType;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("sizeoftype")
public class IRSizeoftype extends IRLeaf{
	public NormalType resType;
	public Type type;
	
	public IRSizeoftype(NormalType resType,Type type) {
		this.resType = resType;
		this.type = type;
	}
	
	public IRSizeoftype(Type type) {
		this.resType = GlobalSymbolTable.intType;
		this.type = type;
	}
	
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
