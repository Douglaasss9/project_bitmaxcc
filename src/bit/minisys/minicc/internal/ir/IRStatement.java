package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRBrfalse.class,name = "brfalse"),
	@JsonSubTypes.Type(value = IRBrtrue.class,name = "brtrue"),
	@JsonSubTypes.Type(value = IRGotoStatement.class,name = "goto"),
	@JsonSubTypes.Type(value = IRLabel.class,name = "label"),
	@JsonSubTypes.Type(value = IRReturnStatement.class,name = "return"),
	@JsonSubTypes.Type(value = IRScope.class,name = "scope"),
	
})
@JsonTypeName("statement")
public abstract class IRStatement extends IRInstruction{
	@Override
	public abstract void accept(IRVisitor visitor);
	
}
