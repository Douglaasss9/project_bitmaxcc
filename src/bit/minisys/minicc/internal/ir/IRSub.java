package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("sub")
public class IRSub extends IRBinaryOperator{
	public IRSub(IRExpression opnd0,IRExpression opnd1, Type type) {
		super(opnd0,opnd1,type);
	}
	
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
