package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("ternaryOperator")
public class IRTernaryOperator extends IRExpression{

	public Type primType;
	public IRExpression opnd0;
	public IRExpression opnd1;
	public IRExpression opnd2;
	
	public IRTernaryOperator() {
		
	}
	public IRTernaryOperator(Type primType,IRExpression opnd0,IRExpression opnd1,IRExpression opnd2) {
		this.primType = primType;
		this.opnd0 = opnd0;
		this.opnd1 = opnd1;
		this.opnd2 = opnd2;
	}
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
