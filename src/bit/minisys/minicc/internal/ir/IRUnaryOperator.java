package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import bit.minisys.minicc.icgen.internal.IRVisitor;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = As.PROPERTY,property = "IRtype",visible = false)
@JsonSubTypes({
	@JsonSubTypes.Type(value = IRInot.class,name = "inot"),
	@JsonSubTypes.Type(value = IRNeg.class,name = "neg"),
})
@JsonTypeName("unaryOperator")
public class IRUnaryOperator extends IRExpression{
	public IRExpression opnd0;
	public IRUnaryOperator(IRExpression opnd0) {
		this.opnd0 = opnd0;
	}
	
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
