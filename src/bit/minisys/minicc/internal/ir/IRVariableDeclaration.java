package bit.minisys.minicc.internal.ir;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("variableDeclaration")
public class IRVariableDeclaration extends IRDeclaration{
	public String name;
	@JsonIgnore
	public String storageClass;			// to be added 
	public Type type;
	@JsonIgnore
	public List<String> attributes;		// to be added
	
	public Integer offsize;
	public VirtualReg reg;
	
	public void setReg(VirtualReg reg) {
		this.reg = reg;
	}
	public VirtualReg getReg() {
		return reg;
	}
	public List<IRExpression> initValues;	// ��ʼ��ֵ
	public IRVariableDeclaration(String name,Type type,Integer offsize) {
		this.name = name;
		this.type = type;
		this.offsize = offsize;
	}
	
	public Type getType() {
		return type;
	}
	public void setInitValues(List<IRExpression> initValues) {
		this.initValues = initValues;
	}
	
	@JsonIgnore
	public void assignInitValues(IRExpression initValues) {
		this.initValues = new LinkedList<IRExpression>();
		this.initValues.add(initValues);
	}

	@Override
	public String returnName() {
		return name;
	}
	
	@Override
	public void accept(IRVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
