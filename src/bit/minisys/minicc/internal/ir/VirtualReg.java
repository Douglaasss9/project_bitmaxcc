package bit.minisys.minicc.internal.ir;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import bit.minisys.minicc.internal.symbol.NormalType;
import bit.minisys.minicc.internal.symbol.Type;

@JsonTypeName("virtualreg")
public class VirtualReg extends IRLeaf{
	Integer id;
	@JsonIgnore
	Type type;
	public VirtualReg() {
		// TODO Auto-generated constructor stub
	}
	
	public VirtualReg(Integer id,Type type) {
		this.id = id;
		this.type = type;
	}
	public String getName() {
		return "%"+"T."+id;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Type getType() {
		return type;
	}
	public Integer getId() {
		return id;
	}

	
}
