package bit.minisys.minicc.internal.symbol;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import bit.minisys.minicc.parser.ast.ASTExpression;
import bit.minisys.minicc.parser.ast.ASTIntegerConstant;


public class ArrayType extends VariableType{
	public Type elementType;		//元素的类别
	public Integer dimension;		//数组维数
	public List<ASTExpression> dimens = new LinkedList<ASTExpression>();// 每一维的初始化维数（这里暂时只能处理初始化为确定整数的情况，表达式不考虑）
	// 1维
	public ArrayType(Integer dimenson,ASTExpression dimens){
		this.dimens.add(dimens);
		this.dimension = dimenson;
		this.type = BuiltInType.ARRAY;
	}
	// 多维
	public ArrayType(Integer dimenson,List<ASTExpression> dimens,ASTExpression expression){
		this.dimens = dimens;
		this.dimens.add(expression);
		this.dimension = dimenson;
		this.type = BuiltInType.ARRAY;
	}
	public ArrayType(Integer dimenson,List<ASTExpression> dimens){
		this.dimens = dimens;
		this.dimension = dimenson;
		this.type = BuiltInType.ARRAY;
	}
	public ArrayType() {
	
	}
	

	// int a[5][10][15]; getbefore(0) : 5; getBefore(1): {5,10};
	public List<ASTExpression> getBefore(int id){
		if(id < dimension) {
			return dimens.subList(0, id);
		}
		return null;
	}
	
	@Override
	public boolean ifMatchType(Type anotherType) {
		return false;
	}
	
	@Override
	public boolean isSameType(Type anotherType) {
		if(!(anotherType instanceof ArrayType)) {
			return false;
		}
		if(!elementType.isSameType(anotherType)) {
			return false;
		}
		if(dimension !=((ArrayType)anotherType).dimension) {
			return false;
		}
		// 这里没判断exprs是否相同
		return true;
	}
	@Override
	public String toString() {
		return super.toString()+" (Dimen:"+dimension+")"+"\nElementType:"+elementType.toString();
	}

	@Override
	public Type getFirstType(Type anotherType) {
		return null;
	}

	public List<ASTExpression> getDimens() {
		return dimens;
	}
	public Type getElementType() {
		return elementType;
	}
	public Integer getDimenson() {
		return dimension;
	}
	// 设置数组元素的类型
	public void setElementType(Type elementType) {
		this.elementType = elementType;
	}
	// 获取该数组类型占字节大小
	@Override
	public Integer getRegisiterSize() {
		Integer sum = 1;
		Integer eleSize = elementType.getRegisiterSize();
		for(int i = 0; i<dimension; i++) {
			Integer size = ((ASTIntegerConstant)dimens.get(i)).value;
			sum *= size;
		}
		sum *= eleSize;
		return sum;
	}
	
	public Integer getLastRegSize(int id) {
		if(id == dimension) {
			return 0;
		}else {
			Integer sum = 1;
			for(int i = id; i<dimension; i++) {
				Integer size = ((ASTIntegerConstant)dimens.get(i)).value;
				sum *= size;
			}
			return sum;
		}
	}
	
	public boolean checkOutofBound(List<ASTExpression> realNums) {
		List<ASTExpression> reverseRealNums = new LinkedList<ASTExpression>();
		// 添加的时候是逆序的
		for(int i = 0; i<realNums.size(); i++) {
			reverseRealNums.add(realNums.get(realNums.size()-i-1));
		}
		for (int i = 0; i < reverseRealNums.size(); i++) {
			ASTExpression requireNum = dimens.get(i);
			ASTExpression realNum = reverseRealNums.get(i);	
			if (realNum instanceof ASTIntegerConstant && requireNum instanceof ASTIntegerConstant) {
				Integer num1 = ((ASTIntegerConstant)requireNum).value;
				Integer num2 = ((ASTIntegerConstant)realNum).value;
				if (num2 >= 0 && num2 < num1) {
					continue;
				}else {
					return false;
				}
			}
		}
		return true;
	}
}
