package bit.minisys.minicc.internal.symbol;

import java.util.LinkedList;
import java.util.List;

public class FunctionType extends Type{
	public enum BuildInFunctionType{
		INLINE,_NORETURN
	}

	public NormalType returnType;
	public String funcname;
	public List<VariableType> paramsType;
	public List<String> paramsNames;

	// 记录在符号表里的符号类型 函数是被声明的 还是被定义的
	public boolean isDefined = false;
	public boolean isDeclarated = false;
	
	public FunctionType() {
		paramsNames = new LinkedList<String>();
		paramsType = new LinkedList<VariableType>();
		this.type = BuiltInType.FUNCTION;
	}
	public void setReturnType(NormalType returnType) {
		this.returnType = returnType;
	}
	public NormalType getReturnType() {
		return returnType;
	}
	public void setIsDefined(boolean f) {
		this.isDefined = f;
	}
	
	public void setIsDeclarated(boolean f) {
		this.isDeclarated = f;
	}
	public void setFuncname(String funcname) {
		this.funcname = funcname;
	}
	
	public void setParamsNames(List<String> paramsNames) {
		this.paramsNames = paramsNames;
	}
	
	public void setParamsType(List<VariableType> paramsType) {
		this.paramsType = paramsType;
	}
	
	public List<VariableType> getParamsType() {
		return paramsType;
	}
	
	public void addParamType(VariableType type) {
		paramsType.add(type);
	}
	public void addParamName(String name) {
		paramsNames.add(name);
	}
	
	
	@Override
	public boolean ifMatchType(Type anotherType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSameType(Type anotherType) {
		// 1. 同名Symbol的类型是不是函数类型
		if(!anotherType.isFunc()) {
			return false;
		}
		// 1.5 判断函数是不是被定义过
		if(((FunctionType)anotherType).isDefined) {
			return false;
		}
		// 2. 函数返回值类型
		NormalType retType = ((FunctionType)anotherType).returnType;
		if(!retType.isSameType(returnType)) {
			return false;
		}
		// 3. 函数参数个数以及对应的类型是否一致
		List<VariableType> tmpParamsType = ((FunctionType)anotherType).getParamsType();
		if(tmpParamsType.size() != paramsType.size()) {
			return false;
		}
		for(int i = 0; i<paramsType.size(); i++) {
			if(!paramsType.get(i).isSameType(((FunctionType)anotherType).getParamsType().get(i))) {
				return false;
			}
		}
		return true;
		
	}


	@Override
	public Type getFirstType(Type anotherType) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		String paramsString="";
		for(int i = 0; i<paramsNames.size(); i++) {
			paramsString+=paramsNames.get(i)+":"+paramsType.get(i).toString()+"\n";
		}
		return super.toString()+"(FunctionName:"+funcname+")"+"\nReturnType:"+returnType.toString()+"\n"
			+paramsString;
	}
	@Override
	public Integer getRegisiterSize() {
		return 4;
	}
	
}
