package bit.minisys.minicc.internal.symbol;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.python.antlr.PythonParser.if_stmt_return;

public class GlobalSymbolTable {
	public SymbolTable globalSymbolTable;	// 全局符号表
	
	public static NormalType intType = new NormalType(Type.BuiltInType.INT);
	public static NormalType voidType = new NormalType(Type.BuiltInType.VOID);
	public static NormalType charType = new NormalType(Type.BuiltInType.CHAR);
	public static NormalType shortType = new NormalType(Type.BuiltInType.SHORT);
	public static NormalType longType = new NormalType(Type.BuiltInType.LONG);
	public static NormalType unsignedType = new NormalType(Type.BuiltInType.UNSIGNED);
	public static NormalType signedType = new NormalType(Type.BuiltInType.SIGNED);
	public static NormalType floatType = new NormalType(Type.BuiltInType.FLOAT);
	public static NormalType doubleType = new NormalType(Type.BuiltInType.DOUBLE);
	public static NormalType stringType = new NormalType(Type.BuiltInType.STRING);
	
	public static NormalType pointerType = new NormalType(Type.BuiltInType.POINTER);
	public static NormalType unknownType = new NormalType(Type.BuiltInType.UNKNOW);
	//public static NormalType booleanType = new NormalType(Type.BuiltInType.BOOLEAN);
	
	public Map<String, Type> map = new LinkedHashMap<String, Type>();
	public static Map<String,FunctionType> builtInFuncs = new HashMap<String, FunctionType>();
	public GlobalSymbolTable() {
		map.put("int", intType);
		map.put("void", voidType);
		map.put("char", charType);
		map.put("short", shortType);
		map.put("long", longType);
		map.put("signed", signedType);
		map.put("unsigned", unsignedType);
		map.put("float", floatType);
		map.put("double", doubleType);
		map.put("string", stringType);
		globalSymbolTable = new SymbolTable(null);
		
		// 增加builtIn Function
		addBuiltInFunc();

	}
	private void addBuiltInFunc() {
		// void Mars_PrintInt(int):
		FunctionType marsPrintIntType = new FunctionType();
		marsPrintIntType.returnType = voidType;
		marsPrintIntType.setFuncname("Mars_PrintInt");
		marsPrintIntType.setIsDeclarated(true);
		marsPrintIntType.setIsDefined(true);
		marsPrintIntType.addParamName("intValue");
		marsPrintIntType.addParamType(intType);
		builtInFuncs.put("Mars_PrintInt", marsPrintIntType);
		globalSymbolTable.define("Mars_PrintInt", marsPrintIntType);
		// int Mars_GetInt():
		FunctionType marsGetIntType = new FunctionType();
		marsGetIntType.returnType = intType;
		marsGetIntType.setFuncname("Mars_GetInt");
		marsGetIntType.setIsDeclarated(true);
		marsGetIntType.setIsDefined(true);
		builtInFuncs.put("Mars_GetInt", marsGetIntType);
		globalSymbolTable.define("Mars_GetInt", marsGetIntType);
		// void Mars_PrintStr(int addr)
		FunctionType marsPrintStrType = new FunctionType();
		marsPrintStrType.returnType = voidType;
		marsPrintStrType.setFuncname("Mars_PrintStr");
		marsPrintStrType.setIsDeclarated(true);
		marsPrintStrType.setIsDefined(true);
		marsPrintStrType.addParamName("strAddr");
		marsPrintStrType.addParamType(intType);
		builtInFuncs.put("Mars_PrintStr", marsPrintStrType);
		globalSymbolTable.define("Mars_PrintStr", marsPrintStrType);
	}
	
	public static boolean isMarsPrintStr(String name) {
		return name.equals("Mars_PrintStr");
	}
	
	public void defineType(String name,Type type) {
		map.put(name, type);
	}
	
	public static boolean isBuiltInFunc(String name) {
		if(builtInFuncs.containsKey(name)) {
			return true;
		}
		return false;
	}
	public static FunctionType getBuiltFuncType(String name) {
		return builtInFuncs.get(name);
	}
}
