package bit.minisys.minicc.internal.symbol;

import org.python.antlr.PythonParser.return_stmt_return;

public class NormalType extends VariableType{
	
	
	public NormalType(BuiltInType type) { 
		this.type = type;
	}
	
	public void setType(BuiltInType type) {
		this.type = type;
	}
	
	
	@Override
	public boolean ifMatchType(Type anotherType) {
		boolean flag = anotherType.getType()!=BuiltInType.VOID;
		switch(this.type) {
		case VOID:
		case FUNCTION:
		case ARRAY:
		case STRING:
			flag = false;break;
		default:
			BuiltInType t = anotherType.getType();
			flag =(t == BuiltInType.INT || t == BuiltInType.DOUBLE ||
					t == BuiltInType.FLOAT ||t == BuiltInType.LONG ||
					t == BuiltInType.SHORT ||t == BuiltInType.CHAR);
		}
		return flag;
	}
	
	@Override
	public Type getFirstType(Type anotherType) {
		int v1 = this.IntValue();
		int v2 = anotherType.IntValue();
		if(v1>0 && v2>0) {
			if(v1>v2)return this;
			else return anotherType;
		}
		return null;
	}
	
	@Override
	public String toString() {
		return super.toString()+" NormalType";
	}
	
	public boolean isSameType(Type type2) {
		if(!(type2 instanceof NormalType)) {
			return false;
		}
		return this.type ==((NormalType)type2).type;
	}

	@Override
	public Integer getRegisiterSize() {
		switch(this.type) {
		case VOID:return 0;
		case INT:return 4;
		case SHORT:return 2;
		case LONG:return 4;
		case FLOAT:return 4;
		case DOUBLE:return 8;
		case CHAR:return 1;
		case POINTER:return 4;
		default
			:
			System.out.println("?????"+this.type);
			return null;
		
		}
	}
}
