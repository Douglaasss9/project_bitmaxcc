package bit.minisys.minicc.internal.symbol;
public class Symbol {
	public String name;	// 符号的名称
	public Type type;	// 符号的类别及初始化值
	
	public Integer offsize;	
	
	public Symbol(String name,Type type) {
		this.name = name;
		this.type = type;
	}
	
	public Symbol(String name,Type type,Integer offsize) {
		this.name = name;
		this.type = type;
		this.offsize = offsize;
		
	}
	
	public Type getType() {
		return type;
	}
	
	public boolean isVariable() {
		if(type instanceof NormalType) {
			return true;
		}
		return false;
	}
	
	public boolean isArray() {
		if(type instanceof ArrayType) {
			return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return name +" "+ type.toString();
	}
}
