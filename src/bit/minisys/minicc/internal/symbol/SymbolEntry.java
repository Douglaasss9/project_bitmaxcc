package bit.minisys.minicc.internal.symbol;

public class SymbolEntry {
	private SymbolEntryType entryType;
	private String dataType;
	private String name;
	private int reg;
	private int frameOffset;
	
	
	public SymbolEntry(SymbolEntryType t, String n){
		this.setEntryType(t);
		this.setName(n);
		this.frameOffset = 0;
	}
	public SymbolEntry(String name, SymbolEntryType st, String dt){
		this.setEntryType(st);
		this.setName(name);
		this.setDataType(dt);
		this.frameOffset = 0;
	}

	public SymbolEntryType getEntryType() {
		return entryType;
	}

	public void setEntryType(SymbolEntryType entryType) {
		this.entryType = entryType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public int getReg() {
		return reg;
	}

	public void setReg(int reg) {
		this.reg = reg;
	}
	public int getFrameOffset() {
		return frameOffset;
	}
	public void setFrameOffset(int frameOffset) {
		this.frameOffset = frameOffset;
	}
}
