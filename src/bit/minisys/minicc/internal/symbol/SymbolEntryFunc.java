package bit.minisys.minicc.internal.symbol;

import java.util.HashSet;
import java.util.Set;

public class SymbolEntryFunc extends SymbolEntry {
	private	int savedRegOffset = 0;
	private int localVarOffset = 0;
	private int stackFrameSize = 0;
	private int maxArgNum = 0;
	private int localVarNum = 0;
	
	private Set<Integer> regNeedSaved;
	
	
	public SymbolEntryFunc(String name, SymbolEntryType st, String dt) {
		super(name, st, dt);
		setRegNeedSaved(new HashSet<Integer>());
	}
	public int getMaxArgNum() {
		return maxArgNum;
	}
	public void setMaxArgNum(int maxArgSize) {
		this.maxArgNum = maxArgSize;
	}
	public int getLocalVarNum() {
		return localVarNum;
	}
	public void setLocalVarNum(int localVarNum) {
		this.localVarNum = localVarNum;
	}
	public int getStackFrameSize() {
		return stackFrameSize;
	}
	public void setStackFrameSize(int stackFrameSize) {
		this.stackFrameSize = stackFrameSize;
	}
	public int getSavedRegOffset() {
		return savedRegOffset;
	}
	public void setSavedRegOffset(int savedRegOffset) {
		this.savedRegOffset = savedRegOffset;
	}
	public int getLocalVarOffset() {
		return localVarOffset;
	}
	public void setLocalVarOffset(int localVarOffset) {
		this.localVarOffset = localVarOffset;
	}
	public void addSavedReg(int regNum) {
		getRegNeedSaved().add(regNum);
	}
	public Set<Integer> getRegNeedSaved() {
		return regNeedSaved;
	}
	public void setRegNeedSaved(Set<Integer> regNeedSaved) {
		this.regNeedSaved = regNeedSaved;
	}
}
