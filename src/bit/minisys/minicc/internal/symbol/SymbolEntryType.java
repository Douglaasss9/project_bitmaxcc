package bit.minisys.minicc.internal.symbol;

public enum SymbolEntryType {
	SET_SYMBOL_GLOBAL_VAR,
	SET_SYMBOL_LOCAL_VAR,
	SET_SYMBOL_FUNC,
	SET_SYMBOL_PARA,
	SET_TYPE_STRUCT,
	SET_TYPE_ENUM,
	SET_UNKNOWN;
}
