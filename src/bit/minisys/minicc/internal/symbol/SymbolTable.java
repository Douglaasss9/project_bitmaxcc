package bit.minisys.minicc.internal.symbol;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bit.minisys.minicc.internal.ir.IRDeclaration;
import bit.minisys.minicc.internal.ir.IRFunctionDeclaration;
import bit.minisys.minicc.internal.ir.IRVariableDeclaration;
import bit.minisys.minicc.parser.ast.*;

public class SymbolTable {
	public SymbolTable father;			// 上层符号表，全局符号表对应null
	public boolean ifGlobal = false;	// 记录是否为全局符号表
	public Map<String, Symbol> symbols = new LinkedHashMap<String, Symbol>();	// 符号表项
	public List<String> labels = new LinkedList<String>(); 		// 记录出现的label，用于语义检查

	private Integer nowOffset = 0;		// 记录当前所有定义的符号的offset
	
	// 下层符号表集合，用于临时输出检查
	private List<SymbolTable> sons = new LinkedList<SymbolTable>();
	// forDeleteSon 记录那些先声明后定义的函数，在声明时为其创建的符号表
	private Map<String, SymbolTable> forDeleteSon = new LinkedHashMap<String, SymbolTable>();
	
	public SymbolTable(SymbolTable father) {
		if(father == null || father.father == null) {
			// 全局遍历在内存中的offset从0开始
			// 函数的第一层局部变量在stack上的offset从0开始
			nowOffset = 0;
		}
		else {
			// 函数内部第二层及以上的局部变量的offset从上一层当前offset开始
			nowOffset = father.nowOffset;
		}

		this.father = father;
		if(father != null)
			father.addSon(this);
	}
	
	// 添加label
	public void addLabel(ASTIdentifier label) {
		labels.add(label.value);
	}
	// ls为goto语句用到的所有label,用于检查函数中是否定义
	public String checkLabel(List<ASTIdentifier> ls){
		for (ASTIdentifier id : ls) {
			if(!labels.contains(id.value)) {
				return id.value;
			}
		}
		return null;
	}
	
	// 获取上层符号表
	public SymbolTable getEnclosingScope() {
		return father;
	}
	// 设置为全局符号表
	public void setGolbal() {
		this.ifGlobal = true;
	}
	// 定义新的符号，符号名name,符号类别type
	public void define(String name,Type type) {
		if(type instanceof FunctionType)
			symbols.put(name, new Symbol(name,type));
		else {
			symbols.put(name, new Symbol(name,type,this.nowOffset));
			// 如果是变量，则为变量分配offset,
			nowOffset += type.getRegisiterSize();
		}
	}
	public void addSon(SymbolTable son){
		sons.add(son);
	}

	
	public void addForDelete(String name,SymbolTable decl) {
		forDeleteSon.put(name, decl);
	}
	

	
	public void update(String name,Type type) {
		if(symbols.containsKey(name)) {
			symbols.remove(name);
		}
		symbols.put(name, new Symbol(name,type));
	}
	
	public void deleteSon(SymbolTable so) {
		if(sons.contains(so)) {
			sons.remove(so);
		}
	}
	public void deleteSon(String name) {
		SymbolTable tmpTable = forDeleteSon.get(name);
		deleteSon(tmpTable);
	}
	
	public Symbol get(String name) {
		if(symbols.containsKey(name)) {
			return symbols.get(name);
		}else if(father != null) {
			return father.get(name);
		}
		return null;
	}
	
	public Symbol getCurrentSymbol(String name) {
		return symbols.get(name);
	}
	public Type getCurrentType(String name) {
		Symbol symbol = getCurrentSymbol(name);
		if(symbol == null)return null;
		else {
			return symbol.type;
		}
	}
	
	// for ir gen
	public LinkedList<IRDeclaration> changeToIR() {
		LinkedList<IRDeclaration>decls = new LinkedList<IRDeclaration>();
		// 对符号表中的每一个声明符号，转变成IR形式
		Iterator iter = symbols.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			String key = (String) entry.getKey();
			Symbol val = (Symbol) entry.getValue();
			Type type = val.type;
			Integer offsize = val.offsize;
			if(type instanceof NormalType) {
				decls.add(new IRVariableDeclaration(key, val.type,offsize));
			}else if(type instanceof ArrayType) {
				decls.add(new IRVariableDeclaration(key, val.type,offsize));
			}else if(type instanceof FunctionType) {
				// 如果是BuiltIn Function,不需要转换
				if(GlobalSymbolTable.isBuiltInFunc(((FunctionType)type).funcname)) {
					continue;
				}
				IRFunctionDeclaration irfuncdecl = new IRFunctionDeclaration(key);
				irfuncdecl.setReturnType(((FunctionType)type).returnType);
				irfuncdecl.setArgs((FunctionType)type);
				decls.add(irfuncdecl);
			}
		}
		return decls;
	}
	
	public Integer getNowOffset() {
		return nowOffset;
	}
	public void setNowOffset(Integer nowOffset) {
		this.nowOffset = nowOffset;
	}
	
	public void print(int id) {
		System.out.println("{");
		System.out.println("第"+id+"层符号表:");
		Iterator iter = symbols.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			String key = (String) entry.getKey();
			Symbol val = (Symbol) entry.getValue();
			System.out.println(key+" -> "+val.toString());
		}
		for (SymbolTable son : sons) {
			son.print(id+1);
		}
		System.out.println("}");
	}
}
