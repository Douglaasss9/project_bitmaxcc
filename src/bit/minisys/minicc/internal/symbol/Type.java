package bit.minisys.minicc.internal.symbol;

import org.python.antlr.PythonParser.return_stmt_return;

public abstract class Type {
	public enum BuiltInType{
		VOID,INT,CHAR,SHORT,LONG,FLOAT,DOUBLE,UNSIGNED,SIGNED,STRING,FUNCTION,ARRAY,UNKNOW,POINTER// for maple ir
	}	
	BuiltInType type;
	
	public BuiltInType getType() {
		return type;
	}
	public abstract boolean ifMatchType(Type anotherType);
	
	public abstract Type getFirstType(Type anotherType);
	public abstract boolean isSameType(Type anotherType);
	
	public Integer IntValue() {
		switch(this.type) {
		case CHAR : return 1;
		case SHORT : return 2;
		case INT:	return 3;
		case LONG:  return 4;
		case FLOAT : return 5;
		case DOUBLE: return 6;
		default:return -1;
		}
	}
	
	public boolean canAssigned() {
		return this.IntValue() != -1;
	}
	public boolean ifGeneralizedInt() {
		Integer value=this.IntValue();
		return value<=4&&value>=1;
	}
	
	public boolean isFunc() {
		return type == BuiltInType.FUNCTION;
	}
	
	public boolean isArray() {
		return type == BuiltInType.ARRAY;
	}
	
	public boolean isNormal() {
		return type == BuiltInType.VOID ||type == BuiltInType.INT ||type == BuiltInType.CHAR ||type == BuiltInType.SHORT ||
				type == BuiltInType.LONG ||type == BuiltInType.FLOAT ||type == BuiltInType.DOUBLE;
	}
	
	@Override
	public String toString() {
		switch(type) {
		case VOID:return "Void";
		case INT:return "Int";
		case SHORT:return "Short";
		case LONG:return "Long";
		case FLOAT:return "Float";
		case DOUBLE:return "Double";
		case CHAR:return "Char";
		case UNSIGNED:return "Unsigned";
		case SIGNED:return "Signed";
		case ARRAY:return "Array";
		case FUNCTION:return "Function";
		case POINTER:return "Addr";
		default:return"Unknown";
		}
	}
	
	
	public String toIRString() {
		switch(type) {
		case VOID:return "void";
		case INT:return "i32";
		case SHORT:return "i16";
		case LONG:return "i64";
		case FLOAT:return "f32";
		case DOUBLE:return "f64";
		case CHAR:return "i8";
		case POINTER:return "a32";
		default:return null;
		}
	}
	
	public abstract Integer getRegisiterSize() ;
}
