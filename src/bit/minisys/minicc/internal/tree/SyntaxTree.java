package bit.minisys.minicc.internal.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

import bit.minisys.minicc.internal.symbol.SymbolEntry;
import bit.minisys.minicc.internal.symbol.SymbolEntryType;
import bit.minisys.minicc.internal.symbol.SymbolTable;

public class SyntaxTree {
	
	public static TreeNode buildParseTree(String parse){
		TreeNode root = null;
		int index = 0;
		int lIndex = 0;
		int rIndex = 0;
		int sIndex = 0;
		String str;
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		parse = parse.replace("<EOF>", "");
		
		//parse = "(compoundStatement { (blockItemList ) })";
		
		while(true){
			if(!stack.isEmpty() && stack.peek().getName().equals("declaration")){
				System.out.println("ln");
			}
			lIndex = parse.indexOf("(", index);
			rIndex = parse.indexOf(")", index);
			if(lIndex < 0 && rIndex < 0){
				break;
			}else if(lIndex < rIndex && lIndex >= index){
				if(index < lIndex){
					str = parse.substring(index, lIndex).trim();
					if(!stack.isEmpty() && str.length() > 0){
						TreeNode n = new TreeNode(TreeNodeType.TNT_UNKNOWN, str);
						stack.peek().addChild(n);
					}
					index = lIndex;
				}
				sIndex = parse.indexOf(" ", lIndex + 1);
				str = parse.substring(index + 1, sIndex).trim();
				TreeNode n = new TreeNode(TreeNodeType.TNT_UNKNOWN, str);
				if(!stack.isEmpty()){
					stack.peek().addChild(n);
				}
				stack.push(n);
				index = sIndex + 1;
			}else if((lIndex > rIndex || lIndex < 0) && rIndex >= index){
				if(index < rIndex){
					str = parse.substring(index, rIndex).trim();
					if(!stack.isEmpty() && str.length() > 0){
						TreeNode n = new TreeNode(TreeNodeType.TNT_UNKNOWN, str);
						stack.peek().addChild(n);
					}
				}
				index = rIndex + 1;
				root = stack.pop();
			}
		}
		
		return root;
	}
	
	public static TreeNode buildParseTree2(String parse){
		Stack<TreeNode> stack = new Stack<TreeNode>();
		int index = 0;
		int length = parse.length();
		while(parse.charAt(length-1) != ')')
			length--;
		
		while(index < length-1){
			if(parse.charAt(index) == '('){
				index++;
				if(parse.charAt(index) == ' '){
					//'('作为节点出现
					stack.peek().addChild(new TreeNode(TreeNodeType.TNT_UNKNOWN, "("));
					index++;
				}else{
					String name = getName(parse, index);
					index += name.length() + 1;
					stack.push(new TreeNode(TreeNodeType.TNT_UNKNOWN, name));
				}
			}else if(parse.charAt(index) == ')'){
				if(parse.charAt(index-1) == ' '){
					//')'作为节点出现
					stack.peek().addChild(new TreeNode(TreeNodeType.TNT_UNKNOWN, ")"));
				}else{
					TreeNode child = stack.pop();
					stack.peek().addChild(child);
				}
				index++;
			}else if(parse.charAt(index) == ' '){
				index++;
			}else{
				//叶节点
				String name = getName(parse, index);
				index += name.length();
				stack.peek().addChild(new TreeNode(TreeNodeType.TNT_UNKNOWN, name));
			}
		}
		
		if(stack.size() != 1){
			System.out.println("error! stack.size=" + stack.size());
		}
		
		//check(stack.peek(), parse);
		
		return stack.peek();
	}
	
	private static String getName(String parse, int index){
		String name = "";
		while(parse.charAt(index) != ' ' && parse.charAt(index) != ')'){
			name += parse.charAt(index);
			index++;
		}
		return name;
	}
	
	public static void parseToSyntax(TreeNode root){
		//System.out.println("removing recursive began");
		removeRecursion(root);
		//System.out.println("removing recursive finished");
		
		root.setType(TreeNodeType.TNT_CMPL_UNIT);
		
		if(root.getChildren().size() < 2){
			root.getChildren().clear();
			return;
		}
		
		TreeNode translationUnit = root.getChildren().remove(0);
		root.getChildren().clear();
		assert translationUnit.getName().equals("translationUnit");
		for(TreeNode externDeclar: translationUnit.getChildren()){
			assert externDeclar.getName().equals("externalDeclaration");
			TreeNode child = externDeclar.getChildren().get(0);
			if(child.getName().equals("functionDefinition")){
				child = reduceFunctionDefinition(child);
			}else if(child.getName().equals("declaration")){
				child = reduceDeclaration(child);
			}else{
				assert child.getName().equals(";");
			}
			root.addChild(child);
		}
	}
	
	private static TreeNode reduceFunctionDefinition(TreeNode funcDef){
		funcDef.setType(TreeNodeType.TNT_FUNC_DEF);
		TreeNode declSpecifiers = null, declarator = null, compoundStatement = null;
		for(TreeNode child: funcDef.getChildren()){
			String name = child.getName();
			if(name.equals("declarationSpecifiers")){
				declSpecifiers = child;
			}
			else if(name.equals("declarator")) {
				declarator = child;
			}
			else if(name.equals("compoundStatement")) {
				compoundStatement = child;
			}
		}
		funcDef.getChildren().clear();
		
		//declarationSpecifiers
		for(TreeNode specifier: declSpecifiers.getChildren()){
			if(specifier.getChildren().get(0).getName().equals("typeSpecifier")){
				specifier.getChildren().get(0).setType(TreeNodeType.TNT_TYPE_SPE);
				funcDef.addChild(specifier.getChildren().get(0));
				break;
			}else if(specifier.getChildren().get(0).getName().equals("storageClassSpecifier")){
				specifier.getChildren().get(0).setType(TreeNodeType.TNT_STRG_SPE);
				funcDef.addChild(specifier.getChildren().get(0));
				break;
			}
		}
		
		//declarator
		if(declarator.getChildren().get(0).getName().equals("pointer")){
			declarator.getChildren().remove(0);
			funcDef.addChild(new TreeNode(TreeNodeType.TNT_PTR, "*"));
		}
		TreeNode directDeclarator = declarator.getChildren().get(0);
		TreeNode Identifier = directDeclarator.getChildren().remove(0);
		Identifier.setType(TreeNodeType.TNT_ID);
		funcDef.addChild(Identifier);
		for(TreeNode child: directDeclarator.getChildren()){
			if(child.getName().equals("parameterTypeList")){
				//if(child.getChildren().size() > 0){
					TreeNode parameterList = child.getChildren().get(0);
					parameterList.setType(TreeNodeType.TNT_PARA_LIST);
					funcDef.addChild(parameterList);
					for(int i = 0; i < parameterList.getChildren().size(); i++){
						TreeNode parameterDeclaration = parameterList.getChildren().get(i);
						if(parameterDeclaration.getName().equals(",")){
							parameterList.getChildren().remove(i);
							i--;
						}else{
							parameterDeclaration.setType(TreeNodeType.TNT_PARA_DECL);
							//assert parameterDeclaration.getChildren().size() >= 2;
							TreeNode declarationSpecifiers = parameterDeclaration.getChildren().remove(0);
							TreeNode declaratorForPara = parameterDeclaration.getChildren().remove(0);
							for(TreeNode specifier: declarationSpecifiers.getChildren()){
								if(specifier.getChildren().get(0).getName().equals("typeSpecifier")){
									specifier.getChildren().get(0).setType(TreeNodeType.TNT_TYPE_SPE);
									parameterDeclaration.addChild(specifier.getChildren().get(0));
									break;
								}
							}
							if(declaratorForPara.getChildren().get(0).getName().equals("pointer")){
								declaratorForPara.getChildren().remove(0);
								parameterDeclaration.addChild(new TreeNode(TreeNodeType.TNT_PTR, "*"));
							}
							TreeNode paraIdentifier = declaratorForPara.getChildren().get(0).getChildren().get(0);
							paraIdentifier.setType(TreeNodeType.TNT_ID);
							parameterDeclaration.addChild(paraIdentifier);
						}
					}
					if(child.getChildren().size() == 3){
						TreeNode varArg = child.getChildren().get(2);
						varArg.setType(TreeNodeType.TNT_VAR_ARG);
					}
				//}
			}
		}
		
		//compoundStatement
		funcDef.addChild(reduceCompoundStatement(compoundStatement));
		
		return funcDef;
	}
	
	private static TreeNode reduceCompoundStatement(TreeNode compoundStatement){
		compoundStatement.setType(TreeNodeType.TNT_CMPND_STMT);
		TreeNode blockItemList = null;
		if(compoundStatement.getChildren().size() == 3){
			blockItemList = compoundStatement.getChildren().get(1);
		}
		compoundStatement.getChildren().clear();
		if(blockItemList != null){
			for(TreeNode blockItem: blockItemList.getChildren()){
				TreeNode declaration = blockItem.getChildren().get(0);
				TreeNode statement = declaration;
				if(declaration.getName().equals("declaration")){
					declaration = reduceDeclaration(declaration);
					//declaration.setType(TreeNodeType.TNT_DECL);
				}else{
					statement = reduceStatement(statement);
				}
				compoundStatement.addChild(statement);
			}
		}
		return compoundStatement;
	}
	
	private static boolean isFunCall(TreeNode root){
		LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		
		while(!queue.isEmpty()){
			TreeNode now = queue.remove();
			if(now.getName().equals("directDeclarator") && now.getChildren().get(0).getName().equals("("))
				return true;
			else if(now.getChildren().size() != 0){
				for(TreeNode child: now.getChildren())
					queue.add(child);
			}
		}
		
		return false;
	}
	
	private static TreeNode reduceDeclarator(TreeNode declr) {
		assert declr.getType() == TreeNodeType.TNT_DECLR;
		
		TreeNode result = new TreeNode(TreeNodeType.TNT_DECLR, "declarator");
		
		ArrayList<TreeNode> childs = declr.getChildren();
		for(int i = 0; i < childs.size(); i++) { 	// reduce brackets
			if(childs.get(i).getName().equals("[")) { 	// left bracket
				assert childs.get(i + 2).getName().equals("]"); 	// for sure
				
				TreeNode constant = childs.get(i + 1);
				TreeNode arr_spe = new TreeNode(TreeNodeType.TNT_ARR_SPE, "arraySpecifier");
				
				arr_spe.addChild(constant);
				result.addChild(arr_spe);
				
				i += 2;
			} else {
				result.addChild(childs.get(i));
			}
		}
		return result;
	}
	
	private static TreeNode reduceDeclaration(TreeNode decl){
		if(isFunCall(decl)){
			//一个参数函数调用
			decl.setName("functionCall");
			decl.setType(TreeNodeType.TNT_FUNC_CALL);
			
			TreeNode identifier = decl.getChildren().get(0);
			while(identifier.getChildren().size() != 0)
				identifier = identifier.getChildren().get(0);
			identifier.setType(TreeNodeType.TNT_ID);
			
			TreeNode para = decl.getChildren().get(1);
			while(para.getChildren().size() != 0)
				if(para.getChildren().get(0).getName().equals("("))
					para = para.getChildren().get(1);
				else
					para = para.getChildren().get(0);
			para.setType(TreeNodeType.TNT_ID);
			
			decl.getChildren().clear();
			decl.getChildren().add(identifier);
			decl.getChildren().add(para);
			
			TreeNode exp = new TreeNode(TreeNodeType.TNT_EXP_STMT, "expressionStatement");
			exp.getChildren().add(decl);
			
			return exp;
		}
		
		decl.setType(TreeNodeType.TNT_DECL);
		TreeNode declarationSpecifiers = decl.getChildren().get(0);
		TreeNode initDeclaratorList = null;
		if(decl.getChildren().size() == 3)
			initDeclaratorList = decl.getChildren().get(1);
		decl.getChildren().clear();
		
		//declarationSpecifiers
		for(TreeNode specifier: declarationSpecifiers.getChildren()){
			String name = specifier.getChildren().get(0).getName();
			if(name.equals("typeSpecifier")){
				if(specifier.getChildren().get(0).getChildren().get(0).getChildren().size() == 0){
					specifier.getChildren().get(0).setType(TreeNodeType.TNT_TYPE_SPE);
					decl.addChild(specifier.getChildren().get(0));
				}else{
					TreeNode identifier = specifier.getChildren().get(0).getChildren().get(0).getChildren().get(0);
					identifier.setType(TreeNodeType.TNT_ID);
					TreeNode declarator = new TreeNode(TreeNodeType.TNT_DECLR, "declarator");
					declarator.addChild(identifier);
					
					TreeNode initDeclarator = new TreeNode(TreeNodeType.TNT_INIT_DECLR, "initDeclarator");
					initDeclarator.addChild(declarator);
					decl.addChild(initDeclarator);
				}
			}else if(name.equals("storageClassSpecifier")){
				specifier.getChildren().get(0).setType(TreeNodeType.TNT_STRG_SPE);
				decl.addChild(specifier.getChildren().get(0));
			}
		}
		
		//initDeclaratorList
		if(initDeclaratorList != null){
			for(TreeNode initDeclarator: initDeclaratorList.getChildren()){
				if(initDeclarator.getName().equals(","))
					continue;
				
				initDeclarator.setType(TreeNodeType.TNT_INIT_DECLR);
				decl.addChild(initDeclarator);
				
				//declarator
				
				TreeNode declarator = initDeclarator.getChildren().get(0);
				declarator.setType(TreeNodeType.TNT_DECLR);
				
				TreeNode pointer = null, directDeclarator = null;
				if(declarator.getChildren().get(0).getName().equals("pointer")){
					pointer = declarator.getChildren().get(0);
					directDeclarator = declarator.getChildren().get(1);
				}else{
					directDeclarator = declarator.getChildren().get(0);
				}
				declarator.getChildren().clear();
				
				//pointer
				if(pointer != null)
					declarator.addChild(new TreeNode(TreeNodeType.TNT_PTR, "*"));

				//directDeclarator
				TreeNode identifier = directDeclarator.getChildren().get(0);
				identifier.setType(TreeNodeType.TNT_ID);
				declarator.addChild(identifier);
				for(int i = 1; i < directDeclarator.getChildren().size(); i++){
					TreeNode node = directDeclarator.getChildren().get(i);
					if(node.getName().equals("assignmentExpression")){
						declarator.addChild(removeUselessExp(node));
					}else if(node.getName().equals("[") || node.getName().equals("]")){
						declarator.addChild(node);
					}else{
						break;
					}
				}
				
				//initializer
				
				if(initDeclarator.getChildren().size() == 3){
					TreeNode initializer = initDeclarator.getChildren().get(2);
					initDeclarator.getChildren().remove(1);
					initDeclarator.getChildren().remove(1);
					initDeclarator.addChild(reduceInitializer(initializer));
				}
				
				// reduce declarator
				ArrayList<TreeNode> child = initDeclarator.getChildren();
				child.set(0, reduceDeclarator(child.get(0))); 
			}
		}
		
		
		return decl;
	}
	
	private static TreeNode reduceInitializer(TreeNode initializer){
		if(initializer.getChildren().size() == 1){
			initializer.setType(TreeNodeType.TNT_INIT_NOML);
			TreeNode assignmentExpression = initializer.getChildren().remove(0);
			initializer.addChild(removeUselessExp(assignmentExpression));
		}else{
			//数组初始化
			initializer.setType(TreeNodeType.TNT_INIT_ARY);
			TreeNode initializerList = initializer.getChildren().get(1);
			initializer.getChildren().clear();
			for(TreeNode node: initializerList.getChildren()){
				if(node.getName().equals("initializer")){
					node = reduceInitializer(node);
					if(node.getChildren().size() == 1)
						node = node.getChildren().get(0);
					initializer.addChild(node);
				}
			}
		}
		return initializer;
	}
	
	private static TreeNode reduceStatement(TreeNode statementLabel){
		TreeNode statement = statementLabel.getChildren().get(0);
		String name = statement.getName();
		if(name.equals("labeledStatement")) {
			statement = reduceLabeledStatement(statement);
		}
		else if(name.equals("compoundStatement")) {
			statement = reduceCompoundStatement(statement);
		}
		else if(name.equals("expressionStatement")) {
			statement = reduceExpressionStatement(statement);
		}
		else if(name.equals("selectionStatement")) {
			statement = reduceSelectionStatement(statement);
		}
		else if(name.equals("iterationStatement")) {
			statement = reduceIterationStatement(statement);
		}
		else if(name.equals("jumpStatement")) {
			statement = reduceJumpStatement(statement);
		}
		
		return statement;
	}
	
	private static TreeNode reduceLabeledStatement(TreeNode labStmt){
		labStmt.setType(TreeNodeType.TNT_LBD_STMT);
		TreeNode stmt = labStmt.getChildren().remove(labStmt.getChildren().size()-1);
		labStmt.getChildren().remove(labStmt.getChildren().size()-1);
		if(labStmt.getChildren().get(0).getName().equals("case")){
			TreeNode constantExpression = labStmt.getChildren().remove(1);
			labStmt.addChild(removeUselessExp(constantExpression));
		}else if(labStmt.getChildren().get(0).getName().equals("default")){
			
		}else{
			labStmt.getChildren().get(0).setType(TreeNodeType.TNT_ID);
		}
		labStmt.addChild(reduceStatement(stmt));
		
		return labStmt;
	}
	
	private static TreeNode reduceIterationStatement(TreeNode itStmt){
		if(itStmt.getChildren().get(0).getName().equals("while")){
			TreeNode exp = itStmt.getChildren().get(2);
			TreeNode stmt = itStmt.getChildren().get(4);
			itStmt.getChildren().clear();
			itStmt.setType(TreeNodeType.TNT_IT_STMT_WHILE);
			itStmt.addChild(reduceExpression(exp));
			itStmt.addChild(reduceStatement(stmt));
		}else if(itStmt.getChildren().get(0).getName().equals("do")){
			TreeNode exp = itStmt.getChildren().get(4);
			TreeNode stmt = itStmt.getChildren().get(1);
			itStmt.getChildren().clear();
			itStmt.setType(TreeNodeType.TNT_IT_STMT_DOWHILE);
			itStmt.addChild(reduceExpression(exp));
			itStmt.addChild(reduceStatement(stmt));
		}else if(itStmt.getChildren().get(0).getName().equals("for")){
			TreeNode stmt = reduceStatement(itStmt.getChildren().get(itStmt.getChildren().size()-1));
			TreeNode[] exp = new TreeNode[3];
			int i = 2;
			if(itStmt.getChildren().get(i).getName().equals("declaration")){
				exp[0] = reduceDeclaration(itStmt.getChildren().get(i));
			}else if(itStmt.getChildren().get(i).getName().equals("expression")){
				exp[0] = reduceExpression(itStmt.getChildren().get(i));
				i++;
			}else{
				exp[0] = new TreeNode(TreeNodeType.TNT_EXP, "expression");
			}
			i++;
			
			if(itStmt.getChildren().get(i).getName().equals("expression")){
				exp[1] = reduceExpression(itStmt.getChildren().get(i));
				i++;
			}else{
				exp[1] = new TreeNode(TreeNodeType.TNT_EXP, "expression");
			}
			i++;
			
			if(itStmt.getChildren().get(i).getName().equals("expression")){
				exp[2] = reduceExpression(itStmt.getChildren().get(i));
				i++;
			}else{
				exp[2] = new TreeNode(TreeNodeType.TNT_EXP, "expression");
			}
			
			itStmt.getChildren().clear();
			itStmt.setType(TreeNodeType.TNT_IT_STMT_FOR);
			
			for(TreeNode node: exp){
				itStmt.addChild(node);
			}
			itStmt.addChild(stmt);
		}
		return itStmt;
	}
	
	private static TreeNode reduceSelectionStatement(TreeNode selStmt){
		String name = selStmt.getChildren().get(0).getName();
		TreeNode exp = selStmt.getChildren().get(2);
		TreeNode stmt = selStmt.getChildren().get(4);
		TreeNode stmt2 = null;
		if(selStmt.getChildren().size() == 7)
			stmt2 = selStmt.getChildren().get(6);
		selStmt.getChildren().clear();
		
		selStmt.addChild(reduceExpression(exp));
		selStmt.addChild(reduceStatement(stmt));

		if(name.equals("if")){
			selStmt.setType(TreeNodeType.TNT_SEL_STMT_IF);
			if(stmt2 != null)
				selStmt.addChild(reduceStatement(stmt2));
		}else{
			selStmt.setType(TreeNodeType.TNT_SEL_STMT_SWITCH);
		}
		
		return selStmt;
	}
	
	private static TreeNode reduceJumpStatement(TreeNode jmpStmt){
		jmpStmt.setType(TreeNodeType.TNT_JMP_STMT);
		jmpStmt.getChildren().remove(jmpStmt.getChildren().size()-1);
		String name = jmpStmt.getChildren().get(0).getName();
		if(name.equals("goto")) {
			jmpStmt.getChildren().get(1).setType(TreeNodeType.TNT_ID);
		}
		else if(name.equals("return")) {
			if(jmpStmt.getChildren().size() == 2){
				TreeNode expression = jmpStmt.getChildren().remove(1);
				jmpStmt.getChildren().add(1, reduceExpression(expression));
			}
		}
		return jmpStmt;
	}
	
	//expressionStatement没有子节点表示空语句“；”
	private static TreeNode reduceExpressionStatement(TreeNode expStmt){
		expStmt.setType(TreeNodeType.TNT_EXP_STMT);
		if(expStmt.getChildren().size() == 2){
			TreeNode exp = expStmt.getChildren().get(0);
			expStmt.getChildren().clear();
			for(TreeNode assExp: exp.getChildren()){
				if(!assExp.getName().equals(","))
					expStmt.addChild(removeUselessExp(assExp));
			}
		}else{
			expStmt.getChildren().clear();
		}
		
		return expStmt;
	}
	
	private static TreeNode reduceExpression(TreeNode exp){
		exp.setType(TreeNodeType.TNT_EXP);
		for(int i = 0; i < exp.getChildren().size(); i++){
			TreeNode assExp = exp.getChildren().get(i);
			if(assExp.getName().equals("assignmentExpression")){
				exp.getChildren().remove(i);
				exp.getChildren().add(i, removeUselessExp(assExp));
			}else{
				exp.getChildren().remove(i);
				i--;
			}
		}
		
		return exp;
	}
	
	/*
	 * 递归地化简细分的expression，消除无用中间节点
	 */
	private static TreeNode removeUselessExp(TreeNode exp){
		if(exp.getName().equals("primaryExpression")){
			if(exp.getChildren().size() == 1){
				if(exp.getChildren().get(0).getName().equals("tokenId")){
					TreeNode tokenId = exp.getChildren().get(0).getChildren().get(0);
					tokenId.setType(TreeNodeType.TNT_ID);
					return tokenId;
				}else if(exp.getChildren().get(0).getName().equals("tokenConstant")){
					TreeNode tokenConstant = exp.getChildren().get(0).getChildren().get(0);
					tokenConstant.setType(TreeNodeType.TNT_CONST);
					return tokenConstant;
				}else if(exp.getChildren().get(0).getName().equals("tokenStringLiteral")){
					TreeNode tokenString = exp.getChildren().get(0).getChildren().get(0);
					tokenString.setType(TreeNodeType.TNT_SCONST);
					return tokenString;
				}
			}else if(exp.getChildren().get(0).getName().equals("(")){
				return reduceExpression(exp.getChildren().get(1));
			}
		}else if(exp.getChildren().size() == 1){
			return removeUselessExp(exp.getChildren().get(0));
		}else{
			String name = exp.getName();
			if(name.equals("assignmentExpression")) {
				TreeNode left = exp.getChildren().get(0);
				TreeNode assOperator = exp.getChildren().get(1);
				TreeNode right = exp.getChildren().get(2);
				exp.getChildren().clear();
				
				exp.addChild(removeUselessExp(left));
				
				assOperator = assOperator.getChildren().get(0);
				assOperator.setType(TreeNodeType.TNT_ASNMT_OPRTR);
				exp.addChild(assOperator);

				exp.addChild(removeUselessExp(right));
				
				exp.setType(TreeNodeType.TNT_ASNMT_EXP);
				
				return exp;
			}else if(name.equals("logicalOrExpression")) {
				exp.setType(TreeNodeType.TNT_LOGIC_OR_EXP);
			}
			else if(name.equals("logicalAndExpression")) {
				exp.setType(TreeNodeType.TNT_LOGIC_AND_EXP);
			}
			else if(name.equals("equalityExpression")) {
				exp.setType(TreeNodeType.TNT_EQU_EXP);
			}
			else if(name.equals("relationalExpression")) {
				exp.setType(TreeNodeType.TNT_REL_EXP);
			}
			else if(name.equals("additiveExpression")) {
				exp.setType(TreeNodeType.TNT_ADD_EXP);
			}
			else if(name.equals("multiplicativeExpression")) {
				exp.setType(TreeNodeType.TNT_MUL_EXP);
			}
			else if(name.equals("unaryExpression")) {
				TreeNode unaryOperator = exp.getChildren().remove(0);
				TreeNode unaryExpression = exp.getChildren().remove(0);
				if(unaryOperator.getName().equals("unaryOperator"))
					unaryOperator = unaryOperator.getChildren().get(0);
				unaryOperator.setType(TreeNodeType.TNT_UNARY_OPRTR);
				exp.addChild(unaryOperator);
				exp.addChild(removeUselessExp(unaryExpression));
				exp.setType(TreeNodeType.TNT_UNARY_EXP);
				return exp;
			}
			else if(name.equals("postfixExpression")) {
				return reducePostfixExp(exp);
			}
			for(int i = 0; i < exp.getChildren().size(); i++){
				TreeNode node = exp.getChildren().get(i);
				if(!node.getChildren().isEmpty()){
					node = removeUselessExp(node);
					exp.getChildren().remove(i);
					exp.getChildren().add(i, node);
				}
			}
			return exp;
		}
		return null;
		
	}
	
	private static TreeNode reducePostfixExp(TreeNode posExp){
		TreeNode postfixExpression = posExp.getChildren().remove(0);
		posExp.getChildren().add(0, removeUselessExp(postfixExpression));
		
		String name = posExp.getChildren().get(1).getName();
		if(name.equals("[")) {
			posExp.setType(TreeNodeType.TNT_ARY_SUB);
			posExp.setName("arraySubscripting");
			posExp.getChildren().remove(3);
			TreeNode exp = posExp.getChildren().remove(2);
			posExp.getChildren().add(2, reduceExpression(exp));
			posExp.getChildren().remove(1);
			if(posExp.getChildren().get(0).getName().equals("arraySubscripting")){
				//多维数组引用
				TreeNode arySub = posExp.getChildren().remove(0);
				for(int i = 0; i < arySub.getChildren().size(); i++){
					posExp.getChildren().add(i, arySub.getChildren().get(i));
				}
			}
		} else if(name.equals("(")) {
			posExp.setType(TreeNodeType.TNT_FUNC_CALL);
			posExp.setName("functionCall");
			posExp.getChildren().remove(1);
			TreeNode argExpList = posExp.getChildren().remove(1);
			if(argExpList.getName().equals("argumentExpressionList")){
				posExp.getChildren().remove(1);
				for(TreeNode assExp: argExpList.getChildren()){
					if(assExp.getName().equals(","))
						continue;
					posExp.addChild(removeUselessExp(assExp));
				}
			}
		} else if(name.equals(".")) {
			posExp.getChildren().get(2).setType(TreeNodeType.TNT_ID);
		} else if(name.equals("->")) {
			posExp.getChildren().get(2).setType(TreeNodeType.TNT_ID);
		}
		else {
			posExp.setType(TreeNodeType.TNT_POST_EXP);
		}
		
		return posExp;
	}
	
	/*
	 * 消除左递归（postfixExpression除外）
	 */
	public static void removeRecursion(TreeNode root){
		LinkedList<TreeNode> nodeList = new LinkedList<TreeNode>();
		nodeList.add(root);
		while(nodeList.size() > 0){
			boolean popFirst = true;
			TreeNode father = nodeList.getFirst();
			if(father.getChildren().size() > 0 && !father.getName().equals("postfixExpression") && //加法和乘法不消除左递归， 计卫星
					!father.getName().equals("additiveExpression") && !father.getName().equals("multiplicativeExpression")){
				TreeNode firstChild = father.getChildren().get(0);
				if(firstChild.getName().equals(father.getName())){
					popFirst = false;
					father.getChildren().remove(0);
					for(int i = 0; i < firstChild.getChildren().size(); i++)
						father.getChildren().add(i, firstChild.getChildren().get(i));
				}
				for(TreeNode child: father.getChildren())
					nodeList.add(child);
			}
			if(popFirst){
				nodeList.remove(0);
			}
		}
	}
	
	public static void printTree(TreeNode root){
		printTree2(root, "\t");
	}
	public static void printTree2(TreeNode root, String indent){
		System.out.println(indent + root.getName());
		for(TreeNode n: root.getChildren()){
			printTree2(n, indent + "\t");
		}
	}
	
	public static TreeNodeType getTreeNodeType(String s){
		if(s.equals("compilationUnit")){
			return TreeNodeType.TNT_CMPL_UNIT;
		}else if(s.equals("functionDefinition")){
			return TreeNodeType.TNT_FUNC_DEF;
		}else if(s.equals("compoundStatement")){
			return TreeNodeType.TNT_CMPND_STMT;
		}else if(s.equals("externalDeclaration")){
			return TreeNodeType.TNT_EXTR_DECL;
		}else if(s.equals("declaration")){
			return TreeNodeType.TNT_DECL;
		}else if(s.equals("assignmentExpression")){
			return TreeNodeType.TNT_STMT_ASSIGN;
		}else{
			return TreeNodeType.TNT_UNKNOWN;
		}
	}

	public static TreeNode parse2Syntax2(TreeNode root, SymbolTable st){
		root.removeSemicolon();
		TreeNodeType type = getTreeNodeType(root.getName());
		if(type == TreeNodeType.TNT_UNKNOWN){
			if(root.getChildren().size() == 1){
				
			}
		}
		
		return root;
	}
	
//	public static TreeNode parse2Syntax(TreeNode root, SymbolTable st){
//		ArrayList<TreeNode> stopList = new ArrayList<TreeNode>();
//		ArrayList<String> stopLabels = new ArrayList<String>();
//		
//		root.removeSemicolon();
//		
//		TreeNodeType type = getTreeNodeType(root.getName());
//		root.setType(type);
//		
//		if(type == TreeNodeType.TNT_UNKNOWN){
//			for(TreeNode c: root.getChildren()){
//				parse2Syntax(c, st);
//			}
//		}else if(type == TreeNodeType.TNT_CMPND_STMT){
//			stopList.clear();
//			stopLabels.clear();
//			stopLabels.add("declaration");
//			stopLabels.add("statement");
//			reduceRecursive(root, stopLabels, stopList);
//			root.getChildren().clear();
//			root.getChildren().addAll(stopList);
//			for(TreeNode c: stopList){
//				parse2Syntax(c, st);
//			}
//		}
//		else if(type == TreeNodeType.TNT_CMPL_UNIT){
//			root.setNestedScope(st);
//			stopList.clear();
//			stopLabels.clear();
//			stopLabels.add("functionDefinition");
//			stopLabels.add("declarator");
//			reduceRecursive(root, stopLabels, stopList);
//			root.getChildren().clear();
//			root.getChildren().addAll(stopList);
//			for(TreeNode c: root.getChildren()){
//				parse2Syntax(c, st);
//			}
//		}else if(type == TreeNodeType.TNT_DECL){//external declarations
//			//type
//			stopList.clear();
//			stopLabels.clear();
//			stopLabels.add("typeSpecifier");
//			reduceRecursive(root, stopLabels, stopList);
//			String varType = stopList.get(0).getByIndex(0).getName();
//			
//			// declarator
//			if(stopList.size() > 1){ //local
//				root.getChildren().set(0, stopList.get(0));
//				root.getChildren().add(stopList.get(1));
//				SymbolEntry se = new SymbolEntry(SymbolEntryType.SET_SYMBOL_VAR, stopList.get(1).getByIndex(0).getByIndex(0).getName());
//				se.setDataType(varType);
//				int index = st.addSymbol(se);
//				root.addSymbolIndex(index);
//			}else{
//				stopList.clear();
//				stopLabels.clear();
//				stopLabels.add("directDeclarator");
//				reduceRecursive(root, stopLabels, stopList);
//				for(TreeNode id: stopList){
//					SymbolEntry se = new SymbolEntry(SymbolEntryType.SET_SYMBOL_VAR, id.getByIndex(0).getName());
//					se.setDataType(varType);
//					int index = st.addSymbol(se);
//					root.addSymbolIndex(index);
//				}
//				//init
//				stopList.clear();
//				stopLabels.clear();
//				stopLabels.add("initDeclarator");
//				reduceRecursive(root, stopLabels, stopList);
//				for(TreeNode e: stopList){
//					root.addChild(e);
//					e.getChildren().set(0, getLeafNode(e.getChildren().get(0)));
//					if(e.getChildren().size() >= 3){ //some of them do not have initial value
//						e.getChildren().set(2, reduceTree(e.getChildren().get(2), "primaryExpression"));
//					}
//				}
//			}
//		}else if(type == TreeNodeType.TNT_FUNC_DEF){//function def
//			//type
//			String funcType = getToken(root.getChildren().get(0));
//			root.getChildren().set(0, getLeafNode(root.getChildren().get(0)));
//			//name
//			TreeNode directDecalarator = root.getByIndex(1).getByIndex(0);
//			String funcName = getToken(directDecalarator.getByIndex(0));
//			
//			//parameters
//			root.getByIndex(1).getChildren().clear();
//			stopLabels.clear();
//			stopList.clear();
//			stopLabels.add("parameterDeclaration");
//			reduceRecursive(directDecalarator, stopLabels, stopList);
//			
//			for(TreeNode p: stopList){
//				root.getByIndex(1).getChildren().add(p);
//				String pType = getToken(p.getByIndex(0));
//				String pName = getToken(p.getByIndex(1));
//				SymbolEntry se = new SymbolEntry(SymbolEntryType.SET_SYMBOL_PARA, pName);
//				se.setDataType(pType);
//				p.getSymbolIndexList().add(st.addSymbol(se));
//				p.getChildren().clear();
//			}
//			
//			//body
//			SymbolTable stFunc = new SymbolTable();
//			stFunc.setTop(st);
//			root.setNestedScope(stFunc);
//			parse2Syntax(root.getByIndex(2), stFunc);
//			
//			//symbol
//			SymbolEntry func = new SymbolEntry(SymbolEntryType.SET_SYMBOL_FUNC, funcName);
//			func.setDataType(funcType);
//			int index = st.addSymbol(func);
//			root.addSymbolIndex(index);
//		}else if(type == TreeNodeType.TNT_STMT_ASSIGN && root.getChildren().size() == 3){//asignment statement
//			root.getChildren().remove(1);
//			root.getChildren().set(0, reduceTree(root.getByIndex(0), "primaryExpression"));
//			root.getChildren().set(1, reduceTree(root.getByIndex(1), "primaryExpression"));
//		}else{
//			for(TreeNode c: root.getChildren()){
//				parse2Syntax(c, st);
//			}
//		}
//		return root;
//	}
//	
	public static String getToken(TreeNode root){
		while(root.getChildren().size() > 0){
			root = root.getByIndex(0);
		}
		return root.getName();
	}
	
	public static ArrayList<String> getTokenList(TreeNode root, String label){
		ArrayList<String> list = new ArrayList<String>();
		if(root.getName().equals(label)){
			list.add(getToken(root));
		}else{
			for(TreeNode c: root.getChildren()){
				ArrayList<String> subList = getTokenList(c, label);
				list.addAll(subList);
			}
		}
		
		return list;
	}
	
	public static TreeNode getLeafNode(TreeNode root){
		while(root.getChildren().size() > 0){
			root = root.getByIndex(0);
		}
		return root;
	}
	public static TreeNode reduceTree(TreeNode root, String stopLabel){
		while(!(root.getName().equals(stopLabel))){
			if(root.getChildren().size() == 1){
				root = root.getByIndex(0);
			}else{
				break;
			}
		}
		return root;
	}
	
	public static void reduceRecursive(TreeNode root, ArrayList<String> stopLabels, ArrayList<TreeNode> list){
		if(root == null) return;
		if(stopLabels.indexOf(root.getName()) >= 0){
			list.add(root);
		}else{
			for(TreeNode c: root.getChildren()){
				reduceRecursive(c, stopLabels, list);
			}
		}
	}
		
	public static void getAllLeaves(TreeNode root, ArrayList<TreeNode> leaves){
		if(root.getChildren().size() >= 1){
			for(TreeNode c: root.getChildren()){
				getAllLeaves(c, leaves);
			}
		}else{
			leaves.add(root);
		}
	}
}
