package bit.minisys.minicc.internal.tree;

import java.util.ArrayList;

import bit.minisys.minicc.internal.symbol.SymbolEntry;
import bit.minisys.minicc.internal.symbol.SymbolTable;

public class TreeNode {
	private TreeNodeType type;
	private String name;
	private ArrayList<SymbolEntry> symbolList;
	private SymbolTable symbolTable;
	//register allocation
	private int regNum;
	private boolean isAllocated;
	public int maxTempRegNum;
	public int calcNo;
	//code generation
	private int labelID;
	
	private ArrayList<TreeNode> children;
	
	public TreeNode(TreeNodeType type, String name){
		this.setType(type);
		this.setName(name);
		this.setChildren(new ArrayList<TreeNode>());
		this.setSymbolList(new ArrayList<SymbolEntry>());
		this.regNum = 0;
		this.isAllocated = false;
		this.maxTempRegNum = 0;
		this.calcNo = -1;
	}
	
	public TreeNode getByIndex(int index){
		if(index < this.getChildren().size()){
			return this.getChildren().get(index);
		}else{
			System.out.println("out of bound!");
			System.exit(-1);
		}
		
		return null;
	}
	public void addSymbol(SymbolEntry se){
		this.symbolList.add(se);
	}
	
	
	public void addChild(TreeNode node){
		this.getChildren().add(node);
	}

	public TreeNodeType getType() {
		return type;
	}

	public void setType(TreeNodeType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<TreeNode> children) {
		this.children = children;
	}

	public SymbolTable getSymbolTable() {
		return symbolTable;
	}

	public void setSymbolTable(SymbolTable symbolTable) {
		this.symbolTable = symbolTable;
	}

	public ArrayList<SymbolEntry> getSymbolList() {
		return symbolList;
	}

	public void setSymbolList(ArrayList<SymbolEntry> symbolList) {
		this.symbolList = symbolList;
	}
	
	public void removeSemicolon(){
		int size = this.children.size();
		if(size > 0 && this.children.get(size - 1).getName().equals(";")){
			this.children.remove(size - 1);
		}
	}
	public void removeCurlyBrac(){
		
	}

	public int getRegNum() {
		return regNum;
	}

	public void setRegNum(int regNum) {
		this.regNum = regNum;
	}

	public boolean getRegAllocatedFlag() {
		return isAllocated;
	}

	public void setRegAllocatedFlag(boolean b) {
        isAllocated = b;
    }

	public int getLabelID() {
		return labelID;
	}

	public void setLabelID(int labelID) {
		this.labelID = labelID;
	}
}
