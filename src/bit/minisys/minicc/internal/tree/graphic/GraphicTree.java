package bit.minisys.minicc.internal.tree.graphic;

import java.util.ArrayList;

import org.antlr.v4.runtime.tree.Tree;

import bit.minisys.minicc.internal.tree.TreeNode;
import bit.minisys.minicc.internal.tree.TreeNodeType;

public class GraphicTree implements Tree{
	
	private ArrayList<GraphicTree> children;
	private GraphicTree parent;
	private String name;
	private TreeNodeType type;
	
	public GraphicTree(TreeNodeType type, String name, GraphicTree parent){
		this.type = type;
		this.name = name;
		this.children = new ArrayList<GraphicTree>();
		this.parent = parent;
	}
	
	public GraphicTree(TreeNode root){
		
		this(root.getType(), root.getName(), null);
		for(TreeNode child: root.getChildren()){
			GraphicTree gChild = convert(child, this);
			this.children.add(gChild);
		}
	}

	@Override
	public Tree getChild(int index) {
		// TODO Auto-generated method stub
		return this.children.get(index);
	}

	@Override
	public int getChildCount() {
		// TODO Auto-generated method stub
		return this.children.size();
	}

	@Override
	public Tree getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	@Override
	public Object getPayload() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStringTree() {
		// TODO Auto-generated method stub
		return this.name;
	}
	
	public static GraphicTree convert(TreeNode root, GraphicTree parent) {
		StringBuilder appendRegInfo = new StringBuilder();
		if (root.getRegAllocatedFlag() == true) {
			appendRegInfo.append("(reg=" + root.getRegNum());
		}
		if (root.calcNo != -1) {
			appendRegInfo.append(", " + "calcNo=" + root.calcNo + ")");
		}
		if (root.getRegAllocatedFlag() && root.calcNo == -1) {
			appendRegInfo.append(")");
		}
		GraphicTree gRoot = new GraphicTree(root.getType(), root.getName() + appendRegInfo, parent);
		for (TreeNode child : root.getChildren()) {
			GraphicTree gChild = convert(child, gRoot);
			gRoot.children.add(gChild);
		}
		return gRoot;
	}

}
