package bit.minisys.minicc.internal.tree.graphic;

import org.antlr.v4.runtime.tree.Tree;

public interface TreeTextProvider {
	String getText(Tree node);
}
