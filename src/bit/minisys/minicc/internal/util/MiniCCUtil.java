package bit.minisys.minicc.internal.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MiniCCUtil {
	
	public static boolean checkFile(String filePath){
		File file = new File(filePath);
		if(!file.exists()){
			System.out.println("file " + file + " does not exist:");
			return false;
		}
		return true;
	}
	
	public static ArrayList<String> readFile(String filePath){
		ArrayList<String> lines = new ArrayList<String>();
		
		if(!checkFile(filePath)) {
			return lines;
		}
		
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String str = null;
			while((str = reader.readLine()) != null) {
				lines.add(str);
			}
			reader.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		return lines;
	}
	public static void createAndWriteFile(String file, String content){
		FileWriter fw;
		try {
			fw = new FileWriter(file, false);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean removeFile(String filePath) {
		File file = new File(filePath);
		if(file.exists()) {
			try {
				return file.delete();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
	
	public static String escape(String s) {
		// TODO
		return s;
	}
	
	public static String removeAllExt(String s) {
		return s.substring(0, s.lastIndexOf('.'));
	}
	public static String remove2Ext(String s) {
		s = s.substring(0,s.lastIndexOf('.'));
		return s.substring(0, s.lastIndexOf('.'));
	}
	
	public static String removeAllExtNew(String s) {
		return s.substring(0, s.indexOf('.'));
	}
}
