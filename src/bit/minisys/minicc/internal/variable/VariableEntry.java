package bit.minisys.minicc.internal.variable;

import java.util.ArrayList;

import bit.minisys.minicc.internal.variable.*;

public class VariableEntry implements Cloneable {
	public int pointer; 	// pointer level
	public Boolean array, unsigned; 	// is array, is unsigned, is long
	public ArrayList<Integer> array_size; 	// array size
	public VariableType type; 	// element/variable type
	
	public VariableEntry() {
		this.pointer = 0; 	// pointer level
		this.array = false;
		this.unsigned = false;
		
		this.type = VariableType.TYPE_INT;
		this.array_size = new ArrayList<Integer>();
	}
	
	public VariableEntry clone() throws CloneNotSupportedException {
		return (VariableEntry) super.clone();
	}
	
	public String typename() {
		return "int";
	}
	
	public int count() {
		int count = this.array_size.get(0);
		for(int i = 1; i < this.array_size.size(); i++) {
			count *= this.array_size.get(i);
		}
		return count;
	}
}
