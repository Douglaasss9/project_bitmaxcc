package bit.minisys.minicc.internal.variable;

// 
public enum VariableType {
	TYPE_INT,
	TYPE_SHORT,
	TYPE_LONG,
	TYPE_LONGLONG,
	TYPE_DOUBLE,
	TYPE_FLOAT,
}
