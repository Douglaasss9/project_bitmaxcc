package bit.minisys.minicc.ir;

import java.util.ArrayList;

public class IRFunction {
	public String returnType;
	public String llvmType;
	public String funcName;
	public ArrayList<IRVariableDeclaration> paramList = new ArrayList<>();
	public IRScope body;
}
