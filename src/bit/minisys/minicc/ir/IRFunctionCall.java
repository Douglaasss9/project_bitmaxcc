package bit.minisys.minicc.ir;

import java.util.ArrayList;

public class IRFunctionCall {
	
	public String returnType;
	public String funcName;
	public ArrayList<IRExpression> paramList = new ArrayList<>();
}
