package bit.minisys.minicc.ir;

import java.util.ArrayList;

public class IRScope extends IRStatement{
	
    public ArrayList<IRVariableDeclaration> variableNames;
    public ArrayList<IRStatement> statements = new ArrayList<>();
	
    public IRScope(){
    	variableNames = new ArrayList<IRVariableDeclaration>();
    }
    
	public void addVariable(IRVariableDeclaration var) {
		variableNames.add(var);
	}
	
	public void addStatement(IRStatement state) {
		statements.add(state);
	}
}
