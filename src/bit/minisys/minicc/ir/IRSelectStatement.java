package bit.minisys.minicc.ir;

import java.util.ArrayList;

public class IRSelectStatement {
	
	//public ArrayList<IRVariableDeclaration> variableNames;
	
	public IRExpression cond;
	public boolean hasFalse ;
	public IRScope trueScope ;
	public IRScope falseScope ;
	
}
