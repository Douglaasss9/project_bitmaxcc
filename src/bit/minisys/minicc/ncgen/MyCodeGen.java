package bit.minisys.minicc.ncgen;

import bit.minisys.minicc.parser.ast.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.symbol.*;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.ncgen.IMiniCCCodeGen;

import java.util.ArrayList;
import java.io.File;
import java.io.FileOutputStream;

public class MyCodeGen implements IMiniCCCodeGen{
	
	
	private StringBuilder MfuncCodes = new StringBuilder();
	private StringBuilder LfuncCodes = new StringBuilder();
	
	private SymbolTableStack symbolTableStack;
	private boolean firstFunction = true;
	private int offset = 0;
	private int parId = 0;
	private int labelId = -1;
	private String currentBreakTo = "";
	
    public MyCodeGen(){
    	symbolTableStack = new SymbolTableStack();
        symbolTableStack.appendSymbolTable("global");
    		
    }
    
    public void initCodes() {
    	MfuncCodes.append("\t\t").append("AREA gdata, DATA, READWRITE\n");
    }
    
    public void finishCodes() {
    	MfuncCodes.append("stop\n").append("\t\tB stop\n");
    	LfuncCodes.append("\nEND");
    }
    
    public String getLabel() {
    	labelId = labelId + 1;
    	return "LABEL" + labelId;
    }
    @Override
    public String run(String iFile, String out_file, MiniCCCfg cfg) throws Exception {
    	
    	System.out.println("------------------I'm there!---------------------");
    	
        System.out.println("filename:"+iFile);

        iFile = iFile.replace(MiniCCCfg.MINICC_ICGEN_OUTPUT_EXT, MiniCCCfg.MINICC_PARSER_OUTPUT_EXT);
        ObjectMapper mapper = new ObjectMapper();
        ASTCompilationUnit program = mapper.readValue(new File(iFile), ASTCompilationUnit.class);
        String oFile = MiniCCUtil.remove2Ext(iFile) + MiniCCCfg.MINICC_CODEGEN_OUTPUT_EXT;
        
        initCodes();
        compilationUnit(program);
        finishCodes();
        
        StringBuilder Codes = new StringBuilder();
        Codes.append(MfuncCodes).append(LfuncCodes);
        String ans = new String(Codes);
        
        FileOutputStream fileOutputStream = new FileOutputStream(oFile);
        fileOutputStream.write(ans.getBytes());
        fileOutputStream.close();
        
        System.out.println("6. ncGen finishedQ");
        return oFile;
    }
    
    private void compilationUnit(ASTCompilationUnit node) {
    	
    	for(ASTNode n: node.items) {
    		if (n instanceof ASTFunctionDefine) {
    			
    			if(firstFunction) {
    				MfuncCodes.append("\n\t\t").append("AREA function, CODE, READONLY\n")
   			 			 .append("\t\t").append("ENTRY\n").append("start\n");
    				firstFunction = false;
    			}
    			
    			offset = 0;
    			parId = 0;
    			functionDefine((ASTFunctionDefine) n);
    		}else if(n instanceof ASTDeclaration) {			    			
    			    			String varType = ((ASTDeclaration) n).specifiers.get(0).value;
    			
    			for (ASTInitList initList: ((ASTDeclaration) n).initLists) {
    				ASTDeclarator declarator = initList.declarator;
    				if (declarator instanceof ASTVariableDeclarator) {
    					    					String varName = ((ASTVariableDeclarator) declarator).identifier.value;
    					int initValue = 0;
                        if (initList.exprs.size() >= 1) {
                                                                                    initValue = ((ASTIntegerConstant) initList.exprs.get(0)).value;
                        }
                        symbolTableStack.addVariable(varName, varType, "global", -1);
                        
                        String name = "G_"+varName;
                        MfuncCodes.append(name).append("\t\t")
                        		  .append("BCD ").append(initValue).append("\n");
    				}else if(declarator instanceof ASTArrayDeclarator) {	
    					    					ArrayList<Integer> dims = new ArrayList<>();
    					ASTDeclarator it = declarator;
    					while (it instanceof ASTArrayDeclarator) {
    						ASTArrayDeclarator arrIt = (ASTArrayDeclarator) it;
                            int dim = ((ASTIntegerConstant) arrIt.expr).value;
                            dims.add(0, dim);

                            it = arrIt.declarator;
                        }
    					String arrName = ((ASTVariableDeclarator) it).identifier.value;
    					    					
    					String name = "G_" + arrName;
    					int dim = 1;
    					for(int i=0;i<dims.size();i++) {
    						dim = dim * dims.get(i);
    					}
    					MfuncCodes.append(name).append("\t\tBCD ");
    					for(int i=0;i<dim-1;i++) {
    						MfuncCodes.append("0, ");
    					}
    					MfuncCodes.append("0\n");
    				}
    			}
    			
    		}
    		
    	}
    	
    }
    
    private void functionDefine(ASTFunctionDefine node) {
    	
    	ASTFunctionDeclarator declarator = (ASTFunctionDeclarator) node.declarator;
    	
    	String functionName = ((ASTVariableDeclarator) declarator.declarator).identifier.value;
        String retType = node.specifiers.get(0).value;
        System.out.println(functionName);
        
        ArrayList<String> paramsType = new ArrayList<>();
        for (ASTParamsDeclarator paramsDeclarator: declarator.params) {
            if (paramsDeclarator.declarator instanceof ASTVariableDeclarator) {
                String paramType = paramsDeclarator.specfiers.get(0).value;
                paramsType.add(paramType);
            } else if (paramsDeclarator.declarator instanceof ASTArrayDeclarator) {
                            }
        }
        
                symbolTableStack.addFunction(functionName, retType, paramsType);

                symbolTableStack.appendSymbolTable(functionName);
        
        StringBuilder params = new StringBuilder();
        boolean flag = true;
             for (ASTParamsDeclarator paramsDeclarator: declarator.params) {
            String type = paramsDeclarator.specfiers.get(0).value;
            
            if (paramsDeclarator.declarator instanceof ASTVariableDeclarator) {
            	ASTVariableDeclarator variableDeclarator = (ASTVariableDeclarator) paramsDeclarator.declarator;
            	String name = variableDeclarator.identifier.value;
            	            
                symbolTableStack.addVariable(name, type, "param",parId);
                parId = parId + 1;
                /*
                String fullName = symbolTableStack.fullVariableName(name);
                if (flag) {
                    flag = false;
                    params.append(llvmType).append(" %").append(fullName);
                } else {
                    params.append(", ").append(llvmType).append(" %").append(fullName);
                }
                */            	
            }else if (paramsDeclarator.declarator instanceof ASTArrayDeclarator) {
                            }
            
        }
        
        if(functionName.equals("main")) {
        	MfuncCodes.append("main_\n");
        	MfuncCodes.append("\t\tpush {fp, lr}\n")
        			  .append("\t\tmov fp, sp\n");
        	
        	statement(node.body, true);
        	
        	MfuncCodes.append("\t\tadd sp, sp, #").append(-offset).append("\n")
        			  .append("\t\tpop {fp, pc}\n");
      
        }else {
        	LfuncCodes.append("\n").append(functionName).append("_\n");
        	LfuncCodes.append("\t\tpush {fp, lr}\n")
        			  .append("\t\tmov fp, sp\n");
        	
        	statement(node.body, false);
        	
        	LfuncCodes.append("\t\tadd sp, sp, #").append(-offset).append("\n")
			  		  .append("\t\tpop {fp, lr}\n")
			  		  .append("\t\tbx lr\n");
        }
        
        symbolTableStack.pop();
    }
    
    private void statement(ASTStatement node, boolean isMain) {
        if (node instanceof ASTCompoundStatement) {
        	compoundStatement((ASTCompoundStatement) node, isMain);
        }
        else if (node instanceof ASTExpressionStatement) {
            expression(((ASTExpressionStatement) node).exprs.get(0), isMain);
        }
        else if (node instanceof ASTSelectionStatement) {
            selectionStatement((ASTSelectionStatement) node, isMain);
        }
        else if (node instanceof ASTReturnStatement) {
            ASTReturnStatement nn = (ASTReturnStatement) node;
            expression(nn.expr.get(0), isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tmov r0, r2\n");
            }else {
            	LfuncCodes.append("\t\tmov r0, r2\n");
            }
 
        }
        else if (node instanceof ASTIterationStatement) {
            iterationStatement((ASTIterationStatement) node, isMain);
        }
        else if (node instanceof ASTLabeledStatement) {
                                              statement(((ASTLabeledStatement) node).stat, isMain);
        }
        else if (node instanceof ASTGotoStatement) {
                                          }
        else if (node instanceof ASTBreakStatement) {
            if(isMain) {
            	MfuncCodes.append("\t\tb ").append(currentBreakTo).append("\n");
            }else {
            	LfuncCodes.append("\t\tb ").append(currentBreakTo).append("\n");
            }
        }
        else if (node instanceof ASTContinueStatement) {
                               }
        else if (node instanceof ASTIterationDeclaredStatement) {
            iterationDeclaredStatement((ASTIterationDeclaredStatement) node, isMain);
        }
    }
    
    private  void compoundStatement(ASTCompoundStatement node, boolean isMain) {
    	
    	for (ASTNode n: node.blockItems) {
    		if (n instanceof ASTDeclaration) {
    			    			
    			ASTDeclaration nn = (ASTDeclaration) n;
    			String varType = nn.specifiers.get(0).value;
    			
    			for (ASTInitList initList: nn.initLists) {
    				ASTDeclarator declarator = initList.declarator;
    				if (declarator instanceof ASTVariableDeclarator) {
                        ASTVariableDeclarator dd = (ASTVariableDeclarator) declarator;
                        String varName = dd.identifier.value;
                                                offset = offset - 4;
                                                symbolTableStack.addVariable(varName, varType, "local", offset);
                        
                        if(isMain) {
                        	MfuncCodes.append("\t\tsub sp, sp, #4\n");
                        }
                        else {
                        	LfuncCodes.append("\t\tsub sp, sp, #4\n");
                        }
                        
                                                if (initList.exprs.size() != 0) {
                        	ASTExpression initValue = initList.exprs.get(0);
                        	
                            expression(initValue, isMain);
                            
                            VariableSymbol var = symbolTableStack.getVariable(varName);
                            if(isMain) {
                    			MfuncCodes.append("\t\tstr r2, [fp, #").append(var.offset)
                    					  .append("]\n");		  
                    		}else {
                    			LfuncCodes.append("\t\tstr r2, [fp, #").append(var.offset)
                				  .append("]\n");
                    		}
                        }
                        
    				}else if (declarator instanceof ASTArrayDeclarator) {
    					
    					    				}
    			}
    		}else if (n instanceof ASTStatement) {
                statement((ASTStatement) n, isMain);
            }
    	}
    }
    
    private void selectionStatement(ASTSelectionStatement node, boolean isMain) {
    	
    	expression(node.cond.get(0), isMain);
    	
    	String lb1 = getLabel();
        String lb2 = getLabel();
        
    	if(isMain) {
    		MfuncCodes.append("\t\tcmp r2, 0\n")
    				  .append("\t\tblg ").append(lb1).append("\n");
    	}else {
    		LfuncCodes.append("\t\tcmp r2, 0\n")
    				  .append("\t\tblg ").append(lb1).append("\n");
    	}
    	
    	if (node.otherwise != null) {
            statement(node.otherwise, isMain);
        }
    	
    	if(isMain) {
    		MfuncCodes.append("\t\tb ").append(lb2).append("\n");
    		MfuncCodes.append(lb1).append("\n");
    	}else {
    		LfuncCodes.append("\t\tb ").append(lb2).append("\n");
    		LfuncCodes.append(lb1).append("\n");
    	}
    	
    	statement(node.then, isMain);
    	
    	if(isMain) {
    		MfuncCodes.append(lb2).append("\n");
    	}else {
    		LfuncCodes.append(lb2).append("\n");
    	}
    }
    
    private void iterationStatement(ASTIterationStatement node, boolean isMain) {
    	
    	for (ASTExpression e: node.init) {
            expression(e, isMain);
        }

        String body = getLabel();
        String next = getLabel();
        String end = getLabel();
        
        if(isMain) {
        	MfuncCodes.append("\t\tb ").append(next).append("\n");
        	MfuncCodes.append(body).append("\n");
        }else {
        	LfuncCodes.append("\t\tb ").append(next).append("\n");
        	LfuncCodes.append(body).append("\n");
        }
        
        
        currentBreakTo = end;
        statement(node.stat, isMain);

        for (ASTExpression e: node.step) {
            expression(e, isMain);
        }
        
        if(isMain) {
        	MfuncCodes.append(next).append("\n");
        }else {
        	LfuncCodes.append(next).append("\n");
        }
        
        expression(node.cond.get(0), isMain);
        
        if(isMain) {
        	MfuncCodes.append("\t\tcmp r2, 0\n");
        	MfuncCodes.append("\t\tbgt r2, 0\n");
        	MfuncCodes.append(end).append("\n");
        }else {
        	LfuncCodes.append("\t\tcmp r2, 0\n");
        	LfuncCodes.append("\t\tbgt r2, 0\n");
        	LfuncCodes.append(end).append("\n");
        }
    }
    
    private void iterationDeclaredStatement(ASTIterationDeclaredStatement node, boolean isMain) {
    	    }
    
    private void expression(ASTExpression node, boolean isMain) {
        if (node instanceof ASTIntegerConstant) {
        	
        	int value = ((ASTIntegerConstant)node).value;
        	if(isMain) {
                MfuncCodes.append(String.format("\t\tmov r2, #%d\n", value));
        	}else {
        		LfuncCodes.append(String.format("\t\tmov r2, #%d\n", value));
        	}
        }
        else if (node instanceof ASTIdentifier) {
        	
        	String varName = ((ASTIdentifier)node).value;
        	VariableSymbol var = symbolTableStack.getVariable(varName);
        	
        	if(var.flag.contentEquals("global")) {
        		if(isMain) {
        			MfuncCodes.append("\t\tldr r2, =G_").append(var.text)
        					  .append("\n");		  
        		}else {
        			LfuncCodes.append("\t\tldr r2, =G_").append(var.text)
					  .append("\n");
        		}
        	}else if(var.flag.contentEquals("local")){
        		if(isMain) {
        			MfuncCodes.append("\t\tldr r2, [fp, #").append(var.offset)
        					  .append("]\n");		  
        		}else {
        			LfuncCodes.append("\t\tldr r2, [fp, #").append(var.offset)
					  .append("]\n");
        		}
        	}else if(var.flag.contentEquals("param")) {
        		if(isMain) {
        			MfuncCodes.append("\t\tmov r2, r").append(var.offset)
        					  .append("\n");		  
        		}else {
        			LfuncCodes.append("\t\tmov r2, r").append(var.offset)
					  		  .append("\n");
        		}
        	}
            
        }
        else if (node instanceof ASTBinaryExpression) {
            binaryExpression((ASTBinaryExpression) node, isMain);
        }
        else if (node instanceof ASTPostfixExpression) {
            postfixExpression((ASTPostfixExpression) node, isMain);
        }
        else if (node instanceof ASTUnaryExpression) {
                    }
        else if (node instanceof ASTConditionExpression) {
                    }
        else if (node instanceof ASTFunctionCall) {
            functionCall((ASTFunctionCall) node, isMain);
        }
        else if (node instanceof ASTArrayAccess) {
                    }
            }
    
    public void binaryExpression(ASTBinaryExpression node, boolean isMain) {
        String op = node.op.value;

        if (op.equals("=")) {
            assignmentExpression(node, isMain);
            return;
        }

        if (op.equals("+")) {
            expression(node.expr2, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpush {r2}\n");
            }else {
            	LfuncCodes.append("\t\tpush {r2}\n"); 			
            }
           
            expression(node.expr1, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpop {r3}\n");
            	MfuncCodes.append("\t\tadd r2, r2, r3\n");
            }else {
            	LfuncCodes.append("\t\tpop {r3}\n");
            	LfuncCodes.append("\t\tadd r2, r2, r3\n");
            }
            
        }
        
        else if (op.equals("-")) {
            expression(node.expr2, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpush {r2}\n");
            }else {
            	LfuncCodes.append("\t\tpush {r2}\n"); 			
            }
           
            expression(node.expr1, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpop {r3}\n");
            	MfuncCodes.append("\t\tsub r2, r2, r3\n");
            }else {
            	LfuncCodes.append("\t\tpop {r3}\n");
            	LfuncCodes.append("\t\tsub r2, r2, r3\n");
            }
        }
        else if (op.equals("*")) {
        	expression(node.expr2, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpush {r2}\n");
            }else {
            	LfuncCodes.append("\t\tpush {r2}\n"); 			
            }
           
            expression(node.expr1, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpop {r3}\n");
            	MfuncCodes.append("\t\tmul r2, r2, r3\n");
            }else {
            	LfuncCodes.append("\t\tpop {r3}\n");
            	LfuncCodes.append("\t\tmul r2, r2, r3\n");
            }
        }
        else if (op.equals("<")) {
            expression(node.expr2, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpush {r2}\n");
            }else {
            	LfuncCodes.append("\t\tpush {r2}\n"); 			
            }
            
            expression(node.expr1, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpop {r3}\n");
            }else {
            	LfuncCodes.append("\t\tpop {r3}\n");
            }
            
            if(isMain) {
            	MfuncCodes.append("\t\tcmp r2, r3\n")
            			  .append("\t\tmov r2, #1\n");
            	String lb = getLabel();
            	MfuncCodes.append("\t\tblt ").append(lb).append("\n")
            			  .append("\t\tmov r2, #0\n")
            			  .append(lb).append("\n");
            }else {
            	LfuncCodes.append("\t\tcmp r2, r3\n")
            			  .append("\t\tmov r2, #1\n");
            	String lb = getLabel();
            	LfuncCodes.append("\t\tblt ").append(lb).append("\n")
            			  .append("\t\tmov r2, #0\n")
            			  .append(lb).append("\n");
            }
        }   
        else if (op.equals(">")) {
            expression(node.expr2, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpush {r2}\n");
            }else {
            	LfuncCodes.append("\t\tpush {r2}\n"); 			
            }
            
            expression(node.expr1, isMain);
            
            if(isMain) {
            	MfuncCodes.append("\t\tpop {r3}\n");
            }else {
            	LfuncCodes.append("\t\tpop {r3}\n");
            }
            
            if(isMain) {
            	MfuncCodes.append("\t\tcmp r2, r3\n")
            			  .append("\t\tmov r2, #1\n");
            	String lb = getLabel();
            	MfuncCodes.append("\t\tbgt ").append(lb).append("\n")
            			  .append("\t\tmov r2, #0\n")
            			  .append(lb).append("\n");
            }else {
            	LfuncCodes.append("\t\tcmp r2, r3\n")
            			  .append("\t\tmov r2, #1\n");
            	String lb = getLabel();
            	LfuncCodes.append("\t\tbgt ").append(lb).append("\n")
            			  .append("\t\tmov r2, #0\n")
            			  .append(lb).append("\n");
            }
        }      
    }
    
    public void assignmentExpression(ASTBinaryExpression node, boolean isMain) {
        ASTIdentifier identifier = (ASTIdentifier) node.expr1;
         
        String varName = identifier.value;
        VariableSymbol var = symbolTableStack.getVariable(varName);
        
        expression(node.expr2, isMain);
        
        if(var.flag.contentEquals("global")) {
    		if(isMain) {
    			MfuncCodes.append("\t\tstr r2, =G_").append(var.text)
    					  .append("\n");		  
    		}else {
    			LfuncCodes.append("\t\tstr r2, =G_").append(var.text)
				  .append("\n");
    		}
    	}else if(var.flag.contentEquals("local")){
    		if(isMain) {
    			MfuncCodes.append("\t\tstr r2, [fp, #").append(var.offset)
    					  .append("]\n");		  
    		}else {
    			LfuncCodes.append("\t\tstr r2, [fp, #").append(var.offset)
				  .append("]\n");
    		}
    	}else if(var.flag.contentEquals("param")) {
    		if(isMain) {
    			MfuncCodes.append("\t\tstr r2, r").append(var.offset)
    					  .append("\n");		  
    		}else {
    			LfuncCodes.append("\t\tstr r2, r").append(var.offset)
				  		  .append("\n");
    		}
    	}
       
    }
    
    public void postfixExpression(ASTPostfixExpression node, boolean isMain) {
        String op = node.op.value;
        String varName = ((ASTIdentifier) node.expr).value;
        
        VariableSymbol var = symbolTableStack.getVariable(varName);
 
        if (op.equals("++")) {
        	if(var.flag.contentEquals("global")) {
        		if(isMain) {
        			MfuncCodes.append("\t\tstr r2, =G_").append(var.text)
        					  .append("\n");		  
        		}else {
        			LfuncCodes.append("\t\tstr r2, =G_").append(var.text)
    				  .append("\n");
        		}
        	}else if(var.flag.contentEquals("local")){
        		if(isMain) {
        			MfuncCodes.append("\t\tldr r2, [fp, #").append(var.offset)
        					  .append("]\n");
        			MfuncCodes.append("\t\tadd r2, r2, #1\n")
        					  .append("\t\tstr r2, [fp, #").append(var.offset)
        					  .append("]\n");
        			
        		}else {
        			LfuncCodes.append("\t\tldr r2, [fp, #").append(var.offset)
					  		  .append("]\n");
        			LfuncCodes.append("\t\tadd r2, r2, #1\n")
        					  .append("\t\tstr r2, [fp, #").append(var.offset)
        					  .append("]\n");
        		}
        	}else if(var.flag.contentEquals("param")) {
        		if(isMain) {
        			MfuncCodes.append("\t\tadd r").append(var.offset)
        					  .append(", r").append(var.offset).append(", #1\n");		  
        		}else {
        			LfuncCodes.append("\t\tadd r").append(var.offset)
					  		  .append(", r").append(var.offset).append(", #1\n");	
        		}
        	}
        }
    }
    
    public void functionCall(ASTFunctionCall node, boolean isMain) {
        String funcName = ((ASTIdentifier) node.funcname).value;

        for (int i = 0; i < node.argList.size(); i++) {
            expression(node.argList.get(i), isMain);
            if(isMain){
            	MfuncCodes.append(String.format("\t\tmov r%d, r2\n",i));
            }else {
            	LfuncCodes.append(String.format("\t\tmov r%d, r2\n",i));
            }
        }
        
        if(isMain){
        	MfuncCodes.append("\t\tbl ").append(funcName).append("_\n")
        			  .append("\t\tmov r2, r0\n");
        }else {
        	LfuncCodes.append("\t\tbl ").append(funcName).append("_\n")
        			  .append("\t\tmov r2, r0\n");
        }
    }
}
