package bit.minisys.minicc.ncgen;

import bit.minisys.minicc.parser.ast.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.symbol.SymbolTableStack;
import bit.minisys.minicc.symbol.VariableSymbol;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.ncgen.IMiniCCCodeGen;
import bit.minisys.minicc.ir.*;

import java.util.ArrayList;
import java.io.File;
import java.io.FileOutputStream;

public class NCGenHelper {
	private StringBuilder MfuncCodes = new StringBuilder();
	private StringBuilder LfuncCodes = new StringBuilder();
	
	private SymbolTableStack symbolTableStack;
	private boolean firstFunction = true;
	private int offset = 0;
	private int parId = 0;
	private int labelId = -1;
	private String currentBreakTo = "";
	
	public NCGenHelper() {
        symbolTableStack = new SymbolTableStack();
        symbolTableStack.appendSymbolTable("global");
    }
	public StringBuilder getCodes() {
		StringBuilder codes = new StringBuilder();
		codes.append(MfuncCodes).append(LfuncCodes);
		return codes;
	}
	
	public void addCodes(String sb, boolean isMain) {
		if(isMain) {
			MfuncCodes.append(sb);
		}else {
			LfuncCodes.append(sb);
		}
	}
	public String getLabel() {
    	labelId = labelId + 1;
    	return "LABEL" + labelId;
    }
	
	public void compilationUnit(IRCompilationUnit cu) {
		
		MfuncCodes.append("\t\t").append("AREA gdata, DATA, READWRITE\n");
				for(int i = 0; i < cu.variableNames.size(); i++) {
			IRVariableDeclaration var = cu.variableNames.get(i);
			
			String varType = var.varType;
			String varName = var.varName;
			int initValue = var.value;
			symbolTableStack.addVariable(varName, varType, "global", -1);
			
			String name = "G_" + varName;
			MfuncCodes.append(name).append("\t\t")
  		  			  .append("BCD ").append(initValue).append("\n");
		}
		
				for(int i = 0; i < cu.arrayNames.size(); i++) {
			
		}
		
				for(int i = 0; i < cu.functionNames.size(); i++) {
			if(firstFunction) {
				MfuncCodes.append("\n\t\t").append("AREA function, CODE, READONLY\n")
			 			 .append("\t\t").append("ENTRY\n").append("start\n");
				firstFunction = false;
			}
			
			IRFunction func = cu.functionNames.get(i); 
			
			offset = 0;
			parId = 0;
			
			String functionName = func.funcName;
			String retType = func.returnType;
			ArrayList<String> paramsType = new ArrayList<>();
			for (IRVariableDeclaration par: func.paramList) {
	        		        	paramsType.add(func.paramList.get(i).varType);
	        }
			
						symbolTableStack.addFunction(functionName, retType, paramsType);

	        			symbolTableStack.appendSymbolTable(functionName);
	        
	        	        StringBuilder params = new StringBuilder();
	        boolean flag = true;				
	        for (IRVariableDeclaration par: func.paramList) {
	        	String type = par.varType;
	        	String name = par.varName;
	        	symbolTableStack.addVariable(name, type, "param", parId);
	        	parId = parId + 1;
	        	 	
	        }
	        
	        if(functionName.equals("main")) {
	        	MfuncCodes.append("main_\n");
	        	MfuncCodes.append("\t\tpush {fp, lr}\n")
	        			  .append("\t\tmov fp, sp\n");
	        	
	        	scope(func.body, true);
	        	
	        	MfuncCodes.append("\t\tadd sp, sp, #").append(-offset).append("\n")
	        			  .append("\t\tpop {fp, pc}\n");
	      
	        }else {
	        	LfuncCodes.append("\n").append(functionName).append("_\n");
	        	LfuncCodes.append("\t\tpush {fp, lr}\n")
	        			  .append("\t\tmov fp, sp\n");
	        	
	        	scope(func.body, false);
	        	
	        	LfuncCodes.append("\t\tadd sp, sp, #").append(-offset).append("\n")
				  		  .append("\t\tpop {fp, lr}\n")
				  		  .append("\t\tbx lr\n");
	        }
	        
	        symbolTableStack.pop();
		}
		
		MfuncCodes.append("stop\n").append("\t\tB stop\n");
    	LfuncCodes.append("\nEND");
	}
	
	public void scope(IRScope sc, boolean isMain) {
		
				for(int i = 0; i < sc.variableNames.size(); i++) {
			
			IRVariableDeclaration var = sc.variableNames.get(i);
			
			String varType = var.varType;
			String varName = var.varName;
			offset = offset - 4;
			var.offset = offset;
			            symbolTableStack.addVariable(varName, varType, "local", offset);
            
            if(isMain) {
            	MfuncCodes.append("\t\tsub sp, sp, #4\n");
            }
            else {
            	LfuncCodes.append("\t\tsub sp, sp, #4\n");
            }
            
            if(var.initExpr != null) {
            	expression(var.initExpr, isMain);
            	
            	VariableSymbol variable = symbolTableStack.getVariable(varName);
                if(isMain) {
        			MfuncCodes.append("\t\tstr r6, [fp, #").append(variable.offset)
        					  .append("]\n");		  
        		}else {
        			LfuncCodes.append("\t\tstr r6, [fp, #").append(variable.offset)
    				  .append("]\n");
        		}
            }
		}
		
		for(int i = 0; i < sc.statements.size(); i++) {
			
			IRStatement state = sc.statements.get(i);
			
			if(state.statType.equals("return")) {
				
	            expression(state.returnStat.expr, isMain);
	            
	            addCodes("\t\tmov r0, r6\n", isMain);
			}
			else if(state.statType.contentEquals("expr")) {
				expression(state.exprStat.expr, isMain);
			}
			else if(state.statType.equals("select")) {
				
				expression(state.selectStat.cond, isMain);
				String lb1 = getLabel();
		        String lb2 = getLabel();
		        
		        addCodes("\t\tcmp r6, 0\n", isMain);
		        addCodes("\t\tblg "+lb1+"\n", isMain);
		        
		        if (state.selectStat.falseScope != null) {
		            scope(state.selectStat.falseScope, isMain);
		        }
		        
		        addCodes("\t\tb "+lb2+"\n", isMain);
		        addCodes(lb1+"\n", isMain);
		        
		        scope(state.selectStat.trueScope, isMain);
		        
		        addCodes(lb2+"\n", isMain);
    	
			}
			else if(state.statType.equals("iterate")) {
				
				for(int j = 0; j < state.iterateStat.varNames.size(); j++) {
					IRVariableDeclaration var = state.iterateStat.varNames.get(j);
					String varType = var.varType;
					String varName = var.varName;
					offset = offset - 4;
					var.offset = offset;
							            symbolTableStack.addVariable(varName, varType, "local", offset);
		            
		            addCodes("\t\tsub sp, sp, #4\n", isMain);
		            
		            if(var.initExpr != null) {
		            	expression(var.initExpr, isMain);
		            	
		            	VariableSymbol variable = symbolTableStack.getVariable(varName);
		                addCodes("\t\tstr r6, [fp, #"+variable.offset+"]\n", isMain);
		            }
				}
				
		        String body = getLabel();
		        String next = getLabel();
		        String end = getLabel();
		        
		        addCodes("\t\tb "+next+"\n", isMain);
		        addCodes(body+"\n", isMain);
		        
		        
		        currentBreakTo = end;
		        scope(state.iterateStat.body, isMain);
		        
		        expression(state.iterateStat.step, isMain);
		        
		        addCodes(next+"\n",isMain);
		        
		        expression(state.iterateStat.cond, isMain);
		        
		        addCodes("\t\tcmp r6, 0\n", isMain);
		        addCodes("\t\tbgt "+body+"\n", isMain);
		        addCodes(end+"\n", isMain);
		        
			}
			
		}
	}
	
	public void expression(IRExpression expr, boolean isMain) {
		
		if(expr.exprType.equals("const")) {
			int value = expr.constval.value;
        	if(isMain) {
                MfuncCodes.append(String.format("\t\tmov r6, #%d\n", value));
        	}else {
        		LfuncCodes.append(String.format("\t\tmov r6, #%d\n", value));
        	}
		}
		else if(expr.exprType.equals("var")) {
			String varName = expr.var.varName;
        	VariableSymbol var = symbolTableStack.getVariable(varName);
        	
        	if(var.flag.contentEquals("global")) {
        		if(isMain) {
        			MfuncCodes.append("\t\tldr r6, =G_").append(var.text)
        					  .append("\n");		  
        		}else {
        			LfuncCodes.append("\t\tldr r6, =G_").append(var.text)
					  .append("\n");
        		}
        	}else if(var.flag.contentEquals("local")){
        		if(isMain) {
        			MfuncCodes.append("\t\tldr r6, [fp, #").append(var.offset)
        					  .append("]\n");		  
        		}else {
        			LfuncCodes.append("\t\tldr r6, [fp, #").append(var.offset)
					  .append("]\n");
        		}
        	}else if(var.flag.contentEquals("param")) {
        		if(isMain) {
        			MfuncCodes.append("\t\tmov r6, r").append(var.offset)
        					  .append("\n");		  
        		}else {
        			LfuncCodes.append("\t\tmov r6, r").append(var.offset)
					  		  .append("\n");
        		}
        	}
		}
		else if(expr.exprType.equals("binary")) {
			
			String op = expr.op;

	        if (op.equals("=")) {
	        	 
	             String varName = expr.opnd0.var.varName;	      
	             VariableSymbol var = symbolTableStack.getVariable(varName);
	             
	             expression(expr.opnd1, isMain);
	             
	             if(var.flag.contentEquals("global")) {
	         		if(isMain) {
	         			MfuncCodes.append("\t\tstr r6, =G_").append(var.text)
	         					  .append("\n");		  
	         		}else {
	         			LfuncCodes.append("\t\tstr r6, =G_").append(var.text)
	     				  		  .append("\n");
	         		}
	         	}else if(var.flag.contentEquals("local")){
	         		if(isMain) {
	         			MfuncCodes.append("\t\tstr r6, [fp, #").append(var.offset)
	         					  .append("]\n");		  
	         		}else {
	         			LfuncCodes.append("\t\tstr r6, [fp, #").append(var.offset)
	     				  		  .append("]\n");
	         		}
	         	}else if(var.flag.contentEquals("param")) {
	         		if(isMain) {
	         			MfuncCodes.append("\t\tstr r6, r").append(var.offset)
	         					  .append("\n");		  
	         		}else {
	         			LfuncCodes.append("\t\tstr r6, r").append(var.offset)
	     				  		  .append("\n");
	         		}
	         	}
	            return;
	        }

	        if (op.equals("+")) {
	            expression(expr.opnd1, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpush {r6}\n");
	            }else {
	            	LfuncCodes.append("\t\tpush {r6}\n"); 			
	            }
	           
	            expression(expr.opnd0, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpop {r7}\n");
	            	MfuncCodes.append("\t\tadd r6, r6, r7\n");
	            }else {
	            	LfuncCodes.append("\t\tpop {r7}\n");
	            	LfuncCodes.append("\t\tadd r6, r6, r7\n");
	            }
	            
	        }
	        
	        else if (op.equals("-")) {
	            expression(expr.opnd1, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpush {r6}\n");
	            }else {
	            	LfuncCodes.append("\t\tpush {r6}\n"); 			
	            }
	           
	            expression(expr.opnd0, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpop {r7}\n");
	            	MfuncCodes.append("\t\tsub r6, r6, r7\n");
	            }else {
	            	LfuncCodes.append("\t\tpop {r7}\n");
	            	LfuncCodes.append("\t\tsub r6, r6, r7\n");
	            }
	        }
	        else if (op.equals("*")) {
	        	expression(expr.opnd1, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpush {r6}\n");
	            }else {
	            	LfuncCodes.append("\t\tpush {r6}\n"); 			
	            }
	           
	            expression(expr.opnd0, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpop {r7}\n");
	            	MfuncCodes.append("\t\tmul r6, r6, r7\n");
	            }else {
	            	LfuncCodes.append("\t\tpop {r7}\n");
	            	LfuncCodes.append("\t\tmul r6, r6, r7\n");
	            }
	        }
	        else if (op.equals("<")) {
	            expression(expr.opnd1, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpush {r6}\n");
	            }else {
	            	LfuncCodes.append("\t\tpush {r6}\n"); 			
	            }
	            
	            expression(expr.opnd0, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpop {r7}\n");
	            }else {
	            	LfuncCodes.append("\t\tpop {r7}\n");
	            }
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tcmp r6, r7\n")
	            			  .append("\t\tmov r6, #1\n");
	            	String lb = getLabel();
	            	MfuncCodes.append("\t\tblt ").append(lb).append("\n")
	            			  .append("\t\tmov r6, #0\n")
	            			  .append(lb).append("\n");
	            }else {
	            	LfuncCodes.append("\t\tcmp r6, r7\n")
	            			  .append("\t\tmov r6, #1\n");
	            	String lb = getLabel();
	            	LfuncCodes.append("\t\tblt ").append(lb).append("\n")
	            			  .append("\t\tmov r6, #0\n")
	            			  .append(lb).append("\n");
	            }
	        }   
	        else if (op.equals(">")) {
	            expression(expr.opnd1, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpush {r6}\n");
	            }else {
	            	LfuncCodes.append("\t\tpush {r6}\n"); 			
	            }
	            
	            expression(expr.opnd0, isMain);
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tpop {r7}\n");
	            }else {
	            	LfuncCodes.append("\t\tpop {r7}\n");
	            }
	            
	            if(isMain) {
	            	MfuncCodes.append("\t\tcmp r6, r7\n")
	            			  .append("\t\tmov r6, #1\n");
	            	String lb = getLabel();
	            	MfuncCodes.append("\t\tbgt ").append(lb).append("\n")
	            			  .append("\t\tmov r6, #0\n")
	            			  .append(lb).append("\n");
	            }else {
	            	LfuncCodes.append("\t\tcmp r6, r7\n")
	            			  .append("\t\tmov r6, #1\n");
	            	String lb = getLabel();
	            	LfuncCodes.append("\t\tbgt ").append(lb).append("\n")
	            			  .append("\t\tmov r6, #0\n")
	            			  .append(lb).append("\n");
	            }
	        }      
		}
		else if(expr.exprType.equals("postfix")) {
			
			String op = expr.op;
			String varName = expr.var.varName;
			VariableSymbol var = symbolTableStack.getVariable(varName);
			
			if(op.equals("++")) {
				if(var.flag.equals("global")) {
					if(isMain) {
						MfuncCodes.append("\t\tldr r6, =G_").append(varName)
								  .append("\n")
								  .append("\t\tadd r6, r6, #1\n")
								  .append("\t\tstr r6, =G_").append(varName)
								  .append("\n");
					}else {
						LfuncCodes.append("\t\tldr r6, =G_").append(varName)
						  .append("\n")
						  .append("\t\tadd r6, r6, 1\n")
						  .append("\t\tstr r6, =G_").append(varName)
						  .append("\n");
					}
				}else if(var.flag.contentEquals("local")){
	        		if(isMain) {
	        			MfuncCodes.append("\t\tldr r6, [fp, #").append(var.offset)
	        					  .append("]\n");
	        			MfuncCodes.append("\t\tadd r6, r6, #1\n")
	        					  .append("\t\tstr r6, [fp, #").append(var.offset)
	        					  .append("]\n");
	        			
	        		}else {
	        			LfuncCodes.append("\t\tldr r6, [fp, #").append(var.offset)
						  		  .append("]\n");
	        			LfuncCodes.append("\t\tadd r6, r6, #1\n")
	        					  .append("\t\tstr r6, [fp, #").append(var.offset)
	        					  .append("]\n");
	        		}
	        	}else if(var.flag.contentEquals("param")) {
	        		if(isMain) {
	        			MfuncCodes.append("\t\tadd r").append(var.offset)
	        					  .append(", r").append(var.offset).append(", #1\n");		  
	        		}else {
	        			LfuncCodes.append("\t\tadd r").append(var.offset)
						  		  .append(", r").append(var.offset).append(", #1\n");	
	        		}
	        	}
			}
			else if(op.equals("--")) {
				if(var.flag.equals("global")) {
					if(isMain) {
						MfuncCodes.append("\t\tldr r6, =G_").append(varName)
								  .append("\n")
								  .append("\t\tsub r6, r6, #1\n")
								  .append("\t\tstr r6, =G_").append(varName)
								  .append("\n");
					}else {
						LfuncCodes.append("\t\tldr r6, =G_").append(varName)
						  .append("\n")
						  .append("\t\tsub r6, r6, 1\n")
						  .append("\t\tstr r6, =G_").append(varName)
						  .append("\n");
					}
				}else if(var.flag.contentEquals("local")){
	        		if(isMain) {
	        			MfuncCodes.append("\t\tldr r6, [fp, #").append(var.offset)
	        					  .append("]\n");
	        			MfuncCodes.append("\t\tsub r6, r6, #1\n")
	        					  .append("\t\tstr r6, [fp, #").append(var.offset)
	        					  .append("]\n");
	        			
	        		}else {
	        			LfuncCodes.append("\t\tldr r6, [fp, #").append(var.offset)
						  		  .append("]\n");
	        			LfuncCodes.append("\t\tsub r6, r6, #1\n")
	        					  .append("\t\tstr r6, [fp, #").append(var.offset)
	        					  .append("]\n");
	        		}
	        	}else if(var.flag.contentEquals("param")) {
	        		if(isMain) {
	        			MfuncCodes.append("\t\tsub r").append(var.offset)
	        					  .append(", r").append(var.offset).append(", #1\n");		  
	        		}else {
	        			LfuncCodes.append("\t\tsub r").append(var.offset)
						  		  .append(", r").append(var.offset).append(", #1\n");	
	        		}
	        	}
			}
			
		}
		else if(expr.exprType.equals("call")) {
			
			String funcName = expr.call.funcName;
			
			for(int j = 0; j < expr.call.paramList.size(); j++) {
	            expression(expr.call.paramList.get(j), isMain);
	            if(isMain){
	            	MfuncCodes.append(String.format("\t\tmov r%d, r6\n",j));
	            }else {
	            	LfuncCodes.append(String.format("\t\tmov r%d, r6\n",j));
	            }
	        }
	        
	        if(isMain){
	        	MfuncCodes.append("\t\tbl ").append(funcName).append("_\n")
	        			  .append("\t\tmov r6, r0\n");
	        }else {
	        	LfuncCodes.append("\t\tbl ").append(funcName).append("_\n")
	        			  .append("\t\tmov r6, r0\n");
	        }
		}
	}
	
}
