package bit.minisys.minicc.ncgen.internal;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.python.antlr.PythonParser.return_stmt_return;
import org.python.modules.jffi.AllocatedDirectMemory;

import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.icgen.internal.IROpVisitor;
import bit.minisys.minicc.icgen.internal.LIRBuilder;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.BasicBlock;
import bit.minisys.minicc.internal.ir.IRCall;
import bit.minisys.minicc.internal.ir.IRConstval;
import bit.minisys.minicc.internal.ir.IRDassign;
import bit.minisys.minicc.internal.ir.IRFunctionDeclaration;
import bit.minisys.minicc.internal.ir.IRIassign;
import bit.minisys.minicc.internal.ir.IRIassignfpoff;
import bit.minisys.minicc.internal.ir.IRInstruction;
import bit.minisys.minicc.internal.ir.IRIread;
import bit.minisys.minicc.internal.ir.IRIreadfpoff;
import bit.minisys.minicc.internal.ir.VirtualReg;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;

// 图着色寄存器分配器
public class GraphColoring {
	
	private Map<VirtualReg, VirtualRegProperty> globalVarPropMap;
	private Integer nowOffset;	
	private Integer regNum;	// phy reg num
	private Set<VirtualReg> hasSpilled;
	private Map<VirtualReg, Integer> regMap;
	private LinkedList<IRInstruction> realInstrs;
	private Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> instrRevMap;	// 记录从IR转换至汇编时每条IR中的vReg和phyReg/stackslot的对应关系
	private Integer paramNumInStack = 0;
	private Boolean stdcall = false;
	public boolean findCall = false;
	public Integer argMoreSize = 0;

	public Integer getNowOffset() {
		return nowOffset;
	}
	public LinkedList<IRInstruction> getRealInstrs() {
		return realInstrs;
	}
	public Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> getInstrRevMap() {
		return instrRevMap;
	}
	
	private Map<VirtualReg,GraphNode> graph;	// 冲突图
	
	public GraphColoring(Integer regNum,Integer paramNumInStack,Map<VirtualReg, VirtualRegProperty> globalVarPropMap,Boolean stdcall) {
		this.graph = new HashMap<VirtualReg, GraphNode>();
		this.regNum = regNum;
		this.paramNumInStack = paramNumInStack;
		this.globalVarPropMap = globalVarPropMap;
		this.stdcall = stdcall;
	}
	
	public void run(IRFunctionDeclaration decl) {
		// 3. 根据可用物理寄存器数目，进行颜色分配
		this.nowOffset = decl.getFarOffset();
		this.hasSpilled = new HashSet<VirtualReg>();
		this.realInstrs = new LinkedList<IRInstruction>();
		this.instrRevMap = new HashMap<IRInstruction, Map<VirtualReg,PhysicsRegisiter>>();
		alloc(decl);
	}
	private void createGraph(IRFunctionDeclaration decl) {

		graph.clear();
		LivenessAnalysis la = new LivenessAnalysis();
		la.run(decl.getStartBB());
		// 2. 根据上述的每个指令的LiveOut集合，构件图
		// 2.1 从开始基本块出发，对每一条指令取出LiveOut集合
		BasicBlock bb = decl.getStartBB();
		IRInstruction instr = bb.getFirst();
		while (instr != null) {
			Set<VirtualReg> liveOut = instr.getLiveOut();
			List<VirtualReg> regs = new LinkedList<VirtualReg>();
			if (liveOut!=null) {
				regs.addAll(liveOut);
				if(liveOut.size() == 0) {
					// 说明是无效赋值，这个语句不用执行寄存器分配，到时候删去即可
					
				}else if (liveOut.size() == 1) {
					// 只有一个
					if (!graph.containsKey(regs.get(0))) {
						GraphNode gNode = new GraphNode(new HashSet<VirtualReg>());
						graph.put(regs.get(0), gNode);
					}
				}else {
					for (int i = 0; i < regs.size()-1; i++) {
						VirtualReg r1 = regs.get(i);	
						for (int j = i+1; j < regs.size(); j++) {
							VirtualReg r2 = regs.get(j);
							if (graph.containsKey(r1)) {
								graph.get(r1).add(r2);
							}else {
								GraphNode gNode = new GraphNode(r2);
								graph.put(r1, gNode);
							}
							if (graph.containsKey(r2)) {
								graph.get(r2).add(r1);
							}else {
								GraphNode gNode = new GraphNode(r1);
								graph.put(r2, gNode);
							}
						}
					}
				}
			}
			instr = instr.getNext();
		}
	}
	private Integer getDifferentColor(Set<Integer> colors) {
		Integer res = -1;
		for (int i = 0; i < regNum; i++) {
			if (!colors.contains(i)) {
				res = i;
				break;
			}
		}
		return res;
		
	}
	private void alloc(IRFunctionDeclaration decl) {
		while(true) {
			// 1. 活跃变量分析，计算每条指令的liveOut (这里需要提供基本块，考虑后续增加了指令之后的指令衔接问题（分特殊情况
			createGraph(decl);
			// 输出一下这个图
		//	outputGraph();
			
			
			LinkedList<VirtualReg> stack = new LinkedList<VirtualReg>();
			LinkedList<VirtualReg> others = new LinkedList<VirtualReg>();
			Boolean flag = false;
			//Set<VirtualReg> visited = new HashSet<VirtualReg>();
			// 1.5 从graph中不断找出所有边数<n的点，然后从图中该点和对应的边，并入栈
			for (Entry<VirtualReg, GraphNode> node : graph.entrySet()) {
				VirtualReg from = node.getKey();
				if (stack.contains(from)) {
					continue;
				}
				Set<VirtualReg> tos = node.getValue().getToRegs();
				Integer size = tos.size();
				//visited.add(from);
				for (VirtualReg virtualReg : tos) {
					if (stack.contains(virtualReg)) {
						size --;
					}
				}
				if (size < regNum) {
					stack.add(from);
				}
			}
			// 1.6 graph中剩余的都是边数>=regnum的vreg,需要一个一个入栈
			while(stack.size() < graph.size()) {
				Integer num = 0;
				VirtualReg goal = null;
				for (Entry<VirtualReg, GraphNode> node : graph.entrySet()) {
					VirtualReg from = node.getKey();
					Set<VirtualReg> tos = node.getValue().getToRegs();
					if (!stack.contains(from) && tos.size() > num) {
						goal = from;
						num = tos.size();
						//flag = true;
						//others.add(from);
					}
				}
				stack.add(goal);
				others.add(goal);
			}
			// 2. 按照逆序，对stack中的vreg分配颜色，如果分配失败，则选择一个逐出，重新执行上述流程
			Map<VirtualReg, Integer> regMap = new HashMap<VirtualReg, Integer>();
			for (int i = stack.size()-1; i>=0 ; i--) {
				VirtualReg vReg = stack.get(i);
				Set<VirtualReg> tos = graph.get(vReg).getToRegs();
				Set<Integer> nowColors = new HashSet<Integer>();
				for (VirtualReg to : tos) {
					if (regMap.containsKey(to)) {
						nowColors.add(regMap.get(to));
					}
				}
				Integer color = getDifferentColor(nowColors);
				if (color<0) {
					// 说明分配失败，需要重新来
					flag = true;
					break;
				}else {
					regMap.put(vReg, color);
				}
			}
			if (flag) {
				// 3.1 从others中选出一个点作为本次spill尝试
				VirtualReg spilled = spillOne(others);
				// 3.2 选出后修改decl中每个基本块内部涉及到这个点的上下文，根据情况增加store和load，
				// 同时修改指令的先后链接，以及基本块的起始
				changeBB(decl,spilled);
				// 3.3 对新的指令划分BB，构建CFG
				Map<String, IRInstruction> labelMap = ((IRFunctionDeclaration)decl).getLabelMap();
				LIRBuilder lirBuilder = new LIRBuilder(labelMap);
				((IRFunctionDeclaration)decl).accept(lirBuilder);
			}else {
				// 分配成功
				this.regMap = regMap;
			//	outputRegmap(regMap);
				collectInstrs(decl);
				break;
			}
		}// end while
	}
	private void collectInstrs(IRFunctionDeclaration decl) {
		// 整理指令序列，并对call指令前后增加store和load
		IRInstruction instr = decl.getStartBB().getFirst();
		while(instr != null) {
			if (instr instanceof IRCall) {
				findCall = true;
				Integer argNum = ((IRCall)instr).args.size();
				if (argNum > paramNumInStack) {
					// fix me 这里按照参数都是int类型
					int tmp = 4*(argNum-paramNumInStack);
					if (tmp > argMoreSize) {
						argMoreSize = tmp;
					}
				}
				for (VirtualReg vreg : instr.getLiveOut()) {
					addStore(decl, vreg);
				}
				realInstrs.add(instr);
				for (VirtualReg vreg : instr.getLiveOut()) {
					addLoad(decl, vreg);
				}
			}else {
				realInstrs.add(instr);
			}
			instr = instr.getNext();
		}
		LinkedList<IRInstruction> tobeDeleted = new LinkedList<IRInstruction>();
		for (int i = 0; i<realInstrs.size(); i++) {
			IRInstruction ir = realInstrs.get(i);
			IROpVisitor irOpVisitor = new IROpVisitor();
			ir.accept(irOpVisitor);
			
			if(irOpVisitor.getParamId() > 0) {
				// 说明当前指令是加载参数到寄存器
				Integer paramId = irOpVisitor.getParamId();
				Integer paramNum = irOpVisitor.getParamNum();
				if (paramId > this.paramNumInStack) {
					VirtualReg vReg = (VirtualReg) ((IRDassign)ir).varName;
					Integer paramOffset = 0;
					if(this.stdcall) {
						paramOffset = -(paramNum - paramId + 1)*4-4;
					}
					else {
						paramOffset = -paramId*4;
					}
					realInstrs.set(i, new IRDassign(vReg, new IRIreadfpoff(vReg.getType(), new IRConstval(GlobalSymbolTable.intType, paramOffset))));
				}
			}
			
			LinkedList<VirtualReg> vRegs = new LinkedList<VirtualReg>();
			if (irOpVisitor.getRes() != null) {
				vRegs.add(irOpVisitor.getRes());
			}
			if (irOpVisitor.getOp1() != null) {
				vRegs.add(irOpVisitor.getOp1());
			}
			if (irOpVisitor.getOp2() != null) {
				vRegs.add(irOpVisitor.getOp2());
			}
			if (irOpVisitor.getCallArgs() != null) {
				vRegs.addAll(irOpVisitor.getCallArgs());
			}
			Map<VirtualReg, PhysicsRegisiter> reserve = new HashMap<VirtualReg, PhysicsRegisiter>();
			Boolean flag = false;
			
			for (VirtualReg virtualReg : vRegs) {
				if (regMap.containsKey(virtualReg)) {
					reserve.put(virtualReg, new PhysicsRegisiter(regMap.get(virtualReg)));
				}else {
					flag = true;
					tobeDeleted.add(realInstrs.get(i));
					//realInstrs.remove(ir);
					break;
				}
			}
			if (!flag) {
				instrRevMap.put(realInstrs.get(i), reserve);
			}
		}
		for (IRInstruction irInstruction : tobeDeleted) {
			realInstrs.remove(irInstruction);
		}
	}
	private void changeBB(IRFunctionDeclaration decl,VirtualReg spilled) {
		// 对里面的每个BB中的每个指令，分两种情况：
		// 1. 如果是为spilled赋值，即spilled = XXX,需要在这条指令下面增加store spilled指令
		// 2. 如果是使用spilled，即 xx = spilled .. ，需要在这条指令之前增加load spilled指令
		InstructionChanger iChanger = new InstructionChanger(spilled,decl.getLocalVarPropMap(),globalVarPropMap,nowOffset);
		decl.accept(iChanger);		
		nowOffset = iChanger.getNowOffset();

	}
	private VirtualReg spillOne(LinkedList<VirtualReg> others) {
		// 这里暂时选择边数最多的点spill
		Integer sum = 0;
		VirtualReg res = null;
		for (VirtualReg virtualReg : others) {
			if (hasSpilled.contains(virtualReg)) {
				continue;
			}
			Set<VirtualReg> tos = graph.get(virtualReg).getToRegs();
			if (sum < tos.size()) {
				sum = tos.size();
				res = virtualReg;
			}
		}
		hasSpilled.add(res);
		return res;
	}
	private void outputRegmap(Map<VirtualReg, Integer> regMap) {
		System.out.println("---------图着色分配结果-----------");
		for (Entry<VirtualReg, Integer> node : regMap.entrySet()) {
			System.out.println("vreg "+node.getKey().getId()+" : "+"preg "+node.getValue());
		}
		System.out.println("--------------------------------");
	}
	private void outputGraph() {
		System.out.println("===========冲突图==========");
		for (Map.Entry<VirtualReg, GraphNode> node : graph.entrySet()) {
			VirtualReg from = node.getKey();
			Set<VirtualReg> tos = node.getValue().getToRegs();
			System.out.print(from.getId()+" : ");
			for (VirtualReg virtualReg : tos) {
				System.out.print(virtualReg.getId()+" ");
			}
			System.out.println();
		}
		System.out.println("==========================");
	}

	private void stackAlloc(IRFunctionDeclaration decl,VirtualReg vreg) {
		Map<VirtualReg, VirtualRegProperty> localVarPropMap = decl.getLocalVarPropMap();
		VirtualRegProperty vrp = new VirtualRegProperty();
		vrp.setOffset(nowOffset);
		localVarPropMap.put(vreg, vrp);
		nowOffset += vreg.getType().getRegisiterSize();
	}
	private void addLoad(IRFunctionDeclaration decl,VirtualReg vreg) {
		Map<VirtualReg, VirtualRegProperty> localVarPropMap = decl.getLocalVarPropMap();
		VirtualRegProperty vrp;
		IRInstruction loadInstr = null;
		if(localVarPropMap.containsKey(vreg)) {
			vrp = localVarPropMap.get(vreg);
			loadInstr = new IRDassign(vreg, new IRIreadfpoff(vreg.getType(),  new IRConstval(GlobalSymbolTable.intType, vrp.getOffset())));
		}else if (globalVarPropMap.containsKey(vreg)) {
			vrp = globalVarPropMap.get(vreg);
			loadInstr = new IRDassign(vreg, new IRIread(vreg.getType(), GlobalSymbolTable.pointerType, new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset())));
		}else {
			// 在local上分配stackSlot
			System.out.println("ERROR LOAD ALLOC.");
			stackAlloc(decl,vreg);
			vrp = localVarPropMap.get(vreg);
			loadInstr = new IRDassign(vreg, new IRIreadfpoff(vreg.getType(),  new IRConstval(GlobalSymbolTable.intType, vrp.getOffset())));
		}
		realInstrs.add(loadInstr);
	}
	private void addStore(IRFunctionDeclaration decl,VirtualReg vreg) {
		Map<VirtualReg, VirtualRegProperty> localVarPropMap = decl.getLocalVarPropMap();
		IRInstruction storeInstr = null;
		VirtualRegProperty vrp;
		if(localVarPropMap.containsKey(vreg)) {
			vrp = localVarPropMap.get(vreg);
			storeInstr = new IRIassignfpoff(vreg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vreg);
		}else if (globalVarPropMap.containsKey(vreg)) {
			vrp = globalVarPropMap.get(vreg);
			storeInstr = new IRIassign(vreg.getType(), new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset()), vreg);
		}else {
			// 在local上分配stackSlot
			stackAlloc(decl,vreg);
			vrp = localVarPropMap.get(vreg);
			storeInstr = new IRIassignfpoff(vreg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vreg);
		}
		realInstrs.add(storeInstr);
	}
}
