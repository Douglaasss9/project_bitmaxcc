package bit.minisys.minicc.ncgen.internal;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bit.minisys.minicc.internal.ir.VirtualReg;


public class GraphNode {
	private Set<VirtualReg> toRegs;
	public GraphNode() {
		// TODO Auto-generated constructor stub
	}
	public GraphNode(Set<VirtualReg> toRegs) {
		this.toRegs = toRegs;
	}
	public GraphNode(VirtualReg toReg) {
		toRegs = new HashSet<VirtualReg>();
		toRegs.add(toReg);
	}
	public void add(List<VirtualReg> others) {
		for (VirtualReg virtualReg : others) {
			if (!toRegs.contains(virtualReg)) {
				toRegs.add(virtualReg);
			}
		}
	}
	public void add(VirtualReg other) {
		if (!toRegs.contains(other)) {
			toRegs.add(other);
		}
	}
	public Set<VirtualReg> getToRegs() {
		return toRegs;
	}
	public void remove(VirtualReg reg) {
		toRegs.remove(reg);
	}
}
