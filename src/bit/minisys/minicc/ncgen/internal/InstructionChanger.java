package bit.minisys.minicc.ncgen.internal;

import java.util.LinkedList;
import java.util.Map;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.*;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;

public class InstructionChanger implements IRVisitor{
	private VirtualReg spilled;
	private LinkedList<IRInstruction> instrs;
	private Map<VirtualReg, VirtualRegProperty> localVarPropMap;
	private Map<VirtualReg, VirtualRegProperty> globalVarPropMap;
	private Integer nowOffset;
	
	public InstructionChanger(VirtualReg spilled,Map<VirtualReg, VirtualRegProperty> localVarPropMap,
	Map<VirtualReg, VirtualRegProperty> globalVarPropMap,Integer nowOffset) {
		this.spilled = spilled;
		this.instrs = new LinkedList<IRInstruction>();
		this.localVarPropMap = localVarPropMap;
		this.globalVarPropMap = globalVarPropMap;
		this.nowOffset = nowOffset;
	}
	public Integer getNowOffset() {
		return nowOffset;
	}
	private void stackAlloc(VirtualReg vreg) {
		VirtualRegProperty vrp = new VirtualRegProperty();
		vrp.setOffset(nowOffset);
		localVarPropMap.put(vreg, vrp);
		nowOffset += vreg.getType().getRegisiterSize();
	}
	private void addLoad() {
		VirtualRegProperty vrp;
		IRInstruction loadInstr = null;
		if(localVarPropMap.containsKey(spilled)) {
			vrp = localVarPropMap.get(spilled);
			loadInstr = new IRDassign(spilled, new IRIreadfpoff(spilled.getType(),  new IRConstval(GlobalSymbolTable.intType, vrp.getOffset())));
		}else if (globalVarPropMap.containsKey(spilled)) {
			vrp = globalVarPropMap.get(spilled);
			loadInstr = new IRDassign(spilled, new IRIread(spilled.getType(), GlobalSymbolTable.pointerType, new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset())));
		}else {
			// 在local上分配stackSlot
			System.out.println("ERROR LOAD ALLOC.");
			stackAlloc(spilled);
			vrp = localVarPropMap.get(spilled);
			loadInstr = new IRDassign(spilled, new IRIreadfpoff(spilled.getType(),  new IRConstval(GlobalSymbolTable.intType, vrp.getOffset())));
		}
		instrs.add(loadInstr);
	}
	private void addStore() {
		IRInstruction storeInstr = null;
		VirtualRegProperty vrp;
		if(localVarPropMap.containsKey(spilled)) {
			vrp = localVarPropMap.get(spilled);
			storeInstr = new IRIassignfpoff(spilled.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), spilled);
		}else if (globalVarPropMap.containsKey(spilled)) {
			vrp = globalVarPropMap.get(spilled);
			storeInstr = new IRIassign(spilled.getType(), new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset()), spilled);
		}else {
			// 在local上分配stackSlot
			stackAlloc(spilled);
			vrp = localVarPropMap.get(spilled);
			storeInstr = new IRIassignfpoff(spilled.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), spilled);
		}
		instrs.add(storeInstr);
	}
	@Override
	public void visit(IRDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRFunctionDeclaration node) {
		// TODO Auto-generated method stub
		visit(node.body);
	}

	@Override
	public void visit(IRVariableDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRInstruction node) {
		if (node instanceof IRExpression) {
			visit((IRExpression)node);
		}else if (node instanceof IRStatement) {
			visit((IRStatement)node);
		}
	}

	@Override
	public void visit(IRStatement node) {
		if(node == null) return;
		if(node instanceof IRBrfalse) {
			visit((IRBrfalse)node);
		}else if (node instanceof IRBrtrue) {
			visit((IRBrtrue)node);
		}else if (node instanceof IRGotoStatement) {
			visit((IRGotoStatement)node);
		}else if (node instanceof IRLabel) {
			visit((IRLabel)node);
		}else if (node instanceof IRReturnStatement) {
			visit((IRReturnStatement)node);
		}else if (node instanceof IRScope) {
			visit((IRScope)node);
		}
	}

	@Override
	public void visit(IRBrfalse node) {
		if (node.opnd0 == spilled) {
			addLoad();
		}
		instrs.add(node);
	}

	@Override
	public void visit(IRBrtrue node) {
		if (node.opnd0 == spilled) {
			addLoad();
		}
		instrs.add(node);
	}

	@Override
	public void visit(IRGotoStatement node) {
		instrs.add(node);
	}

	@Override
	public void visit(IRLabel node) {
		instrs.add(node);
	}

	@Override
	public void visit(IRReturnStatement node) {
		if (node.opnd != null && node.opnd == spilled) {
			addLoad();
		}
		instrs.add(node);
	}

	@Override
	public void visit(IRScope node) {
		LinkedList<IRInstruction> tmp = new LinkedList<IRInstruction>();
		tmp.addAll(instrs);
		instrs = new LinkedList<IRInstruction>();
		for (IRInstruction instr : node.instructions) {
			visit(instr);
		}
		node.instructions.clear();
		node.instructions.addAll(instrs);
		instrs = tmp;
		instrs.add(node);
	}

	@Override
	public void visit(IRExpression node) {
		if(node instanceof IRUnaryOperator) {
			visit((IRUnaryOperator)node);
		}else if (node instanceof IRBinaryOperator) {
			visit((IRBinaryOperator)node);
		}else if (node instanceof IRTernaryOperator) {
			visit((IRTernaryOperator)node);
		}else if (node instanceof IRCastOperator) {
			visit((IRCastOperator)node);
		}else if (node instanceof IRCall) {
			visit((IRCall)node);
		}else if (node instanceof IRLeaf) {
			visit((IRLeaf)node);
		}else if (node instanceof IRIread) {
			visit((IRIread)node);
		}else if (node instanceof IRDassign) {
			visit((IRDassign)node);
		}else if (node instanceof IRIassign) {
			visit((IRIassign)node);
		}else if (node instanceof IRIassignoff) {
			visit((IRIassignoff)node);
		}else if (node instanceof IRIreadfpoff) {
			visit((IRIreadfpoff)node);
		}else if (node instanceof IRIreadoff) {
			visit((IRIreadoff)node);
		}else if (node instanceof IRIassignfpoff) {
			visit((IRIassignfpoff)node);
		}
	}

	@Override
	public void visit(IRUnaryOperator node) {
		visit(node.opnd0);
	}

	@Override
	public void visit(IRInot node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNeg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBinaryOperator node) {
		visit(node.opnd0);
		if (node.opnd0 != node.opnd1) {
			visit(node.opnd1);
		}
	}

	@Override
	public void visit(IRAdd node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBxor node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCmp node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRDiv node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IREq node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRMul node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRRem node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRShl node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSub node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRTernaryOperator node) {
		visit(node.opnd0);
		if (node.opnd0 != node.opnd1) {
			visit(node.opnd1);
		}
		if (node.opnd0 != node.opnd2 && node.opnd1 != node.opnd2) {
			visit(node.opnd2);
		}	
	}

	@Override
	public void visit(IRCastOperator node) {
		if (node instanceof IRCvt) {
			visit((IRCvt)node);
		}
	}

	@Override
	public void visit(IRCvt node) {
		visit(node.opnd0);
	}

	@Override
	public void visit(IRCall node) {
		for (IRExpression opnd : node.args) {
			visit(opnd);
		}
		instrs.add(node);
	}

	@Override
	public void visit(IRLeaf node) {
		if (node instanceof VirtualReg) {
			visit((VirtualReg)node);
		}
	}

	@Override
	public void visit(IRConstval node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSizeoftype node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAddrof node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(VirtualReg node) {
		if (node == spilled) {
			addLoad();
		}
	}

	@Override
	public void visit(IRDassign node) {
		visit(node.rhsExpr);
		instrs.add(node);
		if (node.varName instanceof VirtualReg && node.varName == spilled) {
			addStore();
		}
	}

	@Override
	public void visit(IRIassign node) {
		visit(node.rhsExpr);
		visit(node.addrExpr);
		instrs.add(node);
	}

	@Override
	public void visit(IRIassignoff node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIassignfpoff node) {
		visit(node.rhsExpr);
		visit(node.offsetExpr);
		instrs.add(node);
	}

	@Override
	public void visit(IRIreadfpoff node) {
		if (node.offsetExpr instanceof VirtualReg && node.offsetExpr == spilled) {
			addLoad();
		}
	}

	@Override
	public void visit(IRIread node) {
		if (node.addrExpr instanceof VirtualReg && node.addrExpr == spilled) {
			addLoad();
		}
	}

	@Override
	public void visit(IRIreadoff node) {
		// TODO Auto-generated method stub
		
	}

}
