package bit.minisys.minicc.ncgen.internal;

// ��Ծ����
public class Interval {
	private Integer start;
	private Integer end;
	private Integer spill;
	private Integer offset;
	private PhysicsRegisiter phyReg;
	
	public Interval(Integer start,Integer end) {
		this.start = start;
		this.end = end;
		this.spill = -1;
		this.offset = -1;
		this.phyReg = null;
	}
	
	public String place(Integer id) {
		if (id > end || id < start) {
			return null;
		}else {
			if (spill == -1) {
				return "reg";
			}else {
				return "stack";
			}
		}
	}
	public PhysicsRegisiter getPhyReg() {
		return phyReg;
	}
	public Integer getStart() {
		return start;
	}
	public Integer getEnd() {
		return end;
	}
	public void setSpill(Integer spill) {
		this.spill = spill;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public void setPhyReg(PhysicsRegisiter phyReg) {
		this.phyReg = phyReg;
	}
	public void update(int start) {
		this.start = start;
	}
}
