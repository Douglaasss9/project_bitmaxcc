package bit.minisys.minicc.ncgen.internal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.icgen.internal.IROpVisitor;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.*;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;

// 线性扫描寄存器分配
public class LinearScan {
	private Map<VirtualReg, Interval> intervals;
	private Map<Interval, VirtualReg> reverse;
	private IRInstruction firstInstr;
	public LinearScan(Map<VirtualReg, Interval> intervals,IRInstruction firstInstr) {
		this.intervals = intervals;
		this.firstInstr = firstInstr;
	}
	private List<Interval> unhandled;
	private List<Interval> active;
	private Integer nowOffset;
	Map<VirtualReg, VirtualRegProperty> localVarPropMap;
	Map<VirtualReg, VirtualRegProperty> globalVarPropMap;
	private LinkedList<IRInstruction> realInstrs;
	private Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> instrRevMap;	// 记录从IR转换至汇编时每条IR中的vReg和phyReg/stackslot的对应关系
	private Integer paramNumInStack = 0;
	private Boolean stdcall = false;
	public boolean findCall = false;
	public Integer argMoreSize = 0;
	
	private ArrayList<PhysicsRegisiter>allPhyReg;
	public Integer getArgMoreSize() {
		return argMoreSize;
	}
	public Integer getNowOffset() {
		return nowOffset;
	}
	public LinkedList<IRInstruction> getRealInstrs() {
		return realInstrs;
	}
	public Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> getInstrRevMap() {
		return instrRevMap;
	}
	public void run(Integer regnum,IRFunctionDeclaration irFuncDecl,Map<VirtualReg, VirtualRegProperty> globalVarPropMap, Integer paramNumInStack, Boolean stdcall) {
		this.nowOffset = irFuncDecl.getFarOffset();
		this.localVarPropMap = irFuncDecl.getLocalVarPropMap();
		this.globalVarPropMap = globalVarPropMap;
		this.realInstrs = new LinkedList<IRInstruction>();
		this.instrRevMap = new HashMap<IRInstruction, Map<VirtualReg,PhysicsRegisiter>>();
		this.nowPhyRegUsedMap = new HashMap<PhysicsRegisiter,VirtualReg>(); 
		this.spillRegInAlloc = new HashMap<Integer, Map<VirtualReg,PhysicsRegisiter>>();
		this.paramNumInStack = paramNumInStack;
		this.stdcall = stdcall;
		this.active = new ArrayList<Interval>();
		this.allPhyReg = new ArrayList<PhysicsRegisiter>();
		init(regnum);
		alloc(regnum);
		rewrite();
	}
	private Integer spillTimes = 0;
	public Integer getSpillTimes() {
		return spillTimes;
	};
	// 添加load指令
	private void addLoad(VirtualReg vreg,PhysicsRegisiter preg) {
		spillTimes++;
		IRInstruction loadInstr = null;
		VirtualRegProperty vrp;
		if(localVarPropMap.containsKey(vreg)) {
			vrp = localVarPropMap.get(vreg);
			loadInstr = new IRDassign(vreg, new IRIreadfpoff(vreg.getType(),  new IRConstval(GlobalSymbolTable.intType, vrp.getOffset())));
		}else if (globalVarPropMap.containsKey(vreg)) {
			vrp = globalVarPropMap.get(vreg);
			loadInstr = new IRDassign(vreg, new IRIread(vreg.getType(), GlobalSymbolTable.pointerType, new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset())));
		}else {
			System.out.println(">>StupidRegisterAlloc: addLoad null.");
		}
		Map<VirtualReg, PhysicsRegisiter> loadMap = new HashMap<VirtualReg, PhysicsRegisiter>();
		loadMap.put(vreg, preg);
		instrRevMap.put(loadInstr, loadMap);
		realInstrs.add(loadInstr);
	}
	// 添加store指令
	private void addStore(VirtualReg vreg,PhysicsRegisiter preg) {
		spillTimes++;
		IRInstruction storeInstr = null;
		VirtualRegProperty vrp;
		if(localVarPropMap.containsKey(vreg)) {
			vrp = localVarPropMap.get(vreg);
			storeInstr = new IRIassignfpoff(vreg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vreg);
		}else if (globalVarPropMap.containsKey(vreg)) {
			vrp = globalVarPropMap.get(vreg);
			storeInstr = new IRIassign(vreg.getType(), new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset()), vreg);
		}else {
			// 在local上分配stackSlot
			stackAlloc(vreg);
			vrp = localVarPropMap.get(vreg);
			storeInstr = new IRIassignfpoff(vreg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vreg);
		}
		Map<VirtualReg, PhysicsRegisiter> storeMap = new HashMap<VirtualReg, PhysicsRegisiter>();
		storeMap.put(vreg, preg);
		instrRevMap.put(storeInstr, storeMap);
		realInstrs.add(storeInstr);
	}
	// 对尚未在stack上分配空间的vreg，分配stackslot
	private void stackAlloc(VirtualReg vreg) {
		VirtualRegProperty vrp = new VirtualRegProperty();
		vrp.setOffset(nowOffset);
		localVarPropMap.put(vreg, vrp);
		nowOffset += vreg.getType().getRegisiterSize();
	}
	
	private boolean classifyReg(IRInstruction instr,IROpVisitor opVisitor,ArrayList<VirtualReg> inPhyRegs,ArrayList<VirtualReg>inStackRegs,Map<VirtualReg, PhysicsRegisiter>regMap) {
		VirtualReg x = opVisitor.getRes();
		VirtualReg y = opVisitor.getOp1();
		VirtualReg z = opVisitor.getOp2();
		LinkedList<VirtualReg> args = opVisitor.getCallArgs();
		Interval interval;
		LinkedList<VirtualReg> allreg = new LinkedList<VirtualReg>();
		if (x!=null) {
			allreg.add(x);
		}
		if (y!=null) {
			allreg.add(y);
		}
		if (z!=null) {
			allreg.add(z);
		}
		if (args!=null) {
			allreg.addAll(args);
		}
		for (VirtualReg vreg : allreg) {
			interval = intervals.get(vreg);
			
			String place = interval.place(instr.getInstrId());
			if (place == null) {
				return false;
			}
			if (place.equals("reg")) {
				inPhyRegs.add(vreg);
				regMap.put(vreg, interval.getPhyReg());
			}else if (place.equals("stack")) {
				inStackRegs.add(vreg);
			}else {
				System.out.println("ERROR : when get Interval's place");
				return false;
			}
		}
		return true;
	}
	private Map<PhysicsRegisiter,VirtualReg> nowPhyRegUsedMap;
	private void searchOnePhyReg(Set<PhysicsRegisiter> nowUsedPhy,IRInstruction nowInstr,ArrayList<VirtualReg> inPhyRegs,Map<VirtualReg, PhysicsRegisiter> regMap,Map<VirtualReg, PhysicsRegisiter> xxregMap,VirtualReg vreg,Map<VirtualReg, PhysicsRegisiter> tmpReg) {
		// 截至目前使用物理寄存器的伪寄存器中，找到一个不在inPhyRegs中的xx
		// 对这个xx增加store操作到其stackslot中，如果没有就分配一个
		// tmpReg记录该xx被临时溢出
		VirtualReg xxvreg = null;
		PhysicsRegisiter xxpreg = null;
		if (regMap.containsKey(vreg)) {
			return;
		}
		for (PhysicsRegisiter preg : allPhyReg) {
			boolean flag = true;
			if (flag) {
				if (nowUsedPhy.contains(preg)) {
					flag = false;
				}
				for (VirtualReg virtualReg : inPhyRegs) {
					if (intervals.get(virtualReg).getPhyReg() == preg) {
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				xxpreg = preg;
				break;
			}
		}
		nowUsedPhy.add(xxpreg);
		for (VirtualReg virtualReg : nowInstr.getLiveOut()) {
			Interval interval = intervals.get(virtualReg);
			String place = interval.place(nowInstr.getInstrId());
			if (place!=null && place.equals("reg")) {
				if (interval.getPhyReg() == xxpreg) {
					xxvreg = virtualReg;
					break;
				}
			}
		}
		if (xxvreg !=null) {
			addStore(xxvreg, xxpreg);
		}
		// 然后增加load操作，从vreg的slot处把其值加载到xx对应的preg中
		addLoad(vreg, xxpreg);
		xxregMap.put(vreg,xxpreg);
		regMap.put(vreg, xxpreg);
		// tmp保存被spill出去的，一会还得load回来
		if (xxvreg!=null) {
			tmpReg.put(xxvreg, xxpreg);
		}
		
	}
	private void rewrite() {
		// 从第一条指令开始按顺序重写 ，按照已经实现的寄存器分配情况
		IRInstruction instr = firstInstr;
 		while(instr != null) {
 			IROpVisitor opVisitor = new IROpVisitor();
 			instr.accept(opVisitor);
 			VirtualReg x = opVisitor.getRes();
			VirtualReg y = opVisitor.getOp1();
			VirtualReg z = opVisitor.getOp2();
			LinkedList<VirtualReg> args = opVisitor.getCallArgs();
 			
			// 如果存在寄存器x根本没分配interval，说明是无用赋值操作，直接跳过
			if ((x != null && intervals.containsKey(x)) || (x==null)) {
				// 需要两个集合记录位于物理寄存器的vreg以及位于栈中的寄存器
				ArrayList<VirtualReg> inPhyRegs = new ArrayList<VirtualReg>();
				ArrayList<VirtualReg> inStackRegs = new ArrayList<VirtualReg>();
				Map<VirtualReg, PhysicsRegisiter> regMap = new HashMap<VirtualReg, PhysicsRegisiter>();
				Boolean flag = classifyReg(instr,opVisitor,inPhyRegs,inStackRegs,regMap);
				if (flag) {
					IRInstruction realNowInstr = instr;
					if(opVisitor.getParamId() > 0) {
						// 说明当前指令是加载参数到寄存器
						Integer paramId = opVisitor.getParamId();
						Integer paramNum = opVisitor.getParamNum();
						if (paramId > this.paramNumInStack) {
							VirtualReg vReg = (VirtualReg) ((IRDassign)instr).varName;
							Integer paramOffset = 0;
							if(this.stdcall) {
								paramOffset = -(paramNum - paramId + 1)*4-4;
							}
							else {
								paramOffset = -paramId*4;
							}
							realNowInstr = new IRDassign(vReg, new IRIreadfpoff(vReg.getType(), new IRConstval(GlobalSymbolTable.intType, paramOffset)));
						}
					}
					if (args != null) {
						findCall = true;
						Integer argNum = ((IRCall)instr).args.size();
						if(argNum>paramNumInStack) {
							// fix me 这里按照参数都是int类型
							int tmp = 4*(argNum-paramNumInStack);
							if (tmp > argMoreSize) {
								argMoreSize = tmp;
							}
						}
						// 如果是call指令
						if (inStackRegs.size() == 0) {
							for (VirtualReg virtualReg : instr.getLiveOut()) {
								Interval interval = intervals.get(virtualReg);
								String place = interval.place(instr.getInstrId());
								if (place!=null && place.equals("reg")) {
									addStore(virtualReg,interval.getPhyReg());
								}
							}
							//funcAddStore(instr.getInstrId());
							realInstrs.add(instr);
							instrRevMap.put(instr, regMap);
							// 再增加load
							for (VirtualReg virtualReg : instr.getLiveOut()) {
								Interval interval = intervals.get(virtualReg);
								String place = interval.place(instr.getInstrId());
								if (place!=null && place.equals("reg")) {
									addLoad(virtualReg,interval.getPhyReg());
								}
							}
						}else {
							ObjectMapper mapper = new ObjectMapper();
							try {
								System.out.println(mapper.writeValueAsString(instr));
							} catch (JsonProcessingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							// 记录临时被spill出去的，一会要store回来
							Map<VirtualReg, PhysicsRegisiter> tmpReg = new HashMap<VirtualReg, PhysicsRegisiter>();
							Set<PhysicsRegisiter> nowUsedPhy = new HashSet<PhysicsRegisiter>();
							Map<VirtualReg, PhysicsRegisiter> xxregMap = new HashMap<VirtualReg, PhysicsRegisiter>();
						//	Map<VirtualReg, PhysicsRegisiter> kkregMap = new HashMap<VirtualReg, PhysicsRegisiter>();
							for (VirtualReg vreg : inStackRegs) {
								// 对每个在stack上的vreg,需要寻找一个本条指令没使用过的物理寄存器溢出
								searchOnePhyReg(nowUsedPhy,instr,inPhyRegs,regMap,xxregMap,vreg,tmpReg);
							}

							for (VirtualReg virtualReg : instr.getLiveOut()) {
								Interval interval = intervals.get(virtualReg);
								String place = interval.place(instr.getInstrId());
								if (place!=null && place.equals("reg")) {
									addStore(virtualReg,interval.getPhyReg());
								}
							}
							for (java.util.Map.Entry<VirtualReg, PhysicsRegisiter> entry : xxregMap.entrySet()) {
								addStore(entry.getKey(), entry.getValue());
							}
							realInstrs.add(realNowInstr);
							instrRevMap.put(realNowInstr, regMap);
							for (VirtualReg virtualReg : instr.getLiveOut()) {
								Interval interval = intervals.get(virtualReg);
								String place = interval.place(instr.getInstrId());
								if (place!=null && place.equals("reg")) {
									addLoad(virtualReg,interval.getPhyReg());
								}
							}
							for (java.util.Map.Entry<VirtualReg, PhysicsRegisiter> entry : xxregMap.entrySet()) {
								addLoad(entry.getKey(), entry.getValue());
							}
							
							for (VirtualReg vreg : inStackRegs) {
								// 把结果存回去
								addStore(vreg, regMap.get(vreg));
							}
							
							// 恢复被临时溢出的reg
							for (java.util.Map.Entry<VirtualReg, PhysicsRegisiter> entry : tmpReg.entrySet()) {
								// 对每个在stack上的vreg,
								VirtualReg vReg = entry.getKey();
								PhysicsRegisiter preg = entry.getValue();
								addLoad(vReg,preg);
							}
							
							
						}
						
					}else {
						// 对于普通指令
						// 如果所有的vreg对应的interval在当前指令编号处都是物理寄存器分配，那么增加map映射,real直接增加本条
						if (inStackRegs.size() == 0) {
							realInstrs.add(realNowInstr);
							instrRevMap.put(realNowInstr, regMap);
							
						}else {
							ObjectMapper mapper = new ObjectMapper();
							try {
								System.out.println(mapper.writeValueAsString(instr));
							} catch (JsonProcessingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							// 否则说明有的vreg对应的interval在这个编号处的寄存器是溢出状态， 
							// 需要选择一个本指令没使用的物理寄存器，real增加store、load操作,添加本条指令，然后再增加load操作
							
							// 记录临时被spill出去的，一会要store回来
							Map<VirtualReg, PhysicsRegisiter> tmpReg = new HashMap<VirtualReg, PhysicsRegisiter>();
							Set<PhysicsRegisiter> nowUsedPhy = new HashSet<PhysicsRegisiter>();
							Map<VirtualReg, PhysicsRegisiter> xxregMap = new HashMap<VirtualReg, PhysicsRegisiter>();
							for (VirtualReg vreg : inStackRegs) {
								// 对每个在stack上的vreg,需要寻找一个本条指令没使用过的物理寄存器溢出
								searchOnePhyReg(nowUsedPhy,instr,inPhyRegs,regMap,xxregMap,vreg,tmpReg);
							}
						//	for (java.util.Map.Entry<VirtualReg, PhysicsRegisiter> entry : xxregMap.entrySet()) {
						//		addStore(entry.getKey(), entry.getValue());
						//	}
							realInstrs.add(realNowInstr);
							instrRevMap.put(realNowInstr, regMap);
						//	for (java.util.Map.Entry<VirtualReg, PhysicsRegisiter> entry : xxregMap.entrySet()) {
						//		addLoad(entry.getKey(), entry.getValue());
						//	}
							for (VirtualReg vreg : inStackRegs) {
								// 把结果存回去
								addStore(vreg, regMap.get(vreg));
							}
							// 恢复被临时溢出的reg
							for (java.util.Map.Entry<VirtualReg, PhysicsRegisiter> entry : tmpReg.entrySet()) {
								// 对每个在stack上的vreg,
								VirtualReg vReg = entry.getKey();
								PhysicsRegisiter preg = entry.getValue();
								addLoad(vReg,preg);
							}
							
						}
						
					}
				}

				
			}
 			instr = instr.getNext();
 		}
	}
	private Map<Integer,Map<VirtualReg, PhysicsRegisiter>> spillRegInAlloc;
	private Map<Interval, PhysicsRegisiter> regUsed = new HashMap<Interval, PhysicsRegisiter>();
	private ArrayList<PhysicsRegisiter> regUnused = new ArrayList<PhysicsRegisiter>();
	private void expireOldInterval(Interval interval) {
		if (active!=null && active.size()>0) {
			for (int i = 0; i<active.size(); i++) {
				Interval activeInterval = active.get(i);
				if (activeInterval.getEnd() >= interval.getStart()) {
					return;
				}
				active.remove(activeInterval);
				regUnused.add(regUsed.get(activeInterval)); // 把这个用完的寄存器添加到free reg pool
				regUsed.remove(activeInterval); //删除该使用记录
			}
		}
		return ;
	}
	private PhysicsRegisiter getOnePhyReg() {
		PhysicsRegisiter res = regUnused.get(regUnused.size()-1);
		regUnused.remove(res);
		return res;
	}
	private void addActive(Interval interval) {
		active.add(interval);
		Collections.sort(active,new Comparator<Interval>() {
			@Override
			public int compare(Interval o1, Interval o2) {
				if (o1.getEnd()>o2.getEnd()) {
					return 1;
				}else if (o1.getEnd() == o2.getEnd()) {
					return 0;
				}else {
					return -1;
				}
			}
		});
	}
	private void spillAtInterval(Interval interval) {
		Interval spill = active.get(active.size()-1);
		active.remove(active.size()-1);
		PhysicsRegisiter preg = regUsed.get(spill);
		interval.setPhyReg(preg);
		regUsed.remove(spill);
		regUsed.put(interval, preg);
		addActive(interval);
		// 给spill分配栈空间
		stackAlloc(spill,interval,preg);
	}
	private void stackAlloc(Interval spill,Interval nowInterval,PhysicsRegisiter preg) {
		VirtualReg vreg = reverse.get(spill);
		Integer realOffset;
		if (localVarPropMap.containsKey(vreg)) {
			VirtualRegProperty vrp = localVarPropMap.get(vreg);
			realOffset = vrp.getOffset();
		}else if (globalVarPropMap.containsKey(vreg)) {
			VirtualRegProperty vrp = globalVarPropMap.get(vreg);
			realOffset = vrp.getOffset();
		}else{
			VirtualRegProperty vrp = new VirtualRegProperty();
			vrp.setOffset(nowOffset);
			realOffset = nowOffset;
			localVarPropMap.put(vreg, vrp);
			nowOffset += vreg.getType().getRegisiterSize();
		}
		// 设置这个interval的stackoffset
		spill.setOffset(realOffset);
		spill.setSpill(nowInterval.getStart());
	}
	private void alloc(int regnum) {
		for (Interval interval : unhandled) {
			expireOldInterval(interval);
			if (regUnused.size()>0) {
				// 还有可用的寄存器
				PhysicsRegisiter preg = getOnePhyReg();
				interval.setPhyReg(preg);
				regUsed.put(interval, preg);
				addActive(interval);
			}else {
				// 需要选择一个spill
				spillAtInterval(interval);
			}
		}
	}
	private void init(int regnum) {
		// 初始化物理寄存器
		
		for(int i = 0; i<regnum; i++) {
			PhysicsRegisiter pr = new PhysicsRegisiter(i);
			regUnused.add(pr);
			allPhyReg.add(pr);
		}
		
		unhandled = new ArrayList<Interval>();
		reverse = new HashMap<Interval, VirtualReg>();
		for (java.util.Map.Entry<VirtualReg, Interval> entry : intervals.entrySet()) {
			unhandled.add(entry.getValue());
			reverse.put(entry.getValue(), entry.getKey());
		}
		Collections.sort(unhandled,new Comparator<Interval>() {
			@Override
			public int compare(Interval o1, Interval o2) {
				if (o1.getStart()>o2.getStart()) {
					return 1;
				}else if (o1.getStart() == o2.getStart()) {
					return 0;
				}else {
					return -1;
				}
			}
		});
		//System.out.println("init after sort:");
		//for (Interval interval : unhandled) {
		//	System.out.println(reverse.get(interval).getName()+":["+interval.getStart()+","+interval.getEnd()+"]");
		//}
	}
}
