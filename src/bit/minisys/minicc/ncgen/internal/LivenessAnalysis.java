package bit.minisys.minicc.ncgen.internal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.icgen.internal.IROpVisitor;
import bit.minisys.minicc.internal.ir.*;


// ִ������������������BB��liveIn liveOut
public class LivenessAnalysis {
	private LinkedList<BasicBlock> bbs;	// dfs�����õ�������BB
//	private LinkedList<BasicBlock> reverBBs;	// bbs������
	private Set<BasicBlock> visit;		// for dfs
	
	private Map<VirtualReg, Interval> intervals; 
	public Map<VirtualReg, Interval> getIntervals() {
		return intervals;
	}
	public IRInstruction getFirstInstr() {
		return bbs.get(0).getFirst();
	}
	public LivenessAnalysis() {
		bbs = new LinkedList<BasicBlock>();
	//	reverBBs = new LinkedList<BasicBlock>();
		visit = new HashSet<BasicBlock>();
	}
	
	public void run(BasicBlock startBB) {
		// ��startBB��ʼ����ÿ��bb��succs��prevs
		processBB(startBB);
		visit.clear();
		//System.out.println("bb's succs and prevs end.");
		// 1. ����LirBuilder�õ���BB���У�����˳�򹹽�����BB����
		//dfs(startBB);
		normalOrder(startBB);
		//System.out.println("bb's dfs end.");
		// 2. Ϊ�õ���BB���У���bbs����ָ���� 0 2 4 ... (IRInstruction.instrId)
		orderInstr();
		//System.out.println("bb's order end.");
		//System.out.println("Order:");
		//outputBBs();
		// 3. ���л�Ծ��������������ÿ��BB���յ�LiveIn LiveOut
		analysis();
		//System.out.println("bb's analysis end.");
		//outputLiveInOut();
		// 4. ��ÿ�������飬������LiveIn LiveOut ����ÿ��ָ���LiveIn LiveOut
		for (BasicBlock bb : bbs) {
			analysisForEachInstr(bb);
		}
		// 5. ����interval Map, �����һ��ָ���������ָ���liveout���У����Ⱦ�+2
		intervals = new HashMap<VirtualReg, Interval>();
		buildIntervals();
	}

	private void buildIntervals() {
		IRInstruction instr = getLastInstr();
		while(instr != null) {
			Set<VirtualReg> liveOut = instr.getLiveOut();
			for (VirtualReg vreg : liveOut) {
				Integer id = instr.getInstrId();
				if (intervals.containsKey(vreg)) {
					// ���³���
					intervals.get(vreg).update(id);
				}else {
					Interval interval = new Interval(id, id+2);
					intervals.put(vreg, interval);
				}
			}
			instr = instr.getPrev();
		}
	}
	private IRInstruction getLastInstr() {
		for(int i = bbs.size()-1; i>=0; i--) {
			if (bbs.get(i).getFirst() == null) {
				continue;
			}
			return bbs.get(i).getEnd();
		}
		return null;
	}
	private void outputIntervals() {
		System.out.println("intervals==============");
		for (java.util.Map.Entry<VirtualReg, Interval> entry: intervals.entrySet()) {
			VirtualReg reg = entry.getKey();
			Interval interval = entry.getValue();
			System.out.println(reg.getName()+":["+interval.getStart()+","+interval.getEnd()+"]");
		}
		System.out.println("==============intervals");
	}
	private void outputBBeachInstr(BasicBlock bb) {
		IRInstruction first = bb.getFirst();
		if (first == null) {
			return;
		}
		System.out.println("BB "+bb.getBbiD()+"each Instr ------ ");
		while(true) {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println("instr"+first.getInstrId());
			try {
				System.out.println(mapper.writeValueAsString(first));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("LiveIn:");
			for (VirtualReg reg : first.getLiveIn()) {
				System.out.println(reg.getName());
			}
			System.out.println("LiveOut:");
			for (VirtualReg reg : first.getLiveOut()) {
				System.out.println(reg.getName());
			}
			if (first == bb.getEnd()) {
				break;
			}
			first = first.getNext();
		}
		System.out.println("----- BB each Instr");
	}
	private void outputBBs() {
		for (BasicBlock bb : bbs) {
			IRInstruction first = bb.getFirst();
			if (first == null) {
				return;
			}
			System.out.println("bb--------------");
			while(first != bb.getEnd()) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					System.out.println(mapper.writeValueAsString(first));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				first = first.getNext();
				if (first == bb.getEnd()) {
					try {
						System.out.println(mapper.writeValueAsString(first));
					} catch (JsonProcessingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			System.out.println("--------------bb");
		}
	}
	private void outputLiveInOut() {
		for (BasicBlock bb : bbs) {
			System.out.println("bb"+bb.getBbiD()+":");
			System.out.println("LiveIn:");
			for (VirtualReg reg : bb.getLiveIn()) {
				System.out.println(reg.getName());
			}
			System.out.println("LiveOut:");
			for (VirtualReg reg : bb.getLiveOut()) {
				System.out.println(reg.getName());
			}
			System.out.println("******************");
		}
	}
	private void outputUseDef() {
		for (BasicBlock bb : bbs) {
			System.out.println("bb"+bb.getBbiD()+":");
			System.out.println("Use:");
			for (VirtualReg reg : bb.getUse()) {
				System.out.println(reg.getName());
			}
			System.out.println("Def(Kill):");
			for (VirtualReg reg : bb.getDef()) {
				System.out.println(reg.getName());
			}
			System.out.println("******************");
		}
	}
	private void analysisForEachInstr(BasicBlock bb) {
		IRInstruction first = bb.getFirst();
		if (first == null) {
			return;
		}
		// �Ӻ���ǰ����BB��ÿ��ָ��
		IRInstruction end = bb.getEnd();

		Set<VirtualReg> liveOut = bb.getLiveOut();

		while(true) {
			Set<VirtualReg> liveIn = new HashSet<VirtualReg>(); 
			IROpVisitor opVisitor = new IROpVisitor();
			end.accept(opVisitor);
			// x = y op z;
			VirtualReg x = opVisitor.getRes();
			VirtualReg y = opVisitor.getOp1();
			VirtualReg z = opVisitor.getOp2();
			LinkedList<VirtualReg> args = opVisitor.getCallArgs();
			Set<VirtualReg> def = new HashSet<VirtualReg>();
			Set<VirtualReg> use = new HashSet<VirtualReg>();
			if(y != null) {
				if(!def.contains(y)) {
					use.add(y);
				}
			}
			if(z != null) {
				if(!def.contains(z)) {
					use.add(z);
				}
			}
			if (args!=null && args.size() >0) {
				for (VirtualReg arg : args) {
					if (!def.contains(arg)) {
						use.add(arg);
					}
				}
			}
			if (x != null) {
				def.add(x);
			}
			liveIn.addAll(liveOut);
			for (VirtualReg virtualReg : def) {
				if(liveIn.contains(virtualReg)) {
					liveIn.remove(virtualReg);
				}
			}
			for (VirtualReg virtualReg : use) {
				if(!liveIn.contains(virtualReg)) {
					liveIn.add(virtualReg);
				}
			}
			end.setLiveIn(liveIn);
			end.setLiveOut(liveOut);
			liveOut = liveIn;
			// ȡ��һ��ָ��
			if (end == first) {
				break;
			}
			end = end.getPrev();
		}
	}
	private void analysis() {
		for (BasicBlock basicBlock : bbs) {
			basicBlock.init();
		}
		
		boolean changed = true;
		while(changed) {
			changed = false;
			for (int i = bbs.size()-1; i>=0; i--) {
				BasicBlock basicBlock = bbs.get(i);
				if(basicBlock.isExit()) {
					continue;
				}
				Boolean flag = basicBlock.updateOutAndIn();
				if(flag) {
					changed = true;
				}
			}
		}
		
		for (BasicBlock basicBlock : bbs) {
			if(basicBlock.isExit()) {
				continue;
			}
			basicBlock.updateOut();
		}
		
	//	System.out.println("LivenessAnalysis end.");
	}
	private void processBB(BasicBlock node) {
		if(node == null) return;
		visit.add(node);
		if(node.getFirst() == null) {
			//The end bb 
			return;
		}
		IRInstruction end = node.getEnd();
		if(end == null )return;
	//	if(end.getNext() != null) {
			BasicBlock tbb = null;
			BasicBlock tbb2 = null;
			if(end instanceof IRBrfalse) {
				tbb = ((IRBrfalse)end).getFalseNext().getBb();
				node.addSucc(tbb);
				tbb2 = ((IRBrfalse)end).getTrueNext().getBb();
				node.addSucc(tbb2);
				//System.out.println("brfalse����"+tbb.getBbiD()+","+tbb2.getBbiD());
			}else if (end instanceof IRBrtrue) {
				tbb = ((IRBrtrue)end).getFalseNext().getBb();
				node.addSucc(tbb);
				tbb2 = ((IRBrtrue)end).getTrueNext().getBb();
				node.addSucc(tbb2);
			}else if (end instanceof IRGotoStatement) {
				tbb = ((IRGotoStatement)end).getAbsNext().getBb();
				node.addSucc(tbb);
			}else if (end instanceof IRReturnStatement) {
				tbb = ((IRReturnStatement)end).getEndBlock();
				node.addSucc(tbb);
			}else {
				tbb = end.getNext().getBb();
				node.addSucc(tbb);
			}
			if(!visit.contains(tbb))
				processBB(tbb);
			if(tbb2!=null && !visit.contains(tbb2))
				processBB(tbb2);
	//	}
		
	}
	private void dfs(BasicBlock node) {
		if(visit.contains(node)) {
			return;
		}
		visit.add(node);
		bbs.add(node);
		//node.succs.forEach(this::dfs);
		for(BasicBlock bb: node.succs) {
			this.dfs(bb);
		}
	}
	private void normalOrder(BasicBlock node) {
		IRInstruction first = node.getFirst();
		while (first != null) {
			BasicBlock bb = first.getBb();
			bbs.add(bb);
			first = bb.getEnd().getNext();
		}
	}
	private void orderInstr() {
		int instrId = 0;
		for (int i = 0; i<bbs.size(); i++) {
			IRInstruction start = bbs.get(i).getFirst();
			if(start == null) break;
			while(start != bbs.get(i).getEnd()) {
				start.setInstrId(instrId);
				instrId += 2;
				start = start.getNext();
			}
			start.setInstrId(instrId);
			instrId += 2;
		}
	}
	
}
