package bit.minisys.minicc.ncgen.internal;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.icgen.internal.IRBuilder;
import bit.minisys.minicc.icgen.internal.MiniCCICGen;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.ncgen.IMiniCCCodeGen;


public class MiniCCCodeGen implements IMiniCCCodeGen{

	private IRBuilder irBuilder;	// ��irgen�׶λ�ȡ
	public MiniCCCodeGen() {
		this.irBuilder = MiniCCICGen.getIrBuilder();
	}
	
	@Override
	public String run(String iFile, MiniCCCfg cfg) throws Exception {
		String oFile = MiniCCUtil.remove2Ext(iFile) + MiniCCCfg.MINICC_CODEGEN_OUTPUT_EXT;
		// String type = mips / riscv / x86
		//type = "mips";//"mips";

		StringBuilder asmBuilder = process(cfg);
		writeAsmFile(asmBuilder,oFile);
		System.out.println("7. Codegen finished!");
		return oFile;
	}

	private StringBuilder process(MiniCCCfg cfg) {
		if(cfg.target.equals("mips")) {
			return processMips(cfg);
		}else if (cfg.target.equals("riscv")) {
			return processRiscv(cfg);
		}else if (cfg.target.equals("x86")){
			return processX86(cfg);
		}
		
		return null;
	}
	// mips
	private StringBuilder processMips(MiniCCCfg cfg) {
		MipsCodeGen mipsCodeGen = new MipsCodeGen(irBuilder);
		mipsCodeGen.run(cfg);
		StringBuilder sb = mipsCodeGen.getAsmSb();
		return sb;
	}
	// risc-v
	private StringBuilder processRiscv(MiniCCCfg cfg) {
		RiscvCodeGen riscvCodeGen = new RiscvCodeGen(irBuilder);
		riscvCodeGen.run(cfg);
		StringBuilder sb = riscvCodeGen.getAsmSb();
		return sb;
	}
	// x86
	private StringBuilder processX86(MiniCCCfg cfg) {
		X86CodeGen x86CodeGen = new X86CodeGen(irBuilder);
		x86CodeGen.run(cfg);
		StringBuilder sb = x86CodeGen.getAsmSb();
		return sb;
	}
	
	private void writeAsmFile(StringBuilder sb,String filename) {
		FileWriter fw;
		try {
			fw = new FileWriter(new File(filename));
			fw.write(sb.toString());
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
