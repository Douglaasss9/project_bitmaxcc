package bit.minisys.minicc.ncgen.internal;
import java.util.Map;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.icgen.internal.ParamVirtualReg;
import bit.minisys.minicc.internal.ir.*;


public class MipsBuilder implements IRVisitor{
	StringBuilder instrSb = new StringBuilder();
	Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> instrRevMap;	// 记录从IR转换至汇编时每条IR中的vReg和phyReg/stackslot的对应关系
	String branchOp = "";
	String funcname = "";
	
	private Integer finalOffset;
	private Integer argsOffset;
	public MipsBuilder(Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> instrRevMap,Integer finalOffset,Integer argsOffset) {
		this.instrRevMap = instrRevMap;
		this.finalOffset = finalOffset;
		this.argsOffset = argsOffset;
	}
	
	public void setFuncname(String funcname) {
		this.funcname = funcname;
	}
	
	public StringBuilder getInstrSb() {
		return instrSb;
	}

	@Override
	public void visit(IRDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRFunctionDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRVariableDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRInstruction node) {
		if(node == null) return;
		if(node instanceof IRStatement) {
			visit((IRStatement)node);	
		}
		else if (node instanceof IRExpression) {
			visit((IRExpression)node);
		}
	}

	@Override
	public void visit(IRStatement node) {
		if(node instanceof IRBrfalse) {
			visit((IRBrfalse)node);
		}else if (node instanceof IRBrtrue) {
			visit((IRBrtrue)node);
		}else if (node instanceof IRGotoStatement) {
			visit((IRGotoStatement)node);
		}else if (node instanceof IRLabel) {
			visit((IRLabel)node);
		}else if (node instanceof IRReturnStatement) {
			visit((IRReturnStatement)node);
		}
	}

	@Override
	public void visit(IRBrfalse node) {
		String label = node.label;
		String op = "beq";
		Integer resId = instrRevMap.get(node).get((VirtualReg)node.opnd0).getRegId();
		buildASM(op, label, resId, null, null,false);
		branchOp = ">";
	}

	@Override
	public void visit(IRBrtrue node) {
		// TODO Auto-generated method stub
		String label = node.label;
		String op = "bne";
		Integer resId = instrRevMap.get(node).get((VirtualReg)node.opnd0).getRegId();
		buildASM(op, label, resId, null, null,false);
		branchOp = ">";
	}


	@Override
	public void visit(IRGotoStatement node) {
		// TODO Auto-generated method stub
		String label = node.label;
		String op = "j";
		buildASM(op, label, null, null, null,false);
	}

	@Override
	public void visit(IRLabel node) {
		buildASM("label", node.label, null, null, null,false);
		if(mipsLabelChange(node.label).equals(funcname)) {
			buildASM("spIn", null, null, null, null, false);
		}
	}

	@Override
	public void visit(IRReturnStatement node) {
		// TODO Auto-generated method stub
		if(node.opnd != null) {
			Integer resId = instrRevMap.get(node).get((VirtualReg)node.opnd).getRegId();
			buildASM("moveReg", null, 2-8, resId, null,false);
		}
		buildASM("spOut", null, null, null, null, false);
		buildASM("jr", null, null, null, null,false);
	}

	@Override
	public void visit(IRScope node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRExpression node) {
		if(node instanceof IRUnaryOperator) {
			visit((IRUnaryOperator)node);
		}else if (node instanceof IRBinaryOperator) {
			visit((IRBinaryOperator)node);
		}else if (node instanceof IRTernaryOperator) {
			visit((IRTernaryOperator)node);
		}else if (node instanceof IRCastOperator) {
			visit((IRCastOperator)node);
		}else if (node instanceof IRCall) {
			visit((IRCall)node);
		}else if (node instanceof IRLeaf) {
			visit((IRLeaf)node);
		}else if (node instanceof IRIread) {
			visit((IRIread)node);
		}else if (node instanceof IRIreadfpoff) {
			visit((IRIreadfpoff)node);
		}else if (node instanceof IRDassign) {
			visit((IRDassign)node);
		}else if (node instanceof IRIassign) {
			visit((IRIassign)node);
		}else if (node instanceof IRIassignfpoff) {
			visit((IRIassignfpoff)node);
		}
	}

	@Override
	public void visit(IRUnaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRInot node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNeg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBinaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAdd node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBxor node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCmp node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRDiv node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IREq node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRMul node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRRem node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRShl node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSub node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRTernaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCastOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCvt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCall node) {
		// 在此之前已经完成了store phyreg
		// 更新sp = sp+finalOffset
		//buildASM("spAdd", null, finalOffset, null, null, false);
		// 跳过oldFp
		buildASM("spSub", null, 4, null, null, false);
		buildASM("saveOldFp", null, null, null, null, false);
		// 保存返回地址的寄存器到栈上
		buildASM("storeRa",null,argsOffset+4,null,null,false);

		// 设置参数
		Integer argNum = node.args.size();
		for(int i = 0; i<4 && i < argNum; i++) {
			Integer phyRegid = instrRevMap.get(node).get((VirtualReg)node.args.get(i)).getRegId();
			buildASM("storeArgReg", null, i+4, phyRegid, null, false);
		}
		for(int i = 4; i<argNum; i++) {
			Integer phyRegid = instrRevMap.get(node).get((VirtualReg)node.args.get(i)).getRegId();
			buildASM("storeArgStack", null, i*4+4, phyRegid, null, false);
		}
		// jal funcname
		buildASM("jal", node.funcname, null, null, null, false);

		// 从栈上回复返回地址到寄存器
		buildASM("loadRa",null,argsOffset+4,null,null,false);
		buildASM("loadOldFp", null, null, null, null, false);
		buildASM("spAdd", null, 4, null, null, false);
		// 更新sp = sp-finalOffset
		//buildASM("spSub", null, finalOffset, null, null, false);
		// 在此之后已经完成了load phyreg
	}


	@Override
	public void visit(IRLeaf node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRConstval node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSizeoftype node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAddrof node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(VirtualReg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIread node) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(IRDassign node) {
		VirtualReg res = (VirtualReg)node.varName;
		Integer resId = instrRevMap.get(node).get(res).getRegId();
		if(node.rhsExpr instanceof IRConstval) {
			Integer opndId1 = (Integer)((IRConstval)node.rhsExpr).value;
			buildASM("moveImm",null,resId,opndId1,null,false);
			branchOp = ">";
		}else if(node.rhsExpr instanceof IRAddrof) {
			// la
			String addrStr = ((IRAddrof)node.rhsExpr).varName;
			buildASM("la",addrStr,resId,null,null,false);
			branchOp = ">";
		}else if(node.rhsExpr instanceof VirtualReg) {
			// mov
			Integer opndId1;
			boolean flag;
			if(node.rhsExpr instanceof ParamVirtualReg) {
				opndId1 = ((ParamVirtualReg)node.rhsExpr).getParamId();
				opndId1 = mipsParamRegIdChange(opndId1);
				flag = true;
			}else {
				opndId1 = instrRevMap.get(node).get((VirtualReg)node.rhsExpr).getRegId();
				flag = false;
			}
			buildASM("moveReg",null,resId,opndId1,null,flag);
		}else if (node.rhsExpr instanceof IRNeg) {
			IRNeg neg = (IRNeg)node.rhsExpr;
			Integer opndId1 = instrRevMap.get(node).get((VirtualReg)neg.opnd0).getRegId();
			buildASM("neg", null, resId, opndId1, null, false);
			branchOp = ">";
		}else if (node.rhsExpr instanceof IRInot) {
			IRInot inot = (IRInot)node.rhsExpr;
			Integer opndId1 = instrRevMap.get(node).get((VirtualReg)inot.opnd0).getRegId();
			buildASM("inot", null, resId, opndId1, null, false);
			branchOp = ">";
		}else if (node.rhsExpr instanceof IRBinaryOperator) {
			// 各种二元操作
			String op = "";
			IRBinaryOperator bop = (IRBinaryOperator)node.rhsExpr;
			Integer opndId1 = instrRevMap.get(node).get((VirtualReg)bop.opnd0).getRegId();
			Integer opndId2 = null;
			if(bop instanceof IRAdd) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "addImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "add";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRSub) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "subImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "sub";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRMul) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "mulImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "mul";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRDiv) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "divImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "div";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRRem) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "modImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "mod";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRLt || bop instanceof IREq || bop instanceof IRGt || bop instanceof IRLe || bop instanceof IRGe || bop instanceof IRNe) {
				if(bop instanceof IRLt) {
					branchOp = "<";
					op = "lt";
				}else if (bop instanceof IREq) {
					branchOp = "==";
					op = "eq";
				}else if (bop instanceof IRGt) {
					branchOp = ">";
					op = "gt";
				}else if (bop instanceof IRLe) {
					branchOp = "<=";
					op = "le";
				}else if (bop instanceof IRGe) {
					branchOp = ">=";
					op = "ge";
				}else if (bop instanceof IRNe) {
					branchOp = "!=";
					op = "ne";
				}
				
				if(bop.opnd1 instanceof IRConstval) {
					op += "Imm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				String cmpOp = "";
				
			}else if (bop instanceof IRShl) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "shlImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "shl";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRLshr) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "lshrImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "lshr";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRBand) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "bandImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "band";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRBxor) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "bxorImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "bxor";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRBior) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "biorImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "bior";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRLand) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "landImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "land";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}else if (bop instanceof IRLior) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "liorImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "lior";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
				branchOp = ">";
			}

			buildASM(op, null, resId, opndId1, opndId2,false);
		}else if (node.rhsExpr instanceof IRIread) {
			// load from ram
			String op = "";
			IRIread rIread = (IRIread)node.rhsExpr;
			Integer opndId1 = null;
			if(rIread.addrExpr instanceof IRConstval) {
				op = "lwRamImm";
				opndId1 = (Integer)((IRConstval)rIread.addrExpr).value;
			}else if (rIread.addrExpr instanceof VirtualReg) {
				op = "lwRamReg";
				opndId1 = instrRevMap.get(node).get((VirtualReg)rIread.addrExpr).getRegId();
			}
			buildASM(op, null, resId, opndId1, null,false);
			branchOp = ">";
		}else if (node.rhsExpr instanceof IRIreadfpoff) {
			// load from stack
			String op = "";
			IRIreadfpoff rIread = (IRIreadfpoff)node.rhsExpr;
			Integer opndId1 = null;
			if(rIread.offsetExpr instanceof IRConstval) {
				op = "lwStackImm";
				opndId1 = (Integer)((IRConstval)rIread.offsetExpr).value;
				opndId1 *= -1;	// 栈向下增长
				if(opndId1 <= 0)
					opndId1 -= 4;
			}else if (rIread.offsetExpr instanceof VirtualReg) {
				op = "lwStackReg";
				opndId1 = instrRevMap.get(node).get((VirtualReg)rIread.offsetExpr).getRegId();
			}
			buildASM(op, null, resId, opndId1, null,false);
			branchOp = ">";
		}
		
	}

	@Override
	public void visit(IRIassign node) {
		// store to ram
		String op = "";
		Integer opndId1 = null;
		if(node.addrExpr instanceof IRConstval) {
			op = "swRamImm";
			opndId1 = (Integer)((IRConstval)node.addrExpr).value;
		}else if (node.addrExpr instanceof VirtualReg) {
			op = "swRamReg";
			opndId1 = instrRevMap.get(node).get((VirtualReg)node.addrExpr).getRegId();
		}
		
		Integer resId = null;
		if(node.rhsExpr instanceof VirtualReg) {
			resId = instrRevMap.get(node).get((VirtualReg)node.rhsExpr).getRegId();
		}
		buildASM(op, null, resId, opndId1, null,false);
	}

	@Override
	public void visit(IRIassignoff node) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void visit(IRIassignfpoff node) {
		// store to stack
		String op = "";
		Integer opndId1 = null;
		if(node.offsetExpr instanceof IRConstval) {
			op = "swStackImm";
			opndId1 = (Integer)((IRConstval)node.offsetExpr).value;
			opndId1 *= -1;
			if(opndId1 <= 0)
				opndId1 -= 4;
		}else if (node.offsetExpr instanceof VirtualReg) {
			op = "swStackReg";
			opndId1 = instrRevMap.get(node).get((VirtualReg)node.offsetExpr).getRegId();
		}
		
		Integer resId = null;
		if(node.rhsExpr instanceof VirtualReg) {
			resId = instrRevMap.get(node).get((VirtualReg)node.rhsExpr).getRegId();
		}
		buildASM(op, null, resId, opndId1, null,false);
	}

	@Override
	public void visit(IRIreadfpoff node) {
		// TODO Auto-generated method stub
		
	}

	private void buildASM(String opname,String label,Integer resId, Integer op1Id, Integer op2Id, boolean flag) {
		Integer op1Reg = op1Id;
		if(op1Id != null) {
			if(flag) {
				op1Reg = op1Id;
			}else {
				 op1Reg = mipsRegIdChange(op1Id);
			}
		}
		if (opname.equals("la")) {
			instrSb.append("\tla "+"$"+mipsRegIdChange(resId)+", "+label);
		}else if (opname.equals("saveOldFp")) {
			instrSb.append("\tsw $fp, ($sp)\n");
			instrSb.append("\tmove $fp, $sp");
		}else if (opname.equals("loadOldFp")) {
			instrSb.append("\tlw $fp, ($sp)");
		}else if (opname.equals("spIn")) {
			instrSb.append("\tsubu $sp, $sp, "+finalOffset);
		}else if (opname.equals("spOut")) {
			instrSb.append("\tmove $sp, $fp");
		}else if (opname.equals("storeRa")) {
			instrSb.append("\tsw "+"$31, "+resId+"($sp)");
		}else if (opname.equals("loadRa")) {
			instrSb.append("\tlw "+"$31, "+resId+"($sp)");
		}else if (opname.equals("spAdd")) {
			instrSb.append("\taddu "+"$sp, $sp, "+resId);
		}else if (opname.equals("spSub")) {
			instrSb.append("\tsubu "+"$sp, $sp, "+resId);
		}else if (opname.equals("storeArgReg")) {
			instrSb.append("\tmove "+"$"+resId+", $"+mipsRegIdChange(op1Id));
		}else if (opname.equals("storeArgStack")) {
			instrSb.append("\tsw "+"$"+mipsRegIdChange(op1Id)+", "+resId+"($fp)");
		}else if (opname.equals("label")) {
			instrSb.append(mipsLabelChange(label)+":");
		}else if (opname.equals("neg")) {
			instrSb.append("\txori "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", 0xffffffff");
		}else if (opname.equals("inot")) {
			instrSb.append("\tseq "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $0");
		}else if (opname.equals("moveReg")) {
			instrSb.append("\tmove "+"$"+mipsRegIdChange(resId)+", $"+op1Reg);
		}else if (opname.equals("moveImm")) {
			instrSb.append("\tli "+"$"+mipsRegIdChange(resId)+", "+op1Id);
		}else if (opname.equals("add")) {
			instrSb.append("\tadd "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("addImm")) {
			instrSb.append("\tadd "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("sub")) {
			instrSb.append("\tsub "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("subImm")) {
			instrSb.append("\tsub "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("mul")) {
			instrSb.append("\tmul "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("mulImm")) {
			instrSb.append("\tmul "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("div")) {
			instrSb.append("\tdiv "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("divImm")) {
			instrSb.append("\tdiv "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("mod")) {
			instrSb.append("\trem "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("modImm")) {
			instrSb.append("\trem "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("shl")) {
			instrSb.append("\tsllv "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("shlImm")) {
			instrSb.append("\tsll "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("lshr")) {
			instrSb.append("\tsrlv "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("lshrImm")) {
			instrSb.append("\tsrl "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("band")) {
			instrSb.append("\tand "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("bandImm")) {
			instrSb.append("\tand "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("bxor")) {
			instrSb.append("\txor "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("bxorImm")) {
			instrSb.append("\txor "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("bior")) {
			instrSb.append("\tor "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("biorImm")) {
			instrSb.append("\tor "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("land")) {
			instrSb.append("\tsne "+"$v1"+", $"+mipsRegIdChange(op1Id)+", $0"+"\n");
			instrSb.append("\tsne "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op2Id)+", $0"+"\n");
			instrSb.append("\tand "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(resId)+", $v1");
		}else if (opname.equals("landImm")) {
			
		}else if (opname.equals("lior")) {
			instrSb.append("\tsne "+"$v1"+", $"+mipsRegIdChange(op1Id)+", $0"+"\n");
			instrSb.append("\tsne "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op2Id)+", $0"+"\n");
			instrSb.append("\tor "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(resId)+", $v1");
		}else if (opname.equals("liorImm")) {
			
		}else if (opname.equals("lt")) {
			instrSb.append("\tslt "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("le")) {
			instrSb.append("\tsle "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("eq")) {
			instrSb.append("\tseq "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("ne")) {
			instrSb.append("\tsne "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("gt")) {
			instrSb.append("\tsgt "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("ge")) {
			instrSb.append("\tsge "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $"+mipsRegIdChange(op2Id));
		}else if (opname.equals("lwStackReg")) {
			instrSb.append("\tsubu "+"$"+mipsRegIdChange(resId)+", $fp, $"+mipsRegIdChange(op1Id)+"\n");
			instrSb.append("\tsubu "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(resId)+", 4\n");
			instrSb.append("\tlw "+"$"+mipsRegIdChange(resId)+", ($"+mipsRegIdChange(resId)+")");
		}else if (opname.equals("lwStackImm")) {
			instrSb.append("\tlw "+"$"+mipsRegIdChange(resId)+", "+op1Id+"($fp)");
		}else if (opname.equals("lwRamReg")) {
			instrSb.append("\tadd "+"$"+mipsRegIdChange(resId)+", $"+mipsRegIdChange(op1Id)+", $gp\n");
			instrSb.append("\tlw "+"$"+mipsRegIdChange(resId)+", ($"+mipsRegIdChange(resId)+")");
		}else if (opname.equals("lwRamImm")) {
			instrSb.append("\tlw "+"$"+mipsRegIdChange(resId)+", "+op1Id+"($gp)");
		}else if (opname.equals("swStackReg")) {
			instrSb.append("\tsubu "+"$v1"+", $fp, $"+mipsRegIdChange(op1Id)+"\n");
			instrSb.append("\tsubu "+"$v1"+", $v1"+", 4\n");
			instrSb.append("\tsw "+"$"+mipsRegIdChange(resId)+", ($v1)");
		}else if (opname.equals("swStackImm")) {
			instrSb.append("\tsw "+"$"+mipsRegIdChange(resId)+", "+op1Id+"($fp)");
		}else if (opname.equals("swRamReg")) {
			instrSb.append("\tadd "+"$v1"+", $"+mipsRegIdChange(op1Id)+", $gp\n");
			instrSb.append("\tsw "+"$"+mipsRegIdChange(resId)+", ($v1)");
		}else if (opname.equals("swRamImm")) {
			instrSb.append("\tsw "+"$"+mipsRegIdChange(resId)+", "+op1Id+"($gp)");
		}else if (opname.equals("beq")) {
			instrSb.append("\tbeq "+"$"+mipsRegIdChange(resId)+", "+"$0, "+mipsLabelChange(label));
		}else if (opname.equals("bne")) {
			instrSb.append("\tbne "+"$"+mipsRegIdChange(resId)+", "+"$0, "+mipsLabelChange(label));
		}else if (opname.equals("j")) {
			instrSb.append("\tj "+mipsLabelChange(label));
		}else if (opname.equals("jr")) {
			instrSb.append("\tjr $31");
		}else if (opname.equals("jal")) {
			instrSb.append("\tjal "+label);
		}
		instrSb.append("\n");
	}
	
	private Integer mipsRegIdChange(Integer oldId) {
		return oldId+8;
	}
	private String mipsLabelChange(String label) {
		if(label.charAt(1) <= '9' && label.charAt(1) >= '0') {
			String res = "_";
			res += label.substring(1);
			return res;
		}else {
			return label.substring(1);
		}
	}
	private Integer mipsParamRegIdChange(Integer paramRegId) {
		return paramRegId+3;
	}

	@Override
	public void visit(IRIreadoff node) {
		// TODO Auto-generated method stub
		
	}

}
