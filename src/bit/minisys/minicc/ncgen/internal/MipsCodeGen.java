package bit.minisys.minicc.ncgen.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.icgen.internal.IRBuilder;
import bit.minisys.minicc.icgen.internal.ParamInfo;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.*;
import bit.minisys.minicc.internal.symbol.*;


public class MipsCodeGen {
	private IRBuilder irBuider;
	private StringBuilder asmSb;
	private static Integer MIPS_REG_NUM = 18; 	// MIPS����ʹ�õ�����Ĵ�����Ŀ$8-$25
	private static Integer MIPS_PARAM_NUM_IN_STACK = 4;
	public MipsCodeGen(IRBuilder irBuider) {
		this.irBuider = irBuider;
	}
	public StringBuilder getAsmSb() {
		return asmSb;
	}
	private String regalloc = "local";
	public void run(MiniCCCfg cfg) {
		IRFile irfile = irBuider.getIrFile();
		if (cfg.ra.equals("ls")) {
			regalloc = "linearscan";
		}else {
			regalloc = "local";
		}
		asmSb = new StringBuilder();
		// -----------����ȫ��������.data------------------
		asmSb.append(".data\n");
		ArrayList<IRVariableDeclaration> dataDeclList=new ArrayList<IRVariableDeclaration>();
		for (IRDeclaration irdecl : irfile.globalSymTab.localSymTab) {
			if(irdecl instanceof IRVariableDeclaration) {
				dataDeclList.add((IRVariableDeclaration)irdecl);
			}
		}
		// �����ڴ��offset��С��������
		Collections.sort(dataDeclList,new Comparator<IRVariableDeclaration>() {
			@Override
			public int compare(IRVariableDeclaration o1, IRVariableDeclaration o2) {
				// TODO Auto-generated method stub
				return o1.offsize-o2.offsize;
			}
		});
		for (IRVariableDeclaration irvd : dataDeclList) {
			// Ϊÿ������.data����
			buildMipsData(irvd,asmSb);
		}
		// ���õ����ַ�����������.data��
		creatStringConstAsm(asmSb,irfile.getGlobalStringConstMap());
		// ----------.data���������-------------------
		
		
		// ----------����ָ���.text-----------------
		asmSb.append(".text\n");
		// ������init�����ú���
		// ��ȡȫ�ֱ�����Ӧ�ļĴ���ӳ��
		Map<VirtualReg, VirtualRegProperty> globalVarPropMap = irBuider.getGlobalVarPropMap();
		// __init
		creatInitAsm(asmSb,irfile.globalSymTab.instructions,globalVarPropMap);
		// ���ú���(Mars_PrintInt, Mars_GetInt, Mars_PrintStr)
		creatBuiltInFuncAsm(asmSb);
		// ��ÿ���������ɶ�Ӧ��ָ��
		int spillCnt = 0;
		for (IRDeclaration irdecl : irfile.globalSymTab.localSymTab) {
			if(irdecl instanceof IRFunctionDeclaration) {
				String funcname = ((IRFunctionDeclaration)irdecl).funcname;
				BasicBlock start = ((IRFunctionDeclaration)irdecl).getStartBB();
				// ��IR��������ɼĴ�������
				Integer finalOffset = 0;
				LinkedList<IRInstruction> realInstrs;
				StringBuilder sb = null;
				if (regalloc.equals("local")) {
					StupidRegisterAlloc stpRegisterAlloc = new StupidRegisterAlloc((IRFunctionDeclaration)irdecl,MIPS_REG_NUM,globalVarPropMap,MIPS_PARAM_NUM_IN_STACK,false);
					stpRegisterAlloc.run(start);
					spillCnt += stpRegisterAlloc.getSpillTimes();
					//stpRegisterAlloc.outputNewInstrs();
					// �ӼĴ�����������ȡ������ջ��Ϣ
					// ��¼�ֲ�����+spillռ�õ�ջ�ռ�(sp + finalOffset)
					finalOffset = stpRegisterAlloc.getNowOffset();
					// �����ǰ����irdecl�ڲ�������������������Ҫ����return address ����$31,finalOffset+=4
					if(stpRegisterAlloc.findCall) {
						finalOffset += 4;
						// 4�������Ĵ����̶�
						finalOffset += 4*MIPS_PARAM_NUM_IN_STACK;
						// �����ǰ�����ڲ����ö��������ȡ����ռ��ջ�ռ�����offset���ӵ�finalOffset
						finalOffset += stpRegisterAlloc.argMoreSize;
					}
					// ����ָ������յ�irָ���б�����spill��load/store��
					realInstrs = stpRegisterAlloc.getRealInstrs();
					// ����mips���
					MipsBuilder mipsBuilder = new MipsBuilder(stpRegisterAlloc.getInstrRevMap(),finalOffset,4*MIPS_PARAM_NUM_IN_STACK+stpRegisterAlloc.argMoreSize);
					mipsBuilder.setFuncname(funcname);
					for (IRInstruction ir : realInstrs) {
						mipsBuilder.visit(ir);
					}
					sb = mipsBuilder.getInstrSb();
				}else if (regalloc.equals("linearscan")) {
					// ��ÿ�������ڲ���
					LivenessAnalysis la = new LivenessAnalysis();
					la.run(((IRFunctionDeclaration) irdecl).getStartBB());
					LinearScan linearScan = new LinearScan(la.getIntervals(),la.getFirstInstr());
					linearScan.run(MIPS_REG_NUM,(IRFunctionDeclaration)irdecl,irBuider.getGlobalVarPropMap(),MIPS_PARAM_NUM_IN_STACK,false);
					spillCnt += linearScan.getSpillTimes();
					
					finalOffset = linearScan.getNowOffset();
					// �����ǰ����irdecl�ڲ�������������������Ҫ����return address ����$31,finalOffset+=4
					if(linearScan.findCall) {
						finalOffset += 4;
						// 4�������Ĵ����̶�
						finalOffset += 4*MIPS_PARAM_NUM_IN_STACK;
						// �����ǰ�����ڲ����ö��������ȡ����ռ��ջ�ռ�����offset���ӵ�finalOffset
						finalOffset += linearScan.argMoreSize;
					}
					// ����ָ������յ�irָ���б�����spill��load/store��
					realInstrs = linearScan.getRealInstrs();
					
					// ����mips���
					MipsBuilder mipsBuilder = new MipsBuilder(linearScan.getInstrRevMap(),finalOffset,4*MIPS_PARAM_NUM_IN_STACK+linearScan.argMoreSize);
					mipsBuilder.setFuncname(funcname);
					for (IRInstruction ir : realInstrs) {
						mipsBuilder.visit(ir);
					}
					sb = mipsBuilder.getInstrSb();
				}
				asmSb.append(sb.toString());
			}
		}
		//System.out.println("���ָ��������"+spillCnt);
	}
	
	// Ϊÿ��ȫ�ֱ�������asm,��ʼ��Ϊ0
	private void buildMipsData(IRVariableDeclaration decl, StringBuilder sb) {
		String name = decl.name;
		Type declType = decl.type;
		if(declType instanceof NormalType) {
			sb.append(name+" : ");
			if(declType == GlobalSymbolTable.intType) {
				sb.append(".word 0\n");
			}
		}else if (declType instanceof ArrayType) {
			sb.append(name+" : ");
			if(((ArrayType)declType).elementType == GlobalSymbolTable.intType) {
				sb.append(".space "+declType.getRegisiterSize()+"\n");
			}
		}
	}
	// Ϊ�õ���str����ȫ�ֱ���
	private void creatStringConstAsm(StringBuilder sb,Map<String, String> StrConstmap) {
		sb.append("blank : .asciiz \" \"\n");
		for(Map.Entry<String, String> entry : StrConstmap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			sb.append(value+" : .asciiz " + key+"\n");
 		}
	}
	// ����asm����ں���__init����������ָ���Լ�ȫ�ֱ����ĳ�ʼ��
	private void creatInitAsm(StringBuilder sb,List<IRInstruction> initInstrs,Map<VirtualReg, VirtualRegProperty>globalVarPropMap) {
		sb.append("__init:\n");
		sb.append("\tlui $sp, 0x8000\n");
		sb.append("\taddi $sp, $sp, 0x0000\n");
		sb.append("\tmove $fp, $sp\n");
		sb.append("\tadd $gp, $gp, 0x8000\n");
		IRFunctionDeclaration initFuncDecl = new IRFunctionDeclaration("__init");
		initFuncDecl.setArgs(null);
		initFuncDecl.setBody(null);
		initFuncDecl.setFarOffset(0);
		initFuncDecl.setLabelMap(null);
		initFuncDecl.setLocalVarPropMap(new HashMap<VirtualReg, VirtualRegProperty>());
		initFuncDecl.setParamsInfo(new HashMap<VirtualReg, ParamInfo>());
		initFuncDecl.setReturnType(GlobalSymbolTable.voidType);
		initFuncDecl.setStartBB(null);
		StupidRegisterAlloc sra = new StupidRegisterAlloc(initFuncDecl, MIPS_REG_NUM, globalVarPropMap,MIPS_PARAM_NUM_IN_STACK,false);
		sra.runInit(initInstrs);
		Integer nowOffset = sra.getNowOffset();
		LinkedList<IRInstruction> realInstrs = sra.getRealInstrs();
		MipsBuilder mipsBuilder = new MipsBuilder(sra.getInstrRevMap(),nowOffset,16+sra.argMoreSize);
		for (IRInstruction ir : realInstrs) {
			mipsBuilder.visit(ir);
		}
		sb.append(mipsBuilder.getInstrSb().toString());
		sb.append("\tjal main\n");
		sb.append("\tli $v0, 10\n");
		sb.append("\tsyscall\n");
	}
	// ����BuiltInFunction
	private void creatBuiltInFuncAsm(StringBuilder sb) {
		//Mars_PrintInt:
		sb.append("Mars_PrintInt:\n");
		sb.append("\tli $v0, 1\n");
		sb.append("\tsyscall\n");
		sb.append("\tli $v0, 4\n");
		sb.append("\tmove $v1, $a0\n");
		sb.append("\tla $a0, blank\n");
		sb.append("\tsyscall\n");
		sb.append("\tmove $a0, $v1\n");
		sb.append("\tjr $ra\n");
		//Mars_GetInt:
		sb.append("Mars_GetInt:\n");
		sb.append("\tli $v0, 5\n");
		sb.append("\tsyscall\n");
		sb.append("\tjr $ra\n");
		//Mars_PrintStr:
		sb.append("Mars_PrintStr:\n");
		sb.append("\tli $v0, 4\n");
		sb.append("\tsyscall\n");
		sb.append("\tjr $ra\n");
	}
}
