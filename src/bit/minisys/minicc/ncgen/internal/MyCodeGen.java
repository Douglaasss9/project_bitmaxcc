package bit.minisys.minicc.ncgen;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.parser.ast.*;
import bit.minisys.minicc.ir.*;

public class MyCodeGen implements IMiniCCCodeGen{
	
	@Override
    public String run(String iFile, MiniCCCfg cfg) throws Exception {
        System.out.println("filename:"+iFile);

        iFile = iFile.replace(MiniCCCfg.MINICC_ICGEN_OUTPUT_EXT, MiniCCCfg.MINICC_PARSER_OUTPUT_EXT);
        ObjectMapper mapper = new ObjectMapper();
        ASTCompilationUnit program = mapper.readValue(new File(iFile), ASTCompilationUnit.class);
        String oFile = MiniCCUtil.remove2Ext(iFile) + MiniCCCfg.MINICC_CODEGEN_OUTPUT_EXT;
        
        IRCompilationUnit IRcu = compilationUnit(program);
        NCGenHelper nc = new NCGenHelper();
        nc.compilationUnit(IRcu);
 
        String ans = new String(nc.getCodes());
        
        
        FileOutputStream fileOutputStream = new FileOutputStream(oFile);
        fileOutputStream.write(ans.getBytes());
        fileOutputStream.close();
        
        System.out.println("6. ncGen finishedQ");
        return oFile;
    }
	
	private IRCompilationUnit compilationUnit(ASTCompilationUnit node) {
		IRCompilationUnit cu = new IRCompilationUnit();
		
		for (ASTNode n: node.items) {
			if (n instanceof ASTFunctionDefine) {
				IRFunction IRfd = functionDefine((ASTFunctionDefine) n);
				cu.addFunction(IRfd);
			}
			else if (n instanceof ASTDeclaration) {
				//全局变量声明
				String varType = ((ASTDeclaration) n).specifiers.get(0).value;
				//String llvmType = type2LLVMType(varType);
				for (ASTInitList initList: ((ASTDeclaration) n).initLists) {
					ASTDeclarator declarator = initList.declarator;
					
					//变量
					if (declarator instanceof ASTVariableDeclarator) {
                        String varName = ((ASTVariableDeclarator) declarator).identifier.value;
                        int initValue = 0;
                        if (initList.exprs.size() >= 1) {
                            initValue = ((ASTIntegerConstant) initList.exprs.get(0)).value;
                        }
                        
                        IRVariableDeclaration vardecl = new IRVariableDeclaration();
                        vardecl.varName = varName;
                        vardecl.varType = varType;
                        //vardecl.llvmType = llvmType;
                        vardecl.flag = "global";
                        vardecl.value = initValue;
                        cu.addVariable(vardecl);
					}
					//数组
					else if(declarator instanceof ASTArrayDeclarator) {
						
						/*
	                        ArrayList<Integer> dims = new ArrayList<>();

	                        ASTDeclarator it = declarator;
	                        while (it instanceof ASTArrayDeclarator) {
	                            ASTArrayDeclarator arrIt = (ASTArrayDeclarator) it;
	                            int dim = ((ASTIntegerConstant) arrIt.expr).value;
	                            dims.add(0, dim);

	                            it = arrIt.declarator;
	                        }

	                        String arrName = ((ASTVariableDeclarator) it).identifier.value;
	                        symbolTableStack.addArray(arrName, dims, type);
	                        String llvmArrType = arr2LLVMType(dims, type);

	                        codes.append(String.format("@%s = global %s zeroinitializer\n", arrName, llvmArrType));
	                    }
	                    */
					}
				}
			}
		
		}
		return cu;
	}
	
	private IRFunction functionDefine(ASTFunctionDefine node) {
		IRFunction fd = new IRFunction();
		
		ASTFunctionDeclarator fundecl = (ASTFunctionDeclarator) node.declarator;
		String functionName = ((ASTVariableDeclarator) fundecl.declarator).identifier.value;
        String retType = node.specifiers.get(0).value;
        //String llvmRetType = type2LLVMType(retType);
       
        fd.returnType = retType;
       // fd.llvmType = llvmRetType;
        fd.funcName = functionName;
        
                
        ArrayList<IRVariableDeclaration> params = new ArrayList<>();
        Integer id = 0;
        
        // 函数参数定义
        for (ASTParamsDeclarator paramsDeclarator: fundecl.params) {
        	String type = paramsDeclarator.specfiers.get(0).value;
            //String llvmType = type2LLVMType(type);
            
            if (paramsDeclarator.declarator instanceof ASTVariableDeclarator) {
                ASTVariableDeclarator variableDeclarator = (ASTVariableDeclarator) paramsDeclarator.declarator;
                String name = variableDeclarator.identifier.value;
                
                IRVariableDeclaration var = new IRVariableDeclaration();
                var.flag = "param";
                var.varType = type;
               // var.llvmType = llvmType;
                var.varName = name;
                var.offset = id;
                id = id + 1;   
                params.add(var);
            } 
            else if (paramsDeclarator.declarator instanceof ASTArrayDeclarator) {
                // todo array param
            }
        }
        fd.paramList = params;
        fd.body = compoundStatement(node.body);
        
        return fd;
	}
	
	private IRScope compoundStatement(ASTCompoundStatement node) {
		IRScope scope = new IRScope();
		
		for (ASTNode n: node.blockItems) {
            if (n instanceof ASTDeclaration) {
            	//局部变量声明
            	ASTDeclaration nn = (ASTDeclaration) n;
            	String varType = nn.specifiers.get(0).value;
            	//String llvmType = type2LLVMType(varType);
            	
            	for (ASTInitList initList: nn.initLists) {
                    ASTDeclarator declarator = initList.declarator;
                    if (declarator instanceof ASTVariableDeclarator) {
                        ASTVariableDeclarator dd = (ASTVariableDeclarator) declarator;
                        String varName = dd.identifier.value;
                        IRVariableDeclaration var = new IRVariableDeclaration();
                        
                        if (initList.exprs.size() != 0) {
                            // 有初始值
                            ASTExpression initValue = initList.exprs.get(0);
                            var.initExpr = expression(initValue);
                        }
                        
                        var.flag = "local";
                        var.varName = varName;
                        var.varType = varType;
                       // var.llvmType = llvmType;
                        scope.addVariable(var);
                    }
                    else if (declarator instanceof ASTArrayDeclarator) {
                    	//TODO
                    }
            	}
            }
            else if (n instanceof ASTStatement) {
                scope.addStatement(statement((ASTStatement) n));
            }
		}
		return scope;
	}
	
	private IRStatement statement(ASTStatement node) {
		IRStatement state = new IRStatement();
		
		if (node instanceof ASTExpressionStatement) {
			state.statType = "expr";
			
			IRExpressionStatement exprState = new IRExpressionStatement();
			exprState.expr = expression(((ASTExpressionStatement) node).exprs.get(0));
			state.exprStat = exprState;
        }
		else if (node instanceof ASTReturnStatement) {
			state.statType = "return";
			
			IRReturnStatement returnState = new IRReturnStatement();
            ASTReturnStatement nn = (ASTReturnStatement) node;
            returnState.expr = expression(nn.expr.get(0));
            state.returnStat = returnState;
        }
		else if (node instanceof ASTSelectionStatement) {
			state.statType = "select";
			
			IRSelectStatement selectStat = new IRSelectStatement();
			ASTSelectionStatement n = (ASTSelectionStatement)node;
			ASTExpression condition = n.cond.get(0);
			selectStat.cond = expression(condition);
	        	        
	        selectStat.trueScope = compoundStatement(((ASTCompoundStatement)n.then));	
	        if (n.otherwise != null) {
	        	selectStat.hasFalse = true;
	            selectStat.falseScope = compoundStatement((ASTCompoundStatement)n.otherwise);
	        }  
	        state.selectStat = selectStat;
        }
		else if (node instanceof ASTIterationStatement) {
            //iterationStatement((ASTIterationStatement) node);
        }
		else if (node instanceof ASTIterationDeclaredStatement) {

			state.statType = "iterate";
			
			IRIterateStatement iterateStat = new IRIterateStatement();
			ASTIterationDeclaredStatement n = (ASTIterationDeclaredStatement)node;
	        String initType = n.init.specifiers.get(0).value;
	       
	        for (ASTInitList initList: n.init.initLists) {
	            ASTDeclarator declarator = initList.declarator;
	            if (declarator instanceof ASTVariableDeclarator) {
	                String varName = ((ASTVariableDeclarator) declarator).identifier.value;
	                
	                IRVariableDeclaration var = new IRVariableDeclaration();
	                var.flag = "local";
	                var.varType = initType;
	                var.varName = varName;
	                
	                if (initList.exprs.size() != 0) {
	                    // has initial value
	                	IRExpression init = new IRExpression();
	                    ASTExpression initValue = initList.exprs.get(0);
	                    init = expression(initValue);
	                    var.initExpr = init;
	                }
	                iterateStat.varNames.add(var);
	            } 
	            else if (declarator instanceof ASTArrayDeclarator) {
	                // todo
	            }
	        }

	        
	        iterateStat.cond = expression(n.cond.get(0));
	        
	        iterateStat.step = expression(n.step.get(0));

	        iterateStat.body = compoundStatement((ASTCompoundStatement)n.stat);
	        
	        state.iterateStat = iterateStat;
        }
		
		return state;
	}
	
private IRExpression expression(ASTExpression node) {
		
		IRExpression expr = new IRExpression();
		if (node instanceof ASTIntegerConstant) {
			
			expr.exprType = "const";
			
			int value = ((ASTIntegerConstant) node).value;
			expr.constval.type = "int";
			expr.constval.value = value;
			
		}
		else if (node instanceof ASTIdentifier) {
			
			expr.exprType = "var";
			
            String varName = ((ASTIdentifier) node).value;
            expr.var.varType = "int";
            expr.var.varName = varName;
                        
        }
		else if (node instanceof ASTBinaryExpression) {
			
			expr.exprType = "binary";
			
			ASTBinaryExpression n = (ASTBinaryExpression)node;
			expr.op = n.op.value;
			expr.initBinary();
	        if (expr.op.equals("=")) {
	        	
	            if (n.expr1 instanceof ASTIdentifier) {
	                String name = ((ASTIdentifier) n.expr1).value;
	                expr.opnd0.exprType = "var";
	                expr.opnd0.var.varName = name;
	            } 
	            expr.opnd1 = expression(n.expr2);
	            return expr;
	        }
	        else {	
	        	
	        	expr.opnd0 = expression(n.expr1);
	        	expr.opnd1 = expression(n.expr2);
	        }
	        
		}
		else if(node instanceof ASTPostfixExpression) {
			
			expr.exprType = "postfix";
			
			ASTPostfixExpression n = (ASTPostfixExpression)node;
			String op = n.op.value;
			expr.op = op;

	        if (op.equals("++")) {
	            if (n.expr instanceof ASTIdentifier) {
	                String name = ((ASTIdentifier) n.expr).value;
	                expr.var.varName = name;
	                expr.var.varType = "int";
	                  
	            }
	        }
	        else if (op.equals("--")) {
	            if (n.expr instanceof ASTIdentifier) {
	                String name = ((ASTIdentifier) n.expr).value;
	                expr.var.varName = name;
	                expr.var.varType = "int";
	            }
	        }
		}
		else if(node instanceof ASTFunctionCall) {
			
			expr.exprType = "call";
					
			ASTFunctionCall n = (ASTFunctionCall)node;
			for (ASTExpression arg: n.argList) {
	            expr.call.paramList.add(expression(arg));
	        }

	        expr.call.funcName = ((ASTIdentifier) n.funcname).value;
	       
		}
		
		return expr;
	}
}
