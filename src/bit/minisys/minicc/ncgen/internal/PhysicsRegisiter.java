package bit.minisys.minicc.ncgen.internal;

import bit.minisys.minicc.internal.symbol.Type;

// 为活跃区间分配的物理寄存器
public class PhysicsRegisiter {
	private Integer regId;
	private Type type;	// 寄存器存储的值类型 ?? 
	
	public PhysicsRegisiter(Integer id,Type type) {
		// TODO Auto-generated constructor stub
		this.regId = id;
		this.type = type;
	}
	public PhysicsRegisiter(Integer id) {
		// TODO Auto-generated constructor stub
		this.regId = id;
	}
	public Integer getRegId() {
		return regId;
	}
}
