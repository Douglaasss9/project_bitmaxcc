package bit.minisys.minicc.ncgen.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.icgen.internal.IRBuilder;
import bit.minisys.minicc.icgen.internal.ParamInfo;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.*;
import bit.minisys.minicc.internal.symbol.*;


public class RiscvCodeGen {
	private IRBuilder irBuider;
	private StringBuilder asmSb;
	private static Integer RISCV_REG_NUM = 17; 	// riscv����ʹ�õ�����Ĵ�����Ŀ 
	public static Integer RISCV_PARAM_NUM_IN_STACK = 4;
	
	public RiscvCodeGen(IRBuilder irBuider) {
		this.irBuider = irBuider;
	}
	public StringBuilder getAsmSb() {
		return asmSb;
	}
	
	public void run(MiniCCCfg cfg) {
		IRFile irfile = irBuider.getIrFile();
		
		asmSb = new StringBuilder();
		// -----------����ȫ��������.data------------------
		asmSb.append(".data\n");
		ArrayList<IRVariableDeclaration> dataDeclList=new ArrayList<IRVariableDeclaration>();
		for (IRDeclaration irdecl : irfile.globalSymTab.localSymTab) {
			if(irdecl instanceof IRVariableDeclaration) {
				dataDeclList.add((IRVariableDeclaration)irdecl);
			}
		}
		// �����ڴ��offset��С��������
		Collections.sort(dataDeclList,new Comparator<IRVariableDeclaration>() {
			@Override
			public int compare(IRVariableDeclaration o1, IRVariableDeclaration o2) {
				// TODO Auto-generated method stub
				return o1.offsize-o2.offsize;
			}
		});
		for (IRVariableDeclaration irvd : dataDeclList) {
			// Ϊÿ������.data����
			buildRiscvData(irvd,asmSb);
		}
		// ���õ����ַ�����������.data��
		creatStringConstAsm(asmSb,irfile.getGlobalStringConstMap());
		// ----------.data���������-------------------
		
		
		// ----------����ָ���.text-----------------
		asmSb.append(".text\n");
		// ������init�����ú���
		// ��ȡȫ�ֱ�����Ӧ�ļĴ���ӳ��
		Map<VirtualReg, VirtualRegProperty> globalVarPropMap = irBuider.getGlobalVarPropMap();
		// __init
		creatInitAsm(asmSb,irfile.globalSymTab.instructions,globalVarPropMap);
		// ���ú���(Mars_PrintInt, Mars_GetInt, Mars_PrintStr)
		creatBuiltInFuncAsm(asmSb);
		// ��ÿ���������ɶ�Ӧ��ָ��
		for (IRDeclaration irdecl : irfile.globalSymTab.localSymTab) {
			if(irdecl instanceof IRFunctionDeclaration) {
				String funcname = ((IRFunctionDeclaration)irdecl).funcname;
				BasicBlock start = ((IRFunctionDeclaration)irdecl).getStartBB();
				// ��IR��������ɼĴ�������
				StupidRegisterAlloc stpRegisterAlloc = new StupidRegisterAlloc((IRFunctionDeclaration)irdecl,RISCV_REG_NUM,globalVarPropMap,RISCV_PARAM_NUM_IN_STACK,false);
				stpRegisterAlloc.run(start);

				// �ӼĴ�����������ȡ������ջ��Ϣ
				// ��¼�ֲ�����+spillռ�õ�ջ�ռ�(sp + finalOffset)
				Integer finalOffset = stpRegisterAlloc.getNowOffset();
				// �����ǰ����irdecl�ڲ�������������������Ҫ����return address ����$31,finalOffset+=4
				if(stpRegisterAlloc.findCall) {
					finalOffset += 4;
					// x�������Ĵ����̶�
					finalOffset += 4*RISCV_PARAM_NUM_IN_STACK;
					// �����ǰ�����ڲ����ö��������ȡ����ռ��ջ�ռ�����offset���ӵ�finalOffset
					finalOffset += stpRegisterAlloc.argMoreSize;
				}
				// ����ָ������յ�irָ���б�����spill��load/store��
				LinkedList<IRInstruction> realInstrs = stpRegisterAlloc.getRealInstrs();
				
				// ����mips���
				RiscvBuilder riscvBuilder = new RiscvBuilder(stpRegisterAlloc.getInstrRevMap(),finalOffset,4*RISCV_PARAM_NUM_IN_STACK+stpRegisterAlloc.argMoreSize);
				riscvBuilder.setFuncname(funcname);
				for (IRInstruction ir : realInstrs) {
					riscvBuilder.visit(ir);
				}
				asmSb.append(riscvBuilder.getInstrSb().toString());
			}
		}
	}
	
	// Ϊÿ��ȫ�ֱ�������asm,��ʼ��Ϊ0
	private void buildRiscvData(IRVariableDeclaration decl, StringBuilder sb) {
		String name = decl.name;
		Type declType = decl.type;
		if(declType instanceof NormalType) {
			sb.append(name+" : ");
			if(declType == GlobalSymbolTable.intType) {
				sb.append(".word 0\n");
			}
		}else if (declType instanceof ArrayType) {
			sb.append(name+" : ");
			if(((ArrayType)declType).elementType == GlobalSymbolTable.intType) {
				sb.append(".zero "+declType.getRegisiterSize()+"\n");
			}
		}
	}
	// Ϊ�õ���str����ȫ�ֱ���
	private void creatStringConstAsm(StringBuilder sb,Map<String, String> StrConstmap) {
		sb.append("blank : .asciz \" \"\n");
		for(Map.Entry<String, String> entry : StrConstmap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			sb.append(value+" : .asciz " + key+"\n");
 		}
	}
	// ����asm����ں���__init����������ָ���Լ�ȫ�ֱ����ĳ�ʼ��
	private void creatInitAsm(StringBuilder sb,List<IRInstruction> initInstrs,Map<VirtualReg, VirtualRegProperty>globalVarPropMap) {
		sb.append("__init:\n");
		sb.append("\tli vsp, 0x80000000\n");	// sp
		sb.append("\taddi s0, vsp, 0\n");			// fp
		sb.append("\tli gp, 0x10010000\n");		// gp
		IRFunctionDeclaration initFuncDecl = new IRFunctionDeclaration("__init");
		initFuncDecl.setArgs(null);
		initFuncDecl.setBody(null);
		initFuncDecl.setFarOffset(0);
		initFuncDecl.setLabelMap(null);
		initFuncDecl.setLocalVarPropMap(new HashMap<VirtualReg, VirtualRegProperty>());
		initFuncDecl.setParamsInfo(new HashMap<VirtualReg, ParamInfo>());
		initFuncDecl.setReturnType(GlobalSymbolTable.voidType);
		initFuncDecl.setStartBB(null);
		StupidRegisterAlloc sra = new StupidRegisterAlloc(initFuncDecl, RISCV_REG_NUM, globalVarPropMap,RISCV_PARAM_NUM_IN_STACK,false);
		sra.runInit(initInstrs);
		Integer nowOffset = sra.getNowOffset();
		LinkedList<IRInstruction> realInstrs = sra.getRealInstrs();
		RiscvBuilder riscBuilder = new RiscvBuilder(sra.getInstrRevMap(),nowOffset,null);
		for (IRInstruction ir : realInstrs) {
			riscBuilder.visit(ir);
		}
		sb.append(riscBuilder.getInstrSb().toString());
		sb.append("\tjal ra, main\n");
		sb.append("\tli vsp, 10\n");
		sb.append("\tsyscall\n");
	}
	// ����BuiltInFunction
	private void creatBuiltInFuncAsm(StringBuilder sb) {
		//Mars_PrintInt:
		sb.append("Mars_PrintInt:\n");
		sb.append("\taddi s1, vsp, 0\n");	// rars��fp��Ϊsyscall�Ĳ����Ĵ���....
		sb.append("\taddi tp, a0, 0\n");	// ��ȡ������tp...
		sb.append("\tli vsp, 1\n");
		sb.append("\tsyscall\n");
		sb.append("\tla tp, blank\n");
		sb.append("\tli vsp, 4\n");
		sb.append("\tsyscall\n");
		sb.append("\taddi vsp, s1, 0\n");
		sb.append("\tret\n");
		//Mars_GetInt:
		sb.append("Mars_GetInt:\n");
		sb.append("\taddi s1, vsp, 0\n");
		sb.append("\tli vsp, 5\n");
		sb.append("\tsyscall\n");
		sb.append("\taddi x10, vsp, 0\n");
		sb.append("\taddi vsp, s1, 0\n");
		sb.append("\tret\n");
		//Mars_PrintStr:
		sb.append("Mars_PrintStr:\n");
		sb.append("\taddi s1, vsp, 0\n");
		sb.append("\taddi tp, a0, 0\n");	// ��ȡ������tp...
		sb.append("\tli vsp, 4\n");
		sb.append("\tsyscall\n");
		sb.append("\taddi vsp, s1, 0\n");
		sb.append("\tret\n");
	}
}
