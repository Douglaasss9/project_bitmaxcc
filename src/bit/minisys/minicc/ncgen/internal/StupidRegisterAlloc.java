package bit.minisys.minicc.ncgen.internal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.python.antlr.PythonParser.if_stmt_return;
import org.python.constantine.platform.darwin.Signal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.icgen.internal.IROpVisitor;
import bit.minisys.minicc.icgen.internal.ParamVirtualReg;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.*;
import bit.minisys.minicc.internal.symbol.*;


public class StupidRegisterAlloc {
	Map<VirtualReg, VirtualRegProperty> localVarPropMap;
	Map<VirtualReg, VirtualRegProperty> globalVarPropMap;
	
	private LinkedList<PhysicsRegisiter> phyRegs;	// 可以使用的物理寄存器
	private Map<VirtualReg, PhysicsRegisiter> regMap;	// 记录当前被分配物理寄存器的vReg
	private Integer nowOffset;						// 记录当前栈帧位置,spill时如果没有对应栈帧位置，则从nowOffset开始分配并更新nowOffset
	private LinkedList<IRInstruction> realInstrs;
	private Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> instrRevMap;	// 记录从IR转换至汇编时每条IR中的vReg和phyReg/stackslot的对应关系
	private Map<PhysicsRegisiter,VirtualReg> phyRegUsed;
	
	private Set<BasicBlock> visit;		
	private LinkedList<BasicBlock> bbs;	// 正常顺序遍历得到的线性BB
	private Set<PhysicsRegisiter> nowInstrUsedPhy = new HashSet<PhysicsRegisiter>();
	public boolean findCall = false;
	public Integer argMoreSize = 0;
	
	private Integer paramNumInStack = 0;
	private Boolean stdcall = false;
	
	public Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> getInstrRevMap() {
		return instrRevMap;
	}
	public LinkedList<IRInstruction> getRealInstrs() {
		return realInstrs;
	}
	public Integer getNowOffset() {
		return nowOffset;
	}
	private void initPhyRegs(int num) {
		phyRegUsed = new HashMap<PhysicsRegisiter, VirtualReg>();
		phyRegs = new LinkedList<PhysicsRegisiter>();
		for(int i = 0; i<num; i++) {
			phyRegs.add(new PhysicsRegisiter(i));
		}
	}
	public StupidRegisterAlloc(IRFunctionDeclaration irFuncDecl,Integer phyRegNum,Map<VirtualReg, VirtualRegProperty> globalVarPropMap, Integer paramNumInStack, Boolean stdcall) {
		// 这里先临时初始化一下物理寄存器
		initPhyRegs(phyRegNum);
		
		// TODO Auto-generated constructor stub
		this.realInstrs = new LinkedList<IRInstruction>();
		this.visit = new HashSet<BasicBlock>();
		this.bbs = new LinkedList<BasicBlock>();
		this.instrRevMap = new HashMap<IRInstruction, Map<VirtualReg,PhysicsRegisiter>>();
		this.regMap = new HashMap<VirtualReg, PhysicsRegisiter>();
		//this.paramsProcessed = new HashSet<VirtualReg>();
		// 1. 从IRBulider获取ramMap
		this.globalVarPropMap = globalVarPropMap;
		// 2. 从IRBuilder获取stackMap
		this.localVarPropMap = irFuncDecl.getLocalVarPropMap();
		// 3. 从IRBuilder获取可以存放临时变量的栈上offset值
		this.nowOffset = irFuncDecl.getFarOffset();
		// 
		this.paramNumInStack = paramNumInStack;
		this.stdcall = stdcall;
	}
	private Integer	spillTimes = 0;
	public Integer getSpillTimes() {
		return spillTimes;
	}
	// 添加load指令
	private void addLoad(VirtualReg vreg,PhysicsRegisiter preg) {
		spillTimes++;
		IRInstruction loadInstr = null;
		VirtualRegProperty vrp;
		if(localVarPropMap.containsKey(vreg)) {
			vrp = localVarPropMap.get(vreg);
			loadInstr = new IRDassign(vreg, new IRIreadfpoff(vreg.getType(),  new IRConstval(GlobalSymbolTable.intType, vrp.getOffset())));
		}else if (globalVarPropMap.containsKey(vreg)) {
			vrp = globalVarPropMap.get(vreg);
			loadInstr = new IRDassign(vreg, new IRIread(vreg.getType(), GlobalSymbolTable.pointerType, new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset())));
		}else {
			System.out.println(">>StupidRegisterAlloc: addLoad null.");
		}
		Map<VirtualReg, PhysicsRegisiter> loadMap = new HashMap<VirtualReg, PhysicsRegisiter>();
		loadMap.put(vreg, preg);
		instrRevMap.put(loadInstr, loadMap);
		realInstrs.add(loadInstr);
	}
	// 添加store指令
	private void addStore(VirtualReg vreg,PhysicsRegisiter preg) {
		spillTimes++;
		IRInstruction storeInstr = null;
		VirtualRegProperty vrp;
		if(localVarPropMap.containsKey(vreg)) {
			vrp = localVarPropMap.get(vreg);
			storeInstr = new IRIassignfpoff(vreg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vreg);
		}else if (globalVarPropMap.containsKey(vreg)) {
			vrp = globalVarPropMap.get(vreg);
			storeInstr = new IRIassign(vreg.getType(), new IRConstval(GlobalSymbolTable.intType,  vrp.getOffset()), vreg);
		}else {
			// 在local上分配stackSlot
			stackAlloc(vreg);
			vrp = localVarPropMap.get(vreg);
			storeInstr = new IRIassignfpoff(vreg.getType(), new IRConstval(GlobalSymbolTable.intType, vrp.getOffset()), vreg);
		}
		Map<VirtualReg, PhysicsRegisiter> storeMap = new HashMap<VirtualReg, PhysicsRegisiter>();
		storeMap.put(vreg, preg);
		instrRevMap.put(storeInstr, storeMap);
		realInstrs.add(storeInstr);
	}
	// 对尚未在stack上分配空间的vreg，分配stackslot
	private void stackAlloc(VirtualReg vreg) {
		VirtualRegProperty vrp = new VirtualRegProperty();
		vrp.setOffset(nowOffset);
		localVarPropMap.put(vreg, vrp);
		ObjectMapper mapper = new ObjectMapper();
		nowOffset += vreg.getType().getRegisiterSize();
	}
	
	
	public void runInit(List<IRInstruction> initInstrs) {
		visit.clear();
		phyRegUsed.clear();
		for (IRInstruction instr : initInstrs) {
			nowInstrUsedPhy.clear();
			IROpVisitor visitor = new IROpVisitor();
			visitor.visit(instr);
			VirtualReg x = visitor.getRes();
			VirtualReg y = visitor.getOp1();
			VirtualReg z = visitor.getOp2();
			Map<VirtualReg, PhysicsRegisiter> singleMap = new HashMap<VirtualReg, PhysicsRegisiter>();
			if(y != null) {
				alloc(y,singleMap,"opnd");
				if (!loadedReg.contains(y)) {
					addLoad(y,singleMap.get(y));
					loadedReg.add(y);
				}
			}
			if(z != null) {
				alloc(z,singleMap,"opnd");
				if (!loadedReg.contains(z)) {
					addLoad(z,singleMap.get(z));
					loadedReg.add(z);
				}
			}
			if(x != null) {
				alloc(x,singleMap,"res");
				loadedReg.add(x);
			}
			instrRevMap.put(instr, singleMap);
			realInstrs.add(instr);
		}
	}
	
	private Set<VirtualReg> loadedReg = new HashSet<VirtualReg>();
	public void run(BasicBlock startBB) {
		// 4. 从LIRBuilder获取startBB,按顺序构建BB列表
		processBB(startBB);
		visit.clear();
		phyRegUsed.clear();
		normalOrder(startBB);
		//paramsProcessed.clear();
		// 5. 对每个BB进行寄存器分配，在开始和结束分别添加load和store(load : use, store: def)
		for (BasicBlock bb : bbs) {
			IRInstruction nowInstr = bb.getFirst();
			if(nowInstr == null) {
				continue;
			}
			loadedReg.clear();	// 记录已经loaded过的vreg
			regMap.clear();
			phyRegUsed.clear();
			//paramsProcessed.clear();
			boolean flag = false;
			if (bb.getEnd() != null && bb.getEnd().isJump()) {
				flag = true;
			}
			while(true) {
				nowInstrUsedPhy.clear();
				IROpVisitor visitor = new IROpVisitor();
				visitor.visit(nowInstr);	// nowInstr : res = opnds
				VirtualReg x = visitor.getRes();
				VirtualReg y = visitor.getOp1();
				VirtualReg z = visitor.getOp2();
				// 对函数调用语句的特殊处理(x = call();或者call();)
				LinkedList<VirtualReg> callArgs = visitor.getCallArgs();

				Map<VirtualReg, PhysicsRegisiter> singleMap = new HashMap<VirtualReg, PhysicsRegisiter>();
				IRInstruction realNowInstr = nowInstr;

				if(visitor.getParamId() > 0) {
					// 说明当前指令是加载参数到寄存器
					Integer paramId = visitor.getParamId();
					Integer paramNum = visitor.getParamNum();
					if (paramId > this.paramNumInStack) {
						VirtualReg vReg = (VirtualReg) ((IRDassign)nowInstr).varName;
						Integer paramOffset = 0;
						if(this.stdcall) {
							paramOffset = -(paramNum - paramId + 1)*4-4;
						}
						else {
							paramOffset = -paramId*4;
						}
						realNowInstr = new IRDassign(vReg, new IRIreadfpoff(vReg.getType(), new IRConstval(GlobalSymbolTable.intType, paramOffset)));
					}
				}
				
				// opnd部分:
				if(y != null) {
					alloc(y,singleMap,"opnd");
					if (!loadedReg.contains(y)) {
						addLoad(y,singleMap.get(y));
						loadedReg.add(y);
					}
				}
				if(z != null) {
					alloc(z,singleMap,"opnd");
					if (!loadedReg.contains(z)) {
						addLoad(z,singleMap.get(z));
						loadedReg.add(z);
					}
				}
				if(callArgs!=null) {
					for (VirtualReg virtualReg : callArgs) {
						alloc(virtualReg, singleMap, "opnd");
						if(!loadedReg.contains(virtualReg)) {
							addLoad(virtualReg, singleMap.get(virtualReg));
							loadedReg.add(virtualReg);
						}
					}
					
					// 记录函数中出现的call指令中调用的函数信息,这部分为了确定栈的参数部分占用空间大小
					findCall = true;
					IRCall callInstr;
					if (nowInstr instanceof IRCall) {
						callInstr = (IRCall) nowInstr;
					}else {
						callInstr = ((IRCall)((IRDassign)nowInstr).rhsExpr);
					}
					Integer argNum = callInstr.args.size();
					if(argNum>paramNumInStack) {
						// fix me 这里按照参数都是int类型
						int tmp = 4*(argNum-paramNumInStack);
						if (tmp > argMoreSize) {
							argMoreSize = tmp;
						}
					}
				}
				
				if(callArgs!=null) {
					// 在call之前增加store指令，把当前的所有使用的物理寄存器的值存到对应的Vreg对应的stack中
					for(Map.Entry<PhysicsRegisiter, VirtualReg> entry:phyRegUsed.entrySet()) {
						PhysicsRegisiter key = entry.getKey();
						VirtualReg value = entry.getValue();
						if(value == x) continue;
						if(!localVarPropMap.containsKey(value) && !globalVarPropMap.containsKey(value)) {
							stackAlloc(value);
						}
						// 增加store
						addStore(value,key);
					}
					// 清空loaded
					loadedReg.clear();
					// 清空当前使用的所有物理寄存器
					regMap.clear();
					phyRegUsed.clear();
					
					// 添加IRCall而非x = call()
					if(nowInstr instanceof IRCall) {
						// fun(..)形式
						instrRevMap.put(nowInstr, singleMap);
						realInstrs.add(nowInstr);
					}else {
						// x = fun(..)形式 拆解为fun(..) 和 x = $2(返回值寄存器);
						alloc(x,singleMap,"res");
						loadedReg.add(x);
						
						instrRevMap.put(((IRDassign)nowInstr).rhsExpr, singleMap);
						realInstrs.add(((IRDassign)nowInstr).rhsExpr);
						
						IRDassign resDassign = new IRDassign(x, new ParamVirtualReg(-1,0));
						instrRevMap.put(resDassign, singleMap);
						realInstrs.add(resDassign);
						
						//addStore(x, singleMap.get(x));
					}
					
				}else {
					// 非call类型指令，正常处理
					// res部分:
					if(x != null) {
						alloc(x,singleMap,"res");
						loadedReg.add(x);
					}
					
					if (nowInstr == bb.getEnd() && flag) {
						// 如果BB指令结束为jump,在此之前增加store
						for (Entry<VirtualReg, PhysicsRegisiter> entry : regMap.entrySet()) {
							VirtualReg key = entry.getKey();
							PhysicsRegisiter value = entry.getValue();
							addStore(key, value);
						}
					}
					
					instrRevMap.put(realNowInstr, singleMap);
					realInstrs.add(realNowInstr);
					
					if (nowInstr == bb.getEnd() && !flag) {
						// 如果BB指令结束为jump,在此之前增加store
						for (Entry<VirtualReg, PhysicsRegisiter> entry : regMap.entrySet()) {
							VirtualReg key = entry.getKey();
							PhysicsRegisiter value = entry.getValue();
							addStore(key, value);
						}
					}
					
					//if(x != null) {
					//	addStore(x, singleMap.get(x));
					//}
					
				}
				
				// 取当前BB的下一条指令，如果是当前BB的最后一条，就退出当前BB
				if(nowInstr == bb.getEnd())
					break;
				nowInstr = nowInstr.getNext();
			}
			

		}
		
		// 输出构建好的新指令列表，以及每条指令对应的寄存器转换关系
		//outputNewInstrs();
	}
	public void outputNewInstrs() {
		for(int i = 0; i<realInstrs.size(); i++) {
			IRInstruction nowInstr = realInstrs.get(i);
			ObjectMapper mapper = new ObjectMapper();
			try {
				System.out.println(mapper.writeValueAsString(nowInstr));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Map<VirtualReg, PhysicsRegisiter> vpMap = instrRevMap.get(nowInstr);
			System.out.print("( ");
			for (Map.Entry<VirtualReg, PhysicsRegisiter> entry : vpMap.entrySet()) {
				VirtualReg key =  entry.getKey();
				PhysicsRegisiter value = entry.getValue();
				System.out.print(key.getName()+"->"+value.getRegId()+",");
			}
			System.out.println(" )");
		}
	}
	private void alloc(VirtualReg vReg, Map<VirtualReg, PhysicsRegisiter> map,String subType) {
		PhysicsRegisiter finalReg = null;
		// 如果vReg已经有一个物理寄存器了，就分配给他
		if(regMap.containsKey(vReg)) {
			finalReg = regMap.get(vReg);
			map.put(vReg, finalReg);
			nowInstrUsedPhy.add(finalReg);
		}
		// 如果没有，则为该寄存器分配（或者spill）
		else {
			PhysicsRegisiter unusedReg = getAvailableReg();
			if(unusedReg == null) {
				// 需要spill ，把spilledReg从原来的分配移除
				VirtualReg spilledReg = spillOne();
				PhysicsRegisiter emptyReg = regMap.get(spilledReg);
				regMap.remove(spilledReg);
				regMap.put(vReg, emptyReg);
				phyRegUsed.remove(emptyReg);
				phyRegUsed.put(emptyReg, vReg);
				
				loadedReg.remove(spilledReg);
				
				map.put(vReg, emptyReg);
				
				addStore(spilledReg, emptyReg);
				unusedReg = emptyReg;	

			}else {
				regMap.put(vReg, unusedReg);
				map.put(vReg, unusedReg);
				phyRegUsed.put(unusedReg, vReg);
			}
			
			finalReg = unusedReg;
		}
	}
	
	
	private VirtualReg spillOne() {
		VirtualReg virtualReg = null;
		for (Map.Entry<VirtualReg, PhysicsRegisiter> entry : regMap.entrySet()) {
			virtualReg =  entry.getKey();
			PhysicsRegisiter phyReg = entry.getValue();
			if(nowInstrUsedPhy.contains(phyReg)) {
				continue;
			}else {
				nowInstrUsedPhy.add(phyReg);
				break;
			}
			
		}
		return virtualReg;
	}
	private PhysicsRegisiter getAvailableReg() {
		for(int i = 0; i<phyRegs.size(); i++) {
			PhysicsRegisiter pr = phyRegs.get(i);
			if(!phyRegUsed.containsKey(pr) && !nowInstrUsedPhy.contains(pr)) {
				nowInstrUsedPhy.add(pr);
				return pr;
			}
				
		}
		return null;
	}
	private void processBB(BasicBlock node) {
		if(node == null) return;
		visit.add(node);
		if(node.getFirst() == null) {
			//The end bb 
			return;
		}
		IRInstruction end = node.getEnd();
		if(end == null )return;
	//	if(end.getNext() != null) {
			BasicBlock tbb = null;
			BasicBlock tbb2 = null;
			if(end instanceof IRBrfalse) {
				tbb = ((IRBrfalse)end).getFalseNext().getBb();
				node.addSucc(tbb);
				tbb2 = ((IRBrfalse)end).getTrueNext().getBb();
				node.addSucc(tbb2);
				//System.out.println("brfalse：："+tbb.getBbiD()+","+tbb2.getBbiD());
			}else if (end instanceof IRBrtrue) {
				tbb = ((IRBrtrue)end).getFalseNext().getBb();
				node.addSucc(tbb);
				tbb2 = ((IRBrtrue)end).getTrueNext().getBb();
				node.addSucc(tbb2);
			}else if (end instanceof IRGotoStatement) {
				tbb = ((IRGotoStatement)end).getAbsNext().getBb();
				node.addSucc(tbb);
			}else if (end instanceof IRReturnStatement) {
				tbb = ((IRReturnStatement)end).getEndBlock();
				node.addSucc(tbb);
			}else {
				tbb = end.getNext().getBb();
				node.addSucc(tbb);
			}
			if(!visit.contains(tbb))
				processBB(tbb);
			if(tbb2!=null && !visit.contains(tbb2))
				processBB(tbb2);
	//	}
		
	}
	private void normalOrder(BasicBlock node) {
		if(visit.contains(node)) {
			return;
		}
		visit.add(node);
		bbs.add(node);
		if(node.isExit())
			return;
		IRInstruction end = node.getEnd();
		if(end != null && end.getNext()!=null) {
			normalOrder(end.getNext().getBb());
		}
	}
}
