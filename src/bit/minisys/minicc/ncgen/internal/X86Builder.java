package bit.minisys.minicc.ncgen.internal;

import java.util.Map;

import bit.minisys.minicc.icgen.internal.IRVisitor;
import bit.minisys.minicc.icgen.internal.ParamVirtualReg;
import bit.minisys.minicc.internal.ir.*;

public class X86Builder implements IRVisitor{
	StringBuilder instrSb = new StringBuilder();
	Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> instrRevMap;	// 记录从IR转换至汇编时每条IR中的vReg和phyReg/stackslot的对应关系
	//String branchOp = "";
	String funcname = "";
	
	private String firstGlobalVarName;
	private Integer finalOffset;
	private Integer argsOffset;
	public X86Builder(Map<IRInstruction, Map<VirtualReg, PhysicsRegisiter>> instrRevMap,Integer finalOffset,Integer argsOffset) {
		this.instrRevMap = instrRevMap;
		this.finalOffset = finalOffset;
		this.argsOffset = argsOffset;
	}
	
	public void setFirstGlobalVarName(String firstGlobalVarName) {
		this.firstGlobalVarName = firstGlobalVarName;
	}
	public void setFuncname(String funcname) {
		this.funcname = funcname;
	}
	
	public StringBuilder getInstrSb() {
		return instrSb;
	}
	
	@Override
	public void visit(IRDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRFunctionDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRVariableDeclaration node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRInstruction node) {
		if(node == null) return;
		if(node instanceof IRStatement) {
			visit((IRStatement)node);	
		}
		else if (node instanceof IRExpression) {
			visit((IRExpression)node);
		}
	}

	@Override
	public void visit(IRStatement node) {
		if(node instanceof IRBrfalse) {
			visit((IRBrfalse)node);
		}else if (node instanceof IRBrtrue) {
			visit((IRBrtrue)node);
		}else if (node instanceof IRGotoStatement) {
			visit((IRGotoStatement)node);
		}else if (node instanceof IRLabel) {
			visit((IRLabel)node);
		}else if (node instanceof IRReturnStatement) {
			visit((IRReturnStatement)node);
		}
	}

	@Override
	public void visit(IRBrfalse node) {
		String label = node.label;
		String op = "";
		Integer resId = instrRevMap.get(node).get((VirtualReg)node.opnd0).getRegId();
		op = "beq";
		buildASM(op, label, resId, null, null,false);
	}

	@Override
	public void visit(IRBrtrue node) {
		String label = node.label;
		String op = "";
		Integer resId = instrRevMap.get(node).get((VirtualReg)node.opnd0).getRegId();
		op = "bne";
		buildASM(op, label, resId, null, null,false);
	}

	@Override
	public void visit(IRGotoStatement node) {
		String label = node.label;
		String op = "j";
		buildASM(op, label, null, null, null,false);
	}

	@Override
	public void visit(IRLabel node) {
		buildASM("label", node.label, null, null, null,false);
		if(x86LabelChange(node.label).equals(funcname)) {
			buildASM("saveOldFp", null, null, null, null, false);
			buildASM("spIn", null, null, null, null, false);
		}
	}

	@Override
	public void visit(IRReturnStatement node) {
		if(node.opnd != null) {
			Integer resId = instrRevMap.get(node).get((VirtualReg)node.opnd).getRegId();
			buildASM("moveReg", null, 0, resId, null,false);
		}
		buildASM("spOut", null, null, null, null, false);
		buildASM("ret", null, null, null, null,false);
	}

	@Override
	public void visit(IRScope node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRExpression node) {
		if(node instanceof IRUnaryOperator) {
			visit((IRUnaryOperator)node);
		}else if (node instanceof IRBinaryOperator) {
			visit((IRBinaryOperator)node);
		}else if (node instanceof IRTernaryOperator) {
			visit((IRTernaryOperator)node);
		}else if (node instanceof IRCastOperator) {
			visit((IRCastOperator)node);
		}else if (node instanceof IRCall) {
			visit((IRCall)node);
		}else if (node instanceof IRLeaf) {
			visit((IRLeaf)node);
		}else if (node instanceof IRIread) {
			visit((IRIread)node);
		}else if (node instanceof IRIreadfpoff) {
			visit((IRIreadfpoff)node);
		}else if (node instanceof IRDassign) {
			visit((IRDassign)node);
		}else if (node instanceof IRIassign) {
			visit((IRIassign)node);
		}else if (node instanceof IRIassignfpoff) {
			visit((IRIassignfpoff)node);
		}
	}

	@Override
	public void visit(IRUnaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRInot node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNeg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBinaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAdd node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRBxor node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCmp node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRDiv node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IREq node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRGt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLand node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLior node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLshr node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRLt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRMul node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRNe node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRRem node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRShl node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSub node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRTernaryOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCastOperator node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCvt node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRCall node) {
		// 设置参数
		Integer argNum = node.args.size();
		for(int i = 1; i<=argNum; i++) {
			Integer phyRegid = instrRevMap.get(node).get((VirtualReg)node.args.get(i-1)).getRegId();
			buildASM("storeArgStack", null, phyRegid, null, null, false);
		}

		// call funcname， call自动设置返回地址
		buildASM("call", node.funcname, null, null, null, false);

		// 弹出参数
		for(int i = 1; i<=argNum; i++) {
			buildASM("popArgStack", null, null, null, null, false);
		}

	}

	@Override
	public void visit(IRLeaf node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRConstval node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRSizeoftype node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRAddrof node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(VirtualReg node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRDassign node) {
		VirtualReg res = (VirtualReg)node.varName;
		Integer resId = instrRevMap.get(node).get(res).getRegId();
		if(node.rhsExpr instanceof IRConstval) {
			Integer opndId1 = (Integer)((IRConstval)node.rhsExpr).value;
			buildASM("moveImm",null,resId,opndId1,null,false);
		}else if(node.rhsExpr instanceof IRAddrof) {
			// la
			String addrStr = ((IRAddrof)node.rhsExpr).varName;
			buildASM("la",addrStr,resId,null,null,false);
		}else if(node.rhsExpr instanceof VirtualReg) {
			// mov
			Integer opndId1;
			boolean flag;
			if(node.rhsExpr instanceof ParamVirtualReg) {
				opndId1 = ((ParamVirtualReg)node.rhsExpr).getParamId();
				opndId1 = x86ParamRegIdChange(opndId1);
				flag = true;
			}else {
				opndId1 = instrRevMap.get(node).get((VirtualReg)node.rhsExpr).getRegId();
				flag = false;
			}
			buildASM("moveReg",null,resId,opndId1,null,flag);
		}else if (node.rhsExpr instanceof IRNeg) {
			IRNeg neg = (IRNeg)node.rhsExpr;
			Integer opndId1 = instrRevMap.get(node).get((VirtualReg)neg.opnd0).getRegId();
			buildASM("neg", null, resId, opndId1, null, false);
		}else if (node.rhsExpr instanceof IRInot) {
			IRInot inot = (IRInot)node.rhsExpr;
			Integer opndId1 = instrRevMap.get(node).get((VirtualReg)inot.opnd0).getRegId();
			buildASM("inot", null, resId, opndId1, null, false);
		}else if (node.rhsExpr instanceof IRBinaryOperator) {
			// 各种二元操作
			String op = "";
			IRBinaryOperator bop = (IRBinaryOperator)node.rhsExpr;
			Integer opndId1 = instrRevMap.get(node).get((VirtualReg)bop.opnd0).getRegId();
			Integer opndId2 = null;
			if(bop instanceof IRAdd) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "addImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "add";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRSub) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "subImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "sub";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRMul) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "mulImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "mul";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRDiv) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "divImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "div";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRRem) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "modImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "mod";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRLt || bop instanceof IREq || bop instanceof IRGt || bop instanceof IRLe || bop instanceof IRGe || bop instanceof IRNe) {
				if(bop instanceof IRLt) {
					op = "lt";
				}else if (bop instanceof IREq) {
					op = "eq";
				}else if (bop instanceof IRGt) {
					op = "gt";
				}else if (bop instanceof IRLe) {
					op = "le";
				}else if (bop instanceof IRGe) {
					op = "ge";
				}else if (bop instanceof IRNe) {
					op = "ne";
				}
				if(bop.opnd1 instanceof IRConstval) {
					op += "Imm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRShl) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "shlImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "shl";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRLshr) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "lshrImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "lshr";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRBand) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "bandImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "band";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRBxor) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "bxorImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "bxor";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRBior) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "biorImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "bior";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRLand) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "landImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "land";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}else if (bop instanceof IRLior) {
				if(bop.opnd1 instanceof IRConstval) {
					op = "liorImm";
					opndId2 = (Integer)((IRConstval)bop.opnd1).value;
				}else {
					op = "lior";
					opndId2 = instrRevMap.get(node).get((VirtualReg)bop.opnd1).getRegId();
				}
			}

			buildASM(op, null, resId, opndId1, opndId2,false);
		}else if (node.rhsExpr instanceof IRIread) {
			// load from ram
			String op = "";
			IRIread rIread = (IRIread)node.rhsExpr;
			Integer opndId1 = null;
			if(rIread.addrExpr instanceof IRConstval) {
				op = "lwRamImm";
				opndId1 = (Integer)((IRConstval)rIread.addrExpr).value;
			}else if (rIread.addrExpr instanceof VirtualReg) {
				op = "lwRamReg";
				opndId1 = instrRevMap.get(node).get((VirtualReg)rIread.addrExpr).getRegId();
			}
			buildASM(op, null, resId, opndId1, null,false);

		}else if (node.rhsExpr instanceof IRIreadfpoff) {
			// load from stack
			String op = "";
			IRIreadfpoff rIread = (IRIreadfpoff)node.rhsExpr;
			Integer opndId1 = null;
			if(rIread.offsetExpr instanceof IRConstval) {
				op = "lwStackImm";
				opndId1 = (Integer)((IRConstval)rIread.offsetExpr).value;
				opndId1 *= -1;	// 栈向下增长
				if(opndId1 <= 0)
					opndId1 -= 4;
			}else if (rIread.offsetExpr instanceof VirtualReg) {
				op = "lwStackReg";
				opndId1 = instrRevMap.get(node).get((VirtualReg)rIread.offsetExpr).getRegId();
			}
			buildASM(op, null, resId, opndId1, null,false);
		}
	}

	@Override
	public void visit(IRIassign node) {
		// store to ram
		String op = "";
		Integer opndId1 = null;
		if(node.addrExpr instanceof IRConstval) {
			op = "swRamImm";
			opndId1 = (Integer)((IRConstval)node.addrExpr).value;
		}else if (node.addrExpr instanceof VirtualReg) {
			op = "swRamReg";
			opndId1 = instrRevMap.get(node).get((VirtualReg)node.addrExpr).getRegId();
		}
		
		Integer resId = null;
		if(node.rhsExpr instanceof VirtualReg) {
			resId = instrRevMap.get(node).get((VirtualReg)node.rhsExpr).getRegId();
		}
		buildASM(op, null, resId, opndId1, null,false);
	}

	@Override
	public void visit(IRIassignoff node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIassignfpoff node) {
		// store to stack
		String op = "";
		Integer opndId1 = null;
		if(node.offsetExpr instanceof IRConstval) {
			op = "swStackImm";
			opndId1 = (Integer)((IRConstval)node.offsetExpr).value;
			opndId1 *= -1;
			if(opndId1 <= 0)
				opndId1 -= 4;
		}else if (node.offsetExpr instanceof VirtualReg) {
			op = "swStackReg";
			opndId1 = instrRevMap.get(node).get((VirtualReg)node.offsetExpr).getRegId();
		}
		
		Integer resId = null;
		if(node.rhsExpr instanceof VirtualReg) {
			resId = instrRevMap.get(node).get((VirtualReg)node.rhsExpr).getRegId();
		}
		buildASM(op, null, resId, opndId1, null,false);
	}
	@Override
	public void visit(IRIreadfpoff node) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IRIread node) {
		// TODO Auto-generated method stub
		
	}

	
	private void buildASM(String opname,String label,Integer resId, Integer op1Id, Integer op2Id, boolean flag) {
		String op1Reg = null;
		if(op1Id != null) {
			op1Reg = x86RegIdChange(op1Id);
		}
		if (opname.equals("la")) {
			instrSb.append("\tlea "+x86RegIdChange(resId)+", "+label);
		}else if (opname.equals("saveOldFp")) {
			instrSb.append("\tpush ebp\n");
			instrSb.append("\tmov ebp, esp");
		}else if (opname.equals("spIn")) {
			instrSb.append("\tsub esp, "+finalOffset);
		}else if (opname.equals("spOut")) {
			instrSb.append("\tmov esp, ebp\n");
			instrSb.append("\tpop ebp");
		}else if (opname.equals("storeArgStack")) {
			instrSb.append("\tpush "+x86RegIdChange(resId));
		}else if (opname.equals("popArgStack")) {
			instrSb.append("\tsub esp, 4");
		}else if (opname.equals("label")) {
			instrSb.append(x86LabelChange(label)+":");
		}else if (opname.equals("neg")) {
			instrSb.append("\tmov "+x86RegIdChange(resId)+", "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tnot "+x86RegIdChange(resId));
		}else if (opname.equals("inot")) {
			instrSb.append("\tcmp "+x86RegIdChange(op1Id)+", 0\n");
			instrSb.append("\tsete "+getXL(resId)+"\n");	// sete只能作用于8位的
			instrSb.append("\tand "+x86RegIdChange(resId)+", 1");
		}else if (opname.equals("moveReg")) {
			instrSb.append("\tmov "+x86RegIdChange(resId)+", "+op1Reg);
		}else if (opname.equals("moveImm")) {
			instrSb.append("\tmov "+x86RegIdChange(resId)+", "+op1Id);
		}else if (opname.equals("add")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tadd esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("addImm")) {
			instrSb.append("\tmov esi, "+op2Id+"\n");
			instrSb.append("\tadd esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("sub")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tsub esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("subImm")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tsub esi, "+op2Id+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("mul")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\timul esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("mulImm")) {
			instrSb.append("\timul "+x86RegIdChange(resId)+", "+x86RegIdChange(op1Id)+", "+op2Id);
		}else if (opname.equals("div")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tpush edx\n");
			instrSb.append("\tmov esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tmov eax, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tcdq\n");
			instrSb.append("\tidiv esi\n");
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tpop edx\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("mod")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tpush edx\n");
			instrSb.append("\tmov esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tmov eax, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tcdq\n");
			instrSb.append("\tidiv esi\n");
			instrSb.append("\tmov esi, edx\n");
			instrSb.append("\tpop edx\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("shl")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tpush ecx\n");
			instrSb.append("\tmov ecx, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tshl esi, cl\n");
			instrSb.append("\tpop ecx\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("lshr")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tpush ecx\n");
			instrSb.append("\tmov ecx, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tsar esi, cl\n");
			instrSb.append("\tpop ecx\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("band")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tand esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("bxor")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\txor esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("bior")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tor esi, "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("land")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp esi, 0\n");
			instrSb.append("\tsetne al\n");	// sete只能作用于8位的
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tcmp "+x86RegIdChange(resId)+", 0\n");
			instrSb.append("\tsetne "+getXL(resId)+"\n");	// sete只能作用于8位的
			instrSb.append("\tand "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("lior")) {
			instrSb.append("\tmov esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp esi, 0\n");
			instrSb.append("\tsetne al\n");	// sete只能作用于8位的
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tcmp "+x86RegIdChange(resId)+", 0\n");
			instrSb.append("\tsetne "+getXL(resId)+"\n");	// sete只能作用于8位的
			instrSb.append("\tand "+x86RegIdChange(resId)+", 1\n");
			instrSb.append("\tor "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("lt")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp "+x86RegIdChange(op1Id)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tsetl al\n");
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("le")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp "+x86RegIdChange(op1Id)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tsetle al\n");
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("eq")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp "+x86RegIdChange(op1Id)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tsete al\n");
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("ne")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp "+x86RegIdChange(op1Id)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tsetne al\n");
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("gt")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp "+x86RegIdChange(op1Id)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tsetg al\n");
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("ge")) {
			instrSb.append("\tpush eax\n");
			instrSb.append("\tcmp "+x86RegIdChange(op1Id)+", "+x86RegIdChange(op2Id)+"\n");
			instrSb.append("\tsetge al\n");
			instrSb.append("\tmov esi, eax\n");
			instrSb.append("\tand esi, 1\n");
			instrSb.append("\tpop eax\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", esi");
		}else if (opname.equals("lwStackReg")) {
			instrSb.append("\tmov esi, ebp\n");
			instrSb.append("\tsub esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tsub esi, 4\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", [esi]");
		}else if (opname.equals("lwStackImm")) {
			instrSb.append("\tmov esi, ebp\n");
			instrSb.append("\tadd esi, "+op1Id+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", [esi]");
		}else if (opname.equals("lwRamReg")) {
			instrSb.append("\tlea esi, "+firstGlobalVarName+"\n");
			instrSb.append("\tadd esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", [esi]");
		}else if (opname.equals("lwRamImm")) {
			instrSb.append("\tlea esi, "+firstGlobalVarName+"\n");
			instrSb.append("\tadd esi, "+op1Id+"\n");
			instrSb.append("\tmov "+x86RegIdChange(resId)+", [esi]");
		}else if (opname.equals("swStackReg")) {
			instrSb.append("\tmov esi, ebp\n");
			instrSb.append("\tsub esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tsub esi, 4\n");
			instrSb.append("\tmov "+"[esi], "+x86RegIdChange(resId));
		}else if (opname.equals("swStackImm")) {
			instrSb.append("\tmov esi, ebp\n");
			instrSb.append("\tadd esi, "+op1Id+"\n");
			instrSb.append("\tmov [esi]"+", "+x86RegIdChange(resId));
		}else if (opname.equals("swRamReg")) {
			instrSb.append("\tlea esi, "+firstGlobalVarName+"\n");
			instrSb.append("\tadd esi, "+x86RegIdChange(op1Id)+"\n");
			instrSb.append("\tmov [esi], "+x86RegIdChange(resId));
		}else if (opname.equals("swRamImm")) {
			instrSb.append("\tlea esi, "+firstGlobalVarName+"\n");
			instrSb.append("\tadd esi, "+op1Id+"\n");
			instrSb.append("\tmov [esi], "+x86RegIdChange(resId));
		}else if (opname.equals("beq")) {
			instrSb.append("\tcmp "+x86RegIdChange(resId)+", 0\n");
			instrSb.append("\tje "+x86LabelChange(label));
		}else if (opname.equals("bne")) {
			instrSb.append("\tcmp "+x86RegIdChange(resId)+", 0\n");
			instrSb.append("\tjne "+x86LabelChange(label));
		}else if (opname.equals("j")) {
			instrSb.append("\tjmp "+x86LabelChange(label));
		}else if (opname.equals("ret")) {
			instrSb.append("\tret");
		}else if (opname.equals("call")) {
			instrSb.append("\tcall "+label+"\n");
			instrSb.append("\tmov edi, eax");
		}
		instrSb.append("\n");
		
		
	}
	private String x86RegIdChange(Integer oldId) {
		switch (oldId) {
		case 0:
			return "eax";
		case 1:
			return "ebx";
		case 2:
			return "ecx";
		case 3:
			return "edx";
		case -1:
			return "edi";
		default:
			return null;
		}
	}
	private String x86LabelChange(String label) {
		if(label.charAt(1) <= '9' && label.charAt(1) >= '0') {
			String res = "_";
			res += label.substring(1);
			return res;
		}else {
			return label.substring(1);
		}
	}
	private Integer x86ParamRegIdChange(Integer oldId) {
		if (oldId == -1) {
			return -1;
		}
		return null;
	}
	private String getXL(Integer oldId) {
		switch (oldId) {
		case 0:
			return "al";
		case 1:
			return "bl";
		case 2:
			return "cl";
		case 3:
			return "dl";
		default:
			return null;
		}
	}
	private Integer getOtherId(Integer id1, Integer id2, Integer id3) {
		for (int i = 0; i < 4; i++) {
			if (id1 != i && id2 != i && id3 != i) {
				return i;
			}
		}
		return null;
	}

	@Override
	public void visit(IRIreadoff node) {
		// TODO Auto-generated method stub
		
	}
}
