package bit.minisys.minicc.ncgen.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.icgen.internal.IRBuilder;
import bit.minisys.minicc.icgen.internal.ParamInfo;
import bit.minisys.minicc.icgen.internal.VirtualRegProperty;
import bit.minisys.minicc.internal.ir.*;
import bit.minisys.minicc.internal.symbol.*;


public class X86CodeGen {
	private IRBuilder irBuider;
	private StringBuilder asmSb;
	private static Integer X86_REG_NUM = 4; 	// x86 : eax,ebx,ecx,edx
	private static Integer X86_PARAM_NUM_IN_STACK = 0;
	public X86CodeGen(IRBuilder irBuider) {
		this.irBuider = irBuider;
	}
	public StringBuilder getAsmSb() {
		return asmSb;
	}
	private String alloc = "local";
	private String firstGlobalVarName;
	
	public void run(MiniCCCfg cfg) {
		IRFile irfile = irBuider.getIrFile();
		if (cfg.ra.equals("ls")) {
			alloc = "linearscan";
		}else {
			alloc = "local";
		}
		asmSb = new StringBuilder();
		asmSb.append(".386\n");
		asmSb.append(".model flat, stdcall\n");
		asmSb.append("option casemap:none\n\n");
		asmSb.append("includelib msvcrt.lib\n");
		asmSb.append("includelib user32.lib\n");
		asmSb.append("includelib kernel32.lib\n\n");
		asmSb.append("printf PROTO C : ptr sbyte, :VARARG\n");
		asmSb.append("scanf PROTO C : ptr sbyte, :VARARG\n");
		asmSb.append("\n");
		// -----------�������ݶ�.data------------------
		asmSb.append(".data\n");
		ArrayList<IRVariableDeclaration> dataDeclList=new ArrayList<IRVariableDeclaration>();
		for (IRDeclaration irdecl : irfile.globalSymTab.localSymTab) {
			if(irdecl instanceof IRVariableDeclaration) {
				dataDeclList.add((IRVariableDeclaration)irdecl);
			}
		}
		// �����ڴ��offset��С��������
		Collections.sort(dataDeclList,new Comparator<IRVariableDeclaration>() {
			@Override
			public int compare(IRVariableDeclaration o1, IRVariableDeclaration o2) {
				// TODO Auto-generated method stub
				return o1.offsize-o2.offsize;
			}
		});
		for (IRVariableDeclaration irvd : dataDeclList) {
			// Ϊÿ������.data����
			buildX86Data(irvd,asmSb);
		}
		if (dataDeclList.size() > 0) {
			firstGlobalVarName = ((IRVariableDeclaration)dataDeclList.get(0)).name;
		}

		// ���õ����ַ�����������.data��
		creatStringConstAsm(asmSb,irfile.getGlobalStringConstMap());
		// ----------�������������-------------------
		asmSb.append("\n");
		// ---------���ɶ�ջ��------------------------
		// ---------��ջ���������--------------------
		
		// ----------����ָ����-----------------------
		
		asmSb.append(".code\n");
		// ������init�����ú���
		// ��ȡȫ�ֱ�����Ӧ�ļĴ���ӳ��
		Map<VirtualReg, VirtualRegProperty> globalVarPropMap = irBuider.getGlobalVarPropMap();
		// __init
		creatInitAsm(asmSb,irfile.globalSymTab.instructions,globalVarPropMap);
		// ���ú���(Mars_PrintInt, Mars_GetInt, Mars_PrintStr)
		creatBuiltInFuncAsm(asmSb);
		// ��ÿ���������ɶ�Ӧ��ָ��
		Integer spillCnt = 0;
		for (IRDeclaration irdecl : irfile.globalSymTab.localSymTab) {
			if(irdecl instanceof IRFunctionDeclaration) {
				String funcname = ((IRFunctionDeclaration)irdecl).funcname;
				Integer finalOffset;
				StringBuilder sb = null;
				if (alloc.equals("local")) {
					BasicBlock start = ((IRFunctionDeclaration)irdecl).getStartBB();
					// ��IR��������ɼĴ�������
					StupidRegisterAlloc stpRegisterAlloc = new StupidRegisterAlloc((IRFunctionDeclaration)irdecl,X86_REG_NUM,globalVarPropMap,X86_PARAM_NUM_IN_STACK,true);
					stpRegisterAlloc.run(start);
					spillCnt += stpRegisterAlloc.getSpillTimes();
				//	stpRegisterAlloc.outputNewInstrs();
					// �ӼĴ�����������ȡ������ջ��Ϣ
					// ��¼�ֲ�����+spillռ�õ�ջ�ռ�(sp + finalOffset)
					finalOffset = stpRegisterAlloc.getNowOffset();
					// �����ǰ����irdecl�ڲ�������������������Ҫ����return address ����$31,finalOffset+=4
					if(stpRegisterAlloc.findCall) {
						//finalOffset += 4;
						// 4�������Ĵ����̶�
						finalOffset += 4*X86_PARAM_NUM_IN_STACK;
						// �����ǰ�����ڲ����ö��������ȡ����ռ��ջ�ռ�����offset���ӵ�finalOffset
						finalOffset += stpRegisterAlloc.argMoreSize;
					}
					// ����ָ������յ�irָ���б�����spill��load/store��
					LinkedList<IRInstruction> realInstrs = stpRegisterAlloc.getRealInstrs();
					
					X86Builder x86Builder = new X86Builder(stpRegisterAlloc.getInstrRevMap(),finalOffset,4*X86_PARAM_NUM_IN_STACK+stpRegisterAlloc.argMoreSize);
					x86Builder.setFuncname(funcname);
					x86Builder.setFirstGlobalVarName(firstGlobalVarName);
					for (IRInstruction ir : realInstrs) {
						x86Builder.visit(ir);
					}
					sb = x86Builder.getInstrSb();
				}else if (alloc.equals("linearscan")) {
					// ��ÿ�������ڲ���
					LivenessAnalysis la = new LivenessAnalysis();
					la.run(((IRFunctionDeclaration) irdecl).getStartBB());
					LinearScan linearScan = new LinearScan(la.getIntervals(),la.getFirstInstr());
					linearScan.run(X86_REG_NUM,(IRFunctionDeclaration)irdecl,irBuider.getGlobalVarPropMap(),X86_PARAM_NUM_IN_STACK,true);
					spillCnt += linearScan.getSpillTimes();
					finalOffset = linearScan.getNowOffset();
					// �����ǰ����irdecl�ڲ�������������������Ҫ����return address ����$31,finalOffset+=4
					if(linearScan.findCall) {
						//finalOffset += 4;
						// 4�������Ĵ����̶�
						finalOffset += 4*X86_PARAM_NUM_IN_STACK;
						// �����ǰ�����ڲ����ö��������ȡ����ռ��ջ�ռ�����offset���ӵ�finalOffset
						finalOffset += linearScan.argMoreSize;
					}
					// ����ָ������յ�irָ���б�����spill��load/store��
					LinkedList<IRInstruction> realInstrs = linearScan.getRealInstrs();
					
					X86Builder x86Builder = new X86Builder(linearScan.getInstrRevMap(),finalOffset,4*X86_PARAM_NUM_IN_STACK+linearScan.argMoreSize);
					x86Builder.setFuncname(funcname);
					x86Builder.setFirstGlobalVarName(firstGlobalVarName);
					for (IRInstruction ir : realInstrs) {
						x86Builder.visit(ir);
					}
					sb = x86Builder.getInstrSb();
				}
				
				asmSb.append(sb.toString());
			}
		}
		
		asmSb.append("end __init\n");
		// ---------ָ�����������--------------------
		//System.out.println("���ָ��������"+spillCnt);
	}
	
	// Ϊÿ��ȫ�ֱ�������asm,��ʼ��Ϊ0
	private void buildX86Data(IRVariableDeclaration decl, StringBuilder sb) {
		String name = decl.name;
		Type declType = decl.type;
		if(declType instanceof NormalType) {
			sb.append(name+"\t");
			if(declType == GlobalSymbolTable.intType) {
				sb.append("dd 0\n");
			}
		}else if (declType instanceof ArrayType) {
			sb.append(name+"\t");
			if(((ArrayType)declType).elementType == GlobalSymbolTable.intType) {
				sb.append("dd "+ declType.getRegisiterSize()/4 + " dup(0)\n");
			}
		}
	}
	// Ϊ�õ���str����ȫ�ֱ���
	private void creatStringConstAsm(StringBuilder sb,Map<String, String> StrConstmap) {
		sb.append("forIntNumber\tdb '%d',0\n");
		sb.append("forString\tdb '%s',0\n");
		sb.append("forEnter\tdb ' ',0\n");
		sb.append("IntNumberHolder dd 0\n");
		for(Map.Entry<String, String> entry : StrConstmap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			key = changeStr(key);
			sb.append(value+"\tdb " + key+",0"+"\n");
 		}
	}
	private String changeStr(String key) {
		String res = "";
		for(int i = 0; i<key.length()-1; i++) {
			if(key.charAt(i) == '\\' && key.charAt(i+1) == 'n') {
				res += "\",0ah,\"";
				i++;
				if (i == key.length()-2) {
					res += "\"";
				}
			}else {
				res += key.charAt(i); 
				if (i+1 == key.length()-1) {
					res += key.charAt(i+1); 
				}
			}
		}

		String subString = "\"\",";
		res = res.replaceAll(subString, "");
		subString = ",\"\"";
		res = res.replaceAll(subString, "");

		return res;
	}
	// ����asm����ں���__init����������ָ���Լ�ȫ�ֱ����ĳ�ʼ��
	private void creatInitAsm(StringBuilder sb,List<IRInstruction> initInstrs,Map<VirtualReg, VirtualRegProperty>globalVarPropMap) {
		sb.append("__init:\n");
		IRFunctionDeclaration initFuncDecl = new IRFunctionDeclaration("__init");
		initFuncDecl.setArgs(null);
		initFuncDecl.setBody(null);
		initFuncDecl.setFarOffset(0);
		initFuncDecl.setLabelMap(null);
		initFuncDecl.setLocalVarPropMap(new HashMap<VirtualReg, VirtualRegProperty>());
		initFuncDecl.setParamsInfo(new HashMap<VirtualReg, ParamInfo>());
		initFuncDecl.setReturnType(GlobalSymbolTable.voidType);
		initFuncDecl.setStartBB(null);
		StupidRegisterAlloc sra = new StupidRegisterAlloc(initFuncDecl, X86_REG_NUM, globalVarPropMap,X86_PARAM_NUM_IN_STACK,true);
		sra.runInit(initInstrs);
		Integer nowOffset = sra.getNowOffset();
		LinkedList<IRInstruction> realInstrs = sra.getRealInstrs();
		X86Builder x86Builder = new X86Builder(sra.getInstrRevMap(),nowOffset,0);
		x86Builder.setFirstGlobalVarName(firstGlobalVarName);
		for (IRInstruction ir : realInstrs) {
			x86Builder.visit(ir);
		}
		sb.append(x86Builder.getInstrSb().toString());
		sb.append("\tcall main\n");
		sb.append("\tret\n");
	}
	// ����BuiltInFunction
	private void creatBuiltInFuncAsm(StringBuilder sb) {
		//Mars_PrintInt:
		sb.append("Mars_PrintInt:\n");
		sb.append("\tmov esi, [esp+4]\n");
		sb.append("\tpushad\n");
		sb.append("\tinvoke printf, offset forIntNumber, esi\n");	// eax�д洢����ֵ
		sb.append("\tinvoke printf, offset forEnter\n");
		sb.append("\tpopad\n");
		sb.append("\tret\n");
		//Mars_GetInt:
		sb.append("Mars_GetInt:\n");
		sb.append("\tpushad\n");
		sb.append("\tinvoke scanf, offset forIntNumber, offset IntNumberHolder\n");
		sb.append("\tpopad\n");
		sb.append("\tlea eax, IntNumberHolder\n");
		sb.append("\tmov eax, [eax]\n");
		sb.append("\tret\n");
		//Mars_PrintStr:
		sb.append("Mars_PrintStr:\n");
		sb.append("\tmov esi, [esp+4]\n");
		sb.append("\tpushad\n");
		sb.append("\tinvoke printf, offset forString, esi\n");	// esi���ַ�����ַ
		sb.append("\tpopad\n");
		sb.append("\tret\n");
	}
}
