package bit.minisys.minicc.optimizer.internal;

import java.io.IOException;
import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.optimizer.IMiniCCOptimizer;

public class MiniCCOptimizer implements IMiniCCOptimizer{

	public String run(String iFile) throws Exception {
		String oFile = MiniCCUtil.removeAllExt(iFile) + MiniCCCfg.MINICC_OPT_OUTPUT_EXT;
		Optimizer opt = new Optimizer(iFile);
		opt.outputOptimized(oFile);
		System.out.println("6. Optimize not finished!");
		return oFile;
	}
}
