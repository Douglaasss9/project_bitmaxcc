package bit.minisys.minicc.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.antlr.v4.gui.TreeViewer;

import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.parser.ast.*;

/*
 * PROGRAM     --> FUNC_LIST
 * FUNC_LIST   --> FUNC FUNC_LIST | e
 * FUNC        --> TYPE ID '(' ARGUMENTS ')' CODE_BLOCK
 * TYPE        --> INT
 * ARGS   	   --> e | ARG_LIST
 * ARG_LIST    --> ARG ',' ARGLIST | ARG
 * ARG    	   --> TYPE ID
 * CODE_BLOCK  --> '{' STMTS '}'
 * STMTS       --> STMT STMTS | e
 * STMT        --> RETURN_STMT
 *
 * RETURN STMT --> RETURN EXPR ';'
 *
 * EXPR        --> TERM EXPR'
 * EXPR'       --> '+' TERM EXPR' | '-' TERM EXPR' | e
 *
 * TERM        --> FACTOR TERM'
 * TERM'       --> '*' FACTOR TERM' | e
 *
 * FACTOR      --> ID  
 * 
 */

class ScannerToken1{
	public String lexme;
	public String type;
	public int	  line;
	public int    column;
}

public class ExampleParser1 implements IMiniCCParser {

	private ArrayList<ScannerToken> tknList;
	private int tokenIndex;
	private ScannerToken nextToken;
	private ScannerToken nextToken2;
	
	@Override
	public String run(String iFile) throws Exception {
		System.out.println("Parsing...");

		String oFile = MiniCCUtil.removeAllExt(iFile) + MiniCCCfg.MINICC_PARSER_OUTPUT_EXT;
		String tFile = MiniCCUtil.removeAllExt(iFile) + MiniCCCfg.MINICC_SCANNER_OUTPUT_EXT;
		
		tknList = loadTokens(tFile);
		//System.out.println(tknList.size());
		tokenIndex = 0;

		ASTNode root = program();
		
		
		String[] dummyStrs = new String[16];
		TreeViewer viewr = new TreeViewer(Arrays.asList(dummyStrs), root);
	    viewr.open();

		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(new File(oFile), root);

		//TODO: write to file
		
		return oFile;
	}
	

	private ArrayList<ScannerToken> loadTokens(String tFile) {
		tknList = new ArrayList<ScannerToken>();
		
		ArrayList<String> tknStr = MiniCCUtil.readFile(tFile);
		
		for(String str: tknStr) {
			if(str.trim().length() <= 0) {
				continue;
			}
			
			ScannerToken st = new ScannerToken();
			//[@0,0:2='int',<'int'>,1:0]
			String[] segs;
			if(str.indexOf("<','>") > 0) {
				str = str.replace("','", "'DOT'");
				
				segs = str.split(",");
				segs[1] = "=','";
				segs[2] = "<','>";
				
			}else {
				segs = str.split(",");
			}
			st.lexme = segs[1].substring(segs[1].indexOf("=") + 1);
			st.type  = segs[2].substring(segs[2].indexOf("<") + 1, segs[2].length() - 1);
			String[] lc = segs[3].split(":");
			st.line = Integer.parseInt(lc[0]);
			st.column = Integer.parseInt(lc[1].replace("]", ""));
			
			tknList.add(st);
		}
		
		return tknList;
	}

	private ScannerToken getToken(int index){
		if (index < tknList.size()){
			return tknList.get(index);
		}
		return null;
	}

	public void matchToken(String type) {
		if(tokenIndex < tknList.size()) {
			ScannerToken next = tknList.get(tokenIndex);
			if(!next.type.equals(type)) {
				System.out.println("[ERROR]Parser: unmatched token, expected = " + type + ", " 
						+ "input = " + next.type);
			}
			else {
				tokenIndex++;
			}
		}
	}

	//PROGRAM --> FUNC_LIST
	public ASTNode program() {
		ASTCompilationUnit p = new ASTCompilationUnit();
		ArrayList<ASTNode> fl = funcList();
		if(fl != null) {
			//p.getSubNodes().add(fl);
			p.items.addAll(fl);
		}
		p.children.addAll(p.items);
		return p;
	}

	//FUNC_LIST --> FUNC FUNC_LIST | e
	public ArrayList<ASTNode> funcList() {
		ArrayList<ASTNode> fl = new ArrayList<ASTNode>();
		
		nextToken = tknList.get(tokenIndex);
		if(nextToken.type.equals("EOF")) {
			return null;
		}
		else {
			ASTNode f = func();
			fl.add(f);
			ArrayList<ASTNode> fl2 = funcList();
			if(fl2 != null) {
				fl.addAll(fl2);
			}
			return fl;
		}
	}

	//FUNC --> TYPE ID '(' ARGUMENTS ')' CODE_BLOCK
	public ASTNode func() {
		ASTFunctionDefine fdef = new ASTFunctionDefine();
		
		ASTToken s = type();
		
		fdef.specifiers.add(s);
		fdef.children.add(s);
		
		ASTFunctionDeclarator fdec = new ASTFunctionDeclarator();

		ASTIdentifier id = new ASTIdentifier();
		id.tokenId = tokenIndex;
		matchToken("Identifier");

		fdef.children.add(id);
		
		matchToken("'('");
		ArrayList<ASTParamsDeclarator> pl = arguments();
		matchToken("')'");
		
		//fdec.identifiers.add(id);
		if(pl != null) {
			fdec.params.addAll(pl);
			fdec.children.addAll(pl);
		}
		
		ASTCompoundStatement cs = codeBlock();

		fdef.declarator = fdec;
		fdef.children.add(fdec);
		fdef.body = cs;
		fdef.children.add(cs);

		
		return fdef;
	}

	//TYPE --> INT |FLOAT | CHART
	public ASTToken type() {
		ScannerToken st = tknList.get(tokenIndex);
		
		ASTToken t = new ASTToken();
		if(st.type.equals("'int'")||st.type.equals("'void'")||st.type.equals("'float'")||st.type.equals("'char'")) {
			t.tokenId = tokenIndex;
			t.value = st.lexme;
			tokenIndex++;
		}
		return t;
	}

	//ARGUMENTS --> e | ARG_LIST
	public ArrayList<ASTParamsDeclarator> arguments() {
		nextToken = tknList.get(tokenIndex);
		if(nextToken.type.equals("')'")) { //ending
			return null;
		}
		else {
			ArrayList<ASTParamsDeclarator> al = argList();
			return al;
		}
	}

	//ARG_LIST --> ARGUMENT ',' ARGLIST | ARGUMENT
	public ArrayList<ASTParamsDeclarator> argList() {
		ArrayList<ASTParamsDeclarator> pdl = new ArrayList<ASTParamsDeclarator>();
		ASTParamsDeclarator pd = argument();
		pdl.add(pd);
		
		nextToken = tknList.get(tokenIndex);
		if(nextToken.type.equals("','")) {
			matchToken("','");
			ArrayList<ASTParamsDeclarator> pdl2 = argList();
			pdl.addAll(pdl2);
		}
		
		return pdl;
	}
		
	//ARGUMENT --> TYPE ID
	public ASTParamsDeclarator argument() {
		ASTParamsDeclarator pd = new ASTParamsDeclarator();
		ASTToken t = type();
		pd.specfiers.add(t);
		
		ASTIdentifier id = new ASTIdentifier();
		id.tokenId = tokenIndex;
		matchToken("Identifier");
		
		ASTVariableDeclarator vd =  new ASTVariableDeclarator();
		vd.identifier = id;
		pd.declarator = vd;
		
		return pd;
	}

	

	//CODE_BLOCK --> '{' STMTS '}'
	public ASTCompoundStatement codeBlock() {
		matchToken("'{'");
		ASTCompoundStatement cs = stmts();
		matchToken("'}'");

		return cs;
	}

	//STMTS --> STMT STMTS | e
	public ASTCompoundStatement stmts() {
		ASTCompoundStatement cs = new ASTCompoundStatement();
		nextToken = tknList.get(tokenIndex);
		/*if (nextToken.type.equals("'}'"))
			return cs;
		else {
			
			ASTStatement s = stmt();
			cs.blockItems.add(s);
			cs.children.add(s);
			
			ASTCompoundStatement cs2 = stmts();
			if(cs2 != null)
				{cs.blockItems.add(cs2);
				 cs.children.add(cs2);
				}			
			return cs;
		}*/
		   
			while((!nextToken.type.equals("'}'"))&&(!nextToken.type.equals("EOF"))) 
			{
				System.out.println("in stmts, Token Type: "+nextToken.type+" "+nextToken.line+" "+nextToken.column);
				ASTStatement s = stmt();
				cs.blockItems.add(s);
				cs.children.add(s);
				nextToken = tknList.get(tokenIndex);
				
			}
			return cs;
	}

	//STMT --> ASSIGN_STMT | RETURN_STMT | DECL_STMT | FUNC_CALL
	public ASTStatement stmt() {
		nextToken = tknList.get(tokenIndex);
		
		if(nextToken.type.equals("'return'")) {
			return returnStmt();
		}
		else if(nextToken.type.equals("'break'")) {
			return breakStmt();
		}
		else if(nextToken.type.equals("'continue'")) {
			return continueStmt();
		}
		else if(nextToken.type.equals("'{'"))
		{
			return codeBlock();
		}
		else if(nextToken.type.equals("'int'")||nextToken.type.equals("'float'")||nextToken.type.equals("'char'")) {
			return declStmt();
		}
		else if(nextToken.type.equals("'if'")) {
			return selectionStmt();
		}
		else if(nextToken.type.equals("'for'")||nextToken.type.equals("'while'")) {
			return iterationStmt();
		}
		else if(nextToken.type.equals("Identifier")||isConstTypeOrUnaryOp(nextToken)) {
			return exprStmt();
		}
		else{
			System.out.println("[ERROR]Parser: unreachable stmt!");
			System.out.println("Token Type: "+nextToken.type);
			System.exit(0);
			return null;
		}
	}
	
	public boolean isConstTypeOrUnaryOp(ScannerToken token)
	{
		if (token.type.equals("IntegerConstant"))
		    return true;
		else if (token.type.equals("FloatingConstant"))
			return true;
		else if (token.type.equals("CharacterConstant"))
			return true;
		else if (token.type.equals("StringLiteral"))
			return true;
		else if (token.type.equals("'++'"))
			return true;
		else if (token.type.equals("'--'"))
			return true;
		else return false;
	}
	
	private ASTExpressionStatement exprStmt()
	{
		ASTExpressionStatement es= new ASTExpressionStatement();
		ASTExpression e=expr();
		es.children.add(e);
		System.out.println("in exprStmt, expr added. next token type: "+nextToken.type);
		matchToken("';'");
		return es;
	}

	private ASTIterationStatement iterationStmt() {
		ASTIterationStatement is=new ASTIterationStatement();
		if(nextToken.type.equals("'for'"))
		{
			matchToken("'for'");
			matchToken("'('");
			nextToken = tknList.get(tokenIndex);
			if(nextToken.type.equals("'int'")||nextToken.type.equals("'float'")||nextToken.type.equals("'char'"))
			{
				matchToken(nextToken.type);
				nextToken = tknList.get(tokenIndex);
			}
			ASTExpression e=expr();
			is.children.add(e);
			matchToken("';'");
			ASTConditionExpression ce= condExpr();
			is.children.add(ce);
			matchToken("';'");
			ASTExpression e2=expr();
			is.children.add(e2);
			matchToken("')'");
			ASTStatement s = stmt();
			is.children.add(s);
			nextToken = tknList.get(tokenIndex);
		}
		else if(nextToken.type.equals("'while'"))
		{
			matchToken("'while'");
			matchToken("'('");
			nextToken = tknList.get(tokenIndex);
			ASTConditionExpression ce= condExpr();
			is.children.add(ce);
			matchToken("')'");
			ASTStatement s = stmt();
			is.children.add(s);
			nextToken = tknList.get(tokenIndex);
		}
		return is;
	}


	private ASTSelectionStatement selectionStmt() {
		// TODO Auto-generated method stub
		matchToken("'if'");
		ASTSelectionStatement ss=new ASTSelectionStatement();
		matchToken("'('");
		ASTConditionExpression ce= condExpr();
		ss.children.add(ce);
		matchToken("')'");
		
		ASTStatement s = stmt();
		ss.children.add(s);
		nextToken = tknList.get(tokenIndex);
		
		if(nextToken.type.equals("'else'"))
		{
			matchToken("'else'");
			ASTStatement s2 = stmt();
			ss.children.add(s2);
			nextToken = tknList.get(tokenIndex);
		}
		
		//matchToken("';'");
		return ss;
	}


	private ASTConditionExpression condExpr() {
		// TODO Auto-generated method stub
		ASTConditionExpression ce=new ASTConditionExpression();
		ASTExpression e=expr();
		ce.children.add(e);
		return ce;
	}


	private ASTExpressionStatement declStmt() {
		// TODO Auto-generated method stub
		ASTExpressionStatement vd=new ASTExpressionStatement();
        ASTToken s = type();
		vd.children.add(s);
		
		nextToken = tknList.get(tokenIndex);
		while ((!nextToken.type.equals("';'"))&&(!nextToken.type.equals("EOF")))
		{
		
		ASTIdentifier id = new ASTIdentifier();
		id.tokenId = tokenIndex;
		matchToken("Identifier");
		vd.children.add(id);
		
		//matchToken("';'");
		nextToken = tknList.get(tokenIndex);
		//System.out.println(nextToken.type);
		ASTStatement ds2=declStmt2();
		vd.children.add(ds2);
		
		nextToken = tknList.get(tokenIndex);
		if(!nextToken.type.equals("';'")) matchToken("','");
		
		}
		matchToken("';'");//System.out.println("in ds1 matched ;");
		
		
		return vd;
	}
	
	
	private ASTStatement declStmt2() {
		// TODO Auto-generated method stub
		//System.out.println("in declStmt2");
		ASTStatement ds2=new ASTExpressionStatement();
		if(nextToken.type.equals("'['"))
		{
			ASTStatement ds3=declStmt3();
			ds2.children.add(ds3);
		}
		else if(nextToken.type.equals("'='"))
		{
            ASTStatement dsEqual=new ASTExpressionStatement();
			matchToken("'='");
			ASTExpression e = expr();
			dsEqual.children.add(e);
			ds2.children.add(dsEqual);
		}
		
		//System.out.println("return ds2 "+nextToken.lexme+" ");
		return ds2;
	}


	private ASTStatement declStmt3() {
		// TODO Auto-generated method stub
		//System.out.println("in declStmt3");
		ASTStatement ds3=new ASTExpressionStatement();
		matchToken("'['");
		//System.out.println(nextToken.lexme);
		ASTExpression num=factor();
		ds3.children.add(num);
		matchToken("']'");
		nextToken = tknList.get(tokenIndex);
		//System.out.println(nextToken.lexme);
		ASTStatement ds4=declStmt4();	
		ds3.children.add(ds4);	
		//System.out.println(nextToken.lexme);
		return ds3;
	}


	private ASTStatement declStmt4() {
		// TODO Auto-generated method stub
		//System.out.println("in declStmt4");
		ASTStatement ds4=new ASTExpressionStatement();
		if(nextToken.type.equals("'='"))
		{
			
			matchToken("'='");
			ASTExpression e = expr();
			ds4.children.add(e);
			return ds4;
		}
		//else return null;
		return ds4;
	}


	/*private ASTStatement declStmt4() {
		// TODO Auto-generated method stub
		ASTStatement ds4=new ASTExpressionStatement();
		matchToken("'['");
		ASTExpression num=factor();
		ds4.children.add(num);
		matchToken("']'");
		if(nextToken.type.equals("'='"))
		{
			matchToken("'='");
			ASTExpression e = expr();
			ds4.children.add(e);
		}
		return ds4;
	}*/
	


	//RETURN_STMT --> RETURN EXPR ';'
	public ASTReturnStatement returnStmt() {
		matchToken("'return'");
		ASTReturnStatement rs = new ASTReturnStatement();
		nextToken = tknList.get(tokenIndex);
		if(!nextToken.type.equals("';'"))
		{ASTExpression e = expr();
		rs.expr.add(e);
		rs.children.add(e);}
		matchToken("';'");
		return rs;
	}

	public ASTBreakStatement breakStmt() {
		ASTBreakStatement bs=new ASTBreakStatement();
		matchToken("'break'");
		nextToken = tknList.get(tokenIndex);
		matchToken("';'");
		return bs;
	}
	
	public ASTContinueStatement continueStmt() {
		ASTContinueStatement cs=new ASTContinueStatement();
		matchToken("'continue'");
		nextToken = tknList.get(tokenIndex);
		matchToken("';'");
		return cs;
	}
	
	//EXPR --> TERM EXPR'
	/*public ASTExpression expr() {
		ASTExpression term = term();
		ASTBinaryExpression be = expr2();
		
		if(be != null) {
			be.expr1 = term;
			return be;
		}else {
			return term;
		}
	}*/

	
	public ASTExpression expr()
	{
		ASTPostfixExpression e = new ASTPostfixExpression();
		nextToken = tknList.get(tokenIndex);
		/*nextToken2 = tknList.get(tokenIndex+1);
		if(isAssignOp(nextToken2))
		{
			System.out.println("entered");
			ASTIdentifier v = var();
			v.tokenId = tokenIndex;
			e.children.add(v);
			matchToken(nextToken2.type);
			ASTExpression e2=expr();
			e.children.add(e2);
			nextToken = tknList.get(tokenIndex);
		}
		else
		{
			ASTPostfixExpression e3=simpleExpression();
			e.children.add(e3);
			nextToken = tknList.get(tokenIndex);
		}*/
		if(nextToken.type.equals("Identifier"))
		{			
			int temp=tokenIndex;
			ASTIdentifier v = var();
			v.tokenId = tokenIndex;
			e.children.add(v);
			nextToken = tknList.get(tokenIndex);
			
			if(isAssignOp(nextToken))
			{
				matchToken(nextToken.type);
				//System.out.println("in expr next token: "+nextToken.type);
				ASTExpression e2=expr();
				e.children.add(e2);
				nextToken = tknList.get(tokenIndex);
			}
			else
			{
				tokenIndex=temp;
				ASTPostfixExpression e3=simpleExpression();
				e.children.add(e3);
				nextToken = tknList.get(tokenIndex);			
			}
		}
		else
		{
			ASTPostfixExpression e3=simpleExpression();
			e.children.add(e3);
			nextToken = tknList.get(tokenIndex);
		}
		return e;
	}
	
	public boolean isAssignOp(ScannerToken token)
	{
		if (token.type.equals("'='"))
		    return true;
		else if (token.type.equals("'+='"))
			return true;
		else if (token.type.equals("'-='"))
			return true;
		else if (token.type.equals("'*='"))
			return true;
		else if (token.type.equals("'\\='"))
			return true;
		else return false;
	} 
	
	
	
	public ASTPostfixExpression simpleExpression() {
		// TODO Auto-generated method stub
		nextToken = tknList.get(tokenIndex);
		ASTPostfixExpression se= new ASTPostfixExpression();
		ASTBinaryExpression addexpr=additiveExpression();
		se.children.add(addexpr);
		nextToken = tknList.get(tokenIndex);
		if(isCmpOp(nextToken))
		{
			matchToken(nextToken.type);
			ASTBinaryExpression addexpr2=additiveExpression();
			se.children.add(addexpr2);
			nextToken = tknList.get(tokenIndex);
		}
		return se;
	}

	public boolean isCmpOp(ScannerToken token)
	{
		if (token.type.equals("'<'"))
		    return true;
		else if (token.type.equals("'>'"))
			return true;
		else if (token.type.equals("'<='"))
			return true;
		else if (token.type.equals("'>='"))
			return true;
		else if (token.type.equals("'=='"))
			return true;
		else if (token.type.equals("'!='"))
			return true;
		else return false;
	}

	public ASTBinaryExpression additiveExpression() {
		// TODO Auto-generated method stub
		nextToken = tknList.get(tokenIndex);
		ASTBinaryExpression ae=new ASTBinaryExpression();
		ASTBinaryExpression t= termp();
		ae.children.add(t);
		ASTBinaryExpression ae2= additiveExpression2();
		ae.children.add(ae2);
		
		return ae;
	}
	
	public ASTBinaryExpression additiveExpression2() {
		nextToken = tknList.get(tokenIndex);
		ASTBinaryExpression ae2= new ASTBinaryExpression();
		if(isAddOp(nextToken))
		{
			matchToken(nextToken.type);
			nextToken = tknList.get(tokenIndex);
			ASTBinaryExpression t= termp();
			ae2.children.add(t);
			ASTBinaryExpression ae2p= additiveExpression2();
			ae2.children.add(ae2p);
			nextToken = tknList.get(tokenIndex);
		}
		return ae2;
	}

	public boolean isAddOp(ScannerToken token)
	{
		if (token.type.equals("'+'"))
		    return true;
		else if (token.type.equals("'-'"))
			return true;
		else return false;
	}
	
	private ASTBinaryExpression termp() //���������ֹ���Դ�term������ͻ
	{
		// TODO Auto-generated method stub
		nextToken = tknList.get(tokenIndex);
		ASTBinaryExpression t=new ASTBinaryExpression();
		ASTExpression f = factor();
		t.children.add(f);
		ASTBinaryExpression t2=termp2();
		t.children.add(t2);
		return t;
	}


	public ASTBinaryExpression termp2() {
		nextToken = tknList.get(tokenIndex);
		ASTBinaryExpression t2= new ASTBinaryExpression();
		if(isMulOp(nextToken))
		{
			matchToken(nextToken.type);
			nextToken = tknList.get(tokenIndex);
			ASTExpression f = factor();
			t2.children.add(f);
			ASTBinaryExpression t2p=termp2();
			t2.children.add(t2p);
			nextToken = tknList.get(tokenIndex);
		}
		return t2;
	}

	
	public boolean isMulOp(ScannerToken token)
	{
		if (token.type.equals("'*'"))
		    return true;
		else if (token.type.equals("'/'"))
			return true;
		else return false;
	}

	/*
	//EXPR' --> '+' TERM EXPR' | '-' TERM EXPR' | e
	public ASTBinaryExpression expr2() {
		nextToken = tknList.get(tokenIndex);
		if (nextToken.type.equals("';'"))
			return new ASTBinaryExpression();
		
		if(nextToken.type.equals("'+'")){
			ASTBinaryExpression be = new ASTBinaryExpression();
			
			ASTToken tkn = new ASTToken();
			tkn.tokenId = tokenIndex;
			matchToken("'+'");
			
			be.op = tkn;
			be.expr2 = term();
			
			ASTBinaryExpression expr = expr2();
			if(expr != null) {
				expr.expr1 = be;
				return expr;
			}
			
			return be;
		}
		else if(nextToken.type.equals("'-'")){
			ASTBinaryExpression be = new ASTBinaryExpression();
			
			ASTToken tkn = new ASTToken();
			tkn.tokenId = tokenIndex;
			matchToken("'-'");
			
			be.op = tkn;
			be.expr2 = term();
			
			ASTBinaryExpression expr = expr2();
			if(expr != null) {
				expr.expr1 = be;
				return expr;
			}
			
			return be;
		}
		else {
			return new ASTBinaryExpression();
		}
	}

	//TERM --> FACTOR TERM2
	public ASTExpression term() {
		ASTExpression f = factor();
		ASTBinaryExpression be = term2();
		
		if(be != null) {
			be.expr1 = f;
			return be;
		}else {
			return f;
		}
	}

	//TERM'--> '*' FACTOR TERM' | '/' FACTOR TERM' | e
	public ASTBinaryExpression term2() {
		nextToken = tknList.get(tokenIndex);
		if(nextToken.type.equals("'*'")){
			ASTBinaryExpression be = new ASTBinaryExpression();
			
			ASTToken tkn = new ASTToken();
			tkn.tokenId = tokenIndex;
			matchToken("'*'");
			
			be.op = tkn;
			be.expr2 = factor();
			
			ASTBinaryExpression term = term2();
			if(term != null) {
				term.expr1 = be;
				return term;
			}
			return be;
		}
		else if(nextToken.type.equals("'/'")){
			ASTBinaryExpression be = new ASTBinaryExpression();
			
			ASTToken tkn = new ASTToken();
			tkn.tokenId = tokenIndex;
			matchToken("'/'");
			
			be.op = tkn;
			be.expr2 = factor();
			
			ASTBinaryExpression term = term2();
			if(term != null) {
				term.expr1 = be;
				return term;
			}
			return be;
		}
		else {
			return new ASTBinaryExpression();
		}
	}
	*/

	//FACTOR --> '(' EXPR ')' | VAR | CONST | FUNC_CALL
	
	public ASTExpression factor() //���������Լ�
	{
		nextToken = tknList.get(tokenIndex);
		if (isUnaryOp(nextToken))
		{
			ASTUnaryExpression ue=new ASTUnaryExpression();
			matchToken(nextToken.type);
			ASTExpression f2=factor2();
			ue.children.add(f2);
			return ue;
		}
		else
		{
			ASTExpression f2p=factor2();
			nextToken = tknList.get(tokenIndex);
			if (isUnaryOp(nextToken))
			{
				ASTUnaryExpression ue2=new ASTUnaryExpression();
				ue2.children.add(f2p);
				matchToken(nextToken.type);
				return ue2;
			}
			else
			{
				return f2p;
			}
            
		}
		
		//return null;
	}
	
	
	public boolean isUnaryOp(ScannerToken token)
	{
		if (token.type.equals("'++'"))
		    return true;
		else if (token.type.equals("'--'"))
			return true;
		else return false;
	}
	
	public ASTExpression factor2() {
		nextToken = tknList.get(tokenIndex);
		if(nextToken.type.equals("'('"))
		{
			matchToken("'('");
			ASTExpression e= expr();
			matchToken("')'");
			
			return e;
		}
		else if(nextToken.type.equals("Identifier")) {
			nextToken2=tknList.get(tokenIndex+1);
			if(nextToken2.type.equals("'('"))
			{
				ASTFunctionCall fc=new ASTFunctionCall();
				ASTIdentifier id2 = new ASTIdentifier();
				id2.tokenId = tokenIndex;
				fc.children.add(id2);
				matchToken("Identifier");
				matchToken("'('");
				nextToken=tknList.get(tokenIndex);
				while((!nextToken.type.equals("')'"))&&(!nextToken.type.equals("EOF")))
				{
					//System.out.println("in while");
					ASTExpression e=expr();
					//System.out.println("in while matched expr");
					fc.children.add(e);
					nextToken=tknList.get(tokenIndex);
					if (!nextToken.type.equals("')'")) matchToken("','");
					nextToken=tknList.get(tokenIndex);
				}
				matchToken("')'");
				return fc;
			}
			else
			{ASTIdentifier id = var();//new ASTIdentifier();
			id.tokenId = tokenIndex;
			//System.out.println("in factor var before match token= "+nextToken.type);
			//matchToken("Identifier");
			//System.out.println("in factor var after match token= "+nextToken.type);
			
			return id;}
		}
		else if(nextToken.type.equals("IntegerConstant"))
		{
		    ASTIntegerConstant ic= new ASTIntegerConstant();
			ic.tokenId=tokenIndex;
			//ic.value=Integer.valueOf(nextToken.lexme);
			matchToken("IntegerConstant");
			return ic;
			
		}
		else if(nextToken.type.equals("FloatingConstant"))
		{
			ASTFloatConstant fc= new ASTFloatConstant();
			fc.tokenId=tokenIndex;
			matchToken("FloatingConstant");
			return fc;
		}
		else if(nextToken.type.equals("CharacterConstant"))
		{
			ASTCharConstant cc= new ASTCharConstant();
			cc.tokenId=tokenIndex;
			matchToken("CharacterConstant");
			return cc;
		}
		else if(nextToken.type.equals("StringLiteral"))
		{
			ASTStringConstant sc= new ASTStringConstant();
			sc.tokenId=tokenIndex;
			matchToken("StringLiteral");
			return sc;
		}
		else{
			return null;
		}
	}
	
	public ASTIdentifier var() 
	{
		nextToken = tknList.get(tokenIndex);
		ASTIdentifier id = new ASTIdentifier();
		id.tokenId = tokenIndex;
		matchToken("Identifier");
		nextToken = tknList.get(tokenIndex);
		if(nextToken.type.equals("'['"))
		{
			//System.out.println("in var [");
			matchToken("'['");
			ASTExpression e=expr();
			id.children.add(e);
			matchToken("']'");
			nextToken = tknList.get(tokenIndex);
		}
		System.out.println("after var, next token type: "+nextToken.type);
		return id;
	}
}
