package bit.minisys.minicc.parser.internal;

import java.util.LinkedList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTreeProperty;

import bit.minisys.minicc.parser.ast.*;
import bit.minisys.minicc.parser.internal.antlr.*;
import bit.minisys.minicc.parser.internal.antlr.CParser.*;

public class ASTBuilder extends CBaseListener{
	private ParseTreeProperty<Object> map = new ParseTreeProperty<Object>();
	private ASTCompilationUnit program;
	
	public ASTCompilationUnit getProgram() {
		return program;
	}
	
	// compilationUnit : externalDeclaration* EOF
	@Override
	public void exitCompilationUnit(CompilationUnitContext ctx) {
		ASTCompilationUnit.Builder builder = new ASTCompilationUnit.Builder();
		for (ExternalDeclarationContext element : ctx.externalDeclaration()) {
			Object object = map.get(element);
			builder.addNode(object);
		}
		//ctx.externalDeclaration().stream().map(map::get).forEachOrdered(builder::addNode);
		program = builder.build();
		map.put(ctx, program);
	}
	
	/*
	 * functionDefinition AST node generate
	 */
	
	// externalDeclaration : functionDefinition
	@Override
	public void exitFunctionDefineItem(FunctionDefineItemContext ctx) {
		map.put(ctx,map.get(ctx.functionDefinition()));
	}
	
	/*
	 * Declaration AST node generate
	 */
	
	// externalDeclaration : declaration
	@Override
	public void exitDeclarationItem(DeclarationItemContext ctx) {
		map.put(ctx,map.get(ctx.declaration()));
	}
	
	// functionDefinition : declarationSpecifiers? directDeclarator declarationList? compoundStatement
	// declarationList : declaration+
	@Override
	public void exitFunctionDefinition(FunctionDefinitionContext ctx) {
        ASTFunctionDefine.Builder builder = new ASTFunctionDefine.Builder();
        if(ctx.declarationSpecifiers() != null) {
        	for (DeclarationSpecifierContext element : ctx.declarationSpecifiers().declarationSpecifier()) {
				Object object = map.get(element);
				builder.addSpecifiers(object);
			}
        	//ctx.declarationSpecifiers().declarationSpecifier().stream()
        	//.map(map::get).forEachOrdered(builder::addSpecifiers);
        }
        builder.setDeclarator((ASTDeclarator)map.get(ctx.declarator()));

        builder.setBody((ASTCompoundStatement)map.get(ctx.compoundStatement()));
        map.put(ctx, builder.build());
	}
	
	// declaration : declarationSpecfiers initDeclaratorList? ';'
	// declarationSpecifiers : declarationSpecifier+
	// initDeclaratorList: initDeclarator (',' initDeclarator)*
	@Override
	public void exitDeclarationUsed(DeclarationUsedContext ctx) {
		ASTDeclaration.Builder builder = new ASTDeclaration.Builder();
    	for (DeclarationSpecifierContext element : ctx.declarationSpecifiers().declarationSpecifier()) {
			Object object = map.get(element);
			builder.addSpecfiers(object);
		}
	//	ctx.declarationSpecifiers().declarationSpecifier().stream()
	//	.map(map::get).forEachOrdered(builder::addSpecfiers);
		if(ctx.initDeclaratorList() != null) {
	    	for (InitDeclaratorContext element : ctx.initDeclaratorList().initDeclarator()) {
				Object object = map.get(element);
				builder.addInitList(object);
			}
			//ctx.initDeclaratorList().initDeclarator().stream()
			//	.map(map::get).forEachOrdered(builder::addInitList);
		}
		map.put(ctx, builder.build());
	}

	// declarationSpecifier :typeSpecifier
	@Override
	public void exitDeclarationSpecifierUsed(DeclarationSpecifierUsedContext ctx) {
		// TODO Auto-generated method stub
		map.put(ctx, map.get(ctx.typeSpecifier()));
	}

	@Override
	public void exitTypeSpecifiersUnused(TypeSpecifiersUnusedContext ctx) {
	}
	// typeSpecifier:type='void'...;
	@Override
	public void exitTypeSpecifiersUsed(TypeSpecifiersUsedContext ctx) {
		switch (ctx.type.getType()) {
		case CParser.Void:
			map.put(ctx, new ASTToken("void", ctx.type.getTokenIndex()));break;
		case CParser.Char:
			map.put(ctx, new ASTToken("char", ctx.type.getTokenIndex()));break;
		case CParser.Short:
			map.put(ctx, new ASTToken("short", ctx.type.getTokenIndex()));break;
		case CParser.Int:
			map.put(ctx, new ASTToken("int", ctx.type.getTokenIndex()));break;
		case CParser.Long:
			map.put(ctx, new ASTToken("long", ctx.type.getTokenIndex()));break;
		case CParser.Float:
			map.put(ctx, new ASTToken("float", ctx.type.getTokenIndex()));break;
		case CParser.Double:
			map.put(ctx, new ASTToken("double", ctx.type.getTokenIndex()));break;
		case CParser.Signed:
			map.put(ctx, new ASTToken("signed", ctx.type.getTokenIndex()));break;
		case CParser.Unsigned:
			map.put(ctx, new ASTToken("unsigned", ctx.type.getTokenIndex()));break;
		default:
			System.out.println("typeSpecfier error.");
			break;
		}
	}
	
	// initDeclarator : directDeclarator ('=' initializer)?
	@Override
	public void exitInitDeclarator(InitDeclaratorContext ctx) {
		ASTInitList.Builder builder = new ASTInitList.Builder();
		builder.setDeclarator((ASTDeclarator)map.get(ctx.declarator()));
		if(ctx.initializer()!=null) {
			builder.addInitialize(map.get(ctx.initializer()));
		}
		map.put(ctx, builder.build());
	}
	
	@Override
	public void exitDeclarator(DeclaratorContext ctx) {
		map.put(ctx, map.get(ctx.directDeclarator()));
	}
	// directDeclarator :  Identifier
	@Override
	public void exitDeclaratorToIdentifier(DeclaratorToIdentifierContext ctx) {
		map.put(ctx, new ASTVariableDeclarator(new ASTIdentifier(ctx.Identifier().getText(),ctx.Identifier().getSymbol().getTokenIndex())));
	}
	// directDeclarator : ( directDeclarator )
	@Override
	public void exitSubDeclarator(SubDeclaratorContext ctx) {
		map.put(ctx, map.get(ctx.declarator()));
	}
	// directDeclarator : directDeclarator '[' assignmentExpression? ']'
	// typeQualifierList:   typeQualifier+
	@Override
	public void exitArrayDeclarator(ArrayDeclaratorContext ctx) {
		ASTArrayDeclarator.Builder builder = new ASTArrayDeclarator.Builder();
		builder.setDecl((ASTDeclarator)map.get(ctx.directDeclarator()));
		if(ctx.assignmentExpression()!=null)
			builder.setExprs(map.get(ctx.assignmentExpression()));
		map.put(ctx, builder.build());
	}
	
	// directDeclarator : directDeclarator '(' parameterTypeList? ')'	
	// parameterTypeList:   parameterList
	//parameterList:   parameterDeclaration (',' parameterDeclaration)*

	@Override
	public void exitFunctionDeclarator(FunctionDeclaratorContext ctx) {
		ASTFunctionDeclarator.Builder builder = new ASTFunctionDeclarator.Builder();
		builder.setDecl((ASTDeclarator)map.get(ctx.directDeclarator()));
		if(ctx.parameterTypeList() != null) {
			builder.addParams(map.get(ctx.parameterTypeList()));
		}
		map.put(ctx, builder.build());
	}
	
	@Override
	public void exitParameterTypeListUsed(ParameterTypeListUsedContext ctx) {
		 List<ASTParamsDeclarator> paramList = new LinkedList<ASTParamsDeclarator>();
		 for (ParameterDeclarationContext pdc : ctx.parameterList().parameterDeclaration()) {
			paramList.add((ASTParamsDeclarator) map.get(pdc));
		}
		map.put(ctx, paramList);
	}
	
	//parameterDeclaration: declarationSpecifiers directDeclarator	
	@Override
	public void exitParamDeclarator(ParamDeclaratorContext ctx) {
		ASTParamsDeclarator.Builder builder = new ASTParamsDeclarator.Builder();
		builder.setDeclarator((ASTDeclarator)map.get(ctx.declarator()));
    	for (DeclarationSpecifierContext element : ctx.declarationSpecifiers().declarationSpecifier()) {
			Object object = map.get(element);
			builder.addSpecfiers(object);
		}
		//ctx.declarationSpecifiers().declarationSpecifier()
			//.stream().map(map::get).forEachOrdered(builder::addSpecfiers);
		map.put(ctx, builder.build());
	}
	//parameterDeclaration: declarationSpecifiers2 directAbstractDeclarator?
	//declarationSpecifiers2��declarationSpecifier+
	@Override
	public void exitParamAbstractDeclarator(ParamAbstractDeclaratorContext ctx) {
		ASTParamsDeclarator.Builder builder = new ASTParamsDeclarator.Builder();
		if(ctx.abstractDeclarator() != null)
			builder.setDeclarator((ASTDeclarator)map.get(ctx.abstractDeclarator()));
    	for (DeclarationSpecifierContext element : ctx.declarationSpecifiers2().declarationSpecifier()) {
			Object object = map.get(element);
			builder.addSpecfiers(object);
		}
		//ctx.declarationSpecifiers2().declarationSpecifier()
		//	.stream().map(map::get).forEachOrdered(builder::addSpecfiers);
		map.put(ctx, builder.build());
	}
	
	@Override
	public void exitAbstractDeclaratorUsed(AbstractDeclaratorUsedContext ctx) {
		map.put(ctx, map.get(ctx.directAbstractDeclarator()));
	}
	
	//directAbstractDeclarator:'('directAbstractDeclarator ')'
	@Override
	public void exitSubAbstractDeclarator(SubAbstractDeclaratorContext ctx) {
		map.put(ctx, map.get(ctx.abstractDeclarator()));
	}
	//directAbstractDeclarator:'[' assignmentExpression? ']'
	//directAbstractDeclarator: directAbstractDeclarator '[' assignmentExpression? ']'
	@Override
	public void exitArrayAbstractDeclarator(ArrayAbstractDeclaratorContext ctx) {
		ASTArrayDeclarator.Builder builder = new ASTArrayDeclarator.Builder();
		if(ctx.directAbstractDeclarator()!=null)
			builder.setDecl((ASTDeclarator)map.get(ctx.directAbstractDeclarator()));
		else
			builder.setDecl(null);
		if(ctx.assignmentExpression()!=null)
			builder.setExprs(map.get(ctx.assignmentExpression()));
		map.put(ctx, builder.build());
	}
	//directAbstractDeclarator:'(' parameterTypeList? ')'
	//directAbstractDeclarator:directAbstractDeclarator '(' parameterTypeList? ')'
	@Override
	public void exitFunctionAbstractDeclarator(FunctionAbstractDeclaratorContext ctx) {
		ASTFunctionDeclarator.Builder builder = new ASTFunctionDeclarator.Builder();
		if(ctx.directAbstractDeclarator()!=null)
			builder.setDecl((ASTDeclarator)map.get(ctx.directAbstractDeclarator()));
		else
			builder.setDecl(null);
		if(ctx.parameterTypeList() != null) {
			builder.addParams(map.get(ctx.parameterTypeList()));
		}
		map.put(ctx, builder.build());
	}
	// initializer : assignmentExpression
	@Override
	public void exitInitializerToExpr(InitializerToExprContext ctx) {
		map.put(ctx, map.get(ctx.assignmentExpression()));
	}
	// initializer : '{' initializerList' ','?}'
	@Override
	public void exitInitializerFromList(InitializerFromListContext ctx) {
		map.put(ctx, map.get(ctx.initializerList()));
	}
	// initializerList : initializer (',' initializer)*
	@Override
	public void exitInitializerList(InitializerListContext ctx) {
		List<ASTExpression> exprs = new LinkedList<ASTExpression>();
		List<InitializerContext> x = ctx.initializer();
		for (InitializerContext initializerContext : x) {
			Object node=map.get(initializerContext);
			if(node instanceof ASTExpression) {
				exprs.add((ASTExpression)node);
			}else if(node instanceof List) {
				for (Object obj : (List)node) {
					exprs.add((ASTExpression)obj);
				}
			}
		}		
        map.put(ctx, exprs);
	}
	
	
	/*
	 * Expression AST node generate
	 */
	
	// primaryExpression: Identifier
	@Override
	public void exitIdentifier(IdentifierContext ctx) {
		map.put(ctx, new ASTIdentifier(ctx.Identifier().getText(),ctx.Identifier().getSymbol().getTokenIndex()));
	}
	// primaryExpression: IntegerConstant
	@Override
	public void exitIntConst(IntConstContext ctx) {
		map.put(ctx, new ASTIntegerConstant(Integer.parseInt(ctx.IntegerConstant().getText()),ctx.IntegerConstant().getSymbol().getTokenIndex()));
	}
	//primaryExpression:FloatingConstant
	@Override
	public void exitFloatConst(FloatConstContext ctx) {
		map.put(ctx, new ASTFloatConstant(Double.parseDouble(ctx.FloatingConstant().getText()),ctx.FloatingConstant().getSymbol().getTokenIndex()));
	}
	//primaryExpression:CharacterConstant
	@Override
	public void exitCharConst(CharConstContext ctx) {
		String string = ctx.CharacterConstant().getText();
		map.put(ctx, new ASTCharConstant(string,ctx.CharacterConstant().getSymbol().getTokenIndex()));
	}
	//primaryExpression:StringLiteral+
	@Override
	public void exitStringConst(StringConstContext ctx) {
		map.put(ctx, new ASTStringConstant(ctx.StringLiteral(0).getText(),ctx.StringLiteral(0).getSymbol().getTokenIndex()));
	}
	//primaryExpression:'(' expression ')'
	@Override
	public void exitSubExpr(SubExprContext ctx) {
		map.put(ctx, map.get(ctx.expression()));
	}
	
	//postfixExpression:primaryExpression
	@Override
	public void exitPostfixExprToPrimaryExpr(PostfixExprToPrimaryExprContext ctx) {
		map.put(ctx, map.get(ctx.primaryExpression()));
	}
	//postfixExpression:postfixExpression'[' expression ']'
	@Override
	public void exitArrayAccess(ArrayAccessContext ctx) {
		ASTArrayAccess.Builder builder = new ASTArrayAccess.Builder();
		ASTExpression arrayName = exprsToExpr(map.get(ctx.postfixExpression()));
		builder.setArrayName(arrayName);
		builder.addElement(map.get(ctx.expression()));
		map.put(ctx, builder.build());
	}
	//postfixExpression:postfixExpression'(' argumentExpressionList? ')'
	//argumentExpressionList:assignmentExpression (',' assignmentExpression)*
	@Override
	public void exitFuncCall(FuncCallContext ctx) {
		ASTFunctionCall.Builder builder = new ASTFunctionCall.Builder();
		ASTExpression funcname = exprsToExpr(map.get(ctx.postfixExpression()));
		builder.setName(funcname);
		if(ctx.argumentExpressionList()!=null) {
	    	for (AssignmentExpressionContext element : ctx.argumentExpressionList().assignmentExpression()) {
				Object object = map.get(element);
				builder.addArg(object);
			}
			//ctx.argumentExpressionList().assignmentExpression()
		//	.stream().map(map::get).forEachOrdered(builder::addArg);
		}

		map.put(ctx, builder.build());
	}
	//postfixExpression:postfixExpression'.' Identifier
	//					postfixExpression'->' Identifier
	@Override
	public void exitMemberAccess(MemberAccessContext ctx) {
		ASTExpression name = exprsToExpr(map.get(ctx.postfixExpression()));
		map.put(ctx, new ASTMemberAccess(
				name, 
				new ASTToken(ctx.op.getText(), ctx.op.getTokenIndex()), 
				new ASTIdentifier((String)ctx.Identifier().getText(),ctx.Identifier().getSymbol().getTokenIndex())
				)
				);
	}
	//postfixExpression:postfixExpression'++'
	//   				postfixExpression'--'
	@Override
	public void exitPostfixExpr(PostfixExprContext ctx) {
		ASTExpression name = exprsToExpr(map.get(ctx.postfixExpression()));
		map.put(ctx, new ASTPostfixExpression(
				name, 
				new ASTToken(ctx.op.getText(), ctx.op.getTokenIndex()))
				);
	}
	//unaryExpression��postfixExpression
	@Override
	public void exitUnaryExprToPostfixExpr(UnaryExprToPostfixExprContext ctx) {
		map.put(ctx, map.get(ctx.postfixExpression()));
	}
	// unaryExpression : op=('++'|'--'|'sizeof') unaryExpression
	@Override
	public void exitUnaryExpr(UnaryExprContext ctx) {
		ASTExpression name = exprsToExpr(map.get(ctx.unaryExpression()));
		map.put(ctx, new ASTUnaryExpression(
				new ASTToken(ctx.op.getText(), ctx.op.getTokenIndex()),
				name));
	}
	//unaryExpression : op=('&' | '*' | '+' | '-' | '~' | '!') castExpression
	@Override
	public void exitUnaryExprCast(UnaryExprCastContext ctx) {
		ASTExpression name = exprsToExpr(map.get(ctx.castExpression()));
		map.put(ctx, new ASTUnaryExpression(
				new ASTToken(ctx.op.getText(), ctx.op.getTokenIndex()),
				name));
	}
	// unaryExpression:op=('sizeof'|'_Alignof') '(' typeName ')'
	@Override
	public void exitUnaryTypename(UnaryTypenameContext ctx) {
		map.put(ctx, new ASTUnaryTypename(
				new ASTToken(ctx.op.getText(), ctx.op.getTokenIndex()),
				(ASTTypename)map.get(ctx.typeName())
				));
	}
	//typeName:specifierQualifierList directAbstractDeclarator?
	@Override
	public void exitTypeName(TypeNameContext ctx) {
		ASTDeclarator declarator;
		if(ctx.abstractDeclarator()!=null)
			declarator = (ASTDeclarator)map.get(ctx.abstractDeclarator());
		else
			declarator = null;
		map.put(ctx, new ASTTypename(
				(List<ASTToken>)map.get(ctx.specifierQualifierList()), 
				declarator)
				);
	}
	//specifierQualifierList:   typeSpecifier specifierQualifierList?
	@Override
	public void exitSpecifierQualifierListUsed(SpecifierQualifierListUsedContext ctx) {
		LinkedList<ASTToken> lists = new LinkedList<ASTToken>();
		lists.add((ASTToken)map.get(ctx.typeSpecifier()));
		if(ctx.specifierQualifierList()!=null) {
			lists.addAll((LinkedList<ASTToken>)map.get(ctx.specifierQualifierList()));
		}
		map.put(ctx, lists);
	}
	
	// castExpression : unaryExpression
	@Override
	public void exitCastExprToUnaryExpr(CastExprToUnaryExprContext ctx) {
		map.put(ctx, map.get(ctx.unaryExpression()));
	}
	//castExpression:'(' typeName ')' castExpression
	@Override
	public void exitCastExpr(CastExprContext ctx) {
		ASTExpression name = exprsToExpr(map.get(ctx.castExpression()));
		map.put(ctx, new ASTCastExpression(
				(ASTTypename)map.get(ctx.typeName()),
				name));
	}
	//binaryExpression:	castExpression
	@Override
	public void exitBinaryExprToCastExpr(BinaryExprToCastExprContext ctx) {
		map.put(ctx, map.get(ctx.castExpression()));
	}
	// binaryExpression:
    //    binaryExpression op=('*' | '/' | '%') binaryExpression   # binaryExpr       
    //|   binaryExpression op=('+' | '-') binaryExpression         # binaryExpr       
    //|   binaryExpression op=('<<'|'>>') binaryExpression         # binaryExpr       
    //|   binaryExpression op=('<' | '>') binaryExpression         # binaryExpr      
    //|   binaryExpression op=('<='|'>=') binaryExpression         # binaryExpr
    //|   binaryExpression op=('=='|'!=') binaryExpression         # binaryExpr       
    //|   binaryExpression op='&' binaryExpression                 # binaryExpr       
    //|   binaryExpression op='^' binaryExpression                 # binaryExpr      
    //|   binaryExpression op='|' binaryExpression                 # binaryExpr       
    //|   binaryExpression op='&&' binaryExpression                # binaryExpr       
    //|   binaryExpression op='||' binaryExpression                # binaryExpr       
	//;
	@Override
	public void exitBinaryExpr(BinaryExprContext ctx) {
		//                                                   !!!!ctx.op.getTokenIndex()
		ASTExpression name1 = exprsToExpr(map.get(ctx.binaryExpression(0)));
		ASTExpression name2 = exprsToExpr(map.get(ctx.binaryExpression(1)));
		map.put(ctx, new ASTBinaryExpression(
				new ASTToken(ctx.op.getText(), ctx.op.getTokenIndex()),
				name1,
				name2
				));
	}
	//conditionalExpression:binaryExpression
	@Override
	public void exitCondExprToBinaryExpr(CondExprToBinaryExprContext ctx) {
		map.put(ctx, map.get(ctx.binaryExpression()));
	}
	//conditionalExpression:binaryExpression '?' expression ':' conditionalExpression
	@Override
	public void exitCondExpr(CondExprContext ctx) {
		ASTExpression name1 = exprsToExpr(map.get(ctx.binaryExpression()));
		ASTExpression name2 = exprsToExpr(map.get(ctx.conditionalExpression()));
		map.put(ctx, new ASTConditionExpression(
				name1, 
				(LinkedList<ASTExpression>)map.get(ctx.expression()), 
				name2
				));
	}
	// assignmentExpression:conditionalExpression
	@Override
	public void exitAssignExprToCondExpr(AssignExprToCondExprContext ctx) {
		map.put(ctx, map.get(ctx.conditionalExpression()));
	}
	//assignmentExpression : unaryExpression assignmentOperator assignmentExpression
	@Override
	public void exitAssignExpr(AssignExprContext ctx) {
		ASTExpression name1 = exprsToExpr(map.get(ctx.unaryExpression()));
		ASTExpression name2 = exprsToExpr(map.get(ctx.assignmentExpression()));
		map.put(ctx, new ASTBinaryExpression(
				(ASTToken)map.get(ctx.assignmentOperator()), 
				name1, 
				name2
				));
	}
	//assignmentOperator: '=' | '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|='
	@Override
	public void exitAssignmentOperator(AssignmentOperatorContext ctx) {
		map.put(ctx, new ASTToken(ctx.getText(), ctx.op.getTokenIndex()));
	}
	//expression : assignmentExpression (',' assignmentExpression)*
	@Override
	public void exitExpression(ExpressionContext ctx) {
		LinkedList<ASTExpression> exprs = new LinkedList<ASTExpression>();
		List<AssignmentExpressionContext> x = ctx.assignmentExpression();
		for (AssignmentExpressionContext ax : x) {
			exprs.add((ASTExpression)map.get(ax));
		}
		map.put(ctx,exprs);
	}
	//constantExpression:conditionalExpression
	@Override
	public void exitConstantExpression(ConstantExpressionContext ctx) {
		map.put(ctx, map.get(ctx.conditionalExpression()));
	}
	
	
	/*
	 * Statement's AST node generate
	 */
	
	// statement : labeledStatement
	@Override
	public void exitLabeledStat(LabeledStatContext ctx) {
		map.put(ctx, map.get(ctx.labeledStatement()));
	}
	// statement :compoundStatement
	@Override
	public void exitCompoundStat(CompoundStatContext ctx) {
		map.put(ctx, map.get(ctx.compoundStatement()));
	}
	// statement :expressionStatement
	@Override
	public void exitExpressionStat(ExpressionStatContext ctx) {
		map.put(ctx, map.get(ctx.expressionStatement()));
	}
	// statement :selectionStatement
	@Override
	public void exitSelectionStat(SelectionStatContext ctx) {
		map.put(ctx, map.get(ctx.selectionStatement()));
	}
	// statement :iterationStatement
	@Override
	public void exitIterationStat(IterationStatContext ctx) {
		map.put(ctx, map.get(ctx.iterationStatement()));
	}
	// statement :jumpStatement
	@Override
	public void exitJumpStat(JumpStatContext ctx) {
		map.put(ctx, map.get(ctx.jumpStatement()));
	}
	//labeledStatement: Identifier ':' statement
	@Override
	public void exitLabeledStatementUsed(LabeledStatementUsedContext ctx) {
		map.put(ctx, new ASTLabeledStatement(
				new ASTIdentifier(ctx.Identifier().getText(),ctx.Identifier().getSymbol().getTokenIndex()), 
				(ASTStatement)map.get(ctx.statement())
				));
	}

	//compoundStatement : '{' blockItem* '}'
	@Override
	public void exitCompoundStatement(CompoundStatementContext ctx) {
		LinkedList<ASTNode> lists = new LinkedList<ASTNode>();
		List<BlockItemContext> x = ctx.blockItem();
		for (BlockItemContext bx : x) {
			lists.add((ASTNode)map.get(bx));
		}
		map.put(ctx, new ASTCompoundStatement(lists));
	}
	//blockItem : statement
	@Override
	public void exitBlockItemStat(BlockItemStatContext ctx) {
		map.put(ctx, map.get(ctx.statement()));
	}
	// blockItem : declaration
	@Override
	public void exitBlockItemDecl(BlockItemDeclContext ctx) {
		map.put(ctx, map.get(ctx.declaration()));
	}
	//expressionStatement:expression?';'
	@Override
	public void exitExpressionStatement(ExpressionStatementContext ctx) {
		if(ctx.expression() != null)
			map.put(ctx, new ASTExpressionStatement((List<ASTExpression>)map.get(ctx.expression())));
		else {
			map.put(ctx, new ASTExpressionStatement(null));
		}
			
	}
	// selectionStatement : 'if' '(' expression ')' statement ('else' statement)?
	@Override
	public void exitSelectionStatementUsed(SelectionStatementUsedContext ctx) {
		ASTStatement otherwise;
		if(ctx.statement().size()>1)
			otherwise = (ASTStatement)map.get(ctx.statement(1));
		else 
			otherwise = null;
		map.put(ctx, new ASTSelectionStatement(
				(LinkedList<ASTExpression>)map.get(ctx.expression()),
				(ASTStatement)map.get(ctx.statement(0)), 
				otherwise
				));
	}
	//iterationStatement:For '(' expression? ';' expression? ';' expression? ')' statement	
	@Override
	public void exitIterationStatExpr(IterationStatExprContext ctx) {
		map.put(ctx, new ASTIterationStatement(
				(LinkedList<ASTExpression>)map.get(ctx.init),
				(LinkedList<ASTExpression>)map.get(ctx.cond), 
				(LinkedList<ASTExpression>)map.get(ctx.step), 
				(ASTStatement)map.get(ctx.statement())
				));
	}
	//iterationStatement:For '(' declaration expression? ';' expression? ')' statement
	@Override
	public void exitIterationStatDecl(IterationStatDeclContext ctx) {
		map.put(ctx, new ASTIterationDeclaredStatement(
				(ASTDeclaration)map.get(ctx.decl),
				(LinkedList<ASTExpression>)map.get(ctx.cond),
				(LinkedList<ASTExpression>)map.get(ctx.step),
				(ASTStatement)map.get(ctx.statement())
				)
				);
	}
	//jumpStatement:'goto' Identifier ';'
	@Override
	public void exitGotoStat(GotoStatContext ctx) {
		map.put(ctx, new ASTGotoStatement(new ASTIdentifier(ctx.Identifier().getText(),ctx.Identifier().getSymbol().getTokenIndex())));
	}
	// jumpStatement:'continue' ';'	
	@Override
	public void exitContinueStat(ContinueStatContext ctx) {
		map.put(ctx, new ASTContinueStatement());
	}
	// jumpStatement: 'break' ';'	
	@Override
	public void exitBreakStat(BreakStatContext ctx) {
		map.put(ctx, new ASTBreakStatement());
	}
	// jumpStatement :  'return' expression? ';'
	@Override
	public void exitReturnStat(ReturnStatContext ctx) {
		if(ctx.expression()!=null)
			map.put(ctx, new ASTReturnStatement((LinkedList<ASTExpression>)map.get(ctx.expression())));
		else
			map.put(ctx, new ASTReturnStatement(null));
	}
	
	private ASTExpression exprsToExpr(Object object) {
		if(object instanceof ASTExpression) {
			return (ASTExpression)object;
		}else if (object instanceof List) {
			return ((LinkedList<ASTExpression>)object).getFirst();
		}else {
			System.out.println("ASTBuilder : Expression error.");
			return null;
		}
	}
}
