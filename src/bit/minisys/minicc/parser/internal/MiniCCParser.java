package bit.minisys.minicc.parser.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.parser.IMiniCCParser;
import bit.minisys.minicc.parser.ast.ASTCompilationUnit;
import bit.minisys.minicc.parser.internal.antlr.*;


public class MiniCCParser implements IMiniCCParser{

	@Override
	public String run(String iFile) throws Exception {
		String oFile = MiniCCUtil.removeAllExt(iFile) + MiniCCCfg.MINICC_PARSER_OUTPUT_EXT;
		
		InputStream iStream = new FileInputStream(iFile);
		ANTLRInputStream inputStream = new ANTLRInputStream(iStream);
		CLexer lexer = new CLexer(inputStream);
		CommonTokenStream tokenStream= new CommonTokenStream(lexer);
		CParser parser = new CParser(tokenStream);
		ParseTree tree = parser.compilationUnit();
	
		// 0 visualization of parsing tree
		TreeViewer viewr = new TreeViewer(Arrays.asList(parser.getRuleNames()),tree);
	    viewr.open();
		
		// 1. ���﷨������treeת���ɹ淶AST
		ParseTreeWalker walker = new ParseTreeWalker();
		ASTBuilder astBuilder = new ASTBuilder();
		walker.walk(astBuilder, tree);
		ASTCompilationUnit program = astBuilder.getProgram();
		
		// 2. �ѹ淶ASTд��json�ļ�
	    ObjectMapper mapper = new ObjectMapper();
	   // System.out.println(mapper.writeValueAsString(program));
		mapper.writeValue(new File(oFile), program);
	     
        System.out.println("3. Parsing Finished!");

		
		return oFile;
	}

}
