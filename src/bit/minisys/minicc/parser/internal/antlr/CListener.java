// Generated from C.g4 by ANTLR 4.8

package bit.minisys.minicc.parser.internal.antlr;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CParser}.
 */
public interface CListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code identifier}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(CParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifier}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(CParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterIntConst(CParser.IntConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitIntConst(CParser.IntConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code floatConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterFloatConst(CParser.FloatConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code floatConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitFloatConst(CParser.FloatConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code charConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterCharConst(CParser.CharConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code charConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitCharConst(CParser.CharConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterStringConst(CParser.StringConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringConst}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitStringConst(CParser.StringConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subExpr}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterSubExpr(CParser.SubExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subExpr}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitSubExpr(CParser.SubExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primaryExpressionUnused}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryExpressionUnused(CParser.PrimaryExpressionUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primaryExpressionUnused}
	 * labeled alternative in {@link CParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryExpressionUnused(CParser.PrimaryExpressionUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#genericSelection}.
	 * @param ctx the parse tree
	 */
	void enterGenericSelection(CParser.GenericSelectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#genericSelection}.
	 * @param ctx the parse tree
	 */
	void exitGenericSelection(CParser.GenericSelectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#genericAssocList}.
	 * @param ctx the parse tree
	 */
	void enterGenericAssocList(CParser.GenericAssocListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#genericAssocList}.
	 * @param ctx the parse tree
	 */
	void exitGenericAssocList(CParser.GenericAssocListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#genericAssociation}.
	 * @param ctx the parse tree
	 */
	void enterGenericAssociation(CParser.GenericAssociationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#genericAssociation}.
	 * @param ctx the parse tree
	 */
	void exitGenericAssociation(CParser.GenericAssociationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code memberAccess}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterMemberAccess(CParser.MemberAccessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code memberAccess}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitMemberAccess(CParser.MemberAccessContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postfixExpressionUnused}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixExpressionUnused(CParser.PostfixExpressionUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postfixExpressionUnused}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixExpressionUnused(CParser.PostfixExpressionUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayAccess}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterArrayAccess(CParser.ArrayAccessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayAccess}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitArrayAccess(CParser.ArrayAccessContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcCall}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterFuncCall(CParser.FuncCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcCall}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitFuncCall(CParser.FuncCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postfixExpr}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixExpr(CParser.PostfixExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postfixExpr}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixExpr(CParser.PostfixExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postfixExprToPrimaryExpr}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixExprToPrimaryExpr(CParser.PostfixExprToPrimaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postfixExprToPrimaryExpr}
	 * labeled alternative in {@link CParser#postfixExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixExprToPrimaryExpr(CParser.PostfixExprToPrimaryExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#argumentExpressionList}.
	 * @param ctx the parse tree
	 */
	void enterArgumentExpressionList(CParser.ArgumentExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#argumentExpressionList}.
	 * @param ctx the parse tree
	 */
	void exitArgumentExpressionList(CParser.ArgumentExpressionListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExprToPostfixExpr}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExprToPostfixExpr(CParser.UnaryExprToPostfixExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExprToPostfixExpr}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExprToPostfixExpr(CParser.UnaryExprToPostfixExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpr(CParser.UnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpr(CParser.UnaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExprCast}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExprCast(CParser.UnaryExprCastContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExprCast}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExprCast(CParser.UnaryExprCastContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryTypename}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryTypename(CParser.UnaryTypenameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryTypename}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryTypename(CParser.UnaryTypenameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpressionUnused}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpressionUnused(CParser.UnaryExpressionUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpressionUnused}
	 * labeled alternative in {@link CParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpressionUnused(CParser.UnaryExpressionUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code castExprToUnaryExpr}
	 * labeled alternative in {@link CParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void enterCastExprToUnaryExpr(CParser.CastExprToUnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code castExprToUnaryExpr}
	 * labeled alternative in {@link CParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void exitCastExprToUnaryExpr(CParser.CastExprToUnaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code castExpr}
	 * labeled alternative in {@link CParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void enterCastExpr(CParser.CastExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code castExpr}
	 * labeled alternative in {@link CParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void exitCastExpr(CParser.CastExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code castExpressionUnused}
	 * labeled alternative in {@link CParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void enterCastExpressionUnused(CParser.CastExpressionUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code castExpressionUnused}
	 * labeled alternative in {@link CParser#castExpression}.
	 * @param ctx the parse tree
	 */
	void exitCastExpressionUnused(CParser.CastExpressionUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryExprToCastExpr}
	 * labeled alternative in {@link CParser#binaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExprToCastExpr(CParser.BinaryExprToCastExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryExprToCastExpr}
	 * labeled alternative in {@link CParser#binaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExprToCastExpr(CParser.BinaryExprToCastExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryExpr}
	 * labeled alternative in {@link CParser#binaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpr(CParser.BinaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryExpr}
	 * labeled alternative in {@link CParser#binaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpr(CParser.BinaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code condExprToBinaryExpr}
	 * labeled alternative in {@link CParser#conditionalExpression}.
	 * @param ctx the parse tree
	 */
	void enterCondExprToBinaryExpr(CParser.CondExprToBinaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code condExprToBinaryExpr}
	 * labeled alternative in {@link CParser#conditionalExpression}.
	 * @param ctx the parse tree
	 */
	void exitCondExprToBinaryExpr(CParser.CondExprToBinaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code condExpr}
	 * labeled alternative in {@link CParser#conditionalExpression}.
	 * @param ctx the parse tree
	 */
	void enterCondExpr(CParser.CondExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code condExpr}
	 * labeled alternative in {@link CParser#conditionalExpression}.
	 * @param ctx the parse tree
	 */
	void exitCondExpr(CParser.CondExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignExprToCondExpr}
	 * labeled alternative in {@link CParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void enterAssignExprToCondExpr(CParser.AssignExprToCondExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignExprToCondExpr}
	 * labeled alternative in {@link CParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void exitAssignExprToCondExpr(CParser.AssignExprToCondExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignExpr}
	 * labeled alternative in {@link CParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void enterAssignExpr(CParser.AssignExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignExpr}
	 * labeled alternative in {@link CParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void exitAssignExpr(CParser.AssignExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentOperator(CParser.AssignmentOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentOperator(CParser.AssignmentOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(CParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(CParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#constantExpression}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpression(CParser.ConstantExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#constantExpression}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpression(CParser.ConstantExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declarationUsed}
	 * labeled alternative in {@link CParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationUsed(CParser.DeclarationUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declarationUsed}
	 * labeled alternative in {@link CParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationUsed(CParser.DeclarationUsedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declarationUnused}
	 * labeled alternative in {@link CParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationUnused(CParser.DeclarationUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declarationUnused}
	 * labeled alternative in {@link CParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationUnused(CParser.DeclarationUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#declarationSpecifiers}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationSpecifiers(CParser.DeclarationSpecifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#declarationSpecifiers}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationSpecifiers(CParser.DeclarationSpecifiersContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#declarationSpecifiers2}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationSpecifiers2(CParser.DeclarationSpecifiers2Context ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#declarationSpecifiers2}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationSpecifiers2(CParser.DeclarationSpecifiers2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code declarationSpecifierUnused}
	 * labeled alternative in {@link CParser#declarationSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationSpecifierUnused(CParser.DeclarationSpecifierUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declarationSpecifierUnused}
	 * labeled alternative in {@link CParser#declarationSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationSpecifierUnused(CParser.DeclarationSpecifierUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declarationSpecifierUsed}
	 * labeled alternative in {@link CParser#declarationSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationSpecifierUsed(CParser.DeclarationSpecifierUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declarationSpecifierUsed}
	 * labeled alternative in {@link CParser#declarationSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationSpecifierUsed(CParser.DeclarationSpecifierUsedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#initDeclaratorList}.
	 * @param ctx the parse tree
	 */
	void enterInitDeclaratorList(CParser.InitDeclaratorListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#initDeclaratorList}.
	 * @param ctx the parse tree
	 */
	void exitInitDeclaratorList(CParser.InitDeclaratorListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#initDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterInitDeclarator(CParser.InitDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#initDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitInitDeclarator(CParser.InitDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#storageClassSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterStorageClassSpecifier(CParser.StorageClassSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#storageClassSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitStorageClassSpecifier(CParser.StorageClassSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeSpecifiersUsed}
	 * labeled alternative in {@link CParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterTypeSpecifiersUsed(CParser.TypeSpecifiersUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeSpecifiersUsed}
	 * labeled alternative in {@link CParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitTypeSpecifiersUsed(CParser.TypeSpecifiersUsedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeSpecifiersUnused}
	 * labeled alternative in {@link CParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterTypeSpecifiersUnused(CParser.TypeSpecifiersUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeSpecifiersUnused}
	 * labeled alternative in {@link CParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitTypeSpecifiersUnused(CParser.TypeSpecifiersUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#otherTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterOtherTypeSpecifier(CParser.OtherTypeSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#otherTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitOtherTypeSpecifier(CParser.OtherTypeSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#structOrUnionSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterStructOrUnionSpecifier(CParser.StructOrUnionSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#structOrUnionSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitStructOrUnionSpecifier(CParser.StructOrUnionSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#structOrUnion}.
	 * @param ctx the parse tree
	 */
	void enterStructOrUnion(CParser.StructOrUnionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#structOrUnion}.
	 * @param ctx the parse tree
	 */
	void exitStructOrUnion(CParser.StructOrUnionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#structDeclarationList}.
	 * @param ctx the parse tree
	 */
	void enterStructDeclarationList(CParser.StructDeclarationListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#structDeclarationList}.
	 * @param ctx the parse tree
	 */
	void exitStructDeclarationList(CParser.StructDeclarationListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#structDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterStructDeclaration(CParser.StructDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#structDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitStructDeclaration(CParser.StructDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code specifierQualifierListUsed}
	 * labeled alternative in {@link CParser#specifierQualifierList}.
	 * @param ctx the parse tree
	 */
	void enterSpecifierQualifierListUsed(CParser.SpecifierQualifierListUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code specifierQualifierListUsed}
	 * labeled alternative in {@link CParser#specifierQualifierList}.
	 * @param ctx the parse tree
	 */
	void exitSpecifierQualifierListUsed(CParser.SpecifierQualifierListUsedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code specifierQualifierListUnused}
	 * labeled alternative in {@link CParser#specifierQualifierList}.
	 * @param ctx the parse tree
	 */
	void enterSpecifierQualifierListUnused(CParser.SpecifierQualifierListUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code specifierQualifierListUnused}
	 * labeled alternative in {@link CParser#specifierQualifierList}.
	 * @param ctx the parse tree
	 */
	void exitSpecifierQualifierListUnused(CParser.SpecifierQualifierListUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#structDeclaratorList}.
	 * @param ctx the parse tree
	 */
	void enterStructDeclaratorList(CParser.StructDeclaratorListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#structDeclaratorList}.
	 * @param ctx the parse tree
	 */
	void exitStructDeclaratorList(CParser.StructDeclaratorListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#structDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterStructDeclarator(CParser.StructDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#structDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitStructDeclarator(CParser.StructDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#enumSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterEnumSpecifier(CParser.EnumSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#enumSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitEnumSpecifier(CParser.EnumSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#enumeratorList}.
	 * @param ctx the parse tree
	 */
	void enterEnumeratorList(CParser.EnumeratorListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#enumeratorList}.
	 * @param ctx the parse tree
	 */
	void exitEnumeratorList(CParser.EnumeratorListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#enumerator}.
	 * @param ctx the parse tree
	 */
	void enterEnumerator(CParser.EnumeratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#enumerator}.
	 * @param ctx the parse tree
	 */
	void exitEnumerator(CParser.EnumeratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#enumerationConstant}.
	 * @param ctx the parse tree
	 */
	void enterEnumerationConstant(CParser.EnumerationConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#enumerationConstant}.
	 * @param ctx the parse tree
	 */
	void exitEnumerationConstant(CParser.EnumerationConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#atomicTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterAtomicTypeSpecifier(CParser.AtomicTypeSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#atomicTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitAtomicTypeSpecifier(CParser.AtomicTypeSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#typeQualifier}.
	 * @param ctx the parse tree
	 */
	void enterTypeQualifier(CParser.TypeQualifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#typeQualifier}.
	 * @param ctx the parse tree
	 */
	void exitTypeQualifier(CParser.TypeQualifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#functionSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterFunctionSpecifier(CParser.FunctionSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#functionSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitFunctionSpecifier(CParser.FunctionSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#alignmentSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterAlignmentSpecifier(CParser.AlignmentSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#alignmentSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitAlignmentSpecifier(CParser.AlignmentSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#declarator}.
	 * @param ctx the parse tree
	 */
	void enterDeclarator(CParser.DeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#declarator}.
	 * @param ctx the parse tree
	 */
	void exitDeclarator(CParser.DeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayDeclarator}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterArrayDeclarator(CParser.ArrayDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayDeclarator}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitArrayDeclarator(CParser.ArrayDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code directDeclaratorUnused}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterDirectDeclaratorUnused(CParser.DirectDeclaratorUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code directDeclaratorUnused}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitDirectDeclaratorUnused(CParser.DirectDeclaratorUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subDeclarator}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterSubDeclarator(CParser.SubDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subDeclarator}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitSubDeclarator(CParser.SubDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionDeclarator}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclarator(CParser.FunctionDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionDeclarator}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclarator(CParser.FunctionDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaratorToIdentifier}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterDeclaratorToIdentifier(CParser.DeclaratorToIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaratorToIdentifier}
	 * labeled alternative in {@link CParser#directDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitDeclaratorToIdentifier(CParser.DeclaratorToIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#gccDeclaratorExtension}.
	 * @param ctx the parse tree
	 */
	void enterGccDeclaratorExtension(CParser.GccDeclaratorExtensionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#gccDeclaratorExtension}.
	 * @param ctx the parse tree
	 */
	void exitGccDeclaratorExtension(CParser.GccDeclaratorExtensionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#gccAttributeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterGccAttributeSpecifier(CParser.GccAttributeSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#gccAttributeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitGccAttributeSpecifier(CParser.GccAttributeSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#gccAttributeList}.
	 * @param ctx the parse tree
	 */
	void enterGccAttributeList(CParser.GccAttributeListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#gccAttributeList}.
	 * @param ctx the parse tree
	 */
	void exitGccAttributeList(CParser.GccAttributeListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#gccAttribute}.
	 * @param ctx the parse tree
	 */
	void enterGccAttribute(CParser.GccAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#gccAttribute}.
	 * @param ctx the parse tree
	 */
	void exitGccAttribute(CParser.GccAttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#nestedParenthesesBlock}.
	 * @param ctx the parse tree
	 */
	void enterNestedParenthesesBlock(CParser.NestedParenthesesBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#nestedParenthesesBlock}.
	 * @param ctx the parse tree
	 */
	void exitNestedParenthesesBlock(CParser.NestedParenthesesBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#pointer}.
	 * @param ctx the parse tree
	 */
	void enterPointer(CParser.PointerContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#pointer}.
	 * @param ctx the parse tree
	 */
	void exitPointer(CParser.PointerContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#typeQualifierList}.
	 * @param ctx the parse tree
	 */
	void enterTypeQualifierList(CParser.TypeQualifierListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#typeQualifierList}.
	 * @param ctx the parse tree
	 */
	void exitTypeQualifierList(CParser.TypeQualifierListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parameterTypeListUsed}
	 * labeled alternative in {@link CParser#parameterTypeList}.
	 * @param ctx the parse tree
	 */
	void enterParameterTypeListUsed(CParser.ParameterTypeListUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parameterTypeListUsed}
	 * labeled alternative in {@link CParser#parameterTypeList}.
	 * @param ctx the parse tree
	 */
	void exitParameterTypeListUsed(CParser.ParameterTypeListUsedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parameterTypeListUnused}
	 * labeled alternative in {@link CParser#parameterTypeList}.
	 * @param ctx the parse tree
	 */
	void enterParameterTypeListUnused(CParser.ParameterTypeListUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parameterTypeListUnused}
	 * labeled alternative in {@link CParser#parameterTypeList}.
	 * @param ctx the parse tree
	 */
	void exitParameterTypeListUnused(CParser.ParameterTypeListUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void enterParameterList(CParser.ParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void exitParameterList(CParser.ParameterListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code paramDeclarator}
	 * labeled alternative in {@link CParser#parameterDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterParamDeclarator(CParser.ParamDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paramDeclarator}
	 * labeled alternative in {@link CParser#parameterDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitParamDeclarator(CParser.ParamDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code paramAbstractDeclarator}
	 * labeled alternative in {@link CParser#parameterDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterParamAbstractDeclarator(CParser.ParamAbstractDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paramAbstractDeclarator}
	 * labeled alternative in {@link CParser#parameterDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitParamAbstractDeclarator(CParser.ParamAbstractDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#identifierList}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierList(CParser.IdentifierListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#identifierList}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierList(CParser.IdentifierListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#typeName}.
	 * @param ctx the parse tree
	 */
	void enterTypeName(CParser.TypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#typeName}.
	 * @param ctx the parse tree
	 */
	void exitTypeName(CParser.TypeNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code abstractDeclaratorUnused}
	 * labeled alternative in {@link CParser#abstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterAbstractDeclaratorUnused(CParser.AbstractDeclaratorUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code abstractDeclaratorUnused}
	 * labeled alternative in {@link CParser#abstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitAbstractDeclaratorUnused(CParser.AbstractDeclaratorUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code abstractDeclaratorUsed}
	 * labeled alternative in {@link CParser#abstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterAbstractDeclaratorUsed(CParser.AbstractDeclaratorUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code abstractDeclaratorUsed}
	 * labeled alternative in {@link CParser#abstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitAbstractDeclaratorUsed(CParser.AbstractDeclaratorUsedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code directAbstractDeclaratorUnused}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterDirectAbstractDeclaratorUnused(CParser.DirectAbstractDeclaratorUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code directAbstractDeclaratorUnused}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitDirectAbstractDeclaratorUnused(CParser.DirectAbstractDeclaratorUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionAbstractDeclarator}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterFunctionAbstractDeclarator(CParser.FunctionAbstractDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionAbstractDeclarator}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitFunctionAbstractDeclarator(CParser.FunctionAbstractDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subAbstractDeclarator}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterSubAbstractDeclarator(CParser.SubAbstractDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subAbstractDeclarator}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitSubAbstractDeclarator(CParser.SubAbstractDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayAbstractDeclarator}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterArrayAbstractDeclarator(CParser.ArrayAbstractDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayAbstractDeclarator}
	 * labeled alternative in {@link CParser#directAbstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitArrayAbstractDeclarator(CParser.ArrayAbstractDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#typedefName}.
	 * @param ctx the parse tree
	 */
	void enterTypedefName(CParser.TypedefNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#typedefName}.
	 * @param ctx the parse tree
	 */
	void exitTypedefName(CParser.TypedefNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code initializerToExpr}
	 * labeled alternative in {@link CParser#initializer}.
	 * @param ctx the parse tree
	 */
	void enterInitializerToExpr(CParser.InitializerToExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code initializerToExpr}
	 * labeled alternative in {@link CParser#initializer}.
	 * @param ctx the parse tree
	 */
	void exitInitializerToExpr(CParser.InitializerToExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code initializerFromList}
	 * labeled alternative in {@link CParser#initializer}.
	 * @param ctx the parse tree
	 */
	void enterInitializerFromList(CParser.InitializerFromListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code initializerFromList}
	 * labeled alternative in {@link CParser#initializer}.
	 * @param ctx the parse tree
	 */
	void exitInitializerFromList(CParser.InitializerFromListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#initializerList}.
	 * @param ctx the parse tree
	 */
	void enterInitializerList(CParser.InitializerListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#initializerList}.
	 * @param ctx the parse tree
	 */
	void exitInitializerList(CParser.InitializerListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#designation}.
	 * @param ctx the parse tree
	 */
	void enterDesignation(CParser.DesignationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#designation}.
	 * @param ctx the parse tree
	 */
	void exitDesignation(CParser.DesignationContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#designatorList}.
	 * @param ctx the parse tree
	 */
	void enterDesignatorList(CParser.DesignatorListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#designatorList}.
	 * @param ctx the parse tree
	 */
	void exitDesignatorList(CParser.DesignatorListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#designator}.
	 * @param ctx the parse tree
	 */
	void enterDesignator(CParser.DesignatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#designator}.
	 * @param ctx the parse tree
	 */
	void exitDesignator(CParser.DesignatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#staticAssertDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterStaticAssertDeclaration(CParser.StaticAssertDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#staticAssertDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitStaticAssertDeclaration(CParser.StaticAssertDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code labeledStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterLabeledStat(CParser.LabeledStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code labeledStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitLabeledStat(CParser.LabeledStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compoundStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterCompoundStat(CParser.CompoundStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compoundStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitCompoundStat(CParser.CompoundStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStat(CParser.ExpressionStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStat(CParser.ExpressionStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectionStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterSelectionStat(CParser.SelectionStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectionStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitSelectionStat(CParser.SelectionStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code iterationStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIterationStat(CParser.IterationStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code iterationStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIterationStat(CParser.IterationStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code jumpStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterJumpStat(CParser.JumpStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code jumpStat}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitJumpStat(CParser.JumpStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code statementUnused}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatementUnused(CParser.StatementUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code statementUnused}
	 * labeled alternative in {@link CParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatementUnused(CParser.StatementUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code labeledStatementUsed}
	 * labeled alternative in {@link CParser#labeledStatement}.
	 * @param ctx the parse tree
	 */
	void enterLabeledStatementUsed(CParser.LabeledStatementUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code labeledStatementUsed}
	 * labeled alternative in {@link CParser#labeledStatement}.
	 * @param ctx the parse tree
	 */
	void exitLabeledStatementUsed(CParser.LabeledStatementUsedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code labeledStatementUnused}
	 * labeled alternative in {@link CParser#labeledStatement}.
	 * @param ctx the parse tree
	 */
	void enterLabeledStatementUnused(CParser.LabeledStatementUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code labeledStatementUnused}
	 * labeled alternative in {@link CParser#labeledStatement}.
	 * @param ctx the parse tree
	 */
	void exitLabeledStatementUnused(CParser.LabeledStatementUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#compoundStatement}.
	 * @param ctx the parse tree
	 */
	void enterCompoundStatement(CParser.CompoundStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#compoundStatement}.
	 * @param ctx the parse tree
	 */
	void exitCompoundStatement(CParser.CompoundStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blockItemStat}
	 * labeled alternative in {@link CParser#blockItem}.
	 * @param ctx the parse tree
	 */
	void enterBlockItemStat(CParser.BlockItemStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blockItemStat}
	 * labeled alternative in {@link CParser#blockItem}.
	 * @param ctx the parse tree
	 */
	void exitBlockItemStat(CParser.BlockItemStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blockItemDecl}
	 * labeled alternative in {@link CParser#blockItem}.
	 * @param ctx the parse tree
	 */
	void enterBlockItemDecl(CParser.BlockItemDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blockItemDecl}
	 * labeled alternative in {@link CParser#blockItem}.
	 * @param ctx the parse tree
	 */
	void exitBlockItemDecl(CParser.BlockItemDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement(CParser.ExpressionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement(CParser.ExpressionStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectionStatementUsed}
	 * labeled alternative in {@link CParser#selectionStatement}.
	 * @param ctx the parse tree
	 */
	void enterSelectionStatementUsed(CParser.SelectionStatementUsedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectionStatementUsed}
	 * labeled alternative in {@link CParser#selectionStatement}.
	 * @param ctx the parse tree
	 */
	void exitSelectionStatementUsed(CParser.SelectionStatementUsedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectionStatementUnused}
	 * labeled alternative in {@link CParser#selectionStatement}.
	 * @param ctx the parse tree
	 */
	void enterSelectionStatementUnused(CParser.SelectionStatementUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectionStatementUnused}
	 * labeled alternative in {@link CParser#selectionStatement}.
	 * @param ctx the parse tree
	 */
	void exitSelectionStatementUnused(CParser.SelectionStatementUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code iterationStatementUnused}
	 * labeled alternative in {@link CParser#iterationStatement}.
	 * @param ctx the parse tree
	 */
	void enterIterationStatementUnused(CParser.IterationStatementUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code iterationStatementUnused}
	 * labeled alternative in {@link CParser#iterationStatement}.
	 * @param ctx the parse tree
	 */
	void exitIterationStatementUnused(CParser.IterationStatementUnusedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code iterationStatExpr}
	 * labeled alternative in {@link CParser#iterationStatement}.
	 * @param ctx the parse tree
	 */
	void enterIterationStatExpr(CParser.IterationStatExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code iterationStatExpr}
	 * labeled alternative in {@link CParser#iterationStatement}.
	 * @param ctx the parse tree
	 */
	void exitIterationStatExpr(CParser.IterationStatExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code iterationStatDecl}
	 * labeled alternative in {@link CParser#iterationStatement}.
	 * @param ctx the parse tree
	 */
	void enterIterationStatDecl(CParser.IterationStatDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code iterationStatDecl}
	 * labeled alternative in {@link CParser#iterationStatement}.
	 * @param ctx the parse tree
	 */
	void exitIterationStatDecl(CParser.IterationStatDeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gotoStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterGotoStat(CParser.GotoStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gotoStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitGotoStat(CParser.GotoStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continueStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStat(CParser.ContinueStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continueStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStat(CParser.ContinueStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code breakStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStat(CParser.BreakStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code breakStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStat(CParser.BreakStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStat(CParser.ReturnStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStat}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStat(CParser.ReturnStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code jumpStatementUnused}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterJumpStatementUnused(CParser.JumpStatementUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code jumpStatementUnused}
	 * labeled alternative in {@link CParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitJumpStatementUnused(CParser.JumpStatementUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompilationUnit(CParser.CompilationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompilationUnit(CParser.CompilationUnitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionDefineItem}
	 * labeled alternative in {@link CParser#externalDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefineItem(CParser.FunctionDefineItemContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionDefineItem}
	 * labeled alternative in {@link CParser#externalDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefineItem(CParser.FunctionDefineItemContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declarationItem}
	 * labeled alternative in {@link CParser#externalDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationItem(CParser.DeclarationItemContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declarationItem}
	 * labeled alternative in {@link CParser#externalDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationItem(CParser.DeclarationItemContext ctx);
	/**
	 * Enter a parse tree produced by the {@code externalDeclarationUnused}
	 * labeled alternative in {@link CParser#externalDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterExternalDeclarationUnused(CParser.ExternalDeclarationUnusedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code externalDeclarationUnused}
	 * labeled alternative in {@link CParser#externalDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitExternalDeclarationUnused(CParser.ExternalDeclarationUnusedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(CParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(CParser.FunctionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CParser#declarationList}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationList(CParser.DeclarationListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CParser#declarationList}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationList(CParser.DeclarationListContext ctx);
}