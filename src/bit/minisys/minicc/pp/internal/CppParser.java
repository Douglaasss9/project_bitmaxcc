package bit.minisys.minicc.pp.internal;

// $ANTLR 3.5 F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g 2017-05-23 23:02:28

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class CppParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AMPERSAND", "AND", "ARGS", "ASSIGNEQUAL", 
		"BITWISEANDEQUAL", "BITWISEOR", "BITWISEOREQUAL", "BITWISEXOR", "BITWISEXOREQUAL", 
		"CHARACTER_LITERAL", "CKEYWORD", "COLON", "COMMA", "COMMENT", "CONCATENATE", 
		"COPERATOR", "DECIMAL_LITERAL", "DEFINE", "DEFINED", "DIRECTIVE", "DIVIDE", 
		"DIVIDEEQUAL", "DOT", "DOTMBR", "ELIF", "ELLIPSIS", "ELSE", "ENDIF", "EQUAL", 
		"ERROR", "ESCAPED_NEWLINE", "EXEC_MACRO", "EXPAND", "EXPR", "EXPR_DEF", 
		"EXPR_GROUP", "EXPR_NDEF", "EXP_ARG", "EXP_ARGS", "End", "EscapeSequence", 
		"Exponent", "FLOATING_POINT_LITERAL", "FloatTypeSuffix", "GREATERTHAN", 
		"GREATERTHANOREQUALTO", "HEX_LITERAL", "HexDigit", "IDENTIFIER", "IF", 
		"IFDEF", "IFNDEF", "INCLUDE", "INCLUDE_EXPAND", "INCLUDE_FILE", "INDEX_OP", 
		"IntegerTypeSuffix", "LCURLY", "LESSTHAN", "LESSTHANOREQUALTO", "LETTER", 
		"LINE", "LINE_COMMENT", "LPAREN", "LSQUARE", "MACRO_DEFINE", "MACRO_TEXT", 
		"MAC_FUNCTION", "MAC_FUNCTION_OBJECT", "MAC_OBJECT", "METHOD_CALL", "MINUS", 
		"MINUSEQUAL", "MINUSMINUS", "MOD", "MODEQUAL", "NOT", "NOTEQUAL", "OCTAL_LITERAL", 
		"OR", "OctalEscape", "PLUS", "PLUSEQUAL", "PLUSPLUS", "POINTER", "POINTERTO", 
		"POINTERTOMBR", "POINTER_AT", "POST_DEC", "POST_INC", "PRAGMA", "QUESTIONMARK", 
		"RCURLY", "REFERANCE", "RPAREN", "RSQUARE", "SCOPE", "SEMICOLON", "SHARPSHARP", 
		"SHIFTLEFT", "SHIFTLEFTEQUAL", "SHIFTRIGHT", "SHIFTRIGHTEQUAL", "SIZEOF", 
		"SIZEOF_TYPE", "STAR", "STRINGIFICATION", "STRING_LITERAL", "STRING_OP", 
		"TEXT_END", "TEXT_GROUP", "TEXT_LINE", "TILDE", "TIMESEQUAL", "TYPECAST", 
		"UNARY_MINUS", "UNARY_PLUS", "UNDEF", "UnicodeEscape", "WARNING", "WS"
	};
	public static final int EOF=-1;
	public static final int AMPERSAND=4;
	public static final int AND=5;
	public static final int ARGS=6;
	public static final int ASSIGNEQUAL=7;
	public static final int BITWISEANDEQUAL=8;
	public static final int BITWISEOR=9;
	public static final int BITWISEOREQUAL=10;
	public static final int BITWISEXOR=11;
	public static final int BITWISEXOREQUAL=12;
	public static final int CHARACTER_LITERAL=13;
	public static final int CKEYWORD=14;
	public static final int COLON=15;
	public static final int COMMA=16;
	public static final int COMMENT=17;
	public static final int CONCATENATE=18;
	public static final int COPERATOR=19;
	public static final int DECIMAL_LITERAL=20;
	public static final int DEFINE=21;
	public static final int DEFINED=22;
	public static final int DIRECTIVE=23;
	public static final int DIVIDE=24;
	public static final int DIVIDEEQUAL=25;
	public static final int DOT=26;
	public static final int DOTMBR=27;
	public static final int ELIF=28;
	public static final int ELLIPSIS=29;
	public static final int ELSE=30;
	public static final int ENDIF=31;
	public static final int EQUAL=32;
	public static final int ERROR=33;
	public static final int ESCAPED_NEWLINE=34;
	public static final int EXEC_MACRO=35;
	public static final int EXPAND=36;
	public static final int EXPR=37;
	public static final int EXPR_DEF=38;
	public static final int EXPR_GROUP=39;
	public static final int EXPR_NDEF=40;
	public static final int EXP_ARG=41;
	public static final int EXP_ARGS=42;
	public static final int End=43;
	public static final int EscapeSequence=44;
	public static final int Exponent=45;
	public static final int FLOATING_POINT_LITERAL=46;
	public static final int FloatTypeSuffix=47;
	public static final int GREATERTHAN=48;
	public static final int GREATERTHANOREQUALTO=49;
	public static final int HEX_LITERAL=50;
	public static final int HexDigit=51;
	public static final int IDENTIFIER=52;
	public static final int IF=53;
	public static final int IFDEF=54;
	public static final int IFNDEF=55;
	public static final int INCLUDE=56;
	public static final int INCLUDE_EXPAND=57;
	public static final int INCLUDE_FILE=58;
	public static final int INDEX_OP=59;
	public static final int IntegerTypeSuffix=60;
	public static final int LCURLY=61;
	public static final int LESSTHAN=62;
	public static final int LESSTHANOREQUALTO=63;
	public static final int LETTER=64;
	public static final int LINE=65;
	public static final int LINE_COMMENT=66;
	public static final int LPAREN=67;
	public static final int LSQUARE=68;
	public static final int MACRO_DEFINE=69;
	public static final int MACRO_TEXT=70;
	public static final int MAC_FUNCTION=71;
	public static final int MAC_FUNCTION_OBJECT=72;
	public static final int MAC_OBJECT=73;
	public static final int METHOD_CALL=74;
	public static final int MINUS=75;
	public static final int MINUSEQUAL=76;
	public static final int MINUSMINUS=77;
	public static final int MOD=78;
	public static final int MODEQUAL=79;
	public static final int NOT=80;
	public static final int NOTEQUAL=81;
	public static final int OCTAL_LITERAL=82;
	public static final int OR=83;
	public static final int OctalEscape=84;
	public static final int PLUS=85;
	public static final int PLUSEQUAL=86;
	public static final int PLUSPLUS=87;
	public static final int POINTER=88;
	public static final int POINTERTO=89;
	public static final int POINTERTOMBR=90;
	public static final int POINTER_AT=91;
	public static final int POST_DEC=92;
	public static final int POST_INC=93;
	public static final int PRAGMA=94;
	public static final int QUESTIONMARK=95;
	public static final int RCURLY=96;
	public static final int REFERANCE=97;
	public static final int RPAREN=98;
	public static final int RSQUARE=99;
	public static final int SCOPE=100;
	public static final int SEMICOLON=101;
	public static final int SHARPSHARP=102;
	public static final int SHIFTLEFT=103;
	public static final int SHIFTLEFTEQUAL=104;
	public static final int SHIFTRIGHT=105;
	public static final int SHIFTRIGHTEQUAL=106;
	public static final int SIZEOF=107;
	public static final int SIZEOF_TYPE=108;
	public static final int STAR=109;
	public static final int STRINGIFICATION=110;
	public static final int STRING_LITERAL=111;
	public static final int STRING_OP=112;
	public static final int TEXT_END=113;
	public static final int TEXT_GROUP=114;
	public static final int TEXT_LINE=115;
	public static final int TILDE=116;
	public static final int TIMESEQUAL=117;
	public static final int TYPECAST=118;
	public static final int UNARY_MINUS=119;
	public static final int UNARY_PLUS=120;
	public static final int UNDEF=121;
	public static final int UnicodeEscape=122;
	public static final int WARNING=123;
	public static final int WS=124;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public CppParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public CppParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
		this.state.ruleMemo = new HashMap[170+1];


	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return CppParser.tokenNames; }
	@Override public String getGrammarFileName() { return "F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g"; }


	public static class preprocess_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "preprocess"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:55:1: preprocess : ( procLine )+ ;
	public final CppParser.preprocess_return preprocess() throws RecognitionException {
		CppParser.preprocess_return retval = new CppParser.preprocess_return();
		retval.start = input.LT(1);
		int preprocess_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope procLine1 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:56:3: ( ( procLine )+ )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:56:5: ( procLine )+
			{
			root_0 = (Object)adaptor.nil();


			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:56:5: ( procLine )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= CHARACTER_LITERAL && LA1_0 <= CKEYWORD)||LA1_0==COMMA||(LA1_0 >= COPERATOR && LA1_0 <= DEFINE)||LA1_0==DIRECTIVE||LA1_0==ERROR||LA1_0==EXEC_MACRO||LA1_0==End||LA1_0==FLOATING_POINT_LITERAL||LA1_0==HEX_LITERAL||(LA1_0 >= IDENTIFIER && LA1_0 <= IF)||(LA1_0 >= INCLUDE && LA1_0 <= INCLUDE_EXPAND)||LA1_0==LINE||LA1_0==LPAREN||LA1_0==OCTAL_LITERAL||LA1_0==PRAGMA||LA1_0==RPAREN||(LA1_0 >= SEMICOLON && LA1_0 <= SHARPSHARP)||LA1_0==SIZEOF||(LA1_0 >= STRINGIFICATION && LA1_0 <= TEXT_END)||LA1_0==UNDEF||(LA1_0 >= WARNING && LA1_0 <= WS)) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:56:5: procLine
					{
					pushFollow(FOLLOW_procLine_in_preprocess222);
					procLine1=procLine();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, procLine1.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 1, preprocess_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "preprocess"


	public static class procLine_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "procLine"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:59:1: procLine : ( DIRECTIVE !| text_line | diagnostics | fileInclusion | macroDefine | macroUndef | conditionalCompilation | lineControl | macroExecution )? End ;
	public final CppParser.procLine_return procLine() throws RecognitionException {
		CppParser.procLine_return retval = new CppParser.procLine_return();
		retval.start = input.LT(1);
		int procLine_StartIndex = input.index();

		Object root_0 = null;

		Token DIRECTIVE2=null;
		Token End11=null;
		ParserRuleReturnScope text_line3 =null;
		ParserRuleReturnScope diagnostics4 =null;
		ParserRuleReturnScope fileInclusion5 =null;
		ParserRuleReturnScope macroDefine6 =null;
		ParserRuleReturnScope macroUndef7 =null;
		ParserRuleReturnScope conditionalCompilation8 =null;
		ParserRuleReturnScope lineControl9 =null;
		ParserRuleReturnScope macroExecution10 =null;

		Object DIRECTIVE2_tree=null;
		Object End11_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:60:3: ( ( DIRECTIVE !| text_line | diagnostics | fileInclusion | macroDefine | macroUndef | conditionalCompilation | lineControl | macroExecution )? End )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:61:3: ( DIRECTIVE !| text_line | diagnostics | fileInclusion | macroDefine | macroUndef | conditionalCompilation | lineControl | macroExecution )? End
			{
			root_0 = (Object)adaptor.nil();


			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:61:3: ( DIRECTIVE !| text_line | diagnostics | fileInclusion | macroDefine | macroUndef | conditionalCompilation | lineControl | macroExecution )?
			int alt2=10;
			switch ( input.LA(1) ) {
				case DIRECTIVE:
					{
					alt2=1;
					}
					break;
				case CHARACTER_LITERAL:
				case CKEYWORD:
				case COMMA:
				case COPERATOR:
				case DECIMAL_LITERAL:
				case FLOATING_POINT_LITERAL:
				case HEX_LITERAL:
				case IDENTIFIER:
				case LPAREN:
				case OCTAL_LITERAL:
				case RPAREN:
				case SEMICOLON:
				case SHARPSHARP:
				case SIZEOF:
				case STRINGIFICATION:
				case STRING_LITERAL:
				case STRING_OP:
				case TEXT_END:
				case WS:
					{
					alt2=2;
					}
					break;
				case ERROR:
				case PRAGMA:
				case WARNING:
					{
					alt2=3;
					}
					break;
				case INCLUDE:
				case INCLUDE_EXPAND:
					{
					alt2=4;
					}
					break;
				case DEFINE:
					{
					alt2=5;
					}
					break;
				case UNDEF:
					{
					alt2=6;
					}
					break;
				case IF:
					{
					alt2=7;
					}
					break;
				case LINE:
					{
					alt2=8;
					}
					break;
				case EXEC_MACRO:
					{
					alt2=9;
					}
					break;
			}
			switch (alt2) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:61:5: DIRECTIVE !
					{
					DIRECTIVE2=(Token)match(input,DIRECTIVE,FOLLOW_DIRECTIVE_in_procLine242); if (state.failed) return retval;
					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:62:5: text_line
					{
					pushFollow(FOLLOW_text_line_in_procLine249);
					text_line3=text_line();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, text_line3.getTree());

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:63:5: diagnostics
					{
					pushFollow(FOLLOW_diagnostics_in_procLine256);
					diagnostics4=diagnostics();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, diagnostics4.getTree());

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:64:5: fileInclusion
					{
					pushFollow(FOLLOW_fileInclusion_in_procLine262);
					fileInclusion5=fileInclusion();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, fileInclusion5.getTree());

					}
					break;
				case 5 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:65:5: macroDefine
					{
					pushFollow(FOLLOW_macroDefine_in_procLine268);
					macroDefine6=macroDefine();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, macroDefine6.getTree());

					}
					break;
				case 6 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:66:5: macroUndef
					{
					pushFollow(FOLLOW_macroUndef_in_procLine274);
					macroUndef7=macroUndef();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, macroUndef7.getTree());

					}
					break;
				case 7 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:67:5: conditionalCompilation
					{
					pushFollow(FOLLOW_conditionalCompilation_in_procLine280);
					conditionalCompilation8=conditionalCompilation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalCompilation8.getTree());

					}
					break;
				case 8 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:68:5: lineControl
					{
					pushFollow(FOLLOW_lineControl_in_procLine286);
					lineControl9=lineControl();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, lineControl9.getTree());

					}
					break;
				case 9 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:69:5: macroExecution
					{
					pushFollow(FOLLOW_macroExecution_in_procLine293);
					macroExecution10=macroExecution();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, macroExecution10.getTree());

					}
					break;

			}

			End11=(Token)match(input,End,FOLLOW_End_in_procLine300); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			End11_tree = (Object)adaptor.create(End11);
			adaptor.addChild(root_0, End11_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 2, procLine_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "procLine"


	public static class fileInclusion_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "fileInclusion"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:73:1: fileInclusion : ( INCLUDE -> ^( INCLUDE ) | INCLUDE_EXPAND -> ^( INCLUDE_EXPAND ) );
	public final CppParser.fileInclusion_return fileInclusion() throws RecognitionException {
		CppParser.fileInclusion_return retval = new CppParser.fileInclusion_return();
		retval.start = input.LT(1);
		int fileInclusion_StartIndex = input.index();

		Object root_0 = null;

		Token INCLUDE12=null;
		Token INCLUDE_EXPAND13=null;

		Object INCLUDE12_tree=null;
		Object INCLUDE_EXPAND13_tree=null;
		RewriteRuleTokenStream stream_INCLUDE_EXPAND=new RewriteRuleTokenStream(adaptor,"token INCLUDE_EXPAND");
		RewriteRuleTokenStream stream_INCLUDE=new RewriteRuleTokenStream(adaptor,"token INCLUDE");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:74:3: ( INCLUDE -> ^( INCLUDE ) | INCLUDE_EXPAND -> ^( INCLUDE_EXPAND ) )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==INCLUDE) ) {
				alt3=1;
			}
			else if ( (LA3_0==INCLUDE_EXPAND) ) {
				alt3=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:74:5: INCLUDE
					{
					INCLUDE12=(Token)match(input,INCLUDE,FOLLOW_INCLUDE_in_fileInclusion314); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_INCLUDE.add(INCLUDE12);

					// AST REWRITE
					// elements: INCLUDE
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 74:14: -> ^( INCLUDE )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:74:18: ^( INCLUDE )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_INCLUDE.nextNode(), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:75:5: INCLUDE_EXPAND
					{
					INCLUDE_EXPAND13=(Token)match(input,INCLUDE_EXPAND,FOLLOW_INCLUDE_EXPAND_in_fileInclusion328); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_INCLUDE_EXPAND.add(INCLUDE_EXPAND13);

					// AST REWRITE
					// elements: INCLUDE_EXPAND
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 75:21: -> ^( INCLUDE_EXPAND )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:75:24: ^( INCLUDE_EXPAND )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_INCLUDE_EXPAND.nextNode(), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 3, fileInclusion_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "fileInclusion"


	public static class macroDefine_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "macroDefine"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:78:1: macroDefine : ( DEFINE IDENTIFIER LPAREN ( WS )? RPAREN ( macro_text )? -> ^( MAC_FUNCTION_OBJECT IDENTIFIER ( macro_text )? ) | DEFINE mac= IDENTIFIER LPAREN ( WS )? (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )? RPAREN ( macro_text )? -> ^( MAC_FUNCTION $mac ( $arg)+ ( macro_text )? ) | DEFINE IDENTIFIER ( macro_text )? -> ^( MAC_OBJECT IDENTIFIER ( macro_text )? ) );
	public final CppParser.macroDefine_return macroDefine() throws RecognitionException {
		CppParser.macroDefine_return retval = new CppParser.macroDefine_return();
		retval.start = input.LT(1);
		int macroDefine_StartIndex = input.index();

		Object root_0 = null;

		Token mac=null;
		Token DEFINE14=null;
		Token IDENTIFIER15=null;
		Token LPAREN16=null;
		Token WS17=null;
		Token RPAREN18=null;
		Token DEFINE20=null;
		Token LPAREN21=null;
		Token WS22=null;
		Token WS23=null;
		Token COMMA24=null;
		Token WS25=null;
		Token WS26=null;
		Token RPAREN27=null;
		Token DEFINE29=null;
		Token IDENTIFIER30=null;
		List<Object> list_arg=null;
		ParserRuleReturnScope macro_text19 =null;
		ParserRuleReturnScope macro_text28 =null;
		ParserRuleReturnScope macro_text31 =null;
		RuleReturnScope arg = null;
		Object mac_tree=null;
		Object DEFINE14_tree=null;
		Object IDENTIFIER15_tree=null;
		Object LPAREN16_tree=null;
		Object WS17_tree=null;
		Object RPAREN18_tree=null;
		Object DEFINE20_tree=null;
		Object LPAREN21_tree=null;
		Object WS22_tree=null;
		Object WS23_tree=null;
		Object COMMA24_tree=null;
		Object WS25_tree=null;
		Object WS26_tree=null;
		Object RPAREN27_tree=null;
		Object DEFINE29_tree=null;
		Object IDENTIFIER30_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
		RewriteRuleTokenStream stream_DEFINE=new RewriteRuleTokenStream(adaptor,"token DEFINE");
		RewriteRuleSubtreeStream stream_macro_text=new RewriteRuleSubtreeStream(adaptor,"rule macro_text");
		RewriteRuleSubtreeStream stream_macroParam=new RewriteRuleSubtreeStream(adaptor,"rule macroParam");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:3: ( DEFINE IDENTIFIER LPAREN ( WS )? RPAREN ( macro_text )? -> ^( MAC_FUNCTION_OBJECT IDENTIFIER ( macro_text )? ) | DEFINE mac= IDENTIFIER LPAREN ( WS )? (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )? RPAREN ( macro_text )? -> ^( MAC_FUNCTION $mac ( $arg)+ ( macro_text )? ) | DEFINE IDENTIFIER ( macro_text )? -> ^( MAC_OBJECT IDENTIFIER ( macro_text )? ) )
			int alt14=3;
			alt14 = dfa14.predict(input);
			switch (alt14) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:5: DEFINE IDENTIFIER LPAREN ( WS )? RPAREN ( macro_text )?
					{
					DEFINE14=(Token)match(input,DEFINE,FOLLOW_DEFINE_in_macroDefine350); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DEFINE.add(DEFINE14);

					IDENTIFIER15=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroDefine353); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER15);

					LPAREN16=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_macroDefine356); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN16);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:32: ( WS )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==WS) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:32: WS
							{
							WS17=(Token)match(input,WS,FOLLOW_WS_in_macroDefine358); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS17);

							}
							break;

					}

					RPAREN18=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_macroDefine361); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN18);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:44: ( macro_text )?
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( ((LA5_0 >= CHARACTER_LITERAL && LA5_0 <= CKEYWORD)||LA5_0==COMMA||(LA5_0 >= COPERATOR && LA5_0 <= DECIMAL_LITERAL)||LA5_0==FLOATING_POINT_LITERAL||LA5_0==HEX_LITERAL||LA5_0==IDENTIFIER||LA5_0==LPAREN||LA5_0==OCTAL_LITERAL||LA5_0==RPAREN||(LA5_0 >= SEMICOLON && LA5_0 <= SHARPSHARP)||LA5_0==SIZEOF||(LA5_0 >= STRINGIFICATION && LA5_0 <= TEXT_END)||LA5_0==WS) ) {
						alt5=1;
					}
					switch (alt5) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:44: macro_text
							{
							pushFollow(FOLLOW_macro_text_in_macroDefine364);
							macro_text19=macro_text();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_macro_text.add(macro_text19.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: IDENTIFIER, macro_text
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 79:57: -> ^( MAC_FUNCTION_OBJECT IDENTIFIER ( macro_text )? )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:60: ^( MAC_FUNCTION_OBJECT IDENTIFIER ( macro_text )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MAC_FUNCTION_OBJECT, "MAC_FUNCTION_OBJECT"), root_1);
						adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:94: ( macro_text )?
						if ( stream_macro_text.hasNext() ) {
							adaptor.addChild(root_1, stream_macro_text.nextTree());
						}
						stream_macro_text.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:5: DEFINE mac= IDENTIFIER LPAREN ( WS )? (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )? RPAREN ( macro_text )?
					{
					DEFINE20=(Token)match(input,DEFINE,FOLLOW_DEFINE_in_macroDefine384); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DEFINE.add(DEFINE20);

					mac=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroDefine388); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(mac);

					LPAREN21=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_macroDefine391); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN21);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:35: ( WS )?
					int alt6=2;
					int LA6_0 = input.LA(1);
					if ( (LA6_0==WS) ) {
						alt6=1;
					}
					switch (alt6) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:35: WS
							{
							WS22=(Token)match(input,WS,FOLLOW_WS_in_macroDefine393); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS22);

							}
							break;

					}

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:40: (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )?
					int alt11=2;
					int LA11_0 = input.LA(1);
					if ( (LA11_0==ELLIPSIS||LA11_0==IDENTIFIER) ) {
						alt11=1;
					}
					switch (alt11) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:41: arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )*
							{
							pushFollow(FOLLOW_macroParam_in_macroDefine400);
							arg=macroParam();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_macroParam.add(arg.getTree());
							if (list_arg==null) list_arg=new ArrayList<Object>();
							list_arg.add(arg.getTree());
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:57: ( WS )?
							int alt7=2;
							int LA7_0 = input.LA(1);
							if ( (LA7_0==WS) ) {
								alt7=1;
							}
							switch (alt7) {
								case 1 :
									// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:57: WS
									{
									WS23=(Token)match(input,WS,FOLLOW_WS_in_macroDefine402); if (state.failed) return retval; 
									if ( state.backtracking==0 ) stream_WS.add(WS23);

									}
									break;

							}

							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:61: ( COMMA ( WS )* arg+= macroParam ( WS )* )*
							loop10:
							while (true) {
								int alt10=2;
								int LA10_0 = input.LA(1);
								if ( (LA10_0==COMMA) ) {
									alt10=1;
								}

								switch (alt10) {
								case 1 :
									// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:62: COMMA ( WS )* arg+= macroParam ( WS )*
									{
									COMMA24=(Token)match(input,COMMA,FOLLOW_COMMA_in_macroDefine406); if (state.failed) return retval; 
									if ( state.backtracking==0 ) stream_COMMA.add(COMMA24);

									// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:68: ( WS )*
									loop8:
									while (true) {
										int alt8=2;
										int LA8_0 = input.LA(1);
										if ( (LA8_0==WS) ) {
											alt8=1;
										}

										switch (alt8) {
										case 1 :
											// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:68: WS
											{
											WS25=(Token)match(input,WS,FOLLOW_WS_in_macroDefine408); if (state.failed) return retval; 
											if ( state.backtracking==0 ) stream_WS.add(WS25);

											}
											break;

										default :
											break loop8;
										}
									}

									pushFollow(FOLLOW_macroParam_in_macroDefine413);
									arg=macroParam();
									state._fsp--;
									if (state.failed) return retval;
									if ( state.backtracking==0 ) stream_macroParam.add(arg.getTree());
									if (list_arg==null) list_arg=new ArrayList<Object>();
									list_arg.add(arg.getTree());
									// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:88: ( WS )*
									loop9:
									while (true) {
										int alt9=2;
										int LA9_0 = input.LA(1);
										if ( (LA9_0==WS) ) {
											alt9=1;
										}

										switch (alt9) {
										case 1 :
											// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:88: WS
											{
											WS26=(Token)match(input,WS,FOLLOW_WS_in_macroDefine415); if (state.failed) return retval; 
											if ( state.backtracking==0 ) stream_WS.add(WS26);

											}
											break;

										default :
											break loop9;
										}
									}

									}
									break;

								default :
									break loop10;
								}
							}

							}
							break;

					}

					RPAREN27=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_macroDefine426); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN27);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:81:11: ( macro_text )?
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( ((LA12_0 >= CHARACTER_LITERAL && LA12_0 <= CKEYWORD)||LA12_0==COMMA||(LA12_0 >= COPERATOR && LA12_0 <= DECIMAL_LITERAL)||LA12_0==FLOATING_POINT_LITERAL||LA12_0==HEX_LITERAL||LA12_0==IDENTIFIER||LA12_0==LPAREN||LA12_0==OCTAL_LITERAL||LA12_0==RPAREN||(LA12_0 >= SEMICOLON && LA12_0 <= SHARPSHARP)||LA12_0==SIZEOF||(LA12_0 >= STRINGIFICATION && LA12_0 <= TEXT_END)||LA12_0==WS) ) {
						alt12=1;
					}
					switch (alt12) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:81:11: macro_text
							{
							pushFollow(FOLLOW_macro_text_in_macroDefine428);
							macro_text28=macro_text();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_macro_text.add(macro_text28.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: macro_text, mac, arg
					// token labels: mac
					// rule labels: retval
					// token list labels: 
					// rule list labels: arg
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_mac=new RewriteRuleTokenStream(adaptor,"token mac",mac);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_arg=new RewriteRuleSubtreeStream(adaptor,"token arg",list_arg);
					root_0 = (Object)adaptor.nil();
					// 81:33: -> ^( MAC_FUNCTION $mac ( $arg)+ ( macro_text )? )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:81:36: ^( MAC_FUNCTION $mac ( $arg)+ ( macro_text )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MAC_FUNCTION, "MAC_FUNCTION"), root_1);
						adaptor.addChild(root_1, stream_mac.nextNode());
						if ( !(stream_arg.hasNext()) ) {
							throw new RewriteEarlyExitException();
						}
						while ( stream_arg.hasNext() ) {
							adaptor.addChild(root_1, stream_arg.nextTree());
						}
						stream_arg.reset();

						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:81:63: ( macro_text )?
						if ( stream_macro_text.hasNext() ) {
							adaptor.addChild(root_1, stream_macro_text.nextTree());
						}
						stream_macro_text.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:82:6: DEFINE IDENTIFIER ( macro_text )?
					{
					DEFINE29=(Token)match(input,DEFINE,FOLLOW_DEFINE_in_macroDefine464); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DEFINE.add(DEFINE29);

					IDENTIFIER30=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroDefine466); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER30);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:82:24: ( macro_text )?
					int alt13=2;
					int LA13_0 = input.LA(1);
					if ( ((LA13_0 >= CHARACTER_LITERAL && LA13_0 <= CKEYWORD)||LA13_0==COMMA||(LA13_0 >= COPERATOR && LA13_0 <= DECIMAL_LITERAL)||LA13_0==FLOATING_POINT_LITERAL||LA13_0==HEX_LITERAL||LA13_0==IDENTIFIER||LA13_0==LPAREN||LA13_0==OCTAL_LITERAL||LA13_0==RPAREN||(LA13_0 >= SEMICOLON && LA13_0 <= SHARPSHARP)||LA13_0==SIZEOF||(LA13_0 >= STRINGIFICATION && LA13_0 <= TEXT_END)||LA13_0==WS) ) {
						alt13=1;
					}
					switch (alt13) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:82:24: macro_text
							{
							pushFollow(FOLLOW_macro_text_in_macroDefine468);
							macro_text31=macro_text();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_macro_text.add(macro_text31.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: IDENTIFIER, macro_text
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 82:43: -> ^( MAC_OBJECT IDENTIFIER ( macro_text )? )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:82:46: ^( MAC_OBJECT IDENTIFIER ( macro_text )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MAC_OBJECT, "MAC_OBJECT"), root_1);
						adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:82:72: ( macro_text )?
						if ( stream_macro_text.hasNext() ) {
							adaptor.addChild(root_1, stream_macro_text.nextTree());
						}
						stream_macro_text.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 4, macroDefine_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "macroDefine"


	public static class macroParam_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "macroParam"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:85:1: macroParam : ( IDENTIFIER ELLIPSIS -> ^( ELLIPSIS IDENTIFIER ) | ELLIPSIS | IDENTIFIER );
	public final CppParser.macroParam_return macroParam() throws RecognitionException {
		CppParser.macroParam_return retval = new CppParser.macroParam_return();
		retval.start = input.LT(1);
		int macroParam_StartIndex = input.index();

		Object root_0 = null;

		Token IDENTIFIER32=null;
		Token ELLIPSIS33=null;
		Token ELLIPSIS34=null;
		Token IDENTIFIER35=null;

		Object IDENTIFIER32_tree=null;
		Object ELLIPSIS33_tree=null;
		Object ELLIPSIS34_tree=null;
		Object IDENTIFIER35_tree=null;
		RewriteRuleTokenStream stream_ELLIPSIS=new RewriteRuleTokenStream(adaptor,"token ELLIPSIS");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:86:3: ( IDENTIFIER ELLIPSIS -> ^( ELLIPSIS IDENTIFIER ) | ELLIPSIS | IDENTIFIER )
			int alt15=3;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==IDENTIFIER) ) {
				int LA15_1 = input.LA(2);
				if ( (LA15_1==ELLIPSIS) ) {
					alt15=1;
				}
				else if ( (LA15_1==EOF||LA15_1==COMMA||LA15_1==RPAREN||LA15_1==WS) ) {
					alt15=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 15, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA15_0==ELLIPSIS) ) {
				alt15=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 15, 0, input);
				throw nvae;
			}

			switch (alt15) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:86:5: IDENTIFIER ELLIPSIS
					{
					IDENTIFIER32=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroParam504); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER32);

					ELLIPSIS33=(Token)match(input,ELLIPSIS,FOLLOW_ELLIPSIS_in_macroParam506); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ELLIPSIS.add(ELLIPSIS33);

					// AST REWRITE
					// elements: ELLIPSIS, IDENTIFIER
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 86:25: -> ^( ELLIPSIS IDENTIFIER )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:86:27: ^( ELLIPSIS IDENTIFIER )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_ELLIPSIS.nextNode(), root_1);
						adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:87:5: ELLIPSIS
					{
					root_0 = (Object)adaptor.nil();


					ELLIPSIS34=(Token)match(input,ELLIPSIS,FOLLOW_ELLIPSIS_in_macroParam520); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ELLIPSIS34_tree = (Object)adaptor.create(ELLIPSIS34);
					adaptor.addChild(root_0, ELLIPSIS34_tree);
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:88:5: IDENTIFIER
					{
					root_0 = (Object)adaptor.nil();


					IDENTIFIER35=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroParam526); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER35_tree = (Object)adaptor.create(IDENTIFIER35);
					adaptor.addChild(root_0, IDENTIFIER35_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 5, macroParam_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "macroParam"


	public static class macroExecution_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "macroExecution"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:91:1: macroExecution : EXEC_MACRO ifexpression -> ^( EXEC_MACRO ifexpression ) ;
	public final CppParser.macroExecution_return macroExecution() throws RecognitionException {
		CppParser.macroExecution_return retval = new CppParser.macroExecution_return();
		retval.start = input.LT(1);
		int macroExecution_StartIndex = input.index();

		Object root_0 = null;

		Token EXEC_MACRO36=null;
		ParserRuleReturnScope ifexpression37 =null;

		Object EXEC_MACRO36_tree=null;
		RewriteRuleTokenStream stream_EXEC_MACRO=new RewriteRuleTokenStream(adaptor,"token EXEC_MACRO");
		RewriteRuleSubtreeStream stream_ifexpression=new RewriteRuleSubtreeStream(adaptor,"rule ifexpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:92:3: ( EXEC_MACRO ifexpression -> ^( EXEC_MACRO ifexpression ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:92:5: EXEC_MACRO ifexpression
			{
			EXEC_MACRO36=(Token)match(input,EXEC_MACRO,FOLLOW_EXEC_MACRO_in_macroExecution539); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_EXEC_MACRO.add(EXEC_MACRO36);

			pushFollow(FOLLOW_ifexpression_in_macroExecution541);
			ifexpression37=ifexpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_ifexpression.add(ifexpression37.getTree());
			// AST REWRITE
			// elements: ifexpression, EXEC_MACRO
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 92:29: -> ^( EXEC_MACRO ifexpression )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:92:32: ^( EXEC_MACRO ifexpression )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_EXEC_MACRO.nextNode(), root_1);
				adaptor.addChild(root_1, stream_ifexpression.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 6, macroExecution_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "macroExecution"


	public static class macroUndef_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "macroUndef"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:95:1: macroUndef : UNDEF mac= IDENTIFIER -> ^( UNDEF IDENTIFIER ) ;
	public final CppParser.macroUndef_return macroUndef() throws RecognitionException {
		CppParser.macroUndef_return retval = new CppParser.macroUndef_return();
		retval.start = input.LT(1);
		int macroUndef_StartIndex = input.index();

		Object root_0 = null;

		Token mac=null;
		Token UNDEF38=null;

		Object mac_tree=null;
		Object UNDEF38_tree=null;
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_UNDEF=new RewriteRuleTokenStream(adaptor,"token UNDEF");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:96:3: ( UNDEF mac= IDENTIFIER -> ^( UNDEF IDENTIFIER ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:96:5: UNDEF mac= IDENTIFIER
			{
			UNDEF38=(Token)match(input,UNDEF,FOLLOW_UNDEF_in_macroUndef564); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_UNDEF.add(UNDEF38);

			mac=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroUndef568); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(mac);

			// AST REWRITE
			// elements: IDENTIFIER, UNDEF
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 96:27: -> ^( UNDEF IDENTIFIER )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:96:30: ^( UNDEF IDENTIFIER )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_UNDEF.nextNode(), root_1);
				adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 7, macroUndef_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "macroUndef"


	public static class conditionalCompilation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "conditionalCompilation"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:99:1: conditionalCompilation : IF ifexp+= ifexpression ifstmt+= statement ( ELIF ifexp+= ifexpression ifstmt+= statement )* ( ELSE elsestmt= statement )? ENDIF -> ^( IF ( $ifexp $ifstmt)+ ( ELSE $elsestmt)? ) ;
	public final CppParser.conditionalCompilation_return conditionalCompilation() throws RecognitionException {
		CppParser.conditionalCompilation_return retval = new CppParser.conditionalCompilation_return();
		retval.start = input.LT(1);
		int conditionalCompilation_StartIndex = input.index();

		Object root_0 = null;

		Token IF39=null;
		Token ELIF40=null;
		Token ELSE41=null;
		Token ENDIF42=null;
		List<Object> list_ifexp=null;
		List<Object> list_ifstmt=null;
		ParserRuleReturnScope elsestmt =null;
		RuleReturnScope ifexp = null;
		RuleReturnScope ifstmt = null;
		Object IF39_tree=null;
		Object ELIF40_tree=null;
		Object ELSE41_tree=null;
		Object ENDIF42_tree=null;
		RewriteRuleTokenStream stream_ELIF=new RewriteRuleTokenStream(adaptor,"token ELIF");
		RewriteRuleTokenStream stream_ENDIF=new RewriteRuleTokenStream(adaptor,"token ENDIF");
		RewriteRuleTokenStream stream_ELSE=new RewriteRuleTokenStream(adaptor,"token ELSE");
		RewriteRuleTokenStream stream_IF=new RewriteRuleTokenStream(adaptor,"token IF");
		RewriteRuleSubtreeStream stream_ifexpression=new RewriteRuleSubtreeStream(adaptor,"rule ifexpression");
		RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:100:3: ( IF ifexp+= ifexpression ifstmt+= statement ( ELIF ifexp+= ifexpression ifstmt+= statement )* ( ELSE elsestmt= statement )? ENDIF -> ^( IF ( $ifexp $ifstmt)+ ( ELSE $elsestmt)? ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:100:5: IF ifexp+= ifexpression ifstmt+= statement ( ELIF ifexp+= ifexpression ifstmt+= statement )* ( ELSE elsestmt= statement )? ENDIF
			{
			IF39=(Token)match(input,IF,FOLLOW_IF_in_conditionalCompilation590); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IF.add(IF39);

			pushFollow(FOLLOW_ifexpression_in_conditionalCompilation595);
			ifexp=ifexpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_ifexpression.add(ifexp.getTree());
			if (list_ifexp==null) list_ifexp=new ArrayList<Object>();
			list_ifexp.add(ifexp.getTree());
			pushFollow(FOLLOW_statement_in_conditionalCompilation600);
			ifstmt=statement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_statement.add(ifstmt.getTree());
			if (list_ifstmt==null) list_ifstmt=new ArrayList<Object>();
			list_ifstmt.add(ifstmt.getTree());
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:101:4: ( ELIF ifexp+= ifexpression ifstmt+= statement )*
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( (LA16_0==ELIF) ) {
					alt16=1;
				}

				switch (alt16) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:101:5: ELIF ifexp+= ifexpression ifstmt+= statement
					{
					ELIF40=(Token)match(input,ELIF,FOLLOW_ELIF_in_conditionalCompilation608); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ELIF.add(ELIF40);

					pushFollow(FOLLOW_ifexpression_in_conditionalCompilation612);
					ifexp=ifexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_ifexpression.add(ifexp.getTree());
					if (list_ifexp==null) list_ifexp=new ArrayList<Object>();
					list_ifexp.add(ifexp.getTree());
					pushFollow(FOLLOW_statement_in_conditionalCompilation617);
					ifstmt=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(ifstmt.getTree());
					if (list_ifstmt==null) list_ifstmt=new ArrayList<Object>();
					list_ifstmt.add(ifstmt.getTree());
					}
					break;

				default :
					break loop16;
				}
			}

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:101:53: ( ELSE elsestmt= statement )?
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==ELSE) ) {
				alt17=1;
			}
			switch (alt17) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:101:54: ELSE elsestmt= statement
					{
					ELSE41=(Token)match(input,ELSE,FOLLOW_ELSE_in_conditionalCompilation624); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ELSE.add(ELSE41);

					pushFollow(FOLLOW_statement_in_conditionalCompilation629);
					elsestmt=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(elsestmt.getTree());
					}
					break;

			}

			ENDIF42=(Token)match(input,ENDIF,FOLLOW_ENDIF_in_conditionalCompilation633); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_ENDIF.add(ENDIF42);

			// AST REWRITE
			// elements: ELSE, ifexp, elsestmt, IF, ifstmt
			// token labels: 
			// rule labels: elsestmt, retval
			// token list labels: 
			// rule list labels: ifstmt, ifexp
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_elsestmt=new RewriteRuleSubtreeStream(adaptor,"rule elsestmt",elsestmt!=null?elsestmt.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_ifstmt=new RewriteRuleSubtreeStream(adaptor,"token ifstmt",list_ifstmt);
			RewriteRuleSubtreeStream stream_ifexp=new RewriteRuleSubtreeStream(adaptor,"token ifexp",list_ifexp);
			root_0 = (Object)adaptor.nil();
			// 102:4: -> ^( IF ( $ifexp $ifstmt)+ ( ELSE $elsestmt)? )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:102:8: ^( IF ( $ifexp $ifstmt)+ ( ELSE $elsestmt)? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_IF.nextNode(), root_1);
				if ( !(stream_ifexp.hasNext()||stream_ifstmt.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_ifexp.hasNext()||stream_ifstmt.hasNext() ) {
					adaptor.addChild(root_1, stream_ifexp.nextTree());
					adaptor.addChild(root_1, stream_ifstmt.nextTree());
				}
				stream_ifexp.reset();
				stream_ifstmt.reset();

				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:102:31: ( ELSE $elsestmt)?
				if ( stream_ELSE.hasNext()||stream_elsestmt.hasNext() ) {
					adaptor.addChild(root_1, stream_ELSE.nextNode());
					adaptor.addChild(root_1, stream_elsestmt.nextTree());
				}
				stream_ELSE.reset();
				stream_elsestmt.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 8, conditionalCompilation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "conditionalCompilation"


	public static class lineControl_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "lineControl"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:105:1: lineControl : LINE n= DECIMAL_LITERAL (theFile= STRING_LITERAL )? -> ^( LINE $n ( $theFile)? ) ;
	public final CppParser.lineControl_return lineControl() throws RecognitionException {
		CppParser.lineControl_return retval = new CppParser.lineControl_return();
		retval.start = input.LT(1);
		int lineControl_StartIndex = input.index();

		Object root_0 = null;

		Token n=null;
		Token theFile=null;
		Token LINE43=null;

		Object n_tree=null;
		Object theFile_tree=null;
		Object LINE43_tree=null;
		RewriteRuleTokenStream stream_LINE=new RewriteRuleTokenStream(adaptor,"token LINE");
		RewriteRuleTokenStream stream_STRING_LITERAL=new RewriteRuleTokenStream(adaptor,"token STRING_LITERAL");
		RewriteRuleTokenStream stream_DECIMAL_LITERAL=new RewriteRuleTokenStream(adaptor,"token DECIMAL_LITERAL");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:106:3: ( LINE n= DECIMAL_LITERAL (theFile= STRING_LITERAL )? -> ^( LINE $n ( $theFile)? ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:106:5: LINE n= DECIMAL_LITERAL (theFile= STRING_LITERAL )?
			{
			LINE43=(Token)match(input,LINE,FOLLOW_LINE_in_lineControl676); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LINE.add(LINE43);

			n=(Token)match(input,DECIMAL_LITERAL,FOLLOW_DECIMAL_LITERAL_in_lineControl680); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_DECIMAL_LITERAL.add(n);

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:106:28: (theFile= STRING_LITERAL )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0==STRING_LITERAL) ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:106:29: theFile= STRING_LITERAL
					{
					theFile=(Token)match(input,STRING_LITERAL,FOLLOW_STRING_LITERAL_in_lineControl685); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STRING_LITERAL.add(theFile);

					}
					break;

			}

			// AST REWRITE
			// elements: LINE, theFile, n
			// token labels: theFile, n
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_theFile=new RewriteRuleTokenStream(adaptor,"token theFile",theFile);
			RewriteRuleTokenStream stream_n=new RewriteRuleTokenStream(adaptor,"token n",n);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 106:55: -> ^( LINE $n ( $theFile)? )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:106:57: ^( LINE $n ( $theFile)? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_LINE.nextNode(), root_1);
				adaptor.addChild(root_1, stream_n.nextNode());
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:106:67: ( $theFile)?
				if ( stream_theFile.hasNext() ) {
					adaptor.addChild(root_1, stream_theFile.nextNode());
				}
				stream_theFile.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 9, lineControl_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "lineControl"


	public static class diagnostics_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "diagnostics"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:109:1: diagnostics : ( WARNING -> ^( WARNING ) | ERROR -> ^( ERROR ) | PRAGMA -> ^( PRAGMA ) );
	public final CppParser.diagnostics_return diagnostics() throws RecognitionException {
		CppParser.diagnostics_return retval = new CppParser.diagnostics_return();
		retval.start = input.LT(1);
		int diagnostics_StartIndex = input.index();

		Object root_0 = null;

		Token WARNING44=null;
		Token ERROR45=null;
		Token PRAGMA46=null;

		Object WARNING44_tree=null;
		Object ERROR45_tree=null;
		Object PRAGMA46_tree=null;
		RewriteRuleTokenStream stream_PRAGMA=new RewriteRuleTokenStream(adaptor,"token PRAGMA");
		RewriteRuleTokenStream stream_ERROR=new RewriteRuleTokenStream(adaptor,"token ERROR");
		RewriteRuleTokenStream stream_WARNING=new RewriteRuleTokenStream(adaptor,"token WARNING");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:110:3: ( WARNING -> ^( WARNING ) | ERROR -> ^( ERROR ) | PRAGMA -> ^( PRAGMA ) )
			int alt19=3;
			switch ( input.LA(1) ) {
			case WARNING:
				{
				alt19=1;
				}
				break;
			case ERROR:
				{
				alt19=2;
				}
				break;
			case PRAGMA:
				{
				alt19=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}
			switch (alt19) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:110:5: WARNING
					{
					WARNING44=(Token)match(input,WARNING,FOLLOW_WARNING_in_diagnostics718); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_WARNING.add(WARNING44);

					// AST REWRITE
					// elements: WARNING
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 110:13: -> ^( WARNING )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:110:16: ^( WARNING )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_WARNING.nextNode(), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:111:5: ERROR
					{
					ERROR45=(Token)match(input,ERROR,FOLLOW_ERROR_in_diagnostics730); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ERROR.add(ERROR45);

					// AST REWRITE
					// elements: ERROR
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 111:11: -> ^( ERROR )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:111:14: ^( ERROR )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_ERROR.nextNode(), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:112:5: PRAGMA
					{
					PRAGMA46=(Token)match(input,PRAGMA,FOLLOW_PRAGMA_in_diagnostics742); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PRAGMA.add(PRAGMA46);

					// AST REWRITE
					// elements: PRAGMA
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 112:12: -> ^( PRAGMA )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:112:15: ^( PRAGMA )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_PRAGMA.nextNode(), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 10, diagnostics_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "diagnostics"


	public static class text_line_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "text_line"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:115:1: text_line : ( source_text )+ -> ^( TEXT_LINE ( source_text )+ ) ;
	public final CppParser.text_line_return text_line() throws RecognitionException {
		CppParser.text_line_return retval = new CppParser.text_line_return();
		retval.start = input.LT(1);
		int text_line_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope source_text47 =null;

		RewriteRuleSubtreeStream stream_source_text=new RewriteRuleSubtreeStream(adaptor,"rule source_text");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:116:3: ( ( source_text )+ -> ^( TEXT_LINE ( source_text )+ ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:116:5: ( source_text )+
			{
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:116:5: ( source_text )+
			int cnt20=0;
			loop20:
			while (true) {
				int alt20=2;
				int LA20_0 = input.LA(1);
				if ( ((LA20_0 >= CHARACTER_LITERAL && LA20_0 <= CKEYWORD)||LA20_0==COMMA||(LA20_0 >= COPERATOR && LA20_0 <= DECIMAL_LITERAL)||LA20_0==FLOATING_POINT_LITERAL||LA20_0==HEX_LITERAL||LA20_0==IDENTIFIER||LA20_0==LPAREN||LA20_0==OCTAL_LITERAL||LA20_0==RPAREN||(LA20_0 >= SEMICOLON && LA20_0 <= SHARPSHARP)||LA20_0==SIZEOF||(LA20_0 >= STRINGIFICATION && LA20_0 <= TEXT_END)||LA20_0==WS) ) {
					alt20=1;
				}

				switch (alt20) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:116:5: source_text
					{
					pushFollow(FOLLOW_source_text_in_text_line763);
					source_text47=source_text();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_source_text.add(source_text47.getTree());
					}
					break;

				default :
					if ( cnt20 >= 1 ) break loop20;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(20, input);
					throw eee;
				}
				cnt20++;
			}

			// AST REWRITE
			// elements: source_text
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 116:22: -> ^( TEXT_LINE ( source_text )+ )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:116:25: ^( TEXT_LINE ( source_text )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT_LINE, "TEXT_LINE"), root_1);
				if ( !(stream_source_text.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_source_text.hasNext() ) {
					adaptor.addChild(root_1, stream_source_text.nextTree());
				}
				stream_source_text.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 11, text_line_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "text_line"


	public static class statement_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "statement"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:120:1: statement : ( procLine )+ ;
	public final CppParser.statement_return statement() throws RecognitionException {
		CppParser.statement_return retval = new CppParser.statement_return();
		retval.start = input.LT(1);
		int statement_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope procLine48 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:121:3: ( ( procLine )+ )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:121:5: ( procLine )+
			{
			root_0 = (Object)adaptor.nil();


			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:121:5: ( procLine )+
			int cnt21=0;
			loop21:
			while (true) {
				int alt21=2;
				int LA21_0 = input.LA(1);
				if ( ((LA21_0 >= CHARACTER_LITERAL && LA21_0 <= CKEYWORD)||LA21_0==COMMA||(LA21_0 >= COPERATOR && LA21_0 <= DEFINE)||LA21_0==DIRECTIVE||LA21_0==ERROR||LA21_0==EXEC_MACRO||LA21_0==End||LA21_0==FLOATING_POINT_LITERAL||LA21_0==HEX_LITERAL||(LA21_0 >= IDENTIFIER && LA21_0 <= IF)||(LA21_0 >= INCLUDE && LA21_0 <= INCLUDE_EXPAND)||LA21_0==LINE||LA21_0==LPAREN||LA21_0==OCTAL_LITERAL||LA21_0==PRAGMA||LA21_0==RPAREN||(LA21_0 >= SEMICOLON && LA21_0 <= SHARPSHARP)||LA21_0==SIZEOF||(LA21_0 >= STRINGIFICATION && LA21_0 <= TEXT_END)||LA21_0==UNDEF||(LA21_0 >= WARNING && LA21_0 <= WS)) ) {
					alt21=1;
				}

				switch (alt21) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:121:5: procLine
					{
					pushFollow(FOLLOW_procLine_in_statement794);
					procLine48=procLine();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, procLine48.getTree());

					}
					break;

				default :
					if ( cnt21 >= 1 ) break loop21;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(21, input);
					throw eee;
				}
				cnt21++;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 12, statement_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "statement"


	public static class type_name_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "type_name"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:124:1: type_name : IDENTIFIER ;
	public final CppParser.type_name_return type_name() throws RecognitionException {
		CppParser.type_name_return retval = new CppParser.type_name_return();
		retval.start = input.LT(1);
		int type_name_StartIndex = input.index();

		Object root_0 = null;

		Token IDENTIFIER49=null;

		Object IDENTIFIER49_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:125:3: ( IDENTIFIER )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:125:5: IDENTIFIER
			{
			root_0 = (Object)adaptor.nil();


			IDENTIFIER49=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_type_name810); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER49_tree = (Object)adaptor.create(IDENTIFIER49);
			adaptor.addChild(root_0, IDENTIFIER49_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 13, type_name_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "type_name"


	public static class ifexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "ifexpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:128:1: ifexpression : ( IDENTIFIER {...}? -> ^( EXPR_NDEF IDENTIFIER ) | IDENTIFIER {...}? -> ^( EXPR_DEF IDENTIFIER ) | assignmentExpression -> ^( EXPR assignmentExpression ) );
	public final CppParser.ifexpression_return ifexpression() throws RecognitionException {
		CppParser.ifexpression_return retval = new CppParser.ifexpression_return();
		retval.start = input.LT(1);
		int ifexpression_StartIndex = input.index();

		Object root_0 = null;

		Token IDENTIFIER50=null;
		Token IDENTIFIER51=null;
		ParserRuleReturnScope assignmentExpression52 =null;

		Object IDENTIFIER50_tree=null;
		Object IDENTIFIER51_tree=null;
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_assignmentExpression=new RewriteRuleSubtreeStream(adaptor,"rule assignmentExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:129:3: ( IDENTIFIER {...}? -> ^( EXPR_NDEF IDENTIFIER ) | IDENTIFIER {...}? -> ^( EXPR_DEF IDENTIFIER ) | assignmentExpression -> ^( EXPR assignmentExpression ) )
			int alt22=3;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==IDENTIFIER) ) {
				int LA22_1 = input.LA(2);
				if ( (synpred33_Cpp()) ) {
					alt22=1;
				}
				else if ( (synpred34_Cpp()) ) {
					alt22=2;
				}
				else if ( (true) ) {
					alt22=3;
				}

			}
			else if ( (LA22_0==AMPERSAND||LA22_0==CHARACTER_LITERAL||LA22_0==DECIMAL_LITERAL||LA22_0==DEFINED||LA22_0==FLOATING_POINT_LITERAL||LA22_0==HEX_LITERAL||LA22_0==LPAREN||LA22_0==MINUS||LA22_0==MINUSMINUS||LA22_0==NOT||LA22_0==OCTAL_LITERAL||LA22_0==PLUS||LA22_0==PLUSPLUS||LA22_0==SIZEOF||LA22_0==STAR||LA22_0==STRING_LITERAL||LA22_0==TILDE) ) {
				alt22=3;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 22, 0, input);
				throw nvae;
			}

			switch (alt22) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:129:5: IDENTIFIER {...}?
					{
					IDENTIFIER50=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_ifexpression823); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER50);

					if ( !((input.LT(-2).getText().equals("ifndef"))) ) {
						if (state.backtracking>0) {state.failed=true; return retval;}
						throw new FailedPredicateException(input, "ifexpression", "input.LT(-2).getText().equals(\"ifndef\")");
					}
					// AST REWRITE
					// elements: IDENTIFIER
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 129:62: -> ^( EXPR_NDEF IDENTIFIER )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:129:65: ^( EXPR_NDEF IDENTIFIER )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPR_NDEF, "EXPR_NDEF"), root_1);
						adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:130:5: IDENTIFIER {...}?
					{
					IDENTIFIER51=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_ifexpression843); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER51);

					if ( !((input.LT(-2).getText().equals("ifdef"))) ) {
						if (state.backtracking>0) {state.failed=true; return retval;}
						throw new FailedPredicateException(input, "ifexpression", "input.LT(-2).getText().equals(\"ifdef\")");
					}
					// AST REWRITE
					// elements: IDENTIFIER
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 130:61: -> ^( EXPR_DEF IDENTIFIER )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:130:64: ^( EXPR_DEF IDENTIFIER )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPR_DEF, "EXPR_DEF"), root_1);
						adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:131:5: assignmentExpression
					{
					pushFollow(FOLLOW_assignmentExpression_in_ifexpression863);
					assignmentExpression52=assignmentExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_assignmentExpression.add(assignmentExpression52.getTree());
					// AST REWRITE
					// elements: assignmentExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 131:26: -> ^( EXPR assignmentExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:131:29: ^( EXPR assignmentExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPR, "EXPR"), root_1);
						adaptor.addChild(root_1, stream_assignmentExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 14, ifexpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "ifexpression"


	public static class assignmentExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "assignmentExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:134:1: assignmentExpression : conditionalExpression ( ( ASSIGNEQUAL ^| TIMESEQUAL ^| DIVIDEEQUAL ^| MODEQUAL ^| PLUSEQUAL ^| MINUSEQUAL ^| SHIFTLEFTEQUAL ^| SHIFTRIGHTEQUAL ^| BITWISEANDEQUAL ^| BITWISEXOREQUAL ^| BITWISEOREQUAL ^) assignmentExpression )? ;
	public final CppParser.assignmentExpression_return assignmentExpression() throws RecognitionException {
		CppParser.assignmentExpression_return retval = new CppParser.assignmentExpression_return();
		retval.start = input.LT(1);
		int assignmentExpression_StartIndex = input.index();

		Object root_0 = null;

		Token ASSIGNEQUAL54=null;
		Token TIMESEQUAL55=null;
		Token DIVIDEEQUAL56=null;
		Token MODEQUAL57=null;
		Token PLUSEQUAL58=null;
		Token MINUSEQUAL59=null;
		Token SHIFTLEFTEQUAL60=null;
		Token SHIFTRIGHTEQUAL61=null;
		Token BITWISEANDEQUAL62=null;
		Token BITWISEXOREQUAL63=null;
		Token BITWISEOREQUAL64=null;
		ParserRuleReturnScope conditionalExpression53 =null;
		ParserRuleReturnScope assignmentExpression65 =null;

		Object ASSIGNEQUAL54_tree=null;
		Object TIMESEQUAL55_tree=null;
		Object DIVIDEEQUAL56_tree=null;
		Object MODEQUAL57_tree=null;
		Object PLUSEQUAL58_tree=null;
		Object MINUSEQUAL59_tree=null;
		Object SHIFTLEFTEQUAL60_tree=null;
		Object SHIFTRIGHTEQUAL61_tree=null;
		Object BITWISEANDEQUAL62_tree=null;
		Object BITWISEXOREQUAL63_tree=null;
		Object BITWISEOREQUAL64_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:135:3: ( conditionalExpression ( ( ASSIGNEQUAL ^| TIMESEQUAL ^| DIVIDEEQUAL ^| MODEQUAL ^| PLUSEQUAL ^| MINUSEQUAL ^| SHIFTLEFTEQUAL ^| SHIFTRIGHTEQUAL ^| BITWISEANDEQUAL ^| BITWISEXOREQUAL ^| BITWISEOREQUAL ^) assignmentExpression )? )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:135:5: conditionalExpression ( ( ASSIGNEQUAL ^| TIMESEQUAL ^| DIVIDEEQUAL ^| MODEQUAL ^| PLUSEQUAL ^| MINUSEQUAL ^| SHIFTLEFTEQUAL ^| SHIFTRIGHTEQUAL ^| BITWISEANDEQUAL ^| BITWISEXOREQUAL ^| BITWISEOREQUAL ^) assignmentExpression )?
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_conditionalExpression_in_assignmentExpression884);
			conditionalExpression53=conditionalExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalExpression53.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:136:4: ( ( ASSIGNEQUAL ^| TIMESEQUAL ^| DIVIDEEQUAL ^| MODEQUAL ^| PLUSEQUAL ^| MINUSEQUAL ^| SHIFTLEFTEQUAL ^| SHIFTRIGHTEQUAL ^| BITWISEANDEQUAL ^| BITWISEXOREQUAL ^| BITWISEOREQUAL ^) assignmentExpression )?
			int alt24=2;
			int LA24_0 = input.LA(1);
			if ( ((LA24_0 >= ASSIGNEQUAL && LA24_0 <= BITWISEANDEQUAL)||LA24_0==BITWISEOREQUAL||LA24_0==BITWISEXOREQUAL||LA24_0==DIVIDEEQUAL||LA24_0==MINUSEQUAL||LA24_0==MODEQUAL||LA24_0==PLUSEQUAL||LA24_0==SHIFTLEFTEQUAL||LA24_0==SHIFTRIGHTEQUAL||LA24_0==TIMESEQUAL) ) {
				alt24=1;
			}
			switch (alt24) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:136:6: ( ASSIGNEQUAL ^| TIMESEQUAL ^| DIVIDEEQUAL ^| MODEQUAL ^| PLUSEQUAL ^| MINUSEQUAL ^| SHIFTLEFTEQUAL ^| SHIFTRIGHTEQUAL ^| BITWISEANDEQUAL ^| BITWISEXOREQUAL ^| BITWISEOREQUAL ^) assignmentExpression
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:136:6: ( ASSIGNEQUAL ^| TIMESEQUAL ^| DIVIDEEQUAL ^| MODEQUAL ^| PLUSEQUAL ^| MINUSEQUAL ^| SHIFTLEFTEQUAL ^| SHIFTRIGHTEQUAL ^| BITWISEANDEQUAL ^| BITWISEXOREQUAL ^| BITWISEOREQUAL ^)
					int alt23=11;
					switch ( input.LA(1) ) {
					case ASSIGNEQUAL:
						{
						alt23=1;
						}
						break;
					case TIMESEQUAL:
						{
						alt23=2;
						}
						break;
					case DIVIDEEQUAL:
						{
						alt23=3;
						}
						break;
					case MODEQUAL:
						{
						alt23=4;
						}
						break;
					case PLUSEQUAL:
						{
						alt23=5;
						}
						break;
					case MINUSEQUAL:
						{
						alt23=6;
						}
						break;
					case SHIFTLEFTEQUAL:
						{
						alt23=7;
						}
						break;
					case SHIFTRIGHTEQUAL:
						{
						alt23=8;
						}
						break;
					case BITWISEANDEQUAL:
						{
						alt23=9;
						}
						break;
					case BITWISEXOREQUAL:
						{
						alt23=10;
						}
						break;
					case BITWISEOREQUAL:
						{
						alt23=11;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 23, 0, input);
						throw nvae;
					}
					switch (alt23) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:136:8: ASSIGNEQUAL ^
							{
							ASSIGNEQUAL54=(Token)match(input,ASSIGNEQUAL,FOLLOW_ASSIGNEQUAL_in_assignmentExpression893); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							ASSIGNEQUAL54_tree = (Object)adaptor.create(ASSIGNEQUAL54);
							root_0 = (Object)adaptor.becomeRoot(ASSIGNEQUAL54_tree, root_0);
							}

							}
							break;
						case 2 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:137:9: TIMESEQUAL ^
							{
							TIMESEQUAL55=(Token)match(input,TIMESEQUAL,FOLLOW_TIMESEQUAL_in_assignmentExpression904); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							TIMESEQUAL55_tree = (Object)adaptor.create(TIMESEQUAL55);
							root_0 = (Object)adaptor.becomeRoot(TIMESEQUAL55_tree, root_0);
							}

							}
							break;
						case 3 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:138:9: DIVIDEEQUAL ^
							{
							DIVIDEEQUAL56=(Token)match(input,DIVIDEEQUAL,FOLLOW_DIVIDEEQUAL_in_assignmentExpression915); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							DIVIDEEQUAL56_tree = (Object)adaptor.create(DIVIDEEQUAL56);
							root_0 = (Object)adaptor.becomeRoot(DIVIDEEQUAL56_tree, root_0);
							}

							}
							break;
						case 4 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:139:9: MODEQUAL ^
							{
							MODEQUAL57=(Token)match(input,MODEQUAL,FOLLOW_MODEQUAL_in_assignmentExpression926); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							MODEQUAL57_tree = (Object)adaptor.create(MODEQUAL57);
							root_0 = (Object)adaptor.becomeRoot(MODEQUAL57_tree, root_0);
							}

							}
							break;
						case 5 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:140:9: PLUSEQUAL ^
							{
							PLUSEQUAL58=(Token)match(input,PLUSEQUAL,FOLLOW_PLUSEQUAL_in_assignmentExpression937); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							PLUSEQUAL58_tree = (Object)adaptor.create(PLUSEQUAL58);
							root_0 = (Object)adaptor.becomeRoot(PLUSEQUAL58_tree, root_0);
							}

							}
							break;
						case 6 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:141:9: MINUSEQUAL ^
							{
							MINUSEQUAL59=(Token)match(input,MINUSEQUAL,FOLLOW_MINUSEQUAL_in_assignmentExpression948); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							MINUSEQUAL59_tree = (Object)adaptor.create(MINUSEQUAL59);
							root_0 = (Object)adaptor.becomeRoot(MINUSEQUAL59_tree, root_0);
							}

							}
							break;
						case 7 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:142:9: SHIFTLEFTEQUAL ^
							{
							SHIFTLEFTEQUAL60=(Token)match(input,SHIFTLEFTEQUAL,FOLLOW_SHIFTLEFTEQUAL_in_assignmentExpression959); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							SHIFTLEFTEQUAL60_tree = (Object)adaptor.create(SHIFTLEFTEQUAL60);
							root_0 = (Object)adaptor.becomeRoot(SHIFTLEFTEQUAL60_tree, root_0);
							}

							}
							break;
						case 8 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:143:9: SHIFTRIGHTEQUAL ^
							{
							SHIFTRIGHTEQUAL61=(Token)match(input,SHIFTRIGHTEQUAL,FOLLOW_SHIFTRIGHTEQUAL_in_assignmentExpression970); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							SHIFTRIGHTEQUAL61_tree = (Object)adaptor.create(SHIFTRIGHTEQUAL61);
							root_0 = (Object)adaptor.becomeRoot(SHIFTRIGHTEQUAL61_tree, root_0);
							}

							}
							break;
						case 9 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:144:7: BITWISEANDEQUAL ^
							{
							BITWISEANDEQUAL62=(Token)match(input,BITWISEANDEQUAL,FOLLOW_BITWISEANDEQUAL_in_assignmentExpression979); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							BITWISEANDEQUAL62_tree = (Object)adaptor.create(BITWISEANDEQUAL62);
							root_0 = (Object)adaptor.becomeRoot(BITWISEANDEQUAL62_tree, root_0);
							}

							}
							break;
						case 10 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:145:7: BITWISEXOREQUAL ^
							{
							BITWISEXOREQUAL63=(Token)match(input,BITWISEXOREQUAL,FOLLOW_BITWISEXOREQUAL_in_assignmentExpression988); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							BITWISEXOREQUAL63_tree = (Object)adaptor.create(BITWISEXOREQUAL63);
							root_0 = (Object)adaptor.becomeRoot(BITWISEXOREQUAL63_tree, root_0);
							}

							}
							break;
						case 11 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:146:7: BITWISEOREQUAL ^
							{
							BITWISEOREQUAL64=(Token)match(input,BITWISEOREQUAL,FOLLOW_BITWISEOREQUAL_in_assignmentExpression997); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							BITWISEOREQUAL64_tree = (Object)adaptor.create(BITWISEOREQUAL64);
							root_0 = (Object)adaptor.becomeRoot(BITWISEOREQUAL64_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_assignmentExpression_in_assignmentExpression1009);
					assignmentExpression65=assignmentExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, assignmentExpression65.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 15, assignmentExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "assignmentExpression"


	public static class conditionalExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "conditionalExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:152:1: conditionalExpression : logicalOrExpression ( QUESTIONMARK ^ assignmentExpression COLON conditionalExpression )? ;
	public final CppParser.conditionalExpression_return conditionalExpression() throws RecognitionException {
		CppParser.conditionalExpression_return retval = new CppParser.conditionalExpression_return();
		retval.start = input.LT(1);
		int conditionalExpression_StartIndex = input.index();

		Object root_0 = null;

		Token QUESTIONMARK67=null;
		Token COLON69=null;
		ParserRuleReturnScope logicalOrExpression66 =null;
		ParserRuleReturnScope assignmentExpression68 =null;
		ParserRuleReturnScope conditionalExpression70 =null;

		Object QUESTIONMARK67_tree=null;
		Object COLON69_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:153:3: ( logicalOrExpression ( QUESTIONMARK ^ assignmentExpression COLON conditionalExpression )? )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:153:5: logicalOrExpression ( QUESTIONMARK ^ assignmentExpression COLON conditionalExpression )?
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_logicalOrExpression_in_conditionalExpression1028);
			logicalOrExpression66=logicalOrExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, logicalOrExpression66.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:154:4: ( QUESTIONMARK ^ assignmentExpression COLON conditionalExpression )?
			int alt25=2;
			int LA25_0 = input.LA(1);
			if ( (LA25_0==QUESTIONMARK) ) {
				alt25=1;
			}
			switch (alt25) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:154:6: QUESTIONMARK ^ assignmentExpression COLON conditionalExpression
					{
					QUESTIONMARK67=(Token)match(input,QUESTIONMARK,FOLLOW_QUESTIONMARK_in_conditionalExpression1036); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					QUESTIONMARK67_tree = (Object)adaptor.create(QUESTIONMARK67);
					root_0 = (Object)adaptor.becomeRoot(QUESTIONMARK67_tree, root_0);
					}

					pushFollow(FOLLOW_assignmentExpression_in_conditionalExpression1039);
					assignmentExpression68=assignmentExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, assignmentExpression68.getTree());

					COLON69=(Token)match(input,COLON,FOLLOW_COLON_in_conditionalExpression1041); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					COLON69_tree = (Object)adaptor.create(COLON69);
					adaptor.addChild(root_0, COLON69_tree);
					}

					pushFollow(FOLLOW_conditionalExpression_in_conditionalExpression1043);
					conditionalExpression70=conditionalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalExpression70.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 16, conditionalExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "conditionalExpression"


	public static class logicalOrExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "logicalOrExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:157:1: logicalOrExpression : logicalAndExpression ( OR ^ logicalAndExpression )* ;
	public final CppParser.logicalOrExpression_return logicalOrExpression() throws RecognitionException {
		CppParser.logicalOrExpression_return retval = new CppParser.logicalOrExpression_return();
		retval.start = input.LT(1);
		int logicalOrExpression_StartIndex = input.index();

		Object root_0 = null;

		Token OR72=null;
		ParserRuleReturnScope logicalAndExpression71 =null;
		ParserRuleReturnScope logicalAndExpression73 =null;

		Object OR72_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:158:3: ( logicalAndExpression ( OR ^ logicalAndExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:158:5: logicalAndExpression ( OR ^ logicalAndExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_logicalAndExpression_in_logicalOrExpression1059);
			logicalAndExpression71=logicalAndExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, logicalAndExpression71.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:158:26: ( OR ^ logicalAndExpression )*
			loop26:
			while (true) {
				int alt26=2;
				int LA26_0 = input.LA(1);
				if ( (LA26_0==OR) ) {
					alt26=1;
				}

				switch (alt26) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:158:27: OR ^ logicalAndExpression
					{
					OR72=(Token)match(input,OR,FOLLOW_OR_in_logicalOrExpression1062); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					OR72_tree = (Object)adaptor.create(OR72);
					root_0 = (Object)adaptor.becomeRoot(OR72_tree, root_0);
					}

					pushFollow(FOLLOW_logicalAndExpression_in_logicalOrExpression1065);
					logicalAndExpression73=logicalAndExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, logicalAndExpression73.getTree());

					}
					break;

				default :
					break loop26;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 17, logicalOrExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "logicalOrExpression"


	public static class logicalAndExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "logicalAndExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:161:1: logicalAndExpression : inclusiveOrExpression ( AND ^ inclusiveOrExpression )* ;
	public final CppParser.logicalAndExpression_return logicalAndExpression() throws RecognitionException {
		CppParser.logicalAndExpression_return retval = new CppParser.logicalAndExpression_return();
		retval.start = input.LT(1);
		int logicalAndExpression_StartIndex = input.index();

		Object root_0 = null;

		Token AND75=null;
		ParserRuleReturnScope inclusiveOrExpression74 =null;
		ParserRuleReturnScope inclusiveOrExpression76 =null;

		Object AND75_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:162:3: ( inclusiveOrExpression ( AND ^ inclusiveOrExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:162:5: inclusiveOrExpression ( AND ^ inclusiveOrExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_inclusiveOrExpression_in_logicalAndExpression1080);
			inclusiveOrExpression74=inclusiveOrExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, inclusiveOrExpression74.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:162:27: ( AND ^ inclusiveOrExpression )*
			loop27:
			while (true) {
				int alt27=2;
				int LA27_0 = input.LA(1);
				if ( (LA27_0==AND) ) {
					alt27=1;
				}

				switch (alt27) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:162:28: AND ^ inclusiveOrExpression
					{
					AND75=(Token)match(input,AND,FOLLOW_AND_in_logicalAndExpression1083); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					AND75_tree = (Object)adaptor.create(AND75);
					root_0 = (Object)adaptor.becomeRoot(AND75_tree, root_0);
					}

					pushFollow(FOLLOW_inclusiveOrExpression_in_logicalAndExpression1086);
					inclusiveOrExpression76=inclusiveOrExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, inclusiveOrExpression76.getTree());

					}
					break;

				default :
					break loop27;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 18, logicalAndExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "logicalAndExpression"


	public static class inclusiveOrExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "inclusiveOrExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:165:1: inclusiveOrExpression : exclusiveOrExpression ( BITWISEOR ^ exclusiveOrExpression )* ;
	public final CppParser.inclusiveOrExpression_return inclusiveOrExpression() throws RecognitionException {
		CppParser.inclusiveOrExpression_return retval = new CppParser.inclusiveOrExpression_return();
		retval.start = input.LT(1);
		int inclusiveOrExpression_StartIndex = input.index();

		Object root_0 = null;

		Token BITWISEOR78=null;
		ParserRuleReturnScope exclusiveOrExpression77 =null;
		ParserRuleReturnScope exclusiveOrExpression79 =null;

		Object BITWISEOR78_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:166:3: ( exclusiveOrExpression ( BITWISEOR ^ exclusiveOrExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:166:5: exclusiveOrExpression ( BITWISEOR ^ exclusiveOrExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression1101);
			exclusiveOrExpression77=exclusiveOrExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, exclusiveOrExpression77.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:166:27: ( BITWISEOR ^ exclusiveOrExpression )*
			loop28:
			while (true) {
				int alt28=2;
				int LA28_0 = input.LA(1);
				if ( (LA28_0==BITWISEOR) ) {
					alt28=1;
				}

				switch (alt28) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:166:28: BITWISEOR ^ exclusiveOrExpression
					{
					BITWISEOR78=(Token)match(input,BITWISEOR,FOLLOW_BITWISEOR_in_inclusiveOrExpression1104); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					BITWISEOR78_tree = (Object)adaptor.create(BITWISEOR78);
					root_0 = (Object)adaptor.becomeRoot(BITWISEOR78_tree, root_0);
					}

					pushFollow(FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression1107);
					exclusiveOrExpression79=exclusiveOrExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exclusiveOrExpression79.getTree());

					}
					break;

				default :
					break loop28;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 19, inclusiveOrExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "inclusiveOrExpression"


	public static class exclusiveOrExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exclusiveOrExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:169:1: exclusiveOrExpression : andExpression ( BITWISEXOR ^ andExpression )* ;
	public final CppParser.exclusiveOrExpression_return exclusiveOrExpression() throws RecognitionException {
		CppParser.exclusiveOrExpression_return retval = new CppParser.exclusiveOrExpression_return();
		retval.start = input.LT(1);
		int exclusiveOrExpression_StartIndex = input.index();

		Object root_0 = null;

		Token BITWISEXOR81=null;
		ParserRuleReturnScope andExpression80 =null;
		ParserRuleReturnScope andExpression82 =null;

		Object BITWISEXOR81_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:170:3: ( andExpression ( BITWISEXOR ^ andExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:170:5: andExpression ( BITWISEXOR ^ andExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_andExpression_in_exclusiveOrExpression1122);
			andExpression80=andExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, andExpression80.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:170:19: ( BITWISEXOR ^ andExpression )*
			loop29:
			while (true) {
				int alt29=2;
				int LA29_0 = input.LA(1);
				if ( (LA29_0==BITWISEXOR) ) {
					alt29=1;
				}

				switch (alt29) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:170:20: BITWISEXOR ^ andExpression
					{
					BITWISEXOR81=(Token)match(input,BITWISEXOR,FOLLOW_BITWISEXOR_in_exclusiveOrExpression1125); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					BITWISEXOR81_tree = (Object)adaptor.create(BITWISEXOR81);
					root_0 = (Object)adaptor.becomeRoot(BITWISEXOR81_tree, root_0);
					}

					pushFollow(FOLLOW_andExpression_in_exclusiveOrExpression1128);
					andExpression82=andExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, andExpression82.getTree());

					}
					break;

				default :
					break loop29;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 20, exclusiveOrExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "exclusiveOrExpression"


	public static class andExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "andExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:173:1: andExpression : equalityExpression ( AMPERSAND ^ equalityExpression )* ;
	public final CppParser.andExpression_return andExpression() throws RecognitionException {
		CppParser.andExpression_return retval = new CppParser.andExpression_return();
		retval.start = input.LT(1);
		int andExpression_StartIndex = input.index();

		Object root_0 = null;

		Token AMPERSAND84=null;
		ParserRuleReturnScope equalityExpression83 =null;
		ParserRuleReturnScope equalityExpression85 =null;

		Object AMPERSAND84_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:174:3: ( equalityExpression ( AMPERSAND ^ equalityExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:174:5: equalityExpression ( AMPERSAND ^ equalityExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_equalityExpression_in_andExpression1143);
			equalityExpression83=equalityExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, equalityExpression83.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:174:24: ( AMPERSAND ^ equalityExpression )*
			loop30:
			while (true) {
				int alt30=2;
				int LA30_0 = input.LA(1);
				if ( (LA30_0==AMPERSAND) ) {
					alt30=1;
				}

				switch (alt30) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:174:25: AMPERSAND ^ equalityExpression
					{
					AMPERSAND84=(Token)match(input,AMPERSAND,FOLLOW_AMPERSAND_in_andExpression1146); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					AMPERSAND84_tree = (Object)adaptor.create(AMPERSAND84);
					root_0 = (Object)adaptor.becomeRoot(AMPERSAND84_tree, root_0);
					}

					pushFollow(FOLLOW_equalityExpression_in_andExpression1149);
					equalityExpression85=equalityExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, equalityExpression85.getTree());

					}
					break;

				default :
					break loop30;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 21, andExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "andExpression"


	public static class equalityExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "equalityExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:177:1: equalityExpression : relationalExpression ( ( NOTEQUAL ^| EQUAL ^) relationalExpression )* ;
	public final CppParser.equalityExpression_return equalityExpression() throws RecognitionException {
		CppParser.equalityExpression_return retval = new CppParser.equalityExpression_return();
		retval.start = input.LT(1);
		int equalityExpression_StartIndex = input.index();

		Object root_0 = null;

		Token NOTEQUAL87=null;
		Token EQUAL88=null;
		ParserRuleReturnScope relationalExpression86 =null;
		ParserRuleReturnScope relationalExpression89 =null;

		Object NOTEQUAL87_tree=null;
		Object EQUAL88_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:178:3: ( relationalExpression ( ( NOTEQUAL ^| EQUAL ^) relationalExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:178:5: relationalExpression ( ( NOTEQUAL ^| EQUAL ^) relationalExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_relationalExpression_in_equalityExpression1164);
			relationalExpression86=relationalExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, relationalExpression86.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:178:26: ( ( NOTEQUAL ^| EQUAL ^) relationalExpression )*
			loop32:
			while (true) {
				int alt32=2;
				int LA32_0 = input.LA(1);
				if ( (LA32_0==EQUAL||LA32_0==NOTEQUAL) ) {
					alt32=1;
				}

				switch (alt32) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:178:27: ( NOTEQUAL ^| EQUAL ^) relationalExpression
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:178:27: ( NOTEQUAL ^| EQUAL ^)
					int alt31=2;
					int LA31_0 = input.LA(1);
					if ( (LA31_0==NOTEQUAL) ) {
						alt31=1;
					}
					else if ( (LA31_0==EQUAL) ) {
						alt31=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 31, 0, input);
						throw nvae;
					}

					switch (alt31) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:178:28: NOTEQUAL ^
							{
							NOTEQUAL87=(Token)match(input,NOTEQUAL,FOLLOW_NOTEQUAL_in_equalityExpression1168); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							NOTEQUAL87_tree = (Object)adaptor.create(NOTEQUAL87);
							root_0 = (Object)adaptor.becomeRoot(NOTEQUAL87_tree, root_0);
							}

							}
							break;
						case 2 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:178:40: EQUAL ^
							{
							EQUAL88=(Token)match(input,EQUAL,FOLLOW_EQUAL_in_equalityExpression1173); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							EQUAL88_tree = (Object)adaptor.create(EQUAL88);
							root_0 = (Object)adaptor.becomeRoot(EQUAL88_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_relationalExpression_in_equalityExpression1177);
					relationalExpression89=relationalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, relationalExpression89.getTree());

					}
					break;

				default :
					break loop32;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 22, equalityExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "equalityExpression"


	public static class relationalExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "relationalExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:181:1: relationalExpression : shiftExpression ( ( ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^) shiftExpression )* ) ;
	public final CppParser.relationalExpression_return relationalExpression() throws RecognitionException {
		CppParser.relationalExpression_return retval = new CppParser.relationalExpression_return();
		retval.start = input.LT(1);
		int relationalExpression_StartIndex = input.index();

		Object root_0 = null;

		Token LESSTHAN91=null;
		Token GREATERTHAN92=null;
		Token LESSTHANOREQUALTO93=null;
		Token GREATERTHANOREQUALTO94=null;
		ParserRuleReturnScope shiftExpression90 =null;
		ParserRuleReturnScope shiftExpression95 =null;

		Object LESSTHAN91_tree=null;
		Object GREATERTHAN92_tree=null;
		Object LESSTHANOREQUALTO93_tree=null;
		Object GREATERTHANOREQUALTO94_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:182:3: ( shiftExpression ( ( ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^) shiftExpression )* ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:182:5: shiftExpression ( ( ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^) shiftExpression )* )
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_shiftExpression_in_relationalExpression1192);
			shiftExpression90=shiftExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, shiftExpression90.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:183:4: ( ( ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^) shiftExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:183:6: ( ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^) shiftExpression )*
			{
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:183:6: ( ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^) shiftExpression )*
			loop34:
			while (true) {
				int alt34=2;
				int LA34_0 = input.LA(1);
				if ( ((LA34_0 >= GREATERTHAN && LA34_0 <= GREATERTHANOREQUALTO)||(LA34_0 >= LESSTHAN && LA34_0 <= LESSTHANOREQUALTO)) ) {
					alt34=1;
				}

				switch (alt34) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:183:8: ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^) shiftExpression
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:183:8: ( LESSTHAN ^| GREATERTHAN ^| LESSTHANOREQUALTO ^| GREATERTHANOREQUALTO ^)
					int alt33=4;
					switch ( input.LA(1) ) {
					case LESSTHAN:
						{
						alt33=1;
						}
						break;
					case GREATERTHAN:
						{
						alt33=2;
						}
						break;
					case LESSTHANOREQUALTO:
						{
						alt33=3;
						}
						break;
					case GREATERTHANOREQUALTO:
						{
						alt33=4;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 33, 0, input);
						throw nvae;
					}
					switch (alt33) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:183:10: LESSTHAN ^
							{
							LESSTHAN91=(Token)match(input,LESSTHAN,FOLLOW_LESSTHAN_in_relationalExpression1203); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							LESSTHAN91_tree = (Object)adaptor.create(LESSTHAN91);
							root_0 = (Object)adaptor.becomeRoot(LESSTHAN91_tree, root_0);
							}

							}
							break;
						case 2 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:184:8: GREATERTHAN ^
							{
							GREATERTHAN92=(Token)match(input,GREATERTHAN,FOLLOW_GREATERTHAN_in_relationalExpression1213); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							GREATERTHAN92_tree = (Object)adaptor.create(GREATERTHAN92);
							root_0 = (Object)adaptor.becomeRoot(GREATERTHAN92_tree, root_0);
							}

							}
							break;
						case 3 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:185:8: LESSTHANOREQUALTO ^
							{
							LESSTHANOREQUALTO93=(Token)match(input,LESSTHANOREQUALTO,FOLLOW_LESSTHANOREQUALTO_in_relationalExpression1223); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							LESSTHANOREQUALTO93_tree = (Object)adaptor.create(LESSTHANOREQUALTO93);
							root_0 = (Object)adaptor.becomeRoot(LESSTHANOREQUALTO93_tree, root_0);
							}

							}
							break;
						case 4 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:186:8: GREATERTHANOREQUALTO ^
							{
							GREATERTHANOREQUALTO94=(Token)match(input,GREATERTHANOREQUALTO,FOLLOW_GREATERTHANOREQUALTO_in_relationalExpression1233); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							GREATERTHANOREQUALTO94_tree = (Object)adaptor.create(GREATERTHANOREQUALTO94);
							root_0 = (Object)adaptor.becomeRoot(GREATERTHANOREQUALTO94_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_shiftExpression_in_relationalExpression1247);
					shiftExpression95=shiftExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, shiftExpression95.getTree());

					}
					break;

				default :
					break loop34;
				}
			}

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 23, relationalExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "relationalExpression"


	public static class shiftExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "shiftExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:192:1: shiftExpression : additiveExpression ( ( SHIFTLEFT ^| SHIFTRIGHT ^) additiveExpression )* ;
	public final CppParser.shiftExpression_return shiftExpression() throws RecognitionException {
		CppParser.shiftExpression_return retval = new CppParser.shiftExpression_return();
		retval.start = input.LT(1);
		int shiftExpression_StartIndex = input.index();

		Object root_0 = null;

		Token SHIFTLEFT97=null;
		Token SHIFTRIGHT98=null;
		ParserRuleReturnScope additiveExpression96 =null;
		ParserRuleReturnScope additiveExpression99 =null;

		Object SHIFTLEFT97_tree=null;
		Object SHIFTRIGHT98_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:193:3: ( additiveExpression ( ( SHIFTLEFT ^| SHIFTRIGHT ^) additiveExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:193:5: additiveExpression ( ( SHIFTLEFT ^| SHIFTRIGHT ^) additiveExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_additiveExpression_in_shiftExpression1271);
			additiveExpression96=additiveExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, additiveExpression96.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:193:24: ( ( SHIFTLEFT ^| SHIFTRIGHT ^) additiveExpression )*
			loop36:
			while (true) {
				int alt36=2;
				int LA36_0 = input.LA(1);
				if ( (LA36_0==SHIFTLEFT||LA36_0==SHIFTRIGHT) ) {
					alt36=1;
				}

				switch (alt36) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:193:25: ( SHIFTLEFT ^| SHIFTRIGHT ^) additiveExpression
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:193:25: ( SHIFTLEFT ^| SHIFTRIGHT ^)
					int alt35=2;
					int LA35_0 = input.LA(1);
					if ( (LA35_0==SHIFTLEFT) ) {
						alt35=1;
					}
					else if ( (LA35_0==SHIFTRIGHT) ) {
						alt35=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 35, 0, input);
						throw nvae;
					}

					switch (alt35) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:193:26: SHIFTLEFT ^
							{
							SHIFTLEFT97=(Token)match(input,SHIFTLEFT,FOLLOW_SHIFTLEFT_in_shiftExpression1275); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							SHIFTLEFT97_tree = (Object)adaptor.create(SHIFTLEFT97);
							root_0 = (Object)adaptor.becomeRoot(SHIFTLEFT97_tree, root_0);
							}

							}
							break;
						case 2 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:193:39: SHIFTRIGHT ^
							{
							SHIFTRIGHT98=(Token)match(input,SHIFTRIGHT,FOLLOW_SHIFTRIGHT_in_shiftExpression1280); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							SHIFTRIGHT98_tree = (Object)adaptor.create(SHIFTRIGHT98);
							root_0 = (Object)adaptor.becomeRoot(SHIFTRIGHT98_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_additiveExpression_in_shiftExpression1284);
					additiveExpression99=additiveExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, additiveExpression99.getTree());

					}
					break;

				default :
					break loop36;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 24, shiftExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "shiftExpression"


	public static class additiveExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "additiveExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:196:1: additiveExpression : multiplicativeExpression ( ( PLUS ^| MINUS ^) multiplicativeExpression )* ;
	public final CppParser.additiveExpression_return additiveExpression() throws RecognitionException {
		CppParser.additiveExpression_return retval = new CppParser.additiveExpression_return();
		retval.start = input.LT(1);
		int additiveExpression_StartIndex = input.index();

		Object root_0 = null;

		Token PLUS101=null;
		Token MINUS102=null;
		ParserRuleReturnScope multiplicativeExpression100 =null;
		ParserRuleReturnScope multiplicativeExpression103 =null;

		Object PLUS101_tree=null;
		Object MINUS102_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:197:3: ( multiplicativeExpression ( ( PLUS ^| MINUS ^) multiplicativeExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:197:5: multiplicativeExpression ( ( PLUS ^| MINUS ^) multiplicativeExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression1299);
			multiplicativeExpression100=multiplicativeExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplicativeExpression100.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:197:30: ( ( PLUS ^| MINUS ^) multiplicativeExpression )*
			loop38:
			while (true) {
				int alt38=2;
				int LA38_0 = input.LA(1);
				if ( (LA38_0==MINUS||LA38_0==PLUS) ) {
					alt38=1;
				}

				switch (alt38) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:197:31: ( PLUS ^| MINUS ^) multiplicativeExpression
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:197:31: ( PLUS ^| MINUS ^)
					int alt37=2;
					int LA37_0 = input.LA(1);
					if ( (LA37_0==PLUS) ) {
						alt37=1;
					}
					else if ( (LA37_0==MINUS) ) {
						alt37=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 37, 0, input);
						throw nvae;
					}

					switch (alt37) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:197:32: PLUS ^
							{
							PLUS101=(Token)match(input,PLUS,FOLLOW_PLUS_in_additiveExpression1303); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							PLUS101_tree = (Object)adaptor.create(PLUS101);
							root_0 = (Object)adaptor.becomeRoot(PLUS101_tree, root_0);
							}

							}
							break;
						case 2 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:197:40: MINUS ^
							{
							MINUS102=(Token)match(input,MINUS,FOLLOW_MINUS_in_additiveExpression1308); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							MINUS102_tree = (Object)adaptor.create(MINUS102);
							root_0 = (Object)adaptor.becomeRoot(MINUS102_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression1312);
					multiplicativeExpression103=multiplicativeExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplicativeExpression103.getTree());

					}
					break;

				default :
					break loop38;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 25, additiveExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "additiveExpression"


	public static class multiplicativeExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "multiplicativeExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:200:1: multiplicativeExpression : unaryExpression ( ( STAR ^| DIVIDE ^| MOD ^) unaryExpression )* ;
	public final CppParser.multiplicativeExpression_return multiplicativeExpression() throws RecognitionException {
		CppParser.multiplicativeExpression_return retval = new CppParser.multiplicativeExpression_return();
		retval.start = input.LT(1);
		int multiplicativeExpression_StartIndex = input.index();

		Object root_0 = null;

		Token STAR105=null;
		Token DIVIDE106=null;
		Token MOD107=null;
		ParserRuleReturnScope unaryExpression104 =null;
		ParserRuleReturnScope unaryExpression108 =null;

		Object STAR105_tree=null;
		Object DIVIDE106_tree=null;
		Object MOD107_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:3: ( unaryExpression ( ( STAR ^| DIVIDE ^| MOD ^) unaryExpression )* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:5: unaryExpression ( ( STAR ^| DIVIDE ^| MOD ^) unaryExpression )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression1327);
			unaryExpression104=unaryExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression104.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:21: ( ( STAR ^| DIVIDE ^| MOD ^) unaryExpression )*
			loop40:
			while (true) {
				int alt40=2;
				int LA40_0 = input.LA(1);
				if ( (LA40_0==DIVIDE||LA40_0==MOD||LA40_0==STAR) ) {
					alt40=1;
				}

				switch (alt40) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:22: ( STAR ^| DIVIDE ^| MOD ^) unaryExpression
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:22: ( STAR ^| DIVIDE ^| MOD ^)
					int alt39=3;
					switch ( input.LA(1) ) {
					case STAR:
						{
						alt39=1;
						}
						break;
					case DIVIDE:
						{
						alt39=2;
						}
						break;
					case MOD:
						{
						alt39=3;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 39, 0, input);
						throw nvae;
					}
					switch (alt39) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:23: STAR ^
							{
							STAR105=(Token)match(input,STAR,FOLLOW_STAR_in_multiplicativeExpression1331); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							STAR105_tree = (Object)adaptor.create(STAR105);
							root_0 = (Object)adaptor.becomeRoot(STAR105_tree, root_0);
							}

							}
							break;
						case 2 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:31: DIVIDE ^
							{
							DIVIDE106=(Token)match(input,DIVIDE,FOLLOW_DIVIDE_in_multiplicativeExpression1336); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							DIVIDE106_tree = (Object)adaptor.create(DIVIDE106);
							root_0 = (Object)adaptor.becomeRoot(DIVIDE106_tree, root_0);
							}

							}
							break;
						case 3 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:201:41: MOD ^
							{
							MOD107=(Token)match(input,MOD,FOLLOW_MOD_in_multiplicativeExpression1341); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							MOD107_tree = (Object)adaptor.create(MOD107);
							root_0 = (Object)adaptor.becomeRoot(MOD107_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression1346);
					unaryExpression108=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression108.getTree());

					}
					break;

				default :
					break loop40;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 26, multiplicativeExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "multiplicativeExpression"


	public static class unaryExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "unaryExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:204:1: unaryExpression : ( PLUSPLUS unaryExpression -> ^( PLUSPLUS unaryExpression ) | MINUSMINUS unaryExpression -> ^( MINUSMINUS unaryExpression ) | SIZEOF unaryExpression -> ^( SIZEOF unaryExpression ) | SIZEOF LPAREN type_name RPAREN -> ^( SIZEOF_TYPE type_name ) | DEFINED type_name -> ^( DEFINED type_name ) | DEFINED LPAREN type_name RPAREN -> ^( DEFINED type_name ) | unaryExpressionNotPlusMinus );
	public final CppParser.unaryExpression_return unaryExpression() throws RecognitionException {
		CppParser.unaryExpression_return retval = new CppParser.unaryExpression_return();
		retval.start = input.LT(1);
		int unaryExpression_StartIndex = input.index();

		Object root_0 = null;

		Token PLUSPLUS109=null;
		Token MINUSMINUS111=null;
		Token SIZEOF113=null;
		Token SIZEOF115=null;
		Token LPAREN116=null;
		Token RPAREN118=null;
		Token DEFINED119=null;
		Token DEFINED121=null;
		Token LPAREN122=null;
		Token RPAREN124=null;
		ParserRuleReturnScope unaryExpression110 =null;
		ParserRuleReturnScope unaryExpression112 =null;
		ParserRuleReturnScope unaryExpression114 =null;
		ParserRuleReturnScope type_name117 =null;
		ParserRuleReturnScope type_name120 =null;
		ParserRuleReturnScope type_name123 =null;
		ParserRuleReturnScope unaryExpressionNotPlusMinus125 =null;

		Object PLUSPLUS109_tree=null;
		Object MINUSMINUS111_tree=null;
		Object SIZEOF113_tree=null;
		Object SIZEOF115_tree=null;
		Object LPAREN116_tree=null;
		Object RPAREN118_tree=null;
		Object DEFINED119_tree=null;
		Object DEFINED121_tree=null;
		Object LPAREN122_tree=null;
		Object RPAREN124_tree=null;
		RewriteRuleTokenStream stream_SIZEOF=new RewriteRuleTokenStream(adaptor,"token SIZEOF");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_MINUSMINUS=new RewriteRuleTokenStream(adaptor,"token MINUSMINUS");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_DEFINED=new RewriteRuleTokenStream(adaptor,"token DEFINED");
		RewriteRuleTokenStream stream_PLUSPLUS=new RewriteRuleTokenStream(adaptor,"token PLUSPLUS");
		RewriteRuleSubtreeStream stream_type_name=new RewriteRuleSubtreeStream(adaptor,"rule type_name");
		RewriteRuleSubtreeStream stream_unaryExpression=new RewriteRuleSubtreeStream(adaptor,"rule unaryExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:205:3: ( PLUSPLUS unaryExpression -> ^( PLUSPLUS unaryExpression ) | MINUSMINUS unaryExpression -> ^( MINUSMINUS unaryExpression ) | SIZEOF unaryExpression -> ^( SIZEOF unaryExpression ) | SIZEOF LPAREN type_name RPAREN -> ^( SIZEOF_TYPE type_name ) | DEFINED type_name -> ^( DEFINED type_name ) | DEFINED LPAREN type_name RPAREN -> ^( DEFINED type_name ) | unaryExpressionNotPlusMinus )
			int alt41=7;
			switch ( input.LA(1) ) {
			case PLUSPLUS:
				{
				alt41=1;
				}
				break;
			case MINUSMINUS:
				{
				alt41=2;
				}
				break;
			case SIZEOF:
				{
				int LA41_3 = input.LA(2);
				if ( (LA41_3==LPAREN) ) {
					int LA41_6 = input.LA(3);
					if ( (LA41_6==IDENTIFIER) ) {
						int LA41_10 = input.LA(4);
						if ( ((LA41_10 >= AMPERSAND && LA41_10 <= AND)||(LA41_10 >= ASSIGNEQUAL && LA41_10 <= BITWISEXOREQUAL)||(LA41_10 >= DIVIDE && LA41_10 <= DOT)||LA41_10==EQUAL||(LA41_10 >= GREATERTHAN && LA41_10 <= GREATERTHANOREQUALTO)||(LA41_10 >= LESSTHAN && LA41_10 <= LESSTHANOREQUALTO)||(LA41_10 >= LPAREN && LA41_10 <= LSQUARE)||(LA41_10 >= MINUS && LA41_10 <= MODEQUAL)||LA41_10==NOTEQUAL||LA41_10==OR||(LA41_10 >= PLUS && LA41_10 <= PLUSPLUS)||LA41_10==POINTERTO||LA41_10==QUESTIONMARK||(LA41_10 >= SHIFTLEFT && LA41_10 <= SHIFTRIGHTEQUAL)||LA41_10==STAR||LA41_10==TIMESEQUAL) ) {
							alt41=3;
						}
						else if ( (LA41_10==RPAREN) ) {
							int LA41_11 = input.LA(5);
							if ( (synpred67_Cpp()) ) {
								alt41=3;
							}
							else if ( (synpred68_Cpp()) ) {
								alt41=4;
							}

							else {
								if (state.backtracking>0) {state.failed=true; return retval;}
								int nvaeMark = input.mark();
								try {
									for (int nvaeConsume = 0; nvaeConsume < 5 - 1; nvaeConsume++) {
										input.consume();
									}
									NoViableAltException nvae =
										new NoViableAltException("", 41, 11, input);
									throw nvae;
								} finally {
									input.rewind(nvaeMark);
								}
							}

						}

						else {
							if (state.backtracking>0) {state.failed=true; return retval;}
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 41, 10, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}
					else if ( (LA41_6==AMPERSAND||LA41_6==CHARACTER_LITERAL||LA41_6==DECIMAL_LITERAL||LA41_6==DEFINED||LA41_6==FLOATING_POINT_LITERAL||LA41_6==HEX_LITERAL||LA41_6==LPAREN||LA41_6==MINUS||LA41_6==MINUSMINUS||LA41_6==NOT||LA41_6==OCTAL_LITERAL||LA41_6==PLUS||LA41_6==PLUSPLUS||LA41_6==SIZEOF||LA41_6==STAR||LA41_6==STRING_LITERAL||LA41_6==TILDE) ) {
						alt41=3;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 41, 6, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA41_3==AMPERSAND||LA41_3==CHARACTER_LITERAL||LA41_3==DECIMAL_LITERAL||LA41_3==DEFINED||LA41_3==FLOATING_POINT_LITERAL||LA41_3==HEX_LITERAL||LA41_3==IDENTIFIER||LA41_3==MINUS||LA41_3==MINUSMINUS||LA41_3==NOT||LA41_3==OCTAL_LITERAL||LA41_3==PLUS||LA41_3==PLUSPLUS||LA41_3==SIZEOF||LA41_3==STAR||LA41_3==STRING_LITERAL||LA41_3==TILDE) ) {
					alt41=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 41, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case DEFINED:
				{
				int LA41_4 = input.LA(2);
				if ( (LA41_4==LPAREN) ) {
					alt41=6;
				}
				else if ( (LA41_4==IDENTIFIER) ) {
					alt41=5;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 41, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case AMPERSAND:
			case CHARACTER_LITERAL:
			case DECIMAL_LITERAL:
			case FLOATING_POINT_LITERAL:
			case HEX_LITERAL:
			case IDENTIFIER:
			case LPAREN:
			case MINUS:
			case NOT:
			case OCTAL_LITERAL:
			case PLUS:
			case STAR:
			case STRING_LITERAL:
			case TILDE:
				{
				alt41=7;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 41, 0, input);
				throw nvae;
			}
			switch (alt41) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:205:5: PLUSPLUS unaryExpression
					{
					PLUSPLUS109=(Token)match(input,PLUSPLUS,FOLLOW_PLUSPLUS_in_unaryExpression1361); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PLUSPLUS.add(PLUSPLUS109);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression1364);
					unaryExpression110=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression110.getTree());
					// AST REWRITE
					// elements: PLUSPLUS, unaryExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 205:31: -> ^( PLUSPLUS unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:205:34: ^( PLUSPLUS unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_PLUSPLUS.nextNode(), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:206:5: MINUSMINUS unaryExpression
					{
					MINUSMINUS111=(Token)match(input,MINUSMINUS,FOLLOW_MINUSMINUS_in_unaryExpression1378); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_MINUSMINUS.add(MINUSMINUS111);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression1380);
					unaryExpression112=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression112.getTree());
					// AST REWRITE
					// elements: MINUSMINUS, unaryExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 206:32: -> ^( MINUSMINUS unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:206:35: ^( MINUSMINUS unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_MINUSMINUS.nextNode(), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:207:5: SIZEOF unaryExpression
					{
					SIZEOF113=(Token)match(input,SIZEOF,FOLLOW_SIZEOF_in_unaryExpression1394); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SIZEOF.add(SIZEOF113);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression1396);
					unaryExpression114=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression114.getTree());
					// AST REWRITE
					// elements: SIZEOF, unaryExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 207:28: -> ^( SIZEOF unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:207:31: ^( SIZEOF unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_SIZEOF.nextNode(), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:208:5: SIZEOF LPAREN type_name RPAREN
					{
					SIZEOF115=(Token)match(input,SIZEOF,FOLLOW_SIZEOF_in_unaryExpression1410); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SIZEOF.add(SIZEOF115);

					LPAREN116=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_unaryExpression1412); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN116);

					pushFollow(FOLLOW_type_name_in_unaryExpression1414);
					type_name117=type_name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type_name.add(type_name117.getTree());
					RPAREN118=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_unaryExpression1416); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN118);

					// AST REWRITE
					// elements: type_name
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 208:36: -> ^( SIZEOF_TYPE type_name )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:208:39: ^( SIZEOF_TYPE type_name )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SIZEOF_TYPE, "SIZEOF_TYPE"), root_1);
						adaptor.addChild(root_1, stream_type_name.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 5 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:209:5: DEFINED type_name
					{
					DEFINED119=(Token)match(input,DEFINED,FOLLOW_DEFINED_in_unaryExpression1430); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DEFINED.add(DEFINED119);

					pushFollow(FOLLOW_type_name_in_unaryExpression1432);
					type_name120=type_name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type_name.add(type_name120.getTree());
					// AST REWRITE
					// elements: DEFINED, type_name
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 209:25: -> ^( DEFINED type_name )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:209:28: ^( DEFINED type_name )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_DEFINED.nextNode(), root_1);
						adaptor.addChild(root_1, stream_type_name.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 6 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:210:5: DEFINED LPAREN type_name RPAREN
					{
					DEFINED121=(Token)match(input,DEFINED,FOLLOW_DEFINED_in_unaryExpression1448); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DEFINED.add(DEFINED121);

					LPAREN122=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_unaryExpression1450); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN122);

					pushFollow(FOLLOW_type_name_in_unaryExpression1452);
					type_name123=type_name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type_name.add(type_name123.getTree());
					RPAREN124=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_unaryExpression1455); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN124);

					// AST REWRITE
					// elements: type_name, DEFINED
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 210:38: -> ^( DEFINED type_name )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:210:40: ^( DEFINED type_name )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_DEFINED.nextNode(), root_1);
						adaptor.addChild(root_1, stream_type_name.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 7 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:211:5: unaryExpressionNotPlusMinus
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_unaryExpressionNotPlusMinus_in_unaryExpression1468);
					unaryExpressionNotPlusMinus125=unaryExpressionNotPlusMinus();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpressionNotPlusMinus125.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 27, unaryExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "unaryExpression"


	public static class unaryExpressionNotPlusMinus_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "unaryExpressionNotPlusMinus"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:214:1: unaryExpressionNotPlusMinus : ( NOT unaryExpression -> ^( NOT unaryExpression ) | TILDE unaryExpression -> ^( TILDE unaryExpression ) | AMPERSAND unaryExpression -> ^( REFERANCE unaryExpression ) | STAR unaryExpression -> ^( POINTER_AT unaryExpression ) | MINUS unaryExpression -> ^( UNARY_MINUS unaryExpression ) | PLUS unaryExpression -> ^( UNARY_PLUS unaryExpression ) | LPAREN type_name RPAREN unaryExpression -> ^( TYPECAST type_name unaryExpression ) | postfixExpression );
	public final CppParser.unaryExpressionNotPlusMinus_return unaryExpressionNotPlusMinus() throws RecognitionException {
		CppParser.unaryExpressionNotPlusMinus_return retval = new CppParser.unaryExpressionNotPlusMinus_return();
		retval.start = input.LT(1);
		int unaryExpressionNotPlusMinus_StartIndex = input.index();

		Object root_0 = null;

		Token NOT126=null;
		Token TILDE128=null;
		Token AMPERSAND130=null;
		Token STAR132=null;
		Token MINUS134=null;
		Token PLUS136=null;
		Token LPAREN138=null;
		Token RPAREN140=null;
		ParserRuleReturnScope unaryExpression127 =null;
		ParserRuleReturnScope unaryExpression129 =null;
		ParserRuleReturnScope unaryExpression131 =null;
		ParserRuleReturnScope unaryExpression133 =null;
		ParserRuleReturnScope unaryExpression135 =null;
		ParserRuleReturnScope unaryExpression137 =null;
		ParserRuleReturnScope type_name139 =null;
		ParserRuleReturnScope unaryExpression141 =null;
		ParserRuleReturnScope postfixExpression142 =null;

		Object NOT126_tree=null;
		Object TILDE128_tree=null;
		Object AMPERSAND130_tree=null;
		Object STAR132_tree=null;
		Object MINUS134_tree=null;
		Object PLUS136_tree=null;
		Object LPAREN138_tree=null;
		Object RPAREN140_tree=null;
		RewriteRuleTokenStream stream_AMPERSAND=new RewriteRuleTokenStream(adaptor,"token AMPERSAND");
		RewriteRuleTokenStream stream_NOT=new RewriteRuleTokenStream(adaptor,"token NOT");
		RewriteRuleTokenStream stream_STAR=new RewriteRuleTokenStream(adaptor,"token STAR");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_TILDE=new RewriteRuleTokenStream(adaptor,"token TILDE");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
		RewriteRuleTokenStream stream_PLUS=new RewriteRuleTokenStream(adaptor,"token PLUS");
		RewriteRuleSubtreeStream stream_type_name=new RewriteRuleSubtreeStream(adaptor,"rule type_name");
		RewriteRuleSubtreeStream stream_unaryExpression=new RewriteRuleSubtreeStream(adaptor,"rule unaryExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:215:3: ( NOT unaryExpression -> ^( NOT unaryExpression ) | TILDE unaryExpression -> ^( TILDE unaryExpression ) | AMPERSAND unaryExpression -> ^( REFERANCE unaryExpression ) | STAR unaryExpression -> ^( POINTER_AT unaryExpression ) | MINUS unaryExpression -> ^( UNARY_MINUS unaryExpression ) | PLUS unaryExpression -> ^( UNARY_PLUS unaryExpression ) | LPAREN type_name RPAREN unaryExpression -> ^( TYPECAST type_name unaryExpression ) | postfixExpression )
			int alt42=8;
			switch ( input.LA(1) ) {
			case NOT:
				{
				alt42=1;
				}
				break;
			case TILDE:
				{
				alt42=2;
				}
				break;
			case AMPERSAND:
				{
				alt42=3;
				}
				break;
			case STAR:
				{
				alt42=4;
				}
				break;
			case MINUS:
				{
				alt42=5;
				}
				break;
			case PLUS:
				{
				alt42=6;
				}
				break;
			case LPAREN:
				{
				int LA42_7 = input.LA(2);
				if ( (synpred77_Cpp()) ) {
					alt42=7;
				}
				else if ( (true) ) {
					alt42=8;
				}

				}
				break;
			case CHARACTER_LITERAL:
			case DECIMAL_LITERAL:
			case FLOATING_POINT_LITERAL:
			case HEX_LITERAL:
			case IDENTIFIER:
			case OCTAL_LITERAL:
			case STRING_LITERAL:
				{
				alt42=8;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 42, 0, input);
				throw nvae;
			}
			switch (alt42) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:215:5: NOT unaryExpression
					{
					NOT126=(Token)match(input,NOT,FOLLOW_NOT_in_unaryExpressionNotPlusMinus1481); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NOT.add(NOT126);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1485);
					unaryExpression127=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression127.getTree());
					// AST REWRITE
					// elements: unaryExpression, NOT
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 215:27: -> ^( NOT unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:215:30: ^( NOT unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_NOT.nextNode(), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:216:5: TILDE unaryExpression
					{
					TILDE128=(Token)match(input,TILDE,FOLLOW_TILDE_in_unaryExpressionNotPlusMinus1499); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_TILDE.add(TILDE128);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1502);
					unaryExpression129=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression129.getTree());
					// AST REWRITE
					// elements: unaryExpression, TILDE
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 216:28: -> ^( TILDE unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:216:31: ^( TILDE unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_TILDE.nextNode(), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:217:5: AMPERSAND unaryExpression
					{
					AMPERSAND130=(Token)match(input,AMPERSAND,FOLLOW_AMPERSAND_in_unaryExpressionNotPlusMinus1517); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_AMPERSAND.add(AMPERSAND130);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1519);
					unaryExpression131=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression131.getTree());
					// AST REWRITE
					// elements: unaryExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 217:31: -> ^( REFERANCE unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:217:34: ^( REFERANCE unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(REFERANCE, "REFERANCE"), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:218:5: STAR unaryExpression
					{
					STAR132=(Token)match(input,STAR,FOLLOW_STAR_in_unaryExpressionNotPlusMinus1533); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STAR.add(STAR132);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1536);
					unaryExpression133=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression133.getTree());
					// AST REWRITE
					// elements: unaryExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 218:27: -> ^( POINTER_AT unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:218:30: ^( POINTER_AT unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(POINTER_AT, "POINTER_AT"), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 5 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:219:5: MINUS unaryExpression
					{
					MINUS134=(Token)match(input,MINUS,FOLLOW_MINUS_in_unaryExpressionNotPlusMinus1550); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_MINUS.add(MINUS134);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1553);
					unaryExpression135=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression135.getTree());
					// AST REWRITE
					// elements: unaryExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 219:29: -> ^( UNARY_MINUS unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:219:32: ^( UNARY_MINUS unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(UNARY_MINUS, "UNARY_MINUS"), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 6 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:220:5: PLUS unaryExpression
					{
					PLUS136=(Token)match(input,PLUS,FOLLOW_PLUS_in_unaryExpressionNotPlusMinus1568); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PLUS.add(PLUS136);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1572);
					unaryExpression137=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression137.getTree());
					// AST REWRITE
					// elements: unaryExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 220:28: -> ^( UNARY_PLUS unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:220:31: ^( UNARY_PLUS unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(UNARY_PLUS, "UNARY_PLUS"), root_1);
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 7 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:221:5: LPAREN type_name RPAREN unaryExpression
					{
					LPAREN138=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_unaryExpressionNotPlusMinus1586); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN138);

					pushFollow(FOLLOW_type_name_in_unaryExpressionNotPlusMinus1588);
					type_name139=type_name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type_name.add(type_name139.getTree());
					RPAREN140=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_unaryExpressionNotPlusMinus1590); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN140);

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1593);
					unaryExpression141=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_unaryExpression.add(unaryExpression141.getTree());
					// AST REWRITE
					// elements: unaryExpression, type_name
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 221:46: -> ^( TYPECAST type_name unaryExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:221:49: ^( TYPECAST type_name unaryExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TYPECAST, "TYPECAST"), root_1);
						adaptor.addChild(root_1, stream_type_name.nextTree());
						adaptor.addChild(root_1, stream_unaryExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 8 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:222:5: postfixExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_postfixExpression_in_unaryExpressionNotPlusMinus1609);
					postfixExpression142=postfixExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, postfixExpression142.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 28, unaryExpressionNotPlusMinus_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "unaryExpressionNotPlusMinus"


	public static class postfixExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "postfixExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:225:1: postfixExpression : primaryExpression (l= LSQUARE ^ assignmentExpression RSQUARE !| DOT ^ IDENTIFIER |s= STAR ^ IDENTIFIER | POINTERTO ^ IDENTIFIER |p= PLUSPLUS ^|m= MINUSMINUS ^)* ;
	public final CppParser.postfixExpression_return postfixExpression() throws RecognitionException {
		CppParser.postfixExpression_return retval = new CppParser.postfixExpression_return();
		retval.start = input.LT(1);
		int postfixExpression_StartIndex = input.index();

		Object root_0 = null;

		Token l=null;
		Token s=null;
		Token p=null;
		Token m=null;
		Token RSQUARE145=null;
		Token DOT146=null;
		Token IDENTIFIER147=null;
		Token IDENTIFIER148=null;
		Token POINTERTO149=null;
		Token IDENTIFIER150=null;
		ParserRuleReturnScope primaryExpression143 =null;
		ParserRuleReturnScope assignmentExpression144 =null;

		Object l_tree=null;
		Object s_tree=null;
		Object p_tree=null;
		Object m_tree=null;
		Object RSQUARE145_tree=null;
		Object DOT146_tree=null;
		Object IDENTIFIER147_tree=null;
		Object IDENTIFIER148_tree=null;
		Object POINTERTO149_tree=null;
		Object IDENTIFIER150_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:226:3: ( primaryExpression (l= LSQUARE ^ assignmentExpression RSQUARE !| DOT ^ IDENTIFIER |s= STAR ^ IDENTIFIER | POINTERTO ^ IDENTIFIER |p= PLUSPLUS ^|m= MINUSMINUS ^)* )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:226:7: primaryExpression (l= LSQUARE ^ assignmentExpression RSQUARE !| DOT ^ IDENTIFIER |s= STAR ^ IDENTIFIER | POINTERTO ^ IDENTIFIER |p= PLUSPLUS ^|m= MINUSMINUS ^)*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_primaryExpression_in_postfixExpression1624);
			primaryExpression143=primaryExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, primaryExpression143.getTree());

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:227:3: (l= LSQUARE ^ assignmentExpression RSQUARE !| DOT ^ IDENTIFIER |s= STAR ^ IDENTIFIER | POINTERTO ^ IDENTIFIER |p= PLUSPLUS ^|m= MINUSMINUS ^)*
			loop43:
			while (true) {
				int alt43=7;
				switch ( input.LA(1) ) {
				case STAR:
					{
					int LA43_1 = input.LA(2);
					if ( (LA43_1==IDENTIFIER) ) {
						int LA43_8 = input.LA(3);
						if ( (synpred80_Cpp()) ) {
							alt43=3;
						}

					}

					}
					break;
				case LSQUARE:
					{
					alt43=1;
					}
					break;
				case DOT:
					{
					alt43=2;
					}
					break;
				case POINTERTO:
					{
					alt43=4;
					}
					break;
				case PLUSPLUS:
					{
					alt43=5;
					}
					break;
				case MINUSMINUS:
					{
					alt43=6;
					}
					break;
				}
				switch (alt43) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:227:7: l= LSQUARE ^ assignmentExpression RSQUARE !
					{
					l=(Token)match(input,LSQUARE,FOLLOW_LSQUARE_in_postfixExpression1634); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					l_tree = (Object)adaptor.create(l);
					root_0 = (Object)adaptor.becomeRoot(l_tree, root_0);
					}

					if ( state.backtracking==0 ) {l.setType(INDEX_OP);}
					pushFollow(FOLLOW_assignmentExpression_in_postfixExpression1639);
					assignmentExpression144=assignmentExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, assignmentExpression144.getTree());

					RSQUARE145=(Token)match(input,RSQUARE,FOLLOW_RSQUARE_in_postfixExpression1641); if (state.failed) return retval;
					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:228:7: DOT ^ IDENTIFIER
					{
					DOT146=(Token)match(input,DOT,FOLLOW_DOT_in_postfixExpression1650); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOT146_tree = (Object)adaptor.create(DOT146);
					root_0 = (Object)adaptor.becomeRoot(DOT146_tree, root_0);
					}

					IDENTIFIER147=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_postfixExpression1658); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER147_tree = (Object)adaptor.create(IDENTIFIER147);
					adaptor.addChild(root_0, IDENTIFIER147_tree);
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:229:5: s= STAR ^ IDENTIFIER
					{
					s=(Token)match(input,STAR,FOLLOW_STAR_in_postfixExpression1673); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					s_tree = (Object)adaptor.create(s);
					root_0 = (Object)adaptor.becomeRoot(s_tree, root_0);
					}

					if ( state.backtracking==0 ) {s.setType(POINTER);}
					IDENTIFIER148=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_postfixExpression1678); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER148_tree = (Object)adaptor.create(IDENTIFIER148);
					adaptor.addChild(root_0, IDENTIFIER148_tree);
					}

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:230:7: POINTERTO ^ IDENTIFIER
					{
					POINTERTO149=(Token)match(input,POINTERTO,FOLLOW_POINTERTO_in_postfixExpression1686); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					POINTERTO149_tree = (Object)adaptor.create(POINTERTO149);
					root_0 = (Object)adaptor.becomeRoot(POINTERTO149_tree, root_0);
					}

					IDENTIFIER150=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_postfixExpression1689); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER150_tree = (Object)adaptor.create(IDENTIFIER150);
					adaptor.addChild(root_0, IDENTIFIER150_tree);
					}

					}
					break;
				case 5 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:231:7: p= PLUSPLUS ^
					{
					p=(Token)match(input,PLUSPLUS,FOLLOW_PLUSPLUS_in_postfixExpression1699); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					p_tree = (Object)adaptor.create(p);
					root_0 = (Object)adaptor.becomeRoot(p_tree, root_0);
					}

					if ( state.backtracking==0 ) {p.setType(POST_INC);}
					}
					break;
				case 6 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:232:7: m= MINUSMINUS ^
					{
					m=(Token)match(input,MINUSMINUS,FOLLOW_MINUSMINUS_in_postfixExpression1715); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					m_tree = (Object)adaptor.create(m);
					root_0 = (Object)adaptor.becomeRoot(m_tree, root_0);
					}

					if ( state.backtracking==0 ) {m.setType(POST_DEC);}
					}
					break;

				default :
					break loop43;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 29, postfixExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "postfixExpression"


	public static class primaryExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "primaryExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:236:1: primaryExpression : ( ( IDENTIFIER LPAREN )=> functionCall | IDENTIFIER | constant | LPAREN assignmentExpression RPAREN -> ^( EXPR_GROUP assignmentExpression ) );
	public final CppParser.primaryExpression_return primaryExpression() throws RecognitionException {
		CppParser.primaryExpression_return retval = new CppParser.primaryExpression_return();
		retval.start = input.LT(1);
		int primaryExpression_StartIndex = input.index();

		Object root_0 = null;

		Token IDENTIFIER152=null;
		Token LPAREN154=null;
		Token RPAREN156=null;
		ParserRuleReturnScope functionCall151 =null;
		ParserRuleReturnScope constant153 =null;
		ParserRuleReturnScope assignmentExpression155 =null;

		Object IDENTIFIER152_tree=null;
		Object LPAREN154_tree=null;
		Object RPAREN156_tree=null;
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleSubtreeStream stream_assignmentExpression=new RewriteRuleSubtreeStream(adaptor,"rule assignmentExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:237:3: ( ( IDENTIFIER LPAREN )=> functionCall | IDENTIFIER | constant | LPAREN assignmentExpression RPAREN -> ^( EXPR_GROUP assignmentExpression ) )
			int alt44=4;
			switch ( input.LA(1) ) {
			case IDENTIFIER:
				{
				int LA44_1 = input.LA(2);
				if ( (synpred84_Cpp()) ) {
					alt44=1;
				}
				else if ( (synpred85_Cpp()) ) {
					alt44=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 44, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case CHARACTER_LITERAL:
			case DECIMAL_LITERAL:
			case FLOATING_POINT_LITERAL:
			case HEX_LITERAL:
			case OCTAL_LITERAL:
			case STRING_LITERAL:
				{
				alt44=3;
				}
				break;
			case LPAREN:
				{
				alt44=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 44, 0, input);
				throw nvae;
			}
			switch (alt44) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:237:7: ( IDENTIFIER LPAREN )=> functionCall
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_functionCall_in_primaryExpression1749);
					functionCall151=functionCall();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, functionCall151.getTree());

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:238:7: IDENTIFIER
					{
					root_0 = (Object)adaptor.nil();


					IDENTIFIER152=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_primaryExpression1757); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER152_tree = (Object)adaptor.create(IDENTIFIER152);
					adaptor.addChild(root_0, IDENTIFIER152_tree);
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:239:7: constant
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_constant_in_primaryExpression1771);
					constant153=constant();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, constant153.getTree());

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:240:7: LPAREN assignmentExpression RPAREN
					{
					LPAREN154=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_primaryExpression1784); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN154);

					pushFollow(FOLLOW_assignmentExpression_in_primaryExpression1787);
					assignmentExpression155=assignmentExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_assignmentExpression.add(assignmentExpression155.getTree());
					RPAREN156=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_primaryExpression1789); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN156);

					// AST REWRITE
					// elements: assignmentExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 240:44: -> ^( EXPR_GROUP assignmentExpression )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:240:47: ^( EXPR_GROUP assignmentExpression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPR_GROUP, "EXPR_GROUP"), root_1);
						adaptor.addChild(root_1, stream_assignmentExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 30, primaryExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "primaryExpression"


	public static class functionCall_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "functionCall"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:243:1: functionCall : id= IDENTIFIER LPAREN ( argList )? RPAREN -> ^( METHOD_CALL $id ( argList )? ) ;
	public final CppParser.functionCall_return functionCall() throws RecognitionException {
		CppParser.functionCall_return retval = new CppParser.functionCall_return();
		retval.start = input.LT(1);
		int functionCall_StartIndex = input.index();

		Object root_0 = null;

		Token id=null;
		Token LPAREN157=null;
		Token RPAREN159=null;
		ParserRuleReturnScope argList158 =null;

		Object id_tree=null;
		Object LPAREN157_tree=null;
		Object RPAREN159_tree=null;
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleSubtreeStream stream_argList=new RewriteRuleSubtreeStream(adaptor,"rule argList");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:244:3: (id= IDENTIFIER LPAREN ( argList )? RPAREN -> ^( METHOD_CALL $id ( argList )? ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:244:5: id= IDENTIFIER LPAREN ( argList )? RPAREN
			{
			id=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_functionCall1816); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(id);

			LPAREN157=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_functionCall1818); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN157);

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:244:26: ( argList )?
			int alt45=2;
			int LA45_0 = input.LA(1);
			if ( (LA45_0==AMPERSAND||LA45_0==CHARACTER_LITERAL||LA45_0==DECIMAL_LITERAL||LA45_0==DEFINED||LA45_0==FLOATING_POINT_LITERAL||LA45_0==HEX_LITERAL||LA45_0==IDENTIFIER||LA45_0==LPAREN||LA45_0==MINUS||LA45_0==MINUSMINUS||LA45_0==NOT||LA45_0==OCTAL_LITERAL||LA45_0==PLUS||LA45_0==PLUSPLUS||LA45_0==SIZEOF||LA45_0==STAR||LA45_0==STRING_LITERAL||LA45_0==TILDE) ) {
				alt45=1;
			}
			switch (alt45) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:244:26: argList
					{
					pushFollow(FOLLOW_argList_in_functionCall1820);
					argList158=argList();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_argList.add(argList158.getTree());
					}
					break;

			}

			RPAREN159=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_functionCall1823); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN159);

			// AST REWRITE
			// elements: argList, id
			// token labels: id
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_id=new RewriteRuleTokenStream(adaptor,"token id",id);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 244:43: -> ^( METHOD_CALL $id ( argList )? )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:244:46: ^( METHOD_CALL $id ( argList )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);
				adaptor.addChild(root_1, stream_id.nextNode());
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:244:64: ( argList )?
				if ( stream_argList.hasNext() ) {
					adaptor.addChild(root_1, stream_argList.nextTree());
				}
				stream_argList.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 31, functionCall_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "functionCall"


	public static class argList_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "argList"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:247:1: argList : assignmentExpression ( COMMA assignmentExpression )* -> ^( ARGS ( assignmentExpression )+ ) ;
	public final CppParser.argList_return argList() throws RecognitionException {
		CppParser.argList_return retval = new CppParser.argList_return();
		retval.start = input.LT(1);
		int argList_StartIndex = input.index();

		Object root_0 = null;

		Token COMMA161=null;
		ParserRuleReturnScope assignmentExpression160 =null;
		ParserRuleReturnScope assignmentExpression162 =null;

		Object COMMA161_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_assignmentExpression=new RewriteRuleSubtreeStream(adaptor,"rule assignmentExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:248:3: ( assignmentExpression ( COMMA assignmentExpression )* -> ^( ARGS ( assignmentExpression )+ ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:248:5: assignmentExpression ( COMMA assignmentExpression )*
			{
			pushFollow(FOLLOW_assignmentExpression_in_argList1849);
			assignmentExpression160=assignmentExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_assignmentExpression.add(assignmentExpression160.getTree());
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:248:26: ( COMMA assignmentExpression )*
			loop46:
			while (true) {
				int alt46=2;
				int LA46_0 = input.LA(1);
				if ( (LA46_0==COMMA) ) {
					alt46=1;
				}

				switch (alt46) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:248:27: COMMA assignmentExpression
					{
					COMMA161=(Token)match(input,COMMA,FOLLOW_COMMA_in_argList1852); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA161);

					pushFollow(FOLLOW_assignmentExpression_in_argList1854);
					assignmentExpression162=assignmentExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_assignmentExpression.add(assignmentExpression162.getTree());
					}
					break;

				default :
					break loop46;
				}
			}

			// AST REWRITE
			// elements: assignmentExpression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 248:56: -> ^( ARGS ( assignmentExpression )+ )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:248:59: ^( ARGS ( assignmentExpression )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ARGS, "ARGS"), root_1);
				if ( !(stream_assignmentExpression.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_assignmentExpression.hasNext() ) {
					adaptor.addChild(root_1, stream_assignmentExpression.nextTree());
				}
				stream_assignmentExpression.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 32, argList_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "argList"


	public static class constant_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "constant"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:251:1: constant : ( HEX_LITERAL | OCTAL_LITERAL | DECIMAL_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | FLOATING_POINT_LITERAL );
	public final CppParser.constant_return constant() throws RecognitionException {
		CppParser.constant_return retval = new CppParser.constant_return();
		retval.start = input.LT(1);
		int constant_StartIndex = input.index();

		Object root_0 = null;

		Token set163=null;

		Object set163_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:252:3: ( HEX_LITERAL | OCTAL_LITERAL | DECIMAL_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | FLOATING_POINT_LITERAL )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:
			{
			root_0 = (Object)adaptor.nil();


			set163=input.LT(1);
			if ( input.LA(1)==CHARACTER_LITERAL||input.LA(1)==DECIMAL_LITERAL||input.LA(1)==FLOATING_POINT_LITERAL||input.LA(1)==HEX_LITERAL||input.LA(1)==OCTAL_LITERAL||input.LA(1)==STRING_LITERAL ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set163));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 33, constant_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "constant"


	public static class source_text_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "source_text"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:262:1: source_text : ( sourceExpression | COMMA | LPAREN | RPAREN | WS );
	public final CppParser.source_text_return source_text() throws RecognitionException {
		CppParser.source_text_return retval = new CppParser.source_text_return();
		retval.start = input.LT(1);
		int source_text_StartIndex = input.index();

		Object root_0 = null;

		Token COMMA165=null;
		Token LPAREN166=null;
		Token RPAREN167=null;
		Token WS168=null;
		ParserRuleReturnScope sourceExpression164 =null;

		Object COMMA165_tree=null;
		Object LPAREN166_tree=null;
		Object RPAREN167_tree=null;
		Object WS168_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:263:3: ( sourceExpression | COMMA | LPAREN | RPAREN | WS )
			int alt47=5;
			switch ( input.LA(1) ) {
			case CHARACTER_LITERAL:
			case CKEYWORD:
			case COPERATOR:
			case DECIMAL_LITERAL:
			case FLOATING_POINT_LITERAL:
			case HEX_LITERAL:
			case IDENTIFIER:
			case OCTAL_LITERAL:
			case SEMICOLON:
			case SHARPSHARP:
			case SIZEOF:
			case STRINGIFICATION:
			case STRING_LITERAL:
			case STRING_OP:
			case TEXT_END:
				{
				alt47=1;
				}
				break;
			case LPAREN:
				{
				int LA47_9 = input.LA(2);
				if ( (synpred94_Cpp()) ) {
					alt47=1;
				}
				else if ( (synpred96_Cpp()) ) {
					alt47=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 47, 9, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case WS:
				{
				int LA47_12 = input.LA(2);
				if ( (synpred94_Cpp()) ) {
					alt47=1;
				}
				else if ( (true) ) {
					alt47=5;
				}

				}
				break;
			case COMMA:
				{
				alt47=2;
				}
				break;
			case RPAREN:
				{
				alt47=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 47, 0, input);
				throw nvae;
			}
			switch (alt47) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:263:7: sourceExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_sourceExpression_in_source_text1936);
					sourceExpression164=sourceExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, sourceExpression164.getTree());

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:264:5: COMMA
					{
					root_0 = (Object)adaptor.nil();


					COMMA165=(Token)match(input,COMMA,FOLLOW_COMMA_in_source_text1942); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					COMMA165_tree = (Object)adaptor.create(COMMA165);
					adaptor.addChild(root_0, COMMA165_tree);
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:265:5: LPAREN
					{
					root_0 = (Object)adaptor.nil();


					LPAREN166=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_source_text1948); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					LPAREN166_tree = (Object)adaptor.create(LPAREN166);
					adaptor.addChild(root_0, LPAREN166_tree);
					}

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:266:5: RPAREN
					{
					root_0 = (Object)adaptor.nil();


					RPAREN167=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_source_text1954); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					RPAREN167_tree = (Object)adaptor.create(RPAREN167);
					adaptor.addChild(root_0, RPAREN167_tree);
					}

					}
					break;
				case 5 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:267:5: WS
					{
					root_0 = (Object)adaptor.nil();


					WS168=(Token)match(input,WS,FOLLOW_WS_in_source_text1960); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					WS168_tree = (Object)adaptor.create(WS168);
					adaptor.addChild(root_0, WS168_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 34, source_text_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "source_text"


	public static class macroExpansion_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "macroExpansion"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:270:1: macroExpansion : (id= IDENTIFIER ( WS )? LPAREN ( WS )? RPAREN -> ^( EXPAND $id) |id= IDENTIFIER ( WS )? LPAREN ( WS )? macArgs ( WS )? RPAREN -> ^( EXPAND $id ( macArgs )? ) );
	public final CppParser.macroExpansion_return macroExpansion() throws RecognitionException {
		CppParser.macroExpansion_return retval = new CppParser.macroExpansion_return();
		retval.start = input.LT(1);
		int macroExpansion_StartIndex = input.index();

		Object root_0 = null;

		Token id=null;
		Token WS169=null;
		Token LPAREN170=null;
		Token WS171=null;
		Token RPAREN172=null;
		Token WS173=null;
		Token LPAREN174=null;
		Token WS175=null;
		Token WS177=null;
		Token RPAREN178=null;
		ParserRuleReturnScope macArgs176 =null;

		Object id_tree=null;
		Object WS169_tree=null;
		Object LPAREN170_tree=null;
		Object WS171_tree=null;
		Object RPAREN172_tree=null;
		Object WS173_tree=null;
		Object LPAREN174_tree=null;
		Object WS175_tree=null;
		Object WS177_tree=null;
		Object RPAREN178_tree=null;
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
		RewriteRuleSubtreeStream stream_macArgs=new RewriteRuleSubtreeStream(adaptor,"rule macArgs");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:3: (id= IDENTIFIER ( WS )? LPAREN ( WS )? RPAREN -> ^( EXPAND $id) |id= IDENTIFIER ( WS )? LPAREN ( WS )? macArgs ( WS )? RPAREN -> ^( EXPAND $id ( macArgs )? ) )
			int alt53=2;
			int LA53_0 = input.LA(1);
			if ( (LA53_0==IDENTIFIER) ) {
				int LA53_1 = input.LA(2);
				if ( (LA53_1==WS) ) {
					int LA53_2 = input.LA(3);
					if ( (LA53_2==LPAREN) ) {
						switch ( input.LA(4) ) {
						case WS:
							{
							int LA53_4 = input.LA(5);
							if ( (LA53_4==RPAREN) ) {
								int LA53_7 = input.LA(6);
								if ( (synpred100_Cpp()) ) {
									alt53=1;
								}
								else if ( (true) ) {
									alt53=2;
								}

							}
							else if ( ((LA53_4 >= CHARACTER_LITERAL && LA53_4 <= CKEYWORD)||LA53_4==COMMA||(LA53_4 >= COPERATOR && LA53_4 <= DECIMAL_LITERAL)||LA53_4==FLOATING_POINT_LITERAL||LA53_4==HEX_LITERAL||LA53_4==IDENTIFIER||LA53_4==LPAREN||LA53_4==OCTAL_LITERAL||(LA53_4 >= SEMICOLON && LA53_4 <= SHARPSHARP)||LA53_4==SIZEOF||(LA53_4 >= STRINGIFICATION && LA53_4 <= TEXT_END)||LA53_4==WS) ) {
								alt53=2;
							}

							else {
								if (state.backtracking>0) {state.failed=true; return retval;}
								int nvaeMark = input.mark();
								try {
									for (int nvaeConsume = 0; nvaeConsume < 5 - 1; nvaeConsume++) {
										input.consume();
									}
									NoViableAltException nvae =
										new NoViableAltException("", 53, 4, input);
									throw nvae;
								} finally {
									input.rewind(nvaeMark);
								}
							}

							}
							break;
						case RPAREN:
							{
							int LA53_5 = input.LA(5);
							if ( (synpred100_Cpp()) ) {
								alt53=1;
							}
							else if ( (true) ) {
								alt53=2;
							}

							}
							break;
						case CHARACTER_LITERAL:
						case CKEYWORD:
						case COMMA:
						case COPERATOR:
						case DECIMAL_LITERAL:
						case FLOATING_POINT_LITERAL:
						case HEX_LITERAL:
						case IDENTIFIER:
						case LPAREN:
						case OCTAL_LITERAL:
						case SEMICOLON:
						case SHARPSHARP:
						case SIZEOF:
						case STRINGIFICATION:
						case STRING_LITERAL:
						case STRING_OP:
						case TEXT_END:
							{
							alt53=2;
							}
							break;
						default:
							if (state.backtracking>0) {state.failed=true; return retval;}
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 53, 3, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 53, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA53_1==LPAREN) ) {
					switch ( input.LA(3) ) {
					case WS:
						{
						int LA53_4 = input.LA(4);
						if ( (LA53_4==RPAREN) ) {
							int LA53_7 = input.LA(5);
							if ( (synpred100_Cpp()) ) {
								alt53=1;
							}
							else if ( (true) ) {
								alt53=2;
							}

						}
						else if ( ((LA53_4 >= CHARACTER_LITERAL && LA53_4 <= CKEYWORD)||LA53_4==COMMA||(LA53_4 >= COPERATOR && LA53_4 <= DECIMAL_LITERAL)||LA53_4==FLOATING_POINT_LITERAL||LA53_4==HEX_LITERAL||LA53_4==IDENTIFIER||LA53_4==LPAREN||LA53_4==OCTAL_LITERAL||(LA53_4 >= SEMICOLON && LA53_4 <= SHARPSHARP)||LA53_4==SIZEOF||(LA53_4 >= STRINGIFICATION && LA53_4 <= TEXT_END)||LA53_4==WS) ) {
							alt53=2;
						}

						else {
							if (state.backtracking>0) {state.failed=true; return retval;}
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 53, 4, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

						}
						break;
					case RPAREN:
						{
						int LA53_5 = input.LA(4);
						if ( (synpred100_Cpp()) ) {
							alt53=1;
						}
						else if ( (true) ) {
							alt53=2;
						}

						}
						break;
					case CHARACTER_LITERAL:
					case CKEYWORD:
					case COMMA:
					case COPERATOR:
					case DECIMAL_LITERAL:
					case FLOATING_POINT_LITERAL:
					case HEX_LITERAL:
					case IDENTIFIER:
					case LPAREN:
					case OCTAL_LITERAL:
					case SEMICOLON:
					case SHARPSHARP:
					case SIZEOF:
					case STRINGIFICATION:
					case STRING_LITERAL:
					case STRING_OP:
					case TEXT_END:
						{
						alt53=2;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 53, 3, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 53, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 53, 0, input);
				throw nvae;
			}

			switch (alt53) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:5: id= IDENTIFIER ( WS )? LPAREN ( WS )? RPAREN
					{
					id=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroExpansion1976); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(id);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:19: ( WS )?
					int alt48=2;
					int LA48_0 = input.LA(1);
					if ( (LA48_0==WS) ) {
						alt48=1;
					}
					switch (alt48) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:19: WS
							{
							WS169=(Token)match(input,WS,FOLLOW_WS_in_macroExpansion1978); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS169);

							}
							break;

					}

					LPAREN170=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_macroExpansion1981); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN170);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:30: ( WS )?
					int alt49=2;
					int LA49_0 = input.LA(1);
					if ( (LA49_0==WS) ) {
						alt49=1;
					}
					switch (alt49) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:30: WS
							{
							WS171=(Token)match(input,WS,FOLLOW_WS_in_macroExpansion1983); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS171);

							}
							break;

					}

					RPAREN172=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_macroExpansion1988); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN172);

					// AST REWRITE
					// elements: id
					// token labels: id
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_id=new RewriteRuleTokenStream(adaptor,"token id",id);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 271:44: -> ^( EXPAND $id)
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:47: ^( EXPAND $id)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPAND, "EXPAND"), root_1);
						adaptor.addChild(root_1, stream_id.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:5: id= IDENTIFIER ( WS )? LPAREN ( WS )? macArgs ( WS )? RPAREN
					{
					id=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_macroExpansion2006); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(id);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:19: ( WS )?
					int alt50=2;
					int LA50_0 = input.LA(1);
					if ( (LA50_0==WS) ) {
						alt50=1;
					}
					switch (alt50) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:19: WS
							{
							WS173=(Token)match(input,WS,FOLLOW_WS_in_macroExpansion2008); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS173);

							}
							break;

					}

					LPAREN174=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_macroExpansion2011); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN174);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:30: ( WS )?
					int alt51=2;
					int LA51_0 = input.LA(1);
					if ( (LA51_0==WS) ) {
						int LA51_1 = input.LA(2);
						if ( (synpred102_Cpp()) ) {
							alt51=1;
						}
					}
					switch (alt51) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:30: WS
							{
							WS175=(Token)match(input,WS,FOLLOW_WS_in_macroExpansion2013); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS175);

							}
							break;

					}

					pushFollow(FOLLOW_macArgs_in_macroExpansion2016);
					macArgs176=macArgs();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_macArgs.add(macArgs176.getTree());
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:43: ( WS )?
					int alt52=2;
					int LA52_0 = input.LA(1);
					if ( (LA52_0==WS) ) {
						alt52=1;
					}
					switch (alt52) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:43: WS
							{
							WS177=(Token)match(input,WS,FOLLOW_WS_in_macroExpansion2019); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS177);

							}
							break;

					}

					RPAREN178=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_macroExpansion2022); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN178);

					// AST REWRITE
					// elements: id, macArgs
					// token labels: id
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_id=new RewriteRuleTokenStream(adaptor,"token id",id);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 272:55: -> ^( EXPAND $id ( macArgs )? )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:58: ^( EXPAND $id ( macArgs )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPAND, "EXPAND"), root_1);
						adaptor.addChild(root_1, stream_id.nextNode());
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:71: ( macArgs )?
						if ( stream_macArgs.hasNext() ) {
							adaptor.addChild(root_1, stream_macArgs.nextTree());
						}
						stream_macArgs.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 35, macroExpansion_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "macroExpansion"


	public static class macArgs_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "macArgs"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:1: macArgs :marg+= mArg ( ( WS )? COMMA ( WS )? marg+= mArg )* -> ^( EXP_ARGS ( $marg)+ ) ;
	public final CppParser.macArgs_return macArgs() throws RecognitionException {
		CppParser.macArgs_return retval = new CppParser.macArgs_return();
		retval.start = input.LT(1);
		int macArgs_StartIndex = input.index();

		Object root_0 = null;

		Token WS179=null;
		Token COMMA180=null;
		Token WS181=null;
		List<Object> list_marg=null;
		RuleReturnScope marg = null;
		Object WS179_tree=null;
		Object COMMA180_tree=null;
		Object WS181_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
		RewriteRuleSubtreeStream stream_mArg=new RewriteRuleSubtreeStream(adaptor,"rule mArg");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:9: (marg+= mArg ( ( WS )? COMMA ( WS )? marg+= mArg )* -> ^( EXP_ARGS ( $marg)+ ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:11: marg+= mArg ( ( WS )? COMMA ( WS )? marg+= mArg )*
			{
			pushFollow(FOLLOW_mArg_in_macArgs2048);
			marg=mArg();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_mArg.add(marg.getTree());
			if (list_marg==null) list_marg=new ArrayList<Object>();
			list_marg.add(marg.getTree());
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:22: ( ( WS )? COMMA ( WS )? marg+= mArg )*
			loop56:
			while (true) {
				int alt56=2;
				int LA56_0 = input.LA(1);
				if ( (LA56_0==WS) ) {
					int LA56_1 = input.LA(2);
					if ( (LA56_1==COMMA) ) {
						alt56=1;
					}

				}
				else if ( (LA56_0==COMMA) ) {
					alt56=1;
				}

				switch (alt56) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:24: ( WS )? COMMA ( WS )? marg+= mArg
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:24: ( WS )?
					int alt54=2;
					int LA54_0 = input.LA(1);
					if ( (LA54_0==WS) ) {
						alt54=1;
					}
					switch (alt54) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:24: WS
							{
							WS179=(Token)match(input,WS,FOLLOW_WS_in_macArgs2052); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS179);

							}
							break;

					}

					COMMA180=(Token)match(input,COMMA,FOLLOW_COMMA_in_macArgs2055); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA180);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:34: ( WS )?
					int alt55=2;
					int LA55_0 = input.LA(1);
					if ( (LA55_0==WS) ) {
						int LA55_1 = input.LA(2);
						if ( (synpred105_Cpp()) ) {
							alt55=1;
						}
					}
					switch (alt55) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:34: WS
							{
							WS181=(Token)match(input,WS,FOLLOW_WS_in_macArgs2057); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS181);

							}
							break;

					}

					pushFollow(FOLLOW_mArg_in_macArgs2062);
					marg=mArg();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_mArg.add(marg.getTree());
					if (list_marg==null) list_marg=new ArrayList<Object>();
					list_marg.add(marg.getTree());
					}
					break;

				default :
					break loop56;
				}
			}

			// AST REWRITE
			// elements: marg
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: marg
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_marg=new RewriteRuleSubtreeStream(adaptor,"token marg",list_marg);
			root_0 = (Object)adaptor.nil();
			// 275:52: -> ^( EXP_ARGS ( $marg)+ )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:55: ^( EXP_ARGS ( $marg)+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXP_ARGS, "EXP_ARGS"), root_1);
				if ( !(stream_marg.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_marg.hasNext() ) {
					adaptor.addChild(root_1, stream_marg.nextTree());
				}
				stream_marg.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 36, macArgs_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "macArgs"


	public static class mArg_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "mArg"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:1: mArg : ( ( sourceExpression )+ -> ^( EXP_ARG ( sourceExpression )+ ) | -> ^( EXP_ARG ) );
	public final CppParser.mArg_return mArg() throws RecognitionException {
		CppParser.mArg_return retval = new CppParser.mArg_return();
		retval.start = input.LT(1);
		int mArg_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope sourceExpression182 =null;

		RewriteRuleSubtreeStream stream_sourceExpression=new RewriteRuleSubtreeStream(adaptor,"rule sourceExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:6: ( ( sourceExpression )+ -> ^( EXP_ARG ( sourceExpression )+ ) | -> ^( EXP_ARG ) )
			int alt58=2;
			switch ( input.LA(1) ) {
			case CHARACTER_LITERAL:
			case CKEYWORD:
			case COPERATOR:
			case DECIMAL_LITERAL:
			case FLOATING_POINT_LITERAL:
			case HEX_LITERAL:
			case IDENTIFIER:
			case LPAREN:
			case OCTAL_LITERAL:
			case SEMICOLON:
			case SHARPSHARP:
			case SIZEOF:
			case STRINGIFICATION:
			case STRING_LITERAL:
			case STRING_OP:
			case TEXT_END:
				{
				alt58=1;
				}
				break;
			case WS:
				{
				int LA58_2 = input.LA(2);
				if ( (synpred108_Cpp()) ) {
					alt58=1;
				}
				else if ( (true) ) {
					alt58=2;
				}

				}
				break;
			case EOF:
			case COMMA:
			case RPAREN:
				{
				alt58=2;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 58, 0, input);
				throw nvae;
			}
			switch (alt58) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: ( sourceExpression )+
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: ( sourceExpression )+
					int cnt57=0;
					loop57:
					while (true) {
						int alt57=2;
						int LA57_0 = input.LA(1);
						if ( (LA57_0==WS) ) {
							int LA57_1 = input.LA(2);
							if ( (synpred107_Cpp()) ) {
								alt57=1;
							}

						}
						else if ( ((LA57_0 >= CHARACTER_LITERAL && LA57_0 <= CKEYWORD)||(LA57_0 >= COPERATOR && LA57_0 <= DECIMAL_LITERAL)||LA57_0==FLOATING_POINT_LITERAL||LA57_0==HEX_LITERAL||LA57_0==IDENTIFIER||LA57_0==LPAREN||LA57_0==OCTAL_LITERAL||(LA57_0 >= SEMICOLON && LA57_0 <= SHARPSHARP)||LA57_0==SIZEOF||(LA57_0 >= STRINGIFICATION && LA57_0 <= TEXT_END)) ) {
							alt57=1;
						}

						switch (alt57) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: sourceExpression
							{
							pushFollow(FOLLOW_sourceExpression_in_mArg2086);
							sourceExpression182=sourceExpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_sourceExpression.add(sourceExpression182.getTree());
							}
							break;

						default :
							if ( cnt57 >= 1 ) break loop57;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(57, input);
							throw eee;
						}
						cnt57++;
					}

					// AST REWRITE
					// elements: sourceExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 278:27: -> ^( EXP_ARG ( sourceExpression )+ )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:30: ^( EXP_ARG ( sourceExpression )+ )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXP_ARG, "EXP_ARG"), root_1);
						if ( !(stream_sourceExpression.hasNext()) ) {
							throw new RewriteEarlyExitException();
						}
						while ( stream_sourceExpression.hasNext() ) {
							adaptor.addChild(root_1, stream_sourceExpression.nextTree());
						}
						stream_sourceExpression.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:279:5: 
					{
					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 279:5: -> ^( EXP_ARG )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:279:8: ^( EXP_ARG )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXP_ARG, "EXP_ARG"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 37, mArg_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "mArg"


	public static class sourceExpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "sourceExpression"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:282:1: sourceExpression : ( ( IDENTIFIER ( WS )? LPAREN )=> macroExpansion | ( primarySource ( WS )? SHARPSHARP )=> concatenate | STRINGIFICATION IDENTIFIER -> ^( STRINGIFICATION IDENTIFIER ) | primarySource | STRING_OP | SIZEOF | LPAREN ( macArgs )? RPAREN -> ^( TEXT_GROUP ( macArgs )? ) | SEMICOLON | TEXT_END | WS );
	public final CppParser.sourceExpression_return sourceExpression() throws RecognitionException {
		CppParser.sourceExpression_return retval = new CppParser.sourceExpression_return();
		retval.start = input.LT(1);
		int sourceExpression_StartIndex = input.index();

		Object root_0 = null;

		Token STRINGIFICATION185=null;
		Token IDENTIFIER186=null;
		Token STRING_OP188=null;
		Token SIZEOF189=null;
		Token LPAREN190=null;
		Token RPAREN192=null;
		Token SEMICOLON193=null;
		Token TEXT_END194=null;
		Token WS195=null;
		ParserRuleReturnScope macroExpansion183 =null;
		ParserRuleReturnScope concatenate184 =null;
		ParserRuleReturnScope primarySource187 =null;
		ParserRuleReturnScope macArgs191 =null;

		Object STRINGIFICATION185_tree=null;
		Object IDENTIFIER186_tree=null;
		Object STRING_OP188_tree=null;
		Object SIZEOF189_tree=null;
		Object LPAREN190_tree=null;
		Object RPAREN192_tree=null;
		Object SEMICOLON193_tree=null;
		Object TEXT_END194_tree=null;
		Object WS195_tree=null;
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_STRINGIFICATION=new RewriteRuleTokenStream(adaptor,"token STRINGIFICATION");
		RewriteRuleSubtreeStream stream_macArgs=new RewriteRuleSubtreeStream(adaptor,"rule macArgs");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:283:3: ( ( IDENTIFIER ( WS )? LPAREN )=> macroExpansion | ( primarySource ( WS )? SHARPSHARP )=> concatenate | STRINGIFICATION IDENTIFIER -> ^( STRINGIFICATION IDENTIFIER ) | primarySource | STRING_OP | SIZEOF | LPAREN ( macArgs )? RPAREN -> ^( TEXT_GROUP ( macArgs )? ) | SEMICOLON | TEXT_END | WS )
			int alt60=10;
			switch ( input.LA(1) ) {
			case IDENTIFIER:
				{
				int LA60_1 = input.LA(2);
				if ( (synpred110_Cpp()) ) {
					alt60=1;
				}
				else if ( (synpred112_Cpp()) ) {
					alt60=2;
				}
				else if ( (synpred114_Cpp()) ) {
					alt60=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 60, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SHARPSHARP:
				{
				int LA60_2 = input.LA(2);
				if ( (synpred112_Cpp()) ) {
					alt60=2;
				}
				else if ( (synpred114_Cpp()) ) {
					alt60=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 60, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case CHARACTER_LITERAL:
			case DECIMAL_LITERAL:
			case FLOATING_POINT_LITERAL:
			case HEX_LITERAL:
			case OCTAL_LITERAL:
			case STRING_LITERAL:
				{
				int LA60_3 = input.LA(2);
				if ( (synpred112_Cpp()) ) {
					alt60=2;
				}
				else if ( (synpred114_Cpp()) ) {
					alt60=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 60, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case CKEYWORD:
				{
				int LA60_4 = input.LA(2);
				if ( (synpred112_Cpp()) ) {
					alt60=2;
				}
				else if ( (synpred114_Cpp()) ) {
					alt60=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 60, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case COPERATOR:
				{
				int LA60_5 = input.LA(2);
				if ( (synpred112_Cpp()) ) {
					alt60=2;
				}
				else if ( (synpred114_Cpp()) ) {
					alt60=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 60, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case STRINGIFICATION:
				{
				alt60=3;
				}
				break;
			case STRING_OP:
				{
				alt60=5;
				}
				break;
			case SIZEOF:
				{
				alt60=6;
				}
				break;
			case LPAREN:
				{
				alt60=7;
				}
				break;
			case SEMICOLON:
				{
				alt60=8;
				}
				break;
			case TEXT_END:
				{
				alt60=9;
				}
				break;
			case WS:
				{
				alt60=10;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 60, 0, input);
				throw nvae;
			}
			switch (alt60) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:283:5: ( IDENTIFIER ( WS )? LPAREN )=> macroExpansion
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_macroExpansion_in_sourceExpression2131);
					macroExpansion183=macroExpansion();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, macroExpansion183.getTree());

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:284:5: ( primarySource ( WS )? SHARPSHARP )=> concatenate
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_concatenate_in_sourceExpression2150);
					concatenate184=concatenate();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, concatenate184.getTree());

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:285:5: STRINGIFICATION IDENTIFIER
					{
					STRINGIFICATION185=(Token)match(input,STRINGIFICATION,FOLLOW_STRINGIFICATION_in_sourceExpression2156); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STRINGIFICATION.add(STRINGIFICATION185);

					IDENTIFIER186=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_sourceExpression2158); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER186);

					// AST REWRITE
					// elements: STRINGIFICATION, IDENTIFIER
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 285:33: -> ^( STRINGIFICATION IDENTIFIER )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:285:36: ^( STRINGIFICATION IDENTIFIER )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_STRINGIFICATION.nextNode(), root_1);
						adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:286:5: primarySource
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_primarySource_in_sourceExpression2173);
					primarySource187=primarySource();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, primarySource187.getTree());

					}
					break;
				case 5 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:287:5: STRING_OP
					{
					root_0 = (Object)adaptor.nil();


					STRING_OP188=(Token)match(input,STRING_OP,FOLLOW_STRING_OP_in_sourceExpression2179); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					STRING_OP188_tree = (Object)adaptor.create(STRING_OP188);
					adaptor.addChild(root_0, STRING_OP188_tree);
					}

					}
					break;
				case 6 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:288:5: SIZEOF
					{
					root_0 = (Object)adaptor.nil();


					SIZEOF189=(Token)match(input,SIZEOF,FOLLOW_SIZEOF_in_sourceExpression2185); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SIZEOF189_tree = (Object)adaptor.create(SIZEOF189);
					adaptor.addChild(root_0, SIZEOF189_tree);
					}

					}
					break;
				case 7 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:289:5: LPAREN ( macArgs )? RPAREN
					{
					LPAREN190=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_sourceExpression2191); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(LPAREN190);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:289:12: ( macArgs )?
					int alt59=2;
					int LA59_0 = input.LA(1);
					if ( ((LA59_0 >= CHARACTER_LITERAL && LA59_0 <= CKEYWORD)||LA59_0==COMMA||(LA59_0 >= COPERATOR && LA59_0 <= DECIMAL_LITERAL)||LA59_0==FLOATING_POINT_LITERAL||LA59_0==HEX_LITERAL||LA59_0==IDENTIFIER||LA59_0==LPAREN||LA59_0==OCTAL_LITERAL||(LA59_0 >= SEMICOLON && LA59_0 <= SHARPSHARP)||LA59_0==SIZEOF||(LA59_0 >= STRINGIFICATION && LA59_0 <= TEXT_END)||LA59_0==WS) ) {
						alt59=1;
					}
					else if ( (LA59_0==RPAREN) ) {
						int LA59_2 = input.LA(2);
						if ( (synpred117_Cpp()) ) {
							alt59=1;
						}
					}
					switch (alt59) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:289:12: macArgs
							{
							pushFollow(FOLLOW_macArgs_in_sourceExpression2193);
							macArgs191=macArgs();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_macArgs.add(macArgs191.getTree());
							}
							break;

					}

					RPAREN192=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_sourceExpression2196); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(RPAREN192);

					// AST REWRITE
					// elements: macArgs
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 289:28: -> ^( TEXT_GROUP ( macArgs )? )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:289:31: ^( TEXT_GROUP ( macArgs )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT_GROUP, "TEXT_GROUP"), root_1);
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:289:44: ( macArgs )?
						if ( stream_macArgs.hasNext() ) {
							adaptor.addChild(root_1, stream_macArgs.nextTree());
						}
						stream_macArgs.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 8 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:290:5: SEMICOLON
					{
					root_0 = (Object)adaptor.nil();


					SEMICOLON193=(Token)match(input,SEMICOLON,FOLLOW_SEMICOLON_in_sourceExpression2211); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SEMICOLON193_tree = (Object)adaptor.create(SEMICOLON193);
					adaptor.addChild(root_0, SEMICOLON193_tree);
					}

					}
					break;
				case 9 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:291:5: TEXT_END
					{
					root_0 = (Object)adaptor.nil();


					TEXT_END194=(Token)match(input,TEXT_END,FOLLOW_TEXT_END_in_sourceExpression2217); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					TEXT_END194_tree = (Object)adaptor.create(TEXT_END194);
					adaptor.addChild(root_0, TEXT_END194_tree);
					}

					}
					break;
				case 10 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:292:5: WS
					{
					root_0 = (Object)adaptor.nil();


					WS195=(Token)match(input,WS,FOLLOW_WS_in_sourceExpression2223); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					WS195_tree = (Object)adaptor.create(WS195);
					adaptor.addChild(root_0, WS195_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 38, sourceExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "sourceExpression"


	public static class concatenate_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "concatenate"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:295:1: concatenate :prim+= primarySource ( ( WS )? SHARPSHARP ( WS )? prim+= primarySource )+ -> ^( CONCATENATE ( $prim)+ ) ;
	public final CppParser.concatenate_return concatenate() throws RecognitionException {
		CppParser.concatenate_return retval = new CppParser.concatenate_return();
		retval.start = input.LT(1);
		int concatenate_StartIndex = input.index();

		Object root_0 = null;

		Token WS196=null;
		Token SHARPSHARP197=null;
		Token WS198=null;
		List<Object> list_prim=null;
		RuleReturnScope prim = null;
		Object WS196_tree=null;
		Object SHARPSHARP197_tree=null;
		Object WS198_tree=null;
		RewriteRuleTokenStream stream_SHARPSHARP=new RewriteRuleTokenStream(adaptor,"token SHARPSHARP");
		RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
		RewriteRuleSubtreeStream stream_primarySource=new RewriteRuleSubtreeStream(adaptor,"rule primarySource");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:3: (prim+= primarySource ( ( WS )? SHARPSHARP ( WS )? prim+= primarySource )+ -> ^( CONCATENATE ( $prim)+ ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:5: prim+= primarySource ( ( WS )? SHARPSHARP ( WS )? prim+= primarySource )+
			{
			pushFollow(FOLLOW_primarySource_in_concatenate2238);
			prim=primarySource();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_primarySource.add(prim.getTree());
			if (list_prim==null) list_prim=new ArrayList<Object>();
			list_prim.add(prim.getTree());
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:25: ( ( WS )? SHARPSHARP ( WS )? prim+= primarySource )+
			int cnt63=0;
			loop63:
			while (true) {
				int alt63=2;
				int LA63_0 = input.LA(1);
				if ( (LA63_0==SHARPSHARP) ) {
					switch ( input.LA(2) ) {
					case WS:
						{
						int LA63_4 = input.LA(3);
						if ( (LA63_4==IDENTIFIER) ) {
							int LA63_7 = input.LA(4);
							if ( (synpred123_Cpp()) ) {
								alt63=1;
							}

						}
						else if ( ((LA63_4 >= CHARACTER_LITERAL && LA63_4 <= CKEYWORD)||(LA63_4 >= COPERATOR && LA63_4 <= DECIMAL_LITERAL)||LA63_4==FLOATING_POINT_LITERAL||LA63_4==HEX_LITERAL||LA63_4==OCTAL_LITERAL||LA63_4==SHARPSHARP||LA63_4==STRING_LITERAL) ) {
							alt63=1;
						}

						}
						break;
					case IDENTIFIER:
						{
						int LA63_5 = input.LA(3);
						if ( (synpred123_Cpp()) ) {
							alt63=1;
						}

						}
						break;
					case CHARACTER_LITERAL:
					case CKEYWORD:
					case COPERATOR:
					case DECIMAL_LITERAL:
					case FLOATING_POINT_LITERAL:
					case HEX_LITERAL:
					case OCTAL_LITERAL:
					case SHARPSHARP:
					case STRING_LITERAL:
						{
						alt63=1;
						}
						break;
					}
				}
				else if ( (LA63_0==WS) ) {
					int LA63_3 = input.LA(2);
					if ( (LA63_3==SHARPSHARP) ) {
						switch ( input.LA(3) ) {
						case WS:
							{
							int LA63_4 = input.LA(4);
							if ( (LA63_4==IDENTIFIER) ) {
								int LA63_7 = input.LA(5);
								if ( (synpred123_Cpp()) ) {
									alt63=1;
								}

							}
							else if ( ((LA63_4 >= CHARACTER_LITERAL && LA63_4 <= CKEYWORD)||(LA63_4 >= COPERATOR && LA63_4 <= DECIMAL_LITERAL)||LA63_4==FLOATING_POINT_LITERAL||LA63_4==HEX_LITERAL||LA63_4==OCTAL_LITERAL||LA63_4==SHARPSHARP||LA63_4==STRING_LITERAL) ) {
								alt63=1;
							}

							}
							break;
						case IDENTIFIER:
							{
							int LA63_5 = input.LA(4);
							if ( (synpred123_Cpp()) ) {
								alt63=1;
							}

							}
							break;
						case CHARACTER_LITERAL:
						case CKEYWORD:
						case COPERATOR:
						case DECIMAL_LITERAL:
						case FLOATING_POINT_LITERAL:
						case HEX_LITERAL:
						case OCTAL_LITERAL:
						case SHARPSHARP:
						case STRING_LITERAL:
							{
							alt63=1;
							}
							break;
						}
					}

				}

				switch (alt63) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:26: ( WS )? SHARPSHARP ( WS )? prim+= primarySource
					{
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:26: ( WS )?
					int alt61=2;
					int LA61_0 = input.LA(1);
					if ( (LA61_0==WS) ) {
						alt61=1;
					}
					switch (alt61) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:26: WS
							{
							WS196=(Token)match(input,WS,FOLLOW_WS_in_concatenate2241); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS196);

							}
							break;

					}

					SHARPSHARP197=(Token)match(input,SHARPSHARP,FOLLOW_SHARPSHARP_in_concatenate2244); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SHARPSHARP.add(SHARPSHARP197);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:42: ( WS )?
					int alt62=2;
					int LA62_0 = input.LA(1);
					if ( (LA62_0==WS) ) {
						alt62=1;
					}
					switch (alt62) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:42: WS
							{
							WS198=(Token)match(input,WS,FOLLOW_WS_in_concatenate2247); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS198);

							}
							break;

					}

					pushFollow(FOLLOW_primarySource_in_concatenate2252);
					prim=primarySource();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_primarySource.add(prim.getTree());
					if (list_prim==null) list_prim=new ArrayList<Object>();
					list_prim.add(prim.getTree());
					}
					break;

				default :
					if ( cnt63 >= 1 ) break loop63;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(63, input);
					throw eee;
				}
				cnt63++;
			}

			// AST REWRITE
			// elements: prim
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: prim
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_prim=new RewriteRuleSubtreeStream(adaptor,"token prim",list_prim);
			root_0 = (Object)adaptor.nil();
			// 296:69: -> ^( CONCATENATE ( $prim)+ )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:72: ^( CONCATENATE ( $prim)+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONCATENATE, "CONCATENATE"), root_1);
				if ( !(stream_prim.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_prim.hasNext() ) {
					adaptor.addChild(root_1, stream_prim.nextTree());
				}
				stream_prim.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 39, concatenate_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "concatenate"


	public static class primarySource_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "primarySource"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:299:1: primarySource : ( SHARPSHARP ( WS )? IDENTIFIER -> ^( SHARPSHARP IDENTIFIER ) | IDENTIFIER | constant | CKEYWORD | COPERATOR );
	public final CppParser.primarySource_return primarySource() throws RecognitionException {
		CppParser.primarySource_return retval = new CppParser.primarySource_return();
		retval.start = input.LT(1);
		int primarySource_StartIndex = input.index();

		Object root_0 = null;

		Token SHARPSHARP199=null;
		Token WS200=null;
		Token IDENTIFIER201=null;
		Token IDENTIFIER202=null;
		Token CKEYWORD204=null;
		Token COPERATOR205=null;
		ParserRuleReturnScope constant203 =null;

		Object SHARPSHARP199_tree=null;
		Object WS200_tree=null;
		Object IDENTIFIER201_tree=null;
		Object IDENTIFIER202_tree=null;
		Object CKEYWORD204_tree=null;
		Object COPERATOR205_tree=null;
		RewriteRuleTokenStream stream_SHARPSHARP=new RewriteRuleTokenStream(adaptor,"token SHARPSHARP");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:300:3: ( SHARPSHARP ( WS )? IDENTIFIER -> ^( SHARPSHARP IDENTIFIER ) | IDENTIFIER | constant | CKEYWORD | COPERATOR )
			int alt65=5;
			switch ( input.LA(1) ) {
			case SHARPSHARP:
				{
				alt65=1;
				}
				break;
			case IDENTIFIER:
				{
				alt65=2;
				}
				break;
			case CHARACTER_LITERAL:
			case DECIMAL_LITERAL:
			case FLOATING_POINT_LITERAL:
			case HEX_LITERAL:
			case OCTAL_LITERAL:
			case STRING_LITERAL:
				{
				alt65=3;
				}
				break;
			case CKEYWORD:
				{
				alt65=4;
				}
				break;
			case COPERATOR:
				{
				alt65=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 65, 0, input);
				throw nvae;
			}
			switch (alt65) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:300:6: SHARPSHARP ( WS )? IDENTIFIER
					{
					SHARPSHARP199=(Token)match(input,SHARPSHARP,FOLLOW_SHARPSHARP_in_primarySource2279); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SHARPSHARP.add(SHARPSHARP199);

					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:300:17: ( WS )?
					int alt64=2;
					int LA64_0 = input.LA(1);
					if ( (LA64_0==WS) ) {
						alt64=1;
					}
					switch (alt64) {
						case 1 :
							// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:300:17: WS
							{
							WS200=(Token)match(input,WS,FOLLOW_WS_in_primarySource2281); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_WS.add(WS200);

							}
							break;

					}

					IDENTIFIER201=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_primarySource2284); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER201);

					// AST REWRITE
					// elements: IDENTIFIER, SHARPSHARP
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 300:32: -> ^( SHARPSHARP IDENTIFIER )
					{
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:300:35: ^( SHARPSHARP IDENTIFIER )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot(stream_SHARPSHARP.nextNode(), root_1);
						adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:301:5: IDENTIFIER
					{
					root_0 = (Object)adaptor.nil();


					IDENTIFIER202=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_primarySource2298); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER202_tree = (Object)adaptor.create(IDENTIFIER202);
					adaptor.addChild(root_0, IDENTIFIER202_tree);
					}

					}
					break;
				case 3 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:302:5: constant
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_constant_in_primarySource2305);
					constant203=constant();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, constant203.getTree());

					}
					break;
				case 4 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:303:5: CKEYWORD
					{
					root_0 = (Object)adaptor.nil();


					CKEYWORD204=(Token)match(input,CKEYWORD,FOLLOW_CKEYWORD_in_primarySource2311); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					CKEYWORD204_tree = (Object)adaptor.create(CKEYWORD204);
					adaptor.addChild(root_0, CKEYWORD204_tree);
					}

					}
					break;
				case 5 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:304:5: COPERATOR
					{
					root_0 = (Object)adaptor.nil();


					COPERATOR205=(Token)match(input,COPERATOR,FOLLOW_COPERATOR_in_primarySource2317); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					COPERATOR205_tree = (Object)adaptor.create(COPERATOR205);
					adaptor.addChild(root_0, COPERATOR205_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 40, primarySource_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "primarySource"


	public static class macro_text_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "macro_text"
	// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:717:1: macro_text : ( source_text )+ -> ^( MACRO_DEFINE ( source_text )+ ) ;
	public final CppParser.macro_text_return macro_text() throws RecognitionException {
		CppParser.macro_text_return retval = new CppParser.macro_text_return();
		retval.start = input.LT(1);
		int macro_text_StartIndex = input.index();

		Object root_0 = null;

		ParserRuleReturnScope source_text206 =null;

		RewriteRuleSubtreeStream stream_source_text=new RewriteRuleSubtreeStream(adaptor,"rule source_text");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return retval; }

			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:718:3: ( ( source_text )+ -> ^( MACRO_DEFINE ( source_text )+ ) )
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:718:5: ( source_text )+
			{
			// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:718:5: ( source_text )+
			int cnt66=0;
			loop66:
			while (true) {
				int alt66=2;
				int LA66_0 = input.LA(1);
				if ( ((LA66_0 >= CHARACTER_LITERAL && LA66_0 <= CKEYWORD)||LA66_0==COMMA||(LA66_0 >= COPERATOR && LA66_0 <= DECIMAL_LITERAL)||LA66_0==FLOATING_POINT_LITERAL||LA66_0==HEX_LITERAL||LA66_0==IDENTIFIER||LA66_0==LPAREN||LA66_0==OCTAL_LITERAL||LA66_0==RPAREN||(LA66_0 >= SEMICOLON && LA66_0 <= SHARPSHARP)||LA66_0==SIZEOF||(LA66_0 >= STRINGIFICATION && LA66_0 <= TEXT_END)||LA66_0==WS) ) {
					alt66=1;
				}

				switch (alt66) {
				case 1 :
					// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:718:5: source_text
					{
					pushFollow(FOLLOW_source_text_in_macro_text4180);
					source_text206=source_text();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_source_text.add(source_text206.getTree());
					}
					break;

				default :
					if ( cnt66 >= 1 ) break loop66;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(66, input);
					throw eee;
				}
				cnt66++;
			}

			// AST REWRITE
			// elements: source_text
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 718:18: -> ^( MACRO_DEFINE ( source_text )+ )
			{
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:718:21: ^( MACRO_DEFINE ( source_text )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MACRO_DEFINE, "MACRO_DEFINE"), root_1);
				if ( !(stream_source_text.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_source_text.hasNext() ) {
					adaptor.addChild(root_1, stream_source_text.nextTree());
				}
				stream_source_text.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 41, macro_text_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "macro_text"

	// $ANTLR start synpred14_Cpp
	public final void synpred14_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:5: ( DEFINE IDENTIFIER LPAREN ( WS )? RPAREN ( macro_text )? )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:5: DEFINE IDENTIFIER LPAREN ( WS )? RPAREN ( macro_text )?
		{
		match(input,DEFINE,FOLLOW_DEFINE_in_synpred14_Cpp350); if (state.failed) return;

		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred14_Cpp353); if (state.failed) return;

		match(input,LPAREN,FOLLOW_LPAREN_in_synpred14_Cpp356); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:32: ( WS )?
		int alt67=2;
		int LA67_0 = input.LA(1);
		if ( (LA67_0==WS) ) {
			alt67=1;
		}
		switch (alt67) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:32: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred14_Cpp358); if (state.failed) return;

				}
				break;

		}

		match(input,RPAREN,FOLLOW_RPAREN_in_synpred14_Cpp361); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:44: ( macro_text )?
		int alt68=2;
		int LA68_0 = input.LA(1);
		if ( ((LA68_0 >= CHARACTER_LITERAL && LA68_0 <= CKEYWORD)||LA68_0==COMMA||(LA68_0 >= COPERATOR && LA68_0 <= DECIMAL_LITERAL)||LA68_0==FLOATING_POINT_LITERAL||LA68_0==HEX_LITERAL||LA68_0==IDENTIFIER||LA68_0==LPAREN||LA68_0==OCTAL_LITERAL||LA68_0==RPAREN||(LA68_0 >= SEMICOLON && LA68_0 <= SHARPSHARP)||LA68_0==SIZEOF||(LA68_0 >= STRINGIFICATION && LA68_0 <= TEXT_END)||LA68_0==WS) ) {
			alt68=1;
		}
		switch (alt68) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:79:44: macro_text
				{
				pushFollow(FOLLOW_macro_text_in_synpred14_Cpp364);
				macro_text();
				state._fsp--;
				if (state.failed) return;

				}
				break;

		}

		}

	}
	// $ANTLR end synpred14_Cpp

	// $ANTLR start synpred22_Cpp
	public final void synpred22_Cpp_fragment() throws RecognitionException {
		Token mac=null;
		List<Object> list_arg=null;
		RuleReturnScope arg = null;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:5: ( DEFINE mac= IDENTIFIER LPAREN ( WS )? (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )? RPAREN ( macro_text )? )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:5: DEFINE mac= IDENTIFIER LPAREN ( WS )? (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )? RPAREN ( macro_text )?
		{
		match(input,DEFINE,FOLLOW_DEFINE_in_synpred22_Cpp384); if (state.failed) return;

		mac=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred22_Cpp388); if (state.failed) return;

		match(input,LPAREN,FOLLOW_LPAREN_in_synpred22_Cpp391); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:35: ( WS )?
		int alt75=2;
		int LA75_0 = input.LA(1);
		if ( (LA75_0==WS) ) {
			alt75=1;
		}
		switch (alt75) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:35: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred22_Cpp393); if (state.failed) return;

				}
				break;

		}

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:40: (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )?
		int alt80=2;
		int LA80_0 = input.LA(1);
		if ( (LA80_0==ELLIPSIS||LA80_0==IDENTIFIER) ) {
			alt80=1;
		}
		switch (alt80) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:41: arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )*
				{
				pushFollow(FOLLOW_macroParam_in_synpred22_Cpp400);
				arg=macroParam();
				state._fsp--;
				if (state.failed) return;

				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:57: ( WS )?
				int alt76=2;
				int LA76_0 = input.LA(1);
				if ( (LA76_0==WS) ) {
					alt76=1;
				}
				switch (alt76) {
					case 1 :
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:57: WS
						{
						match(input,WS,FOLLOW_WS_in_synpred22_Cpp402); if (state.failed) return;

						}
						break;

				}

				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:61: ( COMMA ( WS )* arg+= macroParam ( WS )* )*
				loop79:
				while (true) {
					int alt79=2;
					int LA79_0 = input.LA(1);
					if ( (LA79_0==COMMA) ) {
						alt79=1;
					}

					switch (alt79) {
					case 1 :
						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:62: COMMA ( WS )* arg+= macroParam ( WS )*
						{
						match(input,COMMA,FOLLOW_COMMA_in_synpred22_Cpp406); if (state.failed) return;

						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:68: ( WS )*
						loop77:
						while (true) {
							int alt77=2;
							int LA77_0 = input.LA(1);
							if ( (LA77_0==WS) ) {
								alt77=1;
							}

							switch (alt77) {
							case 1 :
								// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:68: WS
								{
								match(input,WS,FOLLOW_WS_in_synpred22_Cpp408); if (state.failed) return;

								}
								break;

							default :
								break loop77;
							}
						}

						pushFollow(FOLLOW_macroParam_in_synpred22_Cpp413);
						arg=macroParam();
						state._fsp--;
						if (state.failed) return;

						// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:88: ( WS )*
						loop78:
						while (true) {
							int alt78=2;
							int LA78_0 = input.LA(1);
							if ( (LA78_0==WS) ) {
								alt78=1;
							}

							switch (alt78) {
							case 1 :
								// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:80:88: WS
								{
								match(input,WS,FOLLOW_WS_in_synpred22_Cpp415); if (state.failed) return;

								}
								break;

							default :
								break loop78;
							}
						}

						}
						break;

					default :
						break loop79;
					}
				}

				}
				break;

		}

		match(input,RPAREN,FOLLOW_RPAREN_in_synpred22_Cpp426); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:81:11: ( macro_text )?
		int alt81=2;
		int LA81_0 = input.LA(1);
		if ( ((LA81_0 >= CHARACTER_LITERAL && LA81_0 <= CKEYWORD)||LA81_0==COMMA||(LA81_0 >= COPERATOR && LA81_0 <= DECIMAL_LITERAL)||LA81_0==FLOATING_POINT_LITERAL||LA81_0==HEX_LITERAL||LA81_0==IDENTIFIER||LA81_0==LPAREN||LA81_0==OCTAL_LITERAL||LA81_0==RPAREN||(LA81_0 >= SEMICOLON && LA81_0 <= SHARPSHARP)||LA81_0==SIZEOF||(LA81_0 >= STRINGIFICATION && LA81_0 <= TEXT_END)||LA81_0==WS) ) {
			alt81=1;
		}
		switch (alt81) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:81:11: macro_text
				{
				pushFollow(FOLLOW_macro_text_in_synpred22_Cpp428);
				macro_text();
				state._fsp--;
				if (state.failed) return;

				}
				break;

		}

		}

	}
	// $ANTLR end synpred22_Cpp

	// $ANTLR start synpred33_Cpp
	public final void synpred33_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:129:5: ( IDENTIFIER {...}?)
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:129:5: IDENTIFIER {...}?
		{
		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred33_Cpp823); if (state.failed) return;

		if ( !((input.LT(-2).getText().equals("ifndef"))) ) {
			if (state.backtracking>0) {state.failed=true; return;}
			throw new FailedPredicateException(input, "synpred33_Cpp", "input.LT(-2).getText().equals(\"ifndef\")");
		}
		}

	}
	// $ANTLR end synpred33_Cpp

	// $ANTLR start synpred34_Cpp
	public final void synpred34_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:130:5: ( IDENTIFIER {...}?)
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:130:5: IDENTIFIER {...}?
		{
		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred34_Cpp843); if (state.failed) return;

		if ( !((input.LT(-2).getText().equals("ifdef"))) ) {
			if (state.backtracking>0) {state.failed=true; return;}
			throw new FailedPredicateException(input, "synpred34_Cpp", "input.LT(-2).getText().equals(\"ifdef\")");
		}
		}

	}
	// $ANTLR end synpred34_Cpp

	// $ANTLR start synpred67_Cpp
	public final void synpred67_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:207:5: ( SIZEOF unaryExpression )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:207:5: SIZEOF unaryExpression
		{
		match(input,SIZEOF,FOLLOW_SIZEOF_in_synpred67_Cpp1394); if (state.failed) return;

		pushFollow(FOLLOW_unaryExpression_in_synpred67_Cpp1396);
		unaryExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred67_Cpp

	// $ANTLR start synpred68_Cpp
	public final void synpred68_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:208:5: ( SIZEOF LPAREN type_name RPAREN )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:208:5: SIZEOF LPAREN type_name RPAREN
		{
		match(input,SIZEOF,FOLLOW_SIZEOF_in_synpred68_Cpp1410); if (state.failed) return;

		match(input,LPAREN,FOLLOW_LPAREN_in_synpred68_Cpp1412); if (state.failed) return;

		pushFollow(FOLLOW_type_name_in_synpred68_Cpp1414);
		type_name();
		state._fsp--;
		if (state.failed) return;

		match(input,RPAREN,FOLLOW_RPAREN_in_synpred68_Cpp1416); if (state.failed) return;

		}

	}
	// $ANTLR end synpred68_Cpp

	// $ANTLR start synpred77_Cpp
	public final void synpred77_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:221:5: ( LPAREN type_name RPAREN unaryExpression )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:221:5: LPAREN type_name RPAREN unaryExpression
		{
		match(input,LPAREN,FOLLOW_LPAREN_in_synpred77_Cpp1586); if (state.failed) return;

		pushFollow(FOLLOW_type_name_in_synpred77_Cpp1588);
		type_name();
		state._fsp--;
		if (state.failed) return;

		match(input,RPAREN,FOLLOW_RPAREN_in_synpred77_Cpp1590); if (state.failed) return;

		pushFollow(FOLLOW_unaryExpression_in_synpred77_Cpp1593);
		unaryExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred77_Cpp

	// $ANTLR start synpred80_Cpp
	public final void synpred80_Cpp_fragment() throws RecognitionException {
		Token s=null;


		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:229:5: (s= STAR IDENTIFIER )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:229:5: s= STAR IDENTIFIER
		{
		s=(Token)match(input,STAR,FOLLOW_STAR_in_synpred80_Cpp1673); if (state.failed) return;

		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred80_Cpp1678); if (state.failed) return;

		}

	}
	// $ANTLR end synpred80_Cpp

	// $ANTLR start synpred84_Cpp
	public final void synpred84_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:237:7: ( IDENTIFIER LPAREN )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:237:8: IDENTIFIER LPAREN
		{
		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred84_Cpp1741); if (state.failed) return;

		match(input,LPAREN,FOLLOW_LPAREN_in_synpred84_Cpp1743); if (state.failed) return;

		}

	}
	// $ANTLR end synpred84_Cpp

	// $ANTLR start synpred85_Cpp
	public final void synpred85_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:238:7: ( IDENTIFIER )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:238:7: IDENTIFIER
		{
		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred85_Cpp1757); if (state.failed) return;

		}

	}
	// $ANTLR end synpred85_Cpp

	// $ANTLR start synpred94_Cpp
	public final void synpred94_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:263:7: ( sourceExpression )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:263:7: sourceExpression
		{
		pushFollow(FOLLOW_sourceExpression_in_synpred94_Cpp1936);
		sourceExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred94_Cpp

	// $ANTLR start synpred96_Cpp
	public final void synpred96_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:265:5: ( LPAREN )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:265:5: LPAREN
		{
		match(input,LPAREN,FOLLOW_LPAREN_in_synpred96_Cpp1948); if (state.failed) return;

		}

	}
	// $ANTLR end synpred96_Cpp

	// $ANTLR start synpred100_Cpp
	public final void synpred100_Cpp_fragment() throws RecognitionException {
		Token id=null;


		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:5: (id= IDENTIFIER ( WS )? LPAREN ( WS )? RPAREN )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:5: id= IDENTIFIER ( WS )? LPAREN ( WS )? RPAREN
		{
		id=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred100_Cpp1976); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:19: ( WS )?
		int alt82=2;
		int LA82_0 = input.LA(1);
		if ( (LA82_0==WS) ) {
			alt82=1;
		}
		switch (alt82) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:19: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred100_Cpp1978); if (state.failed) return;

				}
				break;

		}

		match(input,LPAREN,FOLLOW_LPAREN_in_synpred100_Cpp1981); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:30: ( WS )?
		int alt83=2;
		int LA83_0 = input.LA(1);
		if ( (LA83_0==WS) ) {
			alt83=1;
		}
		switch (alt83) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:271:30: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred100_Cpp1983); if (state.failed) return;

				}
				break;

		}

		match(input,RPAREN,FOLLOW_RPAREN_in_synpred100_Cpp1988); if (state.failed) return;

		}

	}
	// $ANTLR end synpred100_Cpp

	// $ANTLR start synpred102_Cpp
	public final void synpred102_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:30: ( WS )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:272:30: WS
		{
		match(input,WS,FOLLOW_WS_in_synpred102_Cpp2013); if (state.failed) return;

		}

	}
	// $ANTLR end synpred102_Cpp

	// $ANTLR start synpred105_Cpp
	public final void synpred105_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:34: ( WS )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:275:34: WS
		{
		match(input,WS,FOLLOW_WS_in_synpred105_Cpp2057); if (state.failed) return;

		}

	}
	// $ANTLR end synpred105_Cpp

	// $ANTLR start synpred107_Cpp
	public final void synpred107_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: ( sourceExpression )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: sourceExpression
		{
		pushFollow(FOLLOW_sourceExpression_in_synpred107_Cpp2086);
		sourceExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred107_Cpp

	// $ANTLR start synpred108_Cpp
	public final void synpred108_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: ( ( sourceExpression )+ )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: ( sourceExpression )+
		{
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: ( sourceExpression )+
		int cnt86=0;
		loop86:
		while (true) {
			int alt86=2;
			int LA86_0 = input.LA(1);
			if ( ((LA86_0 >= CHARACTER_LITERAL && LA86_0 <= CKEYWORD)||(LA86_0 >= COPERATOR && LA86_0 <= DECIMAL_LITERAL)||LA86_0==FLOATING_POINT_LITERAL||LA86_0==HEX_LITERAL||LA86_0==IDENTIFIER||LA86_0==LPAREN||LA86_0==OCTAL_LITERAL||(LA86_0 >= SEMICOLON && LA86_0 <= SHARPSHARP)||LA86_0==SIZEOF||(LA86_0 >= STRINGIFICATION && LA86_0 <= TEXT_END)||LA86_0==WS) ) {
				alt86=1;
			}

			switch (alt86) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:278:8: sourceExpression
				{
				pushFollow(FOLLOW_sourceExpression_in_synpred108_Cpp2086);
				sourceExpression();
				state._fsp--;
				if (state.failed) return;

				}
				break;

			default :
				if ( cnt86 >= 1 ) break loop86;
				if (state.backtracking>0) {state.failed=true; return;}
				EarlyExitException eee = new EarlyExitException(86, input);
				throw eee;
			}
			cnt86++;
		}

		}

	}
	// $ANTLR end synpred108_Cpp

	// $ANTLR start synpred110_Cpp
	public final void synpred110_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:283:5: ( IDENTIFIER ( WS )? LPAREN )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:283:6: IDENTIFIER ( WS )? LPAREN
		{
		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred110_Cpp2122); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:283:17: ( WS )?
		int alt87=2;
		int LA87_0 = input.LA(1);
		if ( (LA87_0==WS) ) {
			alt87=1;
		}
		switch (alt87) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:283:17: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred110_Cpp2124); if (state.failed) return;

				}
				break;

		}

		match(input,LPAREN,FOLLOW_LPAREN_in_synpred110_Cpp2127); if (state.failed) return;

		}

	}
	// $ANTLR end synpred110_Cpp

	// $ANTLR start synpred112_Cpp
	public final void synpred112_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:284:5: ( primarySource ( WS )? SHARPSHARP )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:284:6: primarySource ( WS )? SHARPSHARP
		{
		pushFollow(FOLLOW_primarySource_in_synpred112_Cpp2138);
		primarySource();
		state._fsp--;
		if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:284:20: ( WS )?
		int alt88=2;
		int LA88_0 = input.LA(1);
		if ( (LA88_0==WS) ) {
			alt88=1;
		}
		switch (alt88) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:284:20: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred112_Cpp2140); if (state.failed) return;

				}
				break;

		}

		match(input,SHARPSHARP,FOLLOW_SHARPSHARP_in_synpred112_Cpp2143); if (state.failed) return;

		}

	}
	// $ANTLR end synpred112_Cpp

	// $ANTLR start synpred114_Cpp
	public final void synpred114_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:286:5: ( primarySource )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:286:5: primarySource
		{
		pushFollow(FOLLOW_primarySource_in_synpred114_Cpp2173);
		primarySource();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred114_Cpp

	// $ANTLR start synpred117_Cpp
	public final void synpred117_Cpp_fragment() throws RecognitionException {
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:289:12: ( macArgs )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:289:12: macArgs
		{
		pushFollow(FOLLOW_macArgs_in_synpred117_Cpp2193);
		macArgs();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred117_Cpp

	// $ANTLR start synpred123_Cpp
	public final void synpred123_Cpp_fragment() throws RecognitionException {
		List<Object> list_prim=null;
		RuleReturnScope prim = null;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:26: ( ( WS )? SHARPSHARP ( WS )? prim+= primarySource )
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:26: ( WS )? SHARPSHARP ( WS )? prim+= primarySource
		{
		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:26: ( WS )?
		int alt90=2;
		int LA90_0 = input.LA(1);
		if ( (LA90_0==WS) ) {
			alt90=1;
		}
		switch (alt90) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:26: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred123_Cpp2241); if (state.failed) return;

				}
				break;

		}

		match(input,SHARPSHARP,FOLLOW_SHARPSHARP_in_synpred123_Cpp2244); if (state.failed) return;

		// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:42: ( WS )?
		int alt91=2;
		int LA91_0 = input.LA(1);
		if ( (LA91_0==WS) ) {
			alt91=1;
		}
		switch (alt91) {
			case 1 :
				// F:\\java\\workspace\\antrl_compiler\\src\\Cpp\\Cpp.g:296:42: WS
				{
				match(input,WS,FOLLOW_WS_in_synpred123_Cpp2247); if (state.failed) return;

				}
				break;

		}

		pushFollow(FOLLOW_primarySource_in_synpred123_Cpp2252);
		prim=primarySource();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred123_Cpp

	// Delegated rules

	public final boolean synpred108_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred108_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred34_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred34_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred96_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred96_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred77_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred77_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred100_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred100_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred117_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred117_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred94_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred94_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred102_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred102_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred14_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred14_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred68_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred68_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred85_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred85_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred112_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred112_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred123_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred123_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred33_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred33_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred110_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred110_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred84_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred84_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred22_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred22_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred107_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred107_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred114_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred114_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred105_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred105_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred80_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred80_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred67_Cpp() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred67_Cpp_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}


	protected DFA14 dfa14 = new DFA14(this);
	static final String DFA14_eotS =
		"\27\uffff";
	static final String DFA14_eofS =
		"\2\uffff\2\4\1\uffff\1\4\1\uffff\1\4\3\uffff\2\4\2\uffff\4\4\2\uffff\1"+
		"\4\1\uffff";
	static final String DFA14_minS =
		"\1\25\1\64\2\15\1\uffff\1\15\1\0\1\15\1\uffff\1\0\1\uffff\2\15\2\0\4\15"+
		"\2\0\1\15\1\0";
	static final String DFA14_maxS =
		"\1\25\1\64\2\174\1\uffff\1\174\1\0\1\174\1\uffff\1\0\1\uffff\2\174\2\0"+
		"\4\174\2\0\1\174\1\0";
	static final String DFA14_acceptS =
		"\4\uffff\1\3\3\uffff\1\2\1\uffff\1\1\14\uffff";
	static final String DFA14_specialS =
		"\6\uffff\1\5\2\uffff\1\6\3\uffff\1\0\1\1\4\uffff\1\2\1\3\1\uffff\1\4}>";
	static final String[] DFA14_transitionS = {
			"\1\1",
			"\1\2",
			"\2\4\1\uffff\1\4\2\uffff\2\4\26\uffff\1\4\2\uffff\1\4\3\uffff\1\4\1"+
			"\uffff\1\4\16\uffff\1\3\16\uffff\1\4\17\uffff\1\4\2\uffff\2\4\4\uffff"+
			"\1\4\2\uffff\4\4\12\uffff\1\4",
			"\2\4\1\uffff\1\4\2\uffff\2\4\10\uffff\1\10\15\uffff\1\4\2\uffff\1\4"+
			"\3\uffff\1\4\1\uffff\1\7\16\uffff\1\4\16\uffff\1\4\17\uffff\1\6\2\uffff"+
			"\2\4\4\uffff\1\4\2\uffff\4\4\12\uffff\1\5",
			"",
			"\2\4\1\uffff\1\4\2\uffff\2\4\10\uffff\1\10\15\uffff\1\4\2\uffff\1\4"+
			"\3\uffff\1\4\1\uffff\1\7\16\uffff\1\4\16\uffff\1\4\17\uffff\1\11\2\uffff"+
			"\2\4\4\uffff\1\4\2\uffff\4\4\12\uffff\1\4",
			"\1\uffff",
			"\2\4\1\uffff\1\14\2\uffff\2\4\10\uffff\1\10\15\uffff\1\4\2\uffff\1\4"+
			"\3\uffff\1\4\1\uffff\1\4\16\uffff\1\4\16\uffff\1\4\17\uffff\1\15\2\uffff"+
			"\2\4\4\uffff\1\4\2\uffff\4\4\12\uffff\1\13",
			"",
			"\1\uffff",
			"",
			"\2\4\1\uffff\1\14\2\uffff\2\4\26\uffff\1\4\2\uffff\1\4\3\uffff\1\4\1"+
			"\uffff\1\4\16\uffff\1\4\16\uffff\1\4\17\uffff\1\16\2\uffff\2\4\4\uffff"+
			"\1\4\2\uffff\4\4\12\uffff\1\4",
			"\2\4\1\uffff\1\4\2\uffff\2\4\10\uffff\1\10\15\uffff\1\4\2\uffff\1\4"+
			"\3\uffff\1\4\1\uffff\1\20\16\uffff\1\4\16\uffff\1\4\17\uffff\1\4\2\uffff"+
			"\2\4\4\uffff\1\4\2\uffff\4\4\12\uffff\1\17",
			"\1\uffff",
			"\1\uffff",
			"\2\4\1\uffff\1\4\2\uffff\2\4\10\uffff\1\10\15\uffff\1\4\2\uffff\1\4"+
			"\3\uffff\1\4\1\uffff\1\20\16\uffff\1\4\16\uffff\1\4\17\uffff\1\4\2\uffff"+
			"\2\4\4\uffff\1\4\2\uffff\4\4\12\uffff\1\21",
			"\2\4\1\uffff\1\14\2\uffff\2\4\10\uffff\1\10\15\uffff\1\4\2\uffff\1\4"+
			"\3\uffff\1\4\1\uffff\1\4\16\uffff\1\4\16\uffff\1\4\17\uffff\1\23\2\uffff"+
			"\2\4\4\uffff\1\4\2\uffff\4\4\12\uffff\1\22",
			"\2\4\1\uffff\1\4\2\uffff\2\4\10\uffff\1\10\15\uffff\1\4\2\uffff\1\4"+
			"\3\uffff\1\4\1\uffff\1\20\16\uffff\1\4\16\uffff\1\4\17\uffff\1\4\2\uffff"+
			"\2\4\4\uffff\1\4\2\uffff\4\4\12\uffff\1\21",
			"\2\4\1\uffff\1\14\2\uffff\2\4\26\uffff\1\4\2\uffff\1\4\3\uffff\1\4\1"+
			"\uffff\1\4\16\uffff\1\4\16\uffff\1\4\17\uffff\1\24\2\uffff\2\4\4\uffff"+
			"\1\4\2\uffff\4\4\12\uffff\1\25",
			"\1\uffff",
			"\1\uffff",
			"\2\4\1\uffff\1\14\2\uffff\2\4\26\uffff\1\4\2\uffff\1\4\3\uffff\1\4\1"+
			"\uffff\1\4\16\uffff\1\4\16\uffff\1\4\17\uffff\1\26\2\uffff\2\4\4\uffff"+
			"\1\4\2\uffff\4\4\12\uffff\1\25",
			"\1\uffff"
	};

	static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
	static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
	static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
	static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
	static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
	static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
	static final short[][] DFA14_transition;

	static {
		int numStates = DFA14_transitionS.length;
		DFA14_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
		}
	}

	protected class DFA14 extends DFA {

		public DFA14(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 14;
			this.eot = DFA14_eot;
			this.eof = DFA14_eof;
			this.min = DFA14_min;
			this.max = DFA14_max;
			this.accept = DFA14_accept;
			this.special = DFA14_special;
			this.transition = DFA14_transition;
		}
		@Override
		public String getDescription() {
			return "78:1: macroDefine : ( DEFINE IDENTIFIER LPAREN ( WS )? RPAREN ( macro_text )? -> ^( MAC_FUNCTION_OBJECT IDENTIFIER ( macro_text )? ) | DEFINE mac= IDENTIFIER LPAREN ( WS )? (arg+= macroParam ( WS )? ( COMMA ( WS )* arg+= macroParam ( WS )* )* )? RPAREN ( macro_text )? -> ^( MAC_FUNCTION $mac ( $arg)+ ( macro_text )? ) | DEFINE IDENTIFIER ( macro_text )? -> ^( MAC_OBJECT IDENTIFIER ( macro_text )? ) );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			TokenStream input = (TokenStream)_input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA14_13 = input.LA(1);
						 
						int index14_13 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred22_Cpp()) ) {s = 8;}
						else if ( (true) ) {s = 4;}
						 
						input.seek(index14_13);
						if ( s>=0 ) return s;
						break;

					case 1 : 
						int LA14_14 = input.LA(1);
						 
						int index14_14 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred22_Cpp()) ) {s = 8;}
						else if ( (true) ) {s = 4;}
						 
						input.seek(index14_14);
						if ( s>=0 ) return s;
						break;

					case 2 : 
						int LA14_19 = input.LA(1);
						 
						int index14_19 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred22_Cpp()) ) {s = 8;}
						else if ( (true) ) {s = 4;}
						 
						input.seek(index14_19);
						if ( s>=0 ) return s;
						break;

					case 3 : 
						int LA14_20 = input.LA(1);
						 
						int index14_20 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred22_Cpp()) ) {s = 8;}
						else if ( (true) ) {s = 4;}
						 
						input.seek(index14_20);
						if ( s>=0 ) return s;
						break;

					case 4 : 
						int LA14_22 = input.LA(1);
						 
						int index14_22 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred22_Cpp()) ) {s = 8;}
						else if ( (true) ) {s = 4;}
						 
						input.seek(index14_22);
						if ( s>=0 ) return s;
						break;

					case 5 : 
						int LA14_6 = input.LA(1);
						 
						int index14_6 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred14_Cpp()) ) {s = 10;}
						else if ( (synpred22_Cpp()) ) {s = 8;}
						else if ( (true) ) {s = 4;}
						 
						input.seek(index14_6);
						if ( s>=0 ) return s;
						break;

					case 6 : 
						int LA14_9 = input.LA(1);
						 
						int index14_9 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred14_Cpp()) ) {s = 10;}
						else if ( (synpred22_Cpp()) ) {s = 8;}
						else if ( (true) ) {s = 4;}
						 
						input.seek(index14_9);
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 14, _s, input);
			error(nvae);
			throw nvae;
		}
	}

	public static final BitSet FOLLOW_procLine_in_preprocess222 = new BitSet(new long[]{0x0334480A00B96002L,0x1A03C8644004000AL});
	public static final BitSet FOLLOW_DIRECTIVE_in_procLine242 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_text_line_in_procLine249 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_diagnostics_in_procLine256 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_fileInclusion_in_procLine262 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_macroDefine_in_procLine268 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_macroUndef_in_procLine274 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_conditionalCompilation_in_procLine280 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_lineControl_in_procLine286 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_macroExecution_in_procLine293 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_End_in_procLine300 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INCLUDE_in_fileInclusion314 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INCLUDE_EXPAND_in_fileInclusion328 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEFINE_in_macroDefine350 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroDefine353 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_macroDefine356 = new BitSet(new long[]{0x0000000000000000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_macroDefine358 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_macroDefine361 = new BitSet(new long[]{0x0014400000196002L,0x1003C86400040008L});
	public static final BitSet FOLLOW_macro_text_in_macroDefine364 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEFINE_in_macroDefine384 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroDefine388 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_macroDefine391 = new BitSet(new long[]{0x0010000020000000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_macroDefine393 = new BitSet(new long[]{0x0010000020000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_macroParam_in_macroDefine400 = new BitSet(new long[]{0x0000000000010000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_macroDefine402 = new BitSet(new long[]{0x0000000000010000L,0x0000000400000000L});
	public static final BitSet FOLLOW_COMMA_in_macroDefine406 = new BitSet(new long[]{0x0010000020000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_WS_in_macroDefine408 = new BitSet(new long[]{0x0010000020000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_macroParam_in_macroDefine413 = new BitSet(new long[]{0x0000000000010000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_macroDefine415 = new BitSet(new long[]{0x0000000000010000L,0x1000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_macroDefine426 = new BitSet(new long[]{0x0014400000196002L,0x1003C86400040008L});
	public static final BitSet FOLLOW_macro_text_in_macroDefine428 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEFINE_in_macroDefine464 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroDefine466 = new BitSet(new long[]{0x0014400000196002L,0x1003C86400040008L});
	public static final BitSet FOLLOW_macro_text_in_macroDefine468 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroParam504 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_ELLIPSIS_in_macroParam506 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ELLIPSIS_in_macroParam520 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroParam526 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EXEC_MACRO_in_macroExecution539 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_ifexpression_in_macroExecution541 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_UNDEF_in_macroUndef564 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroUndef568 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_conditionalCompilation590 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_ifexpression_in_conditionalCompilation595 = new BitSet(new long[]{0x0334480A00B96000L,0x1A03C8644004000AL});
	public static final BitSet FOLLOW_statement_in_conditionalCompilation600 = new BitSet(new long[]{0x00000000D0000000L});
	public static final BitSet FOLLOW_ELIF_in_conditionalCompilation608 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_ifexpression_in_conditionalCompilation612 = new BitSet(new long[]{0x0334480A00B96000L,0x1A03C8644004000AL});
	public static final BitSet FOLLOW_statement_in_conditionalCompilation617 = new BitSet(new long[]{0x00000000D0000000L});
	public static final BitSet FOLLOW_ELSE_in_conditionalCompilation624 = new BitSet(new long[]{0x0334480A00B96000L,0x1A03C8644004000AL});
	public static final BitSet FOLLOW_statement_in_conditionalCompilation629 = new BitSet(new long[]{0x0000000080000000L});
	public static final BitSet FOLLOW_ENDIF_in_conditionalCompilation633 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LINE_in_lineControl676 = new BitSet(new long[]{0x0000000000100000L});
	public static final BitSet FOLLOW_DECIMAL_LITERAL_in_lineControl680 = new BitSet(new long[]{0x0000000000000002L,0x0000800000000000L});
	public static final BitSet FOLLOW_STRING_LITERAL_in_lineControl685 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WARNING_in_diagnostics718 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ERROR_in_diagnostics730 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRAGMA_in_diagnostics742 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_source_text_in_text_line763 = new BitSet(new long[]{0x0014400000196002L,0x1003C86400040008L});
	public static final BitSet FOLLOW_procLine_in_statement794 = new BitSet(new long[]{0x0334480A00B96002L,0x1A03C8644004000AL});
	public static final BitSet FOLLOW_IDENTIFIER_in_type_name810 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_ifexpression823 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_ifexpression843 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assignmentExpression_in_ifexpression863 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_assignmentExpression884 = new BitSet(new long[]{0x0000000002001582L,0x0020050000409000L});
	public static final BitSet FOLLOW_ASSIGNEQUAL_in_assignmentExpression893 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_TIMESEQUAL_in_assignmentExpression904 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_DIVIDEEQUAL_in_assignmentExpression915 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_MODEQUAL_in_assignmentExpression926 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_PLUSEQUAL_in_assignmentExpression937 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_MINUSEQUAL_in_assignmentExpression948 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_SHIFTLEFTEQUAL_in_assignmentExpression959 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_SHIFTRIGHTEQUAL_in_assignmentExpression970 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_BITWISEANDEQUAL_in_assignmentExpression979 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_BITWISEXOREQUAL_in_assignmentExpression988 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_BITWISEOREQUAL_in_assignmentExpression997 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_assignmentExpression_in_assignmentExpression1009 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_logicalOrExpression_in_conditionalExpression1028 = new BitSet(new long[]{0x0000000000000002L,0x0000000080000000L});
	public static final BitSet FOLLOW_QUESTIONMARK_in_conditionalExpression1036 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_assignmentExpression_in_conditionalExpression1039 = new BitSet(new long[]{0x0000000000008000L});
	public static final BitSet FOLLOW_COLON_in_conditionalExpression1041 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_conditionalExpression_in_conditionalExpression1043 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_logicalAndExpression_in_logicalOrExpression1059 = new BitSet(new long[]{0x0000000000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_OR_in_logicalOrExpression1062 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_logicalAndExpression_in_logicalOrExpression1065 = new BitSet(new long[]{0x0000000000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_inclusiveOrExpression_in_logicalAndExpression1080 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_AND_in_logicalAndExpression1083 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_inclusiveOrExpression_in_logicalAndExpression1086 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression1101 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_BITWISEOR_in_inclusiveOrExpression1104 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression1107 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_andExpression_in_exclusiveOrExpression1122 = new BitSet(new long[]{0x0000000000000802L});
	public static final BitSet FOLLOW_BITWISEXOR_in_exclusiveOrExpression1125 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_andExpression_in_exclusiveOrExpression1128 = new BitSet(new long[]{0x0000000000000802L});
	public static final BitSet FOLLOW_equalityExpression_in_andExpression1143 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_AMPERSAND_in_andExpression1146 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_equalityExpression_in_andExpression1149 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_relationalExpression_in_equalityExpression1164 = new BitSet(new long[]{0x0000000100000002L,0x0000000000020000L});
	public static final BitSet FOLLOW_NOTEQUAL_in_equalityExpression1168 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_EQUAL_in_equalityExpression1173 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_relationalExpression_in_equalityExpression1177 = new BitSet(new long[]{0x0000000100000002L,0x0000000000020000L});
	public static final BitSet FOLLOW_shiftExpression_in_relationalExpression1192 = new BitSet(new long[]{0xC003000000000002L});
	public static final BitSet FOLLOW_LESSTHAN_in_relationalExpression1203 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_GREATERTHAN_in_relationalExpression1213 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_LESSTHANOREQUALTO_in_relationalExpression1223 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_GREATERTHANOREQUALTO_in_relationalExpression1233 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_shiftExpression_in_relationalExpression1247 = new BitSet(new long[]{0xC003000000000002L});
	public static final BitSet FOLLOW_additiveExpression_in_shiftExpression1271 = new BitSet(new long[]{0x0000000000000002L,0x0000028000000000L});
	public static final BitSet FOLLOW_SHIFTLEFT_in_shiftExpression1275 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_SHIFTRIGHT_in_shiftExpression1280 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_additiveExpression_in_shiftExpression1284 = new BitSet(new long[]{0x0000000000000002L,0x0000028000000000L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression1299 = new BitSet(new long[]{0x0000000000000002L,0x0000000000200800L});
	public static final BitSet FOLLOW_PLUS_in_additiveExpression1303 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_MINUS_in_additiveExpression1308 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression1312 = new BitSet(new long[]{0x0000000000000002L,0x0000000000200800L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression1327 = new BitSet(new long[]{0x0000000001000002L,0x0000200000004000L});
	public static final BitSet FOLLOW_STAR_in_multiplicativeExpression1331 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_DIVIDE_in_multiplicativeExpression1336 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_MOD_in_multiplicativeExpression1341 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression1346 = new BitSet(new long[]{0x0000000001000002L,0x0000200000004000L});
	public static final BitSet FOLLOW_PLUSPLUS_in_unaryExpression1361 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression1364 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MINUSMINUS_in_unaryExpression1378 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression1380 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SIZEOF_in_unaryExpression1394 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression1396 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SIZEOF_in_unaryExpression1410 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_unaryExpression1412 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_type_name_in_unaryExpression1414 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_unaryExpression1416 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEFINED_in_unaryExpression1430 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_type_name_in_unaryExpression1432 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEFINED_in_unaryExpression1448 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_unaryExpression1450 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_type_name_in_unaryExpression1452 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_unaryExpression1455 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unaryExpressionNotPlusMinus_in_unaryExpression1468 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_unaryExpressionNotPlusMinus1481 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1485 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TILDE_in_unaryExpressionNotPlusMinus1499 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1502 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_AMPERSAND_in_unaryExpressionNotPlusMinus1517 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1519 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STAR_in_unaryExpressionNotPlusMinus1533 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1536 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MINUS_in_unaryExpressionNotPlusMinus1550 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1553 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PLUS_in_unaryExpressionNotPlusMinus1568 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1572 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_unaryExpressionNotPlusMinus1586 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_type_name_in_unaryExpressionNotPlusMinus1588 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_unaryExpressionNotPlusMinus1590 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus1593 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_postfixExpression_in_unaryExpressionNotPlusMinus1609 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primaryExpression_in_postfixExpression1624 = new BitSet(new long[]{0x0000000004000002L,0x0000200002802010L});
	public static final BitSet FOLLOW_LSQUARE_in_postfixExpression1634 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_assignmentExpression_in_postfixExpression1639 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
	public static final BitSet FOLLOW_RSQUARE_in_postfixExpression1641 = new BitSet(new long[]{0x0000000004000002L,0x0000200002802010L});
	public static final BitSet FOLLOW_DOT_in_postfixExpression1650 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_postfixExpression1658 = new BitSet(new long[]{0x0000000004000002L,0x0000200002802010L});
	public static final BitSet FOLLOW_STAR_in_postfixExpression1673 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_postfixExpression1678 = new BitSet(new long[]{0x0000000004000002L,0x0000200002802010L});
	public static final BitSet FOLLOW_POINTERTO_in_postfixExpression1686 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_postfixExpression1689 = new BitSet(new long[]{0x0000000004000002L,0x0000200002802010L});
	public static final BitSet FOLLOW_PLUSPLUS_in_postfixExpression1699 = new BitSet(new long[]{0x0000000004000002L,0x0000200002802010L});
	public static final BitSet FOLLOW_MINUSMINUS_in_postfixExpression1715 = new BitSet(new long[]{0x0000000004000002L,0x0000200002802010L});
	public static final BitSet FOLLOW_functionCall_in_primaryExpression1749 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_primaryExpression1757 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_constant_in_primaryExpression1771 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_primaryExpression1784 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_assignmentExpression_in_primaryExpression1787 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_primaryExpression1789 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_functionCall1816 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_functionCall1818 = new BitSet(new long[]{0x0014400000502010L,0x0010A80400A52808L});
	public static final BitSet FOLLOW_argList_in_functionCall1820 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_functionCall1823 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assignmentExpression_in_argList1849 = new BitSet(new long[]{0x0000000000010002L});
	public static final BitSet FOLLOW_COMMA_in_argList1852 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_assignmentExpression_in_argList1854 = new BitSet(new long[]{0x0000000000010002L});
	public static final BitSet FOLLOW_sourceExpression_in_source_text1936 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_COMMA_in_source_text1942 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_source_text1948 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_RPAREN_in_source_text1954 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WS_in_source_text1960 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroExpansion1976 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000008L});
	public static final BitSet FOLLOW_WS_in_macroExpansion1978 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_macroExpansion1981 = new BitSet(new long[]{0x0000000000000000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_macroExpansion1983 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_macroExpansion1988 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_macroExpansion2006 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000008L});
	public static final BitSet FOLLOW_WS_in_macroExpansion2008 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_macroExpansion2011 = new BitSet(new long[]{0x0014400000196000L,0x1003C86000040008L});
	public static final BitSet FOLLOW_WS_in_macroExpansion2013 = new BitSet(new long[]{0x0014400000196000L,0x1003C86000040008L});
	public static final BitSet FOLLOW_macArgs_in_macroExpansion2016 = new BitSet(new long[]{0x0000000000000000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_macroExpansion2019 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_macroExpansion2022 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_mArg_in_macArgs2048 = new BitSet(new long[]{0x0000000000010002L,0x1000000000000000L});
	public static final BitSet FOLLOW_WS_in_macArgs2052 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_COMMA_in_macArgs2055 = new BitSet(new long[]{0x0014400000196000L,0x1003C86000040008L});
	public static final BitSet FOLLOW_WS_in_macArgs2057 = new BitSet(new long[]{0x0014400000196000L,0x1003C86000040008L});
	public static final BitSet FOLLOW_mArg_in_macArgs2062 = new BitSet(new long[]{0x0000000000010002L,0x1000000000000000L});
	public static final BitSet FOLLOW_sourceExpression_in_mArg2086 = new BitSet(new long[]{0x0014400000186002L,0x1003C86000040008L});
	public static final BitSet FOLLOW_macroExpansion_in_sourceExpression2131 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_concatenate_in_sourceExpression2150 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRINGIFICATION_in_sourceExpression2156 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_sourceExpression2158 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primarySource_in_sourceExpression2173 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_OP_in_sourceExpression2179 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SIZEOF_in_sourceExpression2185 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_sourceExpression2191 = new BitSet(new long[]{0x0014400000196000L,0x1003C86400040008L});
	public static final BitSet FOLLOW_macArgs_in_sourceExpression2193 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_sourceExpression2196 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMICOLON_in_sourceExpression2211 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TEXT_END_in_sourceExpression2217 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WS_in_sourceExpression2223 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primarySource_in_concatenate2238 = new BitSet(new long[]{0x0000000000000000L,0x1000004000000000L});
	public static final BitSet FOLLOW_WS_in_concatenate2241 = new BitSet(new long[]{0x0000000000000000L,0x0000004000000000L});
	public static final BitSet FOLLOW_SHARPSHARP_in_concatenate2244 = new BitSet(new long[]{0x0014400000186000L,0x1000804000040000L});
	public static final BitSet FOLLOW_WS_in_concatenate2247 = new BitSet(new long[]{0x0014400000186000L,0x0000804000040000L});
	public static final BitSet FOLLOW_primarySource_in_concatenate2252 = new BitSet(new long[]{0x0000000000000002L,0x1000004000000000L});
	public static final BitSet FOLLOW_SHARPSHARP_in_primarySource2279 = new BitSet(new long[]{0x0010000000000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_WS_in_primarySource2281 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_primarySource2284 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_primarySource2298 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_constant_in_primarySource2305 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CKEYWORD_in_primarySource2311 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_COPERATOR_in_primarySource2317 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_source_text_in_macro_text4180 = new BitSet(new long[]{0x0014400000196002L,0x1003C86400040008L});
	public static final BitSet FOLLOW_DEFINE_in_synpred14_Cpp350 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred14_Cpp353 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_synpred14_Cpp356 = new BitSet(new long[]{0x0000000000000000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_synpred14_Cpp358 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_synpred14_Cpp361 = new BitSet(new long[]{0x0014400000196002L,0x1003C86400040008L});
	public static final BitSet FOLLOW_macro_text_in_synpred14_Cpp364 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEFINE_in_synpred22_Cpp384 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred22_Cpp388 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_synpred22_Cpp391 = new BitSet(new long[]{0x0010000020000000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_synpred22_Cpp393 = new BitSet(new long[]{0x0010000020000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_macroParam_in_synpred22_Cpp400 = new BitSet(new long[]{0x0000000000010000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_synpred22_Cpp402 = new BitSet(new long[]{0x0000000000010000L,0x0000000400000000L});
	public static final BitSet FOLLOW_COMMA_in_synpred22_Cpp406 = new BitSet(new long[]{0x0010000020000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_WS_in_synpred22_Cpp408 = new BitSet(new long[]{0x0010000020000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_macroParam_in_synpred22_Cpp413 = new BitSet(new long[]{0x0000000000010000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_synpred22_Cpp415 = new BitSet(new long[]{0x0000000000010000L,0x1000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_synpred22_Cpp426 = new BitSet(new long[]{0x0014400000196002L,0x1003C86400040008L});
	public static final BitSet FOLLOW_macro_text_in_synpred22_Cpp428 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred33_Cpp823 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred34_Cpp843 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SIZEOF_in_synpred67_Cpp1394 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_synpred67_Cpp1396 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SIZEOF_in_synpred68_Cpp1410 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_synpred68_Cpp1412 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_type_name_in_synpred68_Cpp1414 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_synpred68_Cpp1416 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_synpred77_Cpp1586 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_type_name_in_synpred77_Cpp1588 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_synpred77_Cpp1590 = new BitSet(new long[]{0x0014400000502010L,0x0010A80000A52808L});
	public static final BitSet FOLLOW_unaryExpression_in_synpred77_Cpp1593 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STAR_in_synpred80_Cpp1673 = new BitSet(new long[]{0x0010000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred80_Cpp1678 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred84_Cpp1741 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_synpred84_Cpp1743 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred85_Cpp1757 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sourceExpression_in_synpred94_Cpp1936 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_synpred96_Cpp1948 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred100_Cpp1976 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000008L});
	public static final BitSet FOLLOW_WS_in_synpred100_Cpp1978 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_synpred100_Cpp1981 = new BitSet(new long[]{0x0000000000000000L,0x1000000400000000L});
	public static final BitSet FOLLOW_WS_in_synpred100_Cpp1983 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_RPAREN_in_synpred100_Cpp1988 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WS_in_synpred102_Cpp2013 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WS_in_synpred105_Cpp2057 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sourceExpression_in_synpred107_Cpp2086 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sourceExpression_in_synpred108_Cpp2086 = new BitSet(new long[]{0x0014400000186002L,0x1003C86000040008L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred110_Cpp2122 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000008L});
	public static final BitSet FOLLOW_WS_in_synpred110_Cpp2124 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LPAREN_in_synpred110_Cpp2127 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primarySource_in_synpred112_Cpp2138 = new BitSet(new long[]{0x0000000000000000L,0x1000004000000000L});
	public static final BitSet FOLLOW_WS_in_synpred112_Cpp2140 = new BitSet(new long[]{0x0000000000000000L,0x0000004000000000L});
	public static final BitSet FOLLOW_SHARPSHARP_in_synpred112_Cpp2143 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primarySource_in_synpred114_Cpp2173 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_macArgs_in_synpred117_Cpp2193 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WS_in_synpred123_Cpp2241 = new BitSet(new long[]{0x0000000000000000L,0x0000004000000000L});
	public static final BitSet FOLLOW_SHARPSHARP_in_synpred123_Cpp2244 = new BitSet(new long[]{0x0014400000186000L,0x1000804000040000L});
	public static final BitSet FOLLOW_WS_in_synpred123_Cpp2247 = new BitSet(new long[]{0x0014400000186000L,0x0000804000040000L});
	public static final BitSet FOLLOW_primarySource_in_synpred123_Cpp2252 = new BitSet(new long[]{0x0000000000000002L});
}
