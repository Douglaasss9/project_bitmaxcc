package bit.minisys.minicc.pp.internal;

import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.pp.IMiniCCPreProcessor;

/**
 * 
 * @author WYF
 * @version 0.1
 * This is a simple and minimum preprocessor 
 */
public class MiniCCPreProcessor implements IMiniCCPreProcessor {
	/**
	 * this is a demo preprocessor that removes \r\n and single line comments
	 * @param iFile : input file
	 * @param oFile : output file
	 */
	public String run(String iFile){
		PreProcessor pp = new PreProcessor(iFile);
		String oFile = MiniCCUtil.removeAllExt(iFile) + MiniCCCfg.MINICC_PP_OUTPUT_EXT;
//		ANTLRPreProcessor app = new ANTLRPreProcessor();
//		try {
//			app.parseFile(iFile, oFile);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		pp.preProcess(oFile);
		System.out.println("1. PreProcess finished!");
		return oFile;
	}
	
}
