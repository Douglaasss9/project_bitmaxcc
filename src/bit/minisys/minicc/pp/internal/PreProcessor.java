package bit.minisys.minicc.pp.internal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.python.antlr.PythonParser.else_clause_return;

import bit.minisys.minicc.internal.util.MiniCCUtil;

public class PreProcessor {
	String path;
	
	public PreProcessor(String filePath) {
		path = filePath;
	}
	public void preProcess(String output){
		if(!MiniCCUtil.checkFile(path)){
			return;
		}
		String processed = "";
		File file = new File(path);
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "";
			Map<String,String> map = new HashMap<String,String>();
			int i = 0;
			while ((line = reader.readLine()) != null) {
				if(line.isEmpty()){
					continue;
				}
				if(line.charAt(0) == '#' && line.charAt(1) == 'i'){
					continue;
				}
				//删除注释和空白行
				int start_0 = line.indexOf("//");
				int start_1 = line.indexOf("/*");
				int end = line.indexOf("*/");
				if(start_0 == 0){
					continue;
				}
				else if(start_0 > 0 && start_0 > start_1){
					line = line.substring(0, start_0);
				}
				else if(start_1 >= 0 && end >= 0 && end - start_1 >= 2){
					line = line.substring(0, start_1);
				}
				else if(start_1 >=0 && end == -1){
					line = line.substring(0, start_1);
					String l;
					while(true){
						l = reader.readLine();
						end = l.indexOf("*/");
						if(end == -1){
							continue;
						}
						else{
							break;
						}
					}
				}
				//删除注释和空白行结束
				processed += line;
				processed += "\r\n";
			}
			
			reader.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MiniCCUtil.createAndWriteFile(output, processed);

		file = new File(output);
		processed = "";
		try {
			BufferedReader reader_1 = new BufferedReader(new FileReader(file));
			String line = "";
			Map<String,String> map = new HashMap<String,String>();
			while ((line = reader_1.readLine()) != null) {
				//加空格
				for(int i = 0;i < line.length();i ++){
					if(line.charAt(i) == '\"'){
						i ++;
						while(line.charAt(i) != '\"'){
							i ++;
						}
					}
					else if(line.charAt(i) == '\''){
						i ++;
						while(line.charAt(i) != '\''){
							i ++;
						}
					}
					else if(
						line.charAt(i) == '(' || line.charAt(i) == ')' || line.charAt(i) == '{'
						|| line.charAt(i) == '}' || line.charAt(i) == '[' || line.charAt(i) == ']'
						|| line.charAt(i) == '?' || line.charAt(i) == ',' || line.charAt(i) == ':'
						|| line.charAt(i) == '%' || line.charAt(i) == '~' || line.charAt(i) == ';' ||
						(line.charAt(i) == '+' && line.charAt(i + 1) != '+' && line.charAt(i + 1) != '=') ||
						(line.charAt(i) == '-' && line.charAt(i + 1) != '-' && line.charAt(i + 1) != '=' && line.charAt(i + 1) != '>') ||
						(line.charAt(i) == '*' && line.charAt(i + 1) != '=') || 
						(line.charAt(i) == '/' && line.charAt(i + 1) != '=') ||
						(line.charAt(i) == '^' && line.charAt(i + 1) != '=') ||
						(line.charAt(i) == '=' && line.charAt(i + 1) != '=') ||
						(line.charAt(i) == ':' && line.charAt(i + 1) != ':') ||
						(line.charAt(i) == '>' && line.charAt(i + 1) != '=') ||
						(line.charAt(i) == '<' && line.charAt(i + 1) != '=') ||
						(line.charAt(i) == '!' && line.charAt(i + 1) != '=') ||
						(line.charAt(i) == '|' && line.charAt(i + 1) != '|') ||
						(line.charAt(i) == '&' && line.charAt(i + 1) != '&') ||
						(line.charAt(i) == '\\' && line.charAt(i + 1) != '\\') ||
						(line.charAt(i) == '/' && line.charAt(i + 1) != '/'))
					{
						line = line.substring(0,i) + " " + line.charAt(i) + " " + line.substring(i+1);
						i = i + 2;
					}
					else if(
							(line.charAt(i) == '+' && line.charAt(i + 1) == '+') ||
							(line.charAt(i) == '+' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '-' && line.charAt(i + 1) == '-') ||
							(line.charAt(i) == '-' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '-' && line.charAt(i + 1) == '>') ||
							(line.charAt(i) == '/' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '*' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '^' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '>' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '<' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '|' && line.charAt(i + 1) == '|') ||
							(line.charAt(i) == '&' && line.charAt(i + 1) == '&') ||
							(line.charAt(i) == '=' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '!' && line.charAt(i + 1) == '=') ||
							(line.charAt(i) == '\\' && line.charAt(i + 1) == '\\') ||
							(line.charAt(i) == '/' && line.charAt(i + 1) == '/')
							){
						i ++;
						line = line.substring(0,i - 1) + " " + line.substring(i-1,i + 1) + " " + line.substring(i+1);
						i = i + 2;
					}
				}
				//加空格结束
				processed += line;
				processed += "\r\n";
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MiniCCUtil.createAndWriteFile(output, processed);
		
		file = new File(output);
		processed = "";
		try {
			BufferedReader reader_0 = new BufferedReader(new FileReader(file));
			String line = "";
			Map<String,String> map = new HashMap<String,String>();
			while ((line = reader_0.readLine()) != null) {
				//宏替换
				int defindno = line.indexOf("#define");
				String[] strarray = line.split("\\s+");
				if(defindno == 0){
					map.put(strarray[1],strarray[2]);
					//processed += line;
				}
				else{
					for(int i = 0;i < strarray.length;i ++){
						if(map.containsKey(strarray[i])){
							strarray[i] = map.get(strarray[i]);
						}
						processed += strarray[i];
						processed += " ";
					}
					processed += "\r\n";
				}
				//宏替换结束
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MiniCCUtil.createAndWriteFile(output, processed);
		//MiniCCUtil.createAndWriteFile("preprocessed.c", processed);
	}
	
	public static void main(String[] args){
		PreProcessor pp = new PreProcessor(".\\input\\test.c");
		pp.preProcess(".\\input\\test.pp.c");
		System.out.println("PreProcess finished!");
	}
}
