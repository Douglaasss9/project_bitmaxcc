package bit.minisys.minicc.scanner.internal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

import org.antlr.v4.gui.TestRig;
import bit.minisys.minicc.MiniCCCfg;
import bit.minisys.minicc.internal.util.MiniCCUtil;
import bit.minisys.minicc.scanner.IMiniCCScanner;

public class MiniCCScanner implements IMiniCCScanner{

	public  String run(String iFile) throws Exception{
		
		String oFile = MiniCCUtil.removeAllExt(iFile) + MiniCCCfg.MINICC_SCANNER_OUTPUT_EXT;
		
		File f = new File(oFile);
        f.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(f);
        PrintStream printStream = new PrintStream(fileOutputStream);
        System.setOut(printStream);
		String[] arg = {"bit.minisys.minicc.parser.internal.antlr.C","compilationUnit","-tokens",iFile};
    	TestRig testRig = new TestRig(arg);
 		if(arg.length >= 2) {
			testRig.process();
 		}
 		fileOutputStream.close();
 		printStream.close();
 		
 		System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
 		
 		System.out.println("2. LexAnalyse finished!");
		return oFile;
	}
}
