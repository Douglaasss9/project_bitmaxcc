package bit.minisys.minicc.semantic.internal;

public class ErrorId {
	public static Integer VAR_NOT_DEFINED = 1;
	public static Integer VAR_DEFINED_MORE_THAN_ONCE = 2;
	public static Integer BREAK_NOT_IN_LOOP = 3;
	public static Integer FUNC_ARG_NOT_MATCH = 4;
	public static Integer OPND_NOT_MATCH = 5;
	public static Integer ARRAYACCESS_OUT_OF_BOUNDS = 6;
	public static Integer GOTO_TARGER_NOT_EXIST = 7;
	public static Integer FUNC_LACK_RETURN = 8;
	public static Integer FUNC_SCOPE_NOT_GLOBAL = 9;
	public static Integer FUNC_RETURN_NOT_MATCH = 10;
	public static Integer FUNC_DEFINITION_NOT_MATCH_WITH_DECLARATION = 11;
	public static Integer CONTINUE_NOT_IN_LOOP = 12;
	
	public static Integer OTHER_ERRORS =999;
}
