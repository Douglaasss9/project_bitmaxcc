package bit.minisys.minicc.semantic.internal;

import java.util.LinkedList;
import java.util.List;

import org.antlr.v4.parse.ANTLRParser.labeledAlt_return;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.python.antlr.PythonParser.return_stmt_return;

// 记录源代码中的错误信息
public class ErrorMessage {
	public List<String> errors;
	public List<Integer> errorId;
	
	public ErrorMessage() {
		errors = new LinkedList<String>();
		errorId = new LinkedList<Integer>();
	}
	public void add(ParserRuleContext node, String mes) {
		String placeString = searchPlace(node);
		errors.add(placeString+":"+mes);
	}
	public void add(String mes,Integer id) {
		errors.add(mes);
		errorId.add(id);
	}
	
	private String searchPlace(ParserRuleContext node) {
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("Line ");
		sBuilder.append(node.start.getLine());
		sBuilder.append(",Column ");
		sBuilder.append(node.start.getCharPositionInLine());
		return sBuilder.toString();
	}
	
	public void print() {
		for(int i = 0; i<errors.size(); i++) {
			Integer errorIdInteger = errorId.get(i);
			System.out.println("ES"+errorIdInteger+" >> "+errors.get(i));
		}
	}
}
