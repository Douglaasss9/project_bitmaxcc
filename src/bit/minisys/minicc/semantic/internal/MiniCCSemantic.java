package bit.minisys.minicc.semantic.internal;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

import bit.minisys.minicc.parser.ast.ASTCompilationUnit;
import bit.minisys.minicc.semantic.IMiniCCSemantic;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;

public class MiniCCSemantic implements IMiniCCSemantic{

	GlobalSymbolTable globalSym;
	@Override
	public String run(String iFile) throws Exception {
		String oFile = iFile;
		// 1. ����json��ʽ��AST������AST��
		//	  ��ֱ�Ӵӿ���﷨�����׶εõ�AST
		ObjectMapper mapper = new ObjectMapper();
		ASTCompilationUnit program = (ASTCompilationUnit)mapper.readValue(new File(iFile), ASTCompilationUnit.class);
	    
		// 2. һ��ɨ��AST, �������ű�
		globalSym = new GlobalSymbolTable();
		ErrorMessage errors = new ErrorMessage();
		SymbolTableBuilder symbolTableBuilder = new SymbolTableBuilder(globalSym,errors);
		boolean errorFlag = false;
		try {
			program.accept(symbolTableBuilder);
		} catch (Exception e) {
			errorFlag = true;
			e.printStackTrace();
		}
		if(errorFlag || errors.errors.size()>0) {
			System.out.println("errors:");
			System.out.println("------------------------------------");
			errors.print();
			System.out.println("------------------------------------");
		}

		System.out.println("4. Semantic finished!");
		return oFile;
	}

	public GlobalSymbolTable getGlt() {
		return globalSym;
	}
}
