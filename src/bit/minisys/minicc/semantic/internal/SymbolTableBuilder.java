package bit.minisys.minicc.semantic.internal;

import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bit.minisys.minicc.parser.ast.*;
import bit.minisys.minicc.internal.symbol.ArrayType;
import bit.minisys.minicc.internal.symbol.FunctionType;
import bit.minisys.minicc.internal.symbol.GlobalSymbolTable;
import bit.minisys.minicc.internal.symbol.NormalType;
import bit.minisys.minicc.internal.symbol.Symbol;
import bit.minisys.minicc.internal.symbol.SymbolTable;
import bit.minisys.minicc.internal.symbol.Type;
import bit.minisys.minicc.internal.symbol.VariableType;
import bit.minisys.minicc.internal.symbol.Type.BuiltInType;

public class SymbolTableBuilder implements ASTVisitor{

	GlobalSymbolTable global;	// global.globalSymbolTable为全局符号表
	ErrorMessage errors;		// 语义检查错误信息
	SymbolTable currentSym;		// 当前处理的AST节点对应的符号表
	SymbolTable paramSym;	
	// declMap用于记录每个declarator对应的Symbol
	Map<ASTNode, Symbol> declMap = new IdentityHashMap<ASTNode, Symbol>();
	// label出现位置可以在使用之前或者之后，所以遍历完全部程序之后单独检查这些identifier是不是label定义了
	List<ASTIdentifier> labels = new LinkedList<ASTIdentifier>();
	NormalType nowFuncType;		// 记录当前正在处理的FunctionDefine的返回类型，用于return的语义检查
	Integer nowInLoop = 0;		// 当前AST节点所处的循环层数，用于break,continue的语义检查
	Boolean lastIsReturn = false;
	
	public SymbolTableBuilder(GlobalSymbolTable global, ErrorMessage errors) {
		// global中包含BuiltInFunction 和 BuiltInType
		this.global = global;
		this.errors = errors;
		// 开始处理时的符号表为全局
		currentSym = global.globalSymbolTable;
	}
	
	@Override
	public void visit(ASTCompilationUnit program){
		program.scope = global.globalSymbolTable;
		for (ASTNode item : program.items) {
			currentSym = global.globalSymbolTable;
			if(item instanceof ASTDeclaration ) {
				visit((ASTDeclaration)item);
			}else if(item instanceof ASTFunctionDefine) {
				visit((ASTFunctionDefine)item);
			}else {
				errors.add("program's items should be Declaration or FunctionDefine",ErrorId.OTHER_ERRORS);
			}
		}
	}
	
	// function definition node
	@Override
	public void visit(ASTFunctionDefine functionDefine) {
		if(functionDefine == null) return;
		// 函数作用域是全局的
		if(currentSym != global.globalSymbolTable) {
			errors.add("FunctionDefine:function define's scope is not global.",ErrorId.FUNC_SCOPE_NOT_GLOBAL);
			return;
		}
		functionDefine.scope = currentSym;
		// 获取函数返回类型
		Type returnType = getSpecifierType(functionDefine.specifiers);
		// 假定返回类型为原始类型（不包括数组和函数）
		if(returnType == null || !(returnType instanceof NormalType)) {
			errors.add("FunctionDefine:Specifier is illegal.",ErrorId.FUNC_RETURN_NOT_MATCH);
			return;
		}
		// 函数定义对应的declarator应该是functionDeclarator类型，其余不合法
		if(!(functionDefine.declarator instanceof ASTFunctionDeclarator)) {
			errors.add("FunctionDefine:Declarator is illegal.",ErrorId.OTHER_ERRORS);
			return;
		}
		// 在访问functionDeclarator内部时，访问参数创建新的scope，下面的compoundStatement就不用visit，间接访问
		visit(functionDefine.declarator);
		if(declMap.get(functionDefine.declarator) == null) {
			return;
		}
		Type declType = declMap.get(functionDefine.declarator).type;
		String declName = declMap.get(functionDefine.declarator).name;
		if(declType instanceof FunctionType) {
			((FunctionType) declType).setReturnType((NormalType)returnType);
			//在这里判断是不是符号表里有了这个函数
			Symbol funcSymbol = currentSym.get(declName);
			if(funcSymbol!=null) {
				// 如果有 ,首先判断是不是这个函数定义对应的声明
				// 需要检查是不是对该声明的实现（返回类型 参数个数 类别 是否已定义过）
				if(funcSymbol.type instanceof FunctionType && ((FunctionType)funcSymbol.type).isDefined) {
					errors.add("FunctionDefine:"+declName+" is defined.", ErrorId.VAR_DEFINED_MORE_THAN_ONCE);
					return;
				}
				if(!((FunctionType) declType).isSameType(funcSymbol.type)) {
					errors.add("FunctionDefine:"+declName+" is not matched with the declaration.",ErrorId.FUNC_DEFINITION_NOT_MATCH_WITH_DECLARATION);
					return;
				}
				// 设置该函数已被实现过，即不允许再次定义
				((FunctionType) declType).setIsDefined(true);
				((FunctionType) declType).setIsDeclarated(true);
				// 更新符号表的信息
				currentSym.update(declName, (FunctionType)declType);
				currentSym.deleteSon(declName);
			}else {
				// 如果没有 加入符号表
				// 用一个变量记录符号表里这个符号是函数定义来的，防止重复定义发生
				((FunctionType) declType).setIsDefined(true);
				currentSym.define(declName, (FunctionType)declType);
			}

		}else {
			errors.add("FunctionDefine:declator must be a funcitonDeclarator",ErrorId.OTHER_ERRORS);
			return;
		}
		// 记录当前正在处理的函数类型信息，用于return的语义检查
		nowFuncType = (NormalType)returnType;
		currentSym = paramSym;	// 设置和参数同样的scope
		// 这里对compound部分不直接visit，因为compound会再次新建作用域，所以直接在此处调用compound中的blockItem
		for (ASTNode item : functionDefine.body.blockItems) {
			if(item instanceof ASTStatement) {
				visit((ASTStatement)item);
			}else if(item instanceof ASTDeclaration) {
				visit((ASTDeclaration)item);
			}
		}
		// 检查label是不是都定义了
		String labelError = currentSym.checkLabel(labels);
		labels.clear();
		if(labelError != null) {
			errors.add("Label: "+labelError+" is not defined.",ErrorId.GOTO_TARGER_NOT_EXIST);
			return;
		}
		// 检查是否有return语句
		if (!lastIsReturn) {
			errors.add("Function:"+declName+" must have a return in the end.",ErrorId.FUNC_LACK_RETURN);
			return;
		}
		lastIsReturn = false;
		// 恢复成全局作用域
		currentSym = global.globalSymbolTable;
	}
	
	// declaration node
	@Override
	public void visit(ASTDeclaration declaration) {
		if(declaration == null)return;
		declaration.scope = currentSym;
		// 获取specifiers
		Type paramType = getSpecifierType(declaration.specifiers);
		if(paramType == null) {
			errors.add("Declaration:paramType is illegal.",ErrorId.OTHER_ERRORS);
			return;
		}
		// 处理声明列表
		for (ASTInitList initList : declaration.initLists) {
			visit(initList);
			
			Symbol symbol = declMap.get(initList);
			if(symbol == null) return;
			String name = symbol.name;
			// 从当前作用域的符号表查找，如果没有则可以定义
			Symbol symbolInTable = currentSym.getCurrentSymbol(name);
			// 如果已经存在了这个符号，并且是函数类型，而且函数没有被声明（说明是定义的），并且新符号也是函数类型
			if(symbolInTable!=null && symbolInTable.type.isFunc() && ((FunctionType) symbolInTable.type).isDeclarated ==false&& symbol.type instanceof FunctionType) {
				((FunctionType)symbol.type).setReturnType((NormalType)paramType);
				if(!((FunctionType) symbolInTable.type).isSameType(symbol.type)) {
					errors.add("Declaration:FunctionDeclaration is not same as the defination before.",ErrorId.FUNC_DEFINITION_NOT_MATCH_WITH_DECLARATION);
					return;
				}else {
					// 否则说明是已经有了声明对应的定义，这一次的声明是合法的
					((FunctionType)symbol.type).setIsDeclarated(true);
					currentSym.deleteSon(paramSym);
				}
				
			}else if(symbolInTable!=null) {
				errors.add("Declaration:"+name+" has been declarated.",ErrorId.VAR_DEFINED_MORE_THAN_ONCE);
				return;
			}
			
			if(symbol.type instanceof NormalType) {
				currentSym.define(symbol.name, paramType);
			}else if (symbol.type instanceof ArrayType) {
				((ArrayType)symbol.type).setElementType(paramType);
				currentSym.define(symbol.name, symbol.type);
			}else if (symbol.type instanceof FunctionType) {
				((FunctionType)symbol.type).setReturnType((NormalType)paramType);
				((FunctionType)symbol.type).setIsDeclarated(true);
				currentSym.define(symbol.name, (FunctionType)symbol.type);
				// 对于先声明后定义，因为先前的声明已经生成了一个子scope，需要记录这个映射关系
				// 定义的时候将其删掉
				currentSym.addForDelete(symbol.name, paramSym);
			}
		}

	}
	@Override
	public void visit(ASTDeclarator declarator) {
		if(declarator == null)
			return;
		if(declarator instanceof ASTArrayDeclarator) {
			visit((ASTArrayDeclarator)declarator);
		}else if(declarator instanceof ASTVariableDeclarator) {
			visit((ASTVariableDeclarator)declarator);
		}else if(declarator instanceof ASTFunctionDeclarator) {
			visit((ASTFunctionDeclarator)declarator);
		}else {
			errors.add("declarator is illegal.",ErrorId.OTHER_ERRORS);
		}
	}
	@Override
	public void visit(ASTArrayDeclarator arrayDeclarator) {
		if(arrayDeclarator == null) return;
		arrayDeclarator.scope = currentSym;
		if(arrayDeclarator.declarator instanceof ASTFunctionDeclarator) {
			errors.add("ArrayDeclarator:Array's Declarator can not be a functionDeclarator.",ErrorId.OTHER_ERRORS);
			return;
		}
		visit(arrayDeclarator.declarator);
		visit(arrayDeclarator.expr);

		// 首先从DeclMap中取 出declarator对应的symbol
		Symbol reSymbol = null;
		if(arrayDeclarator.declarator == null) {
			// 说明是抽象数组声明 : 譬如int []的[]部分
			reSymbol = new Symbol("", new ArrayType(1, arrayDeclarator.expr));
			declMap.put(arrayDeclarator, reSymbol);
			return;
		}
		
		Symbol symbol = declMap.get(arrayDeclarator.declarator);
		if(symbol == null) {
			errors.add("ArrayDeclarator:Array's declarator can not be null.",ErrorId.OTHER_ERRORS);
			return;
		}
		// 判断symbol类型 ： variable, array, func
		Type type = symbol.type;
		if(type instanceof NormalType) {
			// variable, 则这个arraydeclarator是个1维数组
			// 从expr中获取参数，一同构造ArraySymbol (其实是构造arrayType)
			ArrayType arrayType = new ArrayType(1,arrayDeclarator.expr);
			reSymbol = new Symbol(symbol.name, arrayType);
		}else if(type instanceof ArrayType) {
			ArrayType arrayType = new ArrayType(((ArrayType) type).dimension+1, ((ArrayType) type).dimens,arrayDeclarator.expr);
			reSymbol = new Symbol(symbol.name, arrayType);
			// array,多维数组
		}else if(type instanceof FunctionType) {
			// func 函数数组， 非法，收集错误信息返回, 前面已经记录过了，可以删掉
			errors.add("Function Array is illegal.",ErrorId.OTHER_ERRORS);
			return;
		}
		// 把构造好的symbol放到declmap，提供给上层节点调用
		declMap.put(arrayDeclarator, reSymbol);
	}
	@Override
	public void visit(ASTVariableDeclarator variableDeclarator) {
		if(variableDeclarator == null)return;
		// 里面只有一个identifier
		variableDeclarator.scope = currentSym;
		declMap.put(variableDeclarator, new Symbol(variableDeclarator.identifier.value,GlobalSymbolTable.unknownType));
	}
	@Override
	public void visit(ASTFunctionDeclarator functionDeclarator) {
		if(functionDeclarator == null) return;
		functionDeclarator.scope = currentSym;
		if(currentSym != global.globalSymbolTable) {
			errors.add("FunctionDeclarator:must be a global declator.",ErrorId.OTHER_ERRORS);
			return;
		}
		if(!(functionDeclarator.declarator instanceof ASTVariableDeclarator)) {
			errors.add("FunctionDeclator:function name should be identifier.",ErrorId.OTHER_ERRORS);
			return;
		}
		
		visit(functionDeclarator.declarator);
		Symbol symbol = declMap.get(functionDeclarator.declarator);
		if(symbol == null) return;

		// 获取declator的结果，得到name
		String funcname = symbol.name;
		FunctionType functionType = new FunctionType();
		functionType.setFuncname(funcname);
		// 处理参数 params:
		if(functionDeclarator.params != null) {
			// 函数参数的作用域和函数体保持一直，用paramSym记录
			currentSym = new SymbolTable(currentSym);
			paramSym = currentSym;
			for (ASTParamsDeclarator param : functionDeclarator.params) {
				param.scope = currentSym;
				// 获取参数的specifiers
				Type paramType = getSpecifierType(param.specfiers);
				// 获取参数的名字
				String paramName = "";
				if(param.declarator!=null) {
					if(param.declarator instanceof ASTFunctionDeclarator) {
						errors.add("FunctionDeclarator:function's declarator can not be a functionDeclarator either.",ErrorId.OTHER_ERRORS);
						return;
					}
					visit(param.declarator);
					Symbol paramSymbol = declMap.get(param.declarator);
					if(paramSymbol == null) return;
					paramName =paramSymbol.name;
					if(paramSymbol.type.isArray()) {
						// 如果参数是数组类型
						Type tmpType = paramType;
						paramType = paramSymbol.type;
						// 设置数组的元素类型
						((ArrayType)paramType).setElementType(tmpType);
					}
					// 在参数列表的scope里定义符号，防止参数重复的问题
					if(paramName != "") {
						if(currentSym.getCurrentSymbol(paramName)!=null) {
							errors.add("Params in FunctionDeclaration should not be the same",ErrorId.OTHER_ERRORS);
							return;
						}
						currentSym.define(paramName, paramType);
					}
					
				}
				//构建（不完全的functionSymbol）
				functionType.addParamType((VariableType)paramType);
				functionType.addParamName(paramName);
			}
			// 回到原作用域
			currentSym = currentSym.getEnclosingScope();
		}
		declMap.put(functionDeclarator, new Symbol(funcname,functionType));
	}
	@Override
	public void visit(ASTParamsDeclarator paramsDeclarator) {
		// 已经在父层节点直接处理了
	}
	@Override
	public void visit(ASTInitList initList) {
		if(initList == null) return;
		initList.scope = currentSym;
		visit(initList.declarator);
		Symbol declSymbol = declMap.get(initList.declarator);
		if(declSymbol == null) return ;
		Type declType =declSymbol.type;
		// 这里判断a = 1,2;这种给单变量赋值多个值
		if (initList.exprs != null) {
			if(declType instanceof NormalType && initList.exprs.size() > 1) {
				errors.add("InitList:a normalType lvalue has one value at most.",ErrorId.OTHER_ERRORS);
				return;
			}else if (declType instanceof FunctionType && initList.exprs.size()>0) {
				errors.add("InitList:a functionType declator is not a lvalue.",ErrorId.OTHER_ERRORS);
				return;
			}else if(declType instanceof ArrayType) {
				// 数组类型的赋值，因为AST设计没有考虑{{}, {}}诸如此类奇怪的赋值规则，此处不进行语义检查
			}
			// 这里并没有把初始值填入符号表，而是放在了IR生成的一遍扫描中完成
			// 此处只检查对应expr的语义
			for (ASTExpression expr : initList.exprs) {
				visit(expr);
			}
			//initList.exprs.stream().forEachOrdered(this::visit);	
		}
		

		declMap.put(initList, declSymbol);
	}

	// Expression node
	@Override
	public void visit(ASTExpression expression) {
		if(expression == null)
			return;
		if(expression instanceof ASTArrayAccess) {
			visit((ASTArrayAccess)expression);
		}else if(expression instanceof ASTBinaryExpression) {
			visit((ASTBinaryExpression)expression);
		}else if(expression instanceof ASTCastExpression) {
			visit((ASTCastExpression)expression);
		}else if(expression instanceof ASTCharConstant) {
			visit((ASTCharConstant)expression);
		}else if(expression instanceof ASTConditionExpression) {
			visit((ASTConditionExpression)expression);
		}else if(expression instanceof ASTFloatConstant) {
			visit((ASTFloatConstant)expression);
		}else if(expression instanceof ASTFunctionCall) {
			visit((ASTFunctionCall)expression);
		}else if(expression instanceof ASTIdentifier) {
			visit((ASTIdentifier)expression);
		}else if(expression instanceof ASTIntegerConstant) {
			visit((ASTIntegerConstant)expression);
		}else if(expression instanceof ASTMemberAccess) {
			visit((ASTMemberAccess)expression);
		}else if(expression instanceof ASTPostfixExpression) {
			visit((ASTPostfixExpression)expression);
		}else if(expression instanceof ASTStringConstant) {
			visit((ASTStringConstant)expression);
		}else if(expression instanceof ASTUnaryExpression) {
			visit((ASTUnaryExpression)expression);
		}else if(expression instanceof ASTUnaryTypename){
			visit((ASTUnaryTypename)expression);
		}
	}
	@Override
	public void visit(ASTArrayAccess arrayAccess) {
		if(arrayAccess == null)return;
		arrayAccess.scope = currentSym;
		List<ASTExpression> tmpExprs = new LinkedList<ASTExpression>();
		int tmpDimens = 0;
		ASTArrayAccess tmpArrayAccess = arrayAccess;
		while(tmpArrayAccess instanceof ASTArrayAccess) {
			if(tmpArrayAccess.elements.size() == 0) {
				errors.add("ArrayAccess:"+"element's index can not be empty.",ErrorId.OTHER_ERRORS);
				return;
			}
			ASTExpression lastExpr = tmpArrayAccess.elements.get(tmpArrayAccess.elements.size()-1);
			tmpExprs.add(lastExpr);
			tmpDimens++;
			if(tmpArrayAccess.arrayName instanceof ASTArrayAccess)
				tmpArrayAccess = (ASTArrayAccess)tmpArrayAccess.arrayName;
			else
				break;
		}
		if(!(tmpArrayAccess.arrayName instanceof ASTIdentifier)) {
			errors.add("ArrayAccess:"+"array name shoule be a identifier.",ErrorId.OTHER_ERRORS);
			return;
		}
		visit(tmpArrayAccess.arrayName);
		for (ASTExpression expr : tmpExprs) {
			visit(expr);
			if(expr.type.getType() != BuiltInType.INT) {
				errors.add("ArrayAccess error:array's element should be Int.",ErrorId.OTHER_ERRORS);
				return;
			}
		}
		// 如果维数和记录的数组维数不一致，则该arrayaccess的type也不同
		Symbol arrSym = currentSym.get(((ASTIdentifier)tmpArrayAccess.arrayName).value);
		ArrayType arrayType = (ArrayType)arrSym.type;
		Integer dimens = arrayType.getDimenson();
		if(tmpDimens == 0 || tmpDimens > dimens ) {
			errors.add("ArrayAccess:dimension error.",ErrorId.OTHER_ERRORS);
			return;
		}
		if (!arrayType.checkOutofBound(tmpExprs)) {
			errors.add("ArrayAccess:Out of Bounds.",ErrorId.ARRAYACCESS_OUT_OF_BOUNDS);
			return;
		}
		if(dimens == tmpDimens) {
			arrayAccess.type = arrayType.elementType;
			arrayAccess.canAssigned = true;
		}else {
			arrayAccess.type = new ArrayType(dimens-tmpDimens,arrayType.getBefore(dimens-tmpDimens));
			arrayAccess.canAssigned = false;
		}
	}
	private static final int EQUAL_OP = 1;
	private static final int LOGIC_OP = 2;
	private static final int LOGIC_EQUAL_OP = 3;
	private static final int COMPARE_OP = 4;
	private static final int ALGO_OP = 5;
	private static final int BIT_OP = 6;
	@Override
	public void visit(ASTBinaryExpression binaryExpression) {
		if(binaryExpression == null) return;
		binaryExpression.scope = currentSym;
		ASTExpression expr1 = binaryExpression.expr1;
		ASTExpression expr2 = binaryExpression.expr2;
		visit(expr1);
		visit(expr2);
		if(expr1.type == null || expr2.type == null) {
		//	errors.add("Expression:type is null.",ErrorId.OTHER_ERRORS);
			return;
		}
		if(!binaryExpression.expr1.type.ifMatchType(binaryExpression.expr2.type)) {
			errors.add("BinaryExpression:two exprs don't match.",ErrorId.OPND_NOT_MATCH);
			return;
		}
		String op = binaryExpression.op.value;
		int opValue = getOpValue(op);

		switch(opValue) {
		case EQUAL_OP:
			if(!expr1.canAssigned) {
				errors.add("AssignExpression:The Left Expression can not be assigned.",ErrorId.OPND_NOT_MATCH);
				return;
			}
			binaryExpression.canAssigned = true;				// 支持连续赋值
			binaryExpression.type = expr1.type.getFirstType(expr2.type);
			if(binaryExpression.type == null) {
				errors.add("BinaryExpression:expression's type doesn't match.",ErrorId.OPND_NOT_MATCH);
			}
			return;
		case LOGIC_OP:
			binaryExpression.canAssigned = false;
			binaryExpression.type = GlobalSymbolTable.intType;			// C语言没有bool,对应类型设置为int
			return;
		case LOGIC_EQUAL_OP:
			binaryExpression.canAssigned = false;
			binaryExpression.type = GlobalSymbolTable.intType;			
			return;
		case COMPARE_OP:
			binaryExpression.canAssigned = false;
			binaryExpression.type = GlobalSymbolTable.intType;		
			return;
		case ALGO_OP:
			binaryExpression.canAssigned = false;
			binaryExpression.type = expr1.type.getFirstType(expr2.type);
			if(binaryExpression.type == null) {
				errors.add("BinaryExpression:(+/-/*///%)expression's type doesn't match.",ErrorId.OPND_NOT_MATCH);
			}
			return;
		case BIT_OP:
			if(!expr1.type.ifGeneralizedInt() || !expr2.type.ifGeneralizedInt()) {
				errors.add("BinaryExpression:(<< >> & | ^)expression's should be int.",ErrorId.OPND_NOT_MATCH);
				return;
			}
			binaryExpression.canAssigned = false;
			binaryExpression.type = expr1.type.getFirstType(expr2.type);
			return;
		default:
			errors.add("BinaryExpression:op"+op+" is not defined.",ErrorId.OTHER_ERRORS);
			return;
		}
	}
	@Override
	public void visit(ASTCastExpression castExpression) {
		castExpression.scope = currentSym;
		if(!dealWithSpecifiers(castExpression.typename.specfiers)) {
			errors.add("Typename:Specifiers are illegal.",ErrorId.OTHER_ERRORS);
			return;
		}
		// 目前只处理简单类型的转换，数组不考虑
		if(castExpression.typename.declarator != null) {
			errors.add("CastExpression:Declarator should be PrimaryType.",ErrorId.OTHER_ERRORS);
			return;
		}
		visit(castExpression.expr);
		if(castExpression.expr.type == null || !castExpression.expr.type.isNormal()) {
			errors.add("CastExpression:old expression's type is illgeal.",ErrorId.OTHER_ERRORS);
			return;
		}
		castExpression.canAssigned = false;
		castExpression.type = getSpecifierType(castExpression.typename.specfiers);
		if(castExpression.type == null) {
			errors.add("CastTypename: Type is illegal.",ErrorId.OTHER_ERRORS);
			return ;
		}
	}
	@Override
	public void visit(ASTConditionExpression conditionExpression) {
		if(conditionExpression == null) return;
		conditionExpression.scope = currentSym;
		if(!(conditionExpression.condExpr instanceof ASTBinaryExpression)) {
			errors.add("ConditionExpression:CondExpr is illegal.",ErrorId.OTHER_ERRORS);
			return;
		}
		visit(conditionExpression.condExpr);
		if(conditionExpression.condExpr.type == null)
			return;
		for (ASTExpression expr : conditionExpression.trueExpr) {
			visit(expr);
		}
	//	conditionExpression.trueExpr.stream().forEachOrdered(this::visit);
		for (ASTExpression expression : conditionExpression.trueExpr) {
			Type type = expression.type;
			if(type == null)
				return;
		}
		visit(conditionExpression.falseExpr);
		if(conditionExpression.falseExpr.type == null)
			return;
		conditionExpression.canAssigned = false;
		ASTExpression lastExpression = conditionExpression.trueExpr.getLast();
		// expr_1 ? exprs : expr_2 , 这个条件表达式的类型 这里取exprs[-1]和expr_2的type中精度较高的一个
		Type resType = lastExpression.type.getFirstType(conditionExpression.falseExpr.type);
		if(resType == null) {
			errors.add("ConditionExpression:Expression's type is illegal.",ErrorId.OTHER_ERRORS);
			return;
		}
		conditionExpression.type = resType;
	}	
	@Override
	public void visit(ASTFunctionCall funcCall) {
		if(funcCall == null) return;
		funcCall.scope = currentSym;
		if(!(funcCall.funcname instanceof ASTIdentifier)) {
			errors.add("FunctionCall:funcname must be a identifier.",ErrorId.OTHER_ERRORS);
			return;
		}
		String name = ((ASTIdentifier)funcCall.funcname).value;
		Symbol symbol = global.globalSymbolTable.get(name);
		if(symbol == null) {
			errors.add("FunctionCall:"+name+" is not declarated.",ErrorId.VAR_NOT_DEFINED);
			return;
		}
		Type funcType = symbol.type;
		if(!(funcType instanceof FunctionType)) {
			errors.add("FunctionCall:"+name+" is not a function.",ErrorId.OTHER_ERRORS);
			return;
		}
		visit(funcCall.funcname);
		
		if(GlobalSymbolTable.isMarsPrintStr(name)) {
			// 特殊判断BuiltInFunc,将String添加到全局表中
			FunctionType functionType = (FunctionType)funcType;
			if(funcCall.argList == null || funcCall.argList.size() != 1) {
				errors.add("FunctionCall:"+"Mars_PrintStr's param num is not matched.",ErrorId.FUNC_ARG_NOT_MATCH);
				return;
			}
			ASTExpression arg = funcCall.argList.get(0);
			if (!(arg instanceof ASTStringConstant)) {
				errors.add("FunctionCall:"+"Mars_PrintStr's param should be a stringConstant.",ErrorId.FUNC_ARG_NOT_MATCH);
				return;
			}
			funcCall.canAssigned = false;
			funcCall.type = functionType.getReturnType();
		}else {
			// 在这里要检查一下参数的个数和符号表中声明的参数个数、类型是否匹配
			int size = funcCall.argList.size();
			for (ASTExpression expr : funcCall.argList) {
				visit(expr);
			}
			//funcCall.argList.stream().forEachOrdered(this::visit);
			FunctionType functionType = (FunctionType)funcType;
			if(size != functionType.paramsType.size()) {
				errors.add("FunctionCall:"+name+"'s param num is not matched.",ErrorId.FUNC_ARG_NOT_MATCH);
				return;
			}
			int i = 0;
			for (ASTExpression expression : funcCall.argList) {
				Type type = expression.type;
				Type argType = functionType.paramsType.get(i++);
				if(type == null || !type.isSameType(argType)) {
					errors.add("FunctionCall:"+name+"'s param type is not matched.",ErrorId.FUNC_ARG_NOT_MATCH);
					return;
				}
			}
			funcCall.canAssigned = false;
			funcCall.type = functionType.getReturnType();
		}

		
	}
	@Override
	public void visit(ASTMemberAccess memberAccess) {
		if(memberAccess == null) return;
		errors.add("MemberAccess:not supported for now.",ErrorId.OTHER_ERRORS);
	}
	@Override
	public void visit(ASTPostfixExpression postfixExpression) {
		if(postfixExpression == null)return;
		postfixExpression.scope = currentSym;
		ASTExpression expr = postfixExpression.expr;
		visit(expr);
		if(!expr.canAssigned ) {
			errors.add("PostfixExpression:"+"expression should be a lvalue.",ErrorId.OTHER_ERRORS);
			return;
		}
		if(expr.type == null)	return;
		if(!expr.type.ifGeneralizedInt()) {
			errors.add("PostfixExpression:Expression's value should be int",ErrorId.OTHER_ERRORS);
			return;
		}
		postfixExpression.type = GlobalSymbolTable.intType;
		postfixExpression.canAssigned = true;
	}
	@Override
	public void visit(ASTTypename typename) {
	}
	
	private static final int INC_UANRY_OP = 1;
	private static final int SIZEOF_UANRY_OP = 2;
	private static final int ADDR_UANRY_OP = 3;
	private static final int POINTER_UANRY_OP = 4;
	private static final int ALGO_UANRY_OP = 5;
	private static final int NEG_UANRY_OP = 6;
	@Override
	public void visit(ASTUnaryExpression unaryExpression) {
		if(unaryExpression == null) return;
		unaryExpression.scope = currentSym;
		ASTExpression expr = unaryExpression.expr;
		visit(expr);
		if(expr.type == null)
			return;
		if(expr.type.getType() == BuiltInType.VOID) {
			errors.add("UnaryExpression:expression can not be a void.",ErrorId.OTHER_ERRORS);
			return;
		}
		int unaryOpValue = getUnaryOpValue(unaryExpression.op.value);
		switch(unaryOpValue) {
		case INC_UANRY_OP:

			if(!expr.canAssigned) {
				errors.add("UnaryExpression:Expression can not be assigned.",ErrorId.OTHER_ERRORS);
				return;
			}
			if(!expr.type.ifGeneralizedInt()) {
				errors.add("UnaryExpression:Expression should be int.",ErrorId.OTHER_ERRORS);
				return;
			}
			unaryExpression.type = unaryExpression.expr.type;
			unaryExpression.canAssigned = true;
			return;
		case SIZEOF_UANRY_OP:
			if(unaryExpression.expr.type.getType() == BuiltInType.FUNCTION) {
				errors.add("UnaryExpression:Type can not be a function",ErrorId.OTHER_ERRORS);
				return;
			}
			unaryExpression.type = GlobalSymbolTable.intType;
			unaryExpression.canAssigned = false;
			return;
		case ADDR_UANRY_OP:
			if(unaryExpression.expr.canAssigned == false || unaryExpression.expr.type.getType() != BuiltInType.FUNCTION) {
				errors.add("UnaryExpression: & expression should be a function or a lvalue.",ErrorId.OTHER_ERRORS);
				return;
			}
			unaryExpression.type = GlobalSymbolTable.intType;
			unaryExpression.canAssigned = false;
			return;
		case POINTER_UANRY_OP:
			errors.add("UnaryExpression:* is not supported.",ErrorId.OTHER_ERRORS);
			return;
		case ALGO_UANRY_OP:
			if(unaryExpression.expr.type.getType() ==BuiltInType.ARRAY || unaryExpression.expr.type.getType() == BuiltInType.FUNCTION) {
				errors.add("UnaryExpression:+ - ! expression can not be array or function.",ErrorId.OTHER_ERRORS);
				return;
			}
			unaryExpression.type = GlobalSymbolTable.intType;
			unaryExpression.canAssigned = false;
			return;
		case NEG_UANRY_OP:
			if(!unaryExpression.expr.type.ifGeneralizedInt()) {
				errors.add("UnaryExpression:~ expression should be int.",ErrorId.OTHER_ERRORS);
				return;
			}
			unaryExpression.type = GlobalSymbolTable.intType;
			unaryExpression.canAssigned = false;
			return;
		default:
			errors.add("UnaryExpression:op"+unaryExpression.op.value+" is not defined.",ErrorId.OTHER_ERRORS);
			return;
		}
	}
	@Override
	public void visit(ASTUnaryTypename unaryTypename) {
		if(unaryTypename == null) return;
		unaryTypename.scope = currentSym;
		if(!dealWithSpecifiers(unaryTypename.typename.specfiers)) {
			errors.add("Typename:Specifiers are illegal.",ErrorId.OTHER_ERRORS);
			return;
		}
		ASTDeclarator declarator = unaryTypename.typename.declarator;
		if(declarator != null) {
			String name = declarator.getName();
			if(currentSym.get(name) == null) {
				errors.add("Typename:"+"declarator is not declarated.",ErrorId.VAR_NOT_DEFINED);
				return;
			}
		}
		unaryTypename.canAssigned = false;
		unaryTypename.type = GlobalSymbolTable.intType;
	}
	
	// statement node
	@Override
	public void visit(ASTStatement statement) {
		if(statement == null)
			return;
		if(statement instanceof ASTIterationDeclaredStatement) {
			visit((ASTIterationDeclaredStatement)statement);
		}else if(statement instanceof ASTIterationStatement) {
			visit((ASTIterationStatement)statement);
		}else if(statement instanceof ASTCompoundStatement) {
			visit((ASTCompoundStatement)statement);
		}else if(statement instanceof ASTSelectionStatement) {
			visit((ASTSelectionStatement)statement);
		}else if(statement instanceof ASTExpressionStatement) {
			visit((ASTExpressionStatement)statement);
		}else if(statement instanceof ASTBreakStatement) {
			visit((ASTBreakStatement)statement);
		}else if(statement instanceof ASTContinueStatement) {
			visit((ASTContinueStatement)statement);
		}else if(statement instanceof ASTReturnStatement) {
			visit((ASTReturnStatement)statement);
		}else if(statement instanceof ASTGotoStatement) {
			visit((ASTGotoStatement)statement);
		}else if(statement instanceof ASTLabeledStatement) {
			visit((ASTLabeledStatement)statement);
		}
		if (statement instanceof ASTReturnStatement) {
			lastIsReturn = true;
		}else if(statement instanceof ASTLabeledStatement){
			ASTStatement tmp = ((ASTLabeledStatement)statement).stat ;
			while(tmp instanceof ASTLabeledStatement) {
				tmp = ((ASTLabeledStatement)tmp).stat;
			}
			if(tmp instanceof ASTReturnStatement)
				lastIsReturn = true;
			else
				lastIsReturn = false;
		}
	}
	@Override
	public void visit(ASTCompoundStatement compoundStat) {
		if(compoundStat == null) return;
		compoundStat.scope = currentSym;	
		currentSym = new SymbolTable(currentSym);
		for (ASTNode item : compoundStat.blockItems) {
			if(item instanceof ASTStatement) {
				// 如果是语句,相当于要做一堆 语义检查（使用的变量有没有定义过，操作合不合法）
				visit((ASTStatement)item);
			}else if(item instanceof ASTDeclaration) {
				// 如果是声明，说明要添加到符号表里
				visit((ASTDeclaration)item);
			}
		}
		// 更新父节点的offset
		Integer childOffset = currentSym.getNowOffset();
		currentSym = compoundStat.scope;
		currentSym.setNowOffset(childOffset);
	}
	@Override
	public void visit(ASTExpressionStatement expressionStat) {
		if(expressionStat == null) return;
		expressionStat.scope = currentSym;
		for (ASTExpression expr : expressionStat.exprs) {
			visit(expr);
		}
		//expressionStat.exprs.stream().forEachOrdered(this::visit);
	}
	@Override
	public void visit(ASTSelectionStatement selectionStat) {
		if(selectionStat == null) return;
		selectionStat.scope = currentSym;
		//cond
		for (ASTExpression expr : selectionStat.cond) {
			visit(expr);
		}
		//selectionStat.cond.stream().forEachOrdered(this::visit);
		// then
		
		currentSym = new SymbolTable(currentSym);
		if(selectionStat.then instanceof ASTCompoundStatement) {
			for (ASTNode item : ((ASTCompoundStatement)selectionStat.then).blockItems) {
				if(item instanceof ASTDeclaration) {
					visit((ASTDeclaration)item);
				}else if (item instanceof ASTStatement) {
					visit((ASTStatement)item);
				}
			}
		}else {
			visit(selectionStat.then);
		}
		Integer childOffset = currentSym.getNowOffset();
		currentSym = currentSym.getEnclosingScope();
		currentSym.setNowOffset(childOffset);
		
		// else
		if(selectionStat.otherwise != null) {
			if(selectionStat.otherwise instanceof ASTSelectionStatement) {
				visit(selectionStat.otherwise);
				// 出来之后 有可能是else{},有可能是else +非if语句，也有可能没有else部分
			}else if(selectionStat.otherwise instanceof ASTCompoundStatement) {
				currentSym = new SymbolTable(currentSym);
				for (ASTNode item : ((ASTCompoundStatement)selectionStat.otherwise).blockItems) {
					if(item instanceof ASTDeclaration) {
						visit((ASTDeclaration)item);
					}else if (item instanceof ASTStatement) {
						visit((ASTStatement)item);
					}
				}
				childOffset = currentSym.getNowOffset();
				currentSym = currentSym.getEnclosingScope();
				currentSym.setNowOffset(childOffset);
			}else {
				currentSym = new SymbolTable(currentSym);
				visit(selectionStat.otherwise);
				childOffset = currentSym.getNowOffset();
				currentSym = currentSym.getEnclosingScope();
				currentSym.setNowOffset(childOffset);
			}				
		}

	}
	@Override
	public void visit(ASTIterationStatement iterationStat) {
		if(iterationStat == null) return;
		iterationStat.scope = currentSym;
		currentSym = new SymbolTable(currentSym);
		// 语义检查
		if(iterationStat.init != null) {
			for (ASTExpression expr : iterationStat.init) {
				visit(expr);
			}
			//iterationStat.init.stream().forEachOrdered(this::visit);
		}
			
		if(iterationStat.cond != null) {
			for (ASTExpression expr : iterationStat.cond) {
				visit(expr);
			}
			//iterationStat.cond.stream().forEachOrdered(this::visit);
		}

		if(iterationStat.step != null) {
			for (ASTExpression expr : iterationStat.step) {
				visit(expr);
			}
		//	iterationStat.step.stream().forEachOrdered(this::visit);
		}
		
		// 循环体
		//nowInLoop++;
		if(iterationStat.stat instanceof ASTCompoundStatement) {
			iterationStat.stat.scope = currentSym;
			nowInLoop++;
			for (ASTNode item : ((ASTCompoundStatement)iterationStat.stat).blockItems) {
				if(item instanceof ASTStatement) {
					visit((ASTStatement)item);
				}else if(item instanceof ASTDeclaration) {
					visit((ASTDeclaration)item);
				}
			}
			nowInLoop--;
		}
		else {
			nowInLoop++;
			visit(iterationStat.stat);
			nowInLoop--;
		}
		//nowInLoop--;
		Integer childOffset = currentSym.getNowOffset();
		currentSym = iterationStat.scope;
		currentSym.setNowOffset(childOffset);
	}
	@Override
	public void visit(ASTIterationDeclaredStatement iterationDeclaredStat) {
		if(iterationDeclaredStat == null) return;
		iterationDeclaredStat.scope = currentSym;
		// ()中的声明也属于{}范围
		currentSym = new SymbolTable(currentSym);
		visit(iterationDeclaredStat.init);
		if(iterationDeclaredStat.cond != null) {
			for (ASTExpression expr : iterationDeclaredStat.cond) {
				visit(expr);
			}
			//iterationDeclaredStat.cond.stream().forEachOrdered(this::visit);
		}
		if(iterationDeclaredStat.step != null) {
			for (ASTExpression expr : iterationDeclaredStat.step) {
				visit(expr);
			}
			//iterationDeclaredStat.step.stream().forEachOrdered(this::visit);
		}
			
		
		if(iterationDeclaredStat.stat instanceof ASTCompoundStatement) {
			iterationDeclaredStat.stat.scope = currentSym;
			nowInLoop++;
			for (ASTNode item : ((ASTCompoundStatement)iterationDeclaredStat.stat).blockItems) {
				if(item instanceof ASTStatement) {
					visit((ASTStatement)item);
				}else if(item instanceof ASTDeclaration) {
					visit((ASTDeclaration)item);
				}
			}
			nowInLoop--;
		}
		else {
			nowInLoop++;
			visit(iterationDeclaredStat.stat);
			nowInLoop--;
		}
		Integer childOffset = currentSym.getNowOffset();
		currentSym = iterationDeclaredStat.scope;
		currentSym.setNowOffset(childOffset);
	}
	@Override
	public void visit(ASTGotoStatement gotoStat) {
		if(gotoStat == null) return;
		gotoStat.scope = currentSym;
		labels.add(gotoStat.label);
	}
	@Override
	public void visit(ASTLabeledStatement labeledStat) {
		if(labeledStat == null)return;
		labeledStat.scope = currentSym;
		currentSym.addLabel(labeledStat.label);
		visit(labeledStat.stat);
	}
	@Override
	public void visit(ASTReturnStatement returnStat) {
		if(returnStat == null) return;
		returnStat.scope = currentSym;
		
		// 默认取最后一个作为返回值
		if(returnStat.expr != null) {
			ASTExpression last = returnStat.expr.getLast();
			for (ASTExpression expr : returnStat.expr) {
				visit(expr);
			}
			//returnStat.expr.stream().forEachOrdered(this::visit);
			Type exprType = last.type;
			if(exprType instanceof NormalType && exprType.ifMatchType(nowFuncType)) {
				
				return;
			}else {
				errors.add("ReturnStatement:return type is not match with function.",ErrorId.FUNC_RETURN_NOT_MATCH);
				return;
			}
		}else {
			if(!nowFuncType.isSameType(GlobalSymbolTable.voidType)) {
				errors.add("ReturnStatement:return type should be void.",ErrorId.FUNC_RETURN_NOT_MATCH);
				return;
			}
		}
		

	}
	@Override
	public void visit(ASTBreakStatement breakStat) {
		if(breakStat == null)return;
		breakStat.scope = currentSym;
		if(nowInLoop == 0) {
			errors.add("BreakStatement:must be in a LoopStatement.",ErrorId.BREAK_NOT_IN_LOOP);
			return;
		}
	}
	@Override
	public void visit(ASTContinueStatement continueStatement) {
		if(continueStatement == null) return;
		continueStatement.scope = currentSym;
		if(nowInLoop == 0) {
			errors.add("ContinueStatement:must be in a LoopStatement.",ErrorId.CONTINUE_NOT_IN_LOOP);
			return;
		}
	}
	
	// leaf node
	@Override
	public void visit(ASTToken token) {
		if(token == null) return;
		token.scope = currentSym;
	}
	@Override
	public void visit(ASTFloatConstant floatConst) {
		if(floatConst == null) return;
		floatConst.scope = currentSym;
		floatConst.canAssigned = false;
		floatConst.type = GlobalSymbolTable.doubleType;
	}
	@Override
	public void visit(ASTCharConstant charConst) {
		if(charConst == null) return;
		charConst.scope = currentSym;
		charConst.canAssigned = false;
		charConst.type = GlobalSymbolTable.intType;
	}
	@Override
	public void visit(ASTIntegerConstant intConst) {
		if(intConst == null) return;
		intConst.scope = currentSym;
		intConst.canAssigned = false;
		intConst.type = GlobalSymbolTable.intType;
	}
	@Override
	public void visit(ASTStringConstant stringConst) {
		if(stringConst == null) return;
		stringConst.scope = currentSym;
		stringConst.canAssigned = false;
		stringConst.type = GlobalSymbolTable.stringType;
	}
	@Override
	public void visit(ASTIdentifier identifier) {
		if(identifier == null) return;
		identifier.scope = currentSym;
		Symbol symbol = identifier.scope.get(identifier.value);
		if(symbol == null) {
			errors.add("Identifier "+identifier.value+" is not defined.",ErrorId.VAR_NOT_DEFINED);
			return ;
		}
	//	identifier.info = symbol;
		identifier.type = symbol.type;
		identifier.canAssigned = symbol.type.canAssigned();
	}

	
	// ---------------------------------辅助操作---------------------------------------- //
	// java 1.6不支持switch String:

	private int getOpValue(String op) {
		if(op.equals("=")||op.equals("*=")||op.equals("/=")||op.equals("%=")
				||op.equals("+=")||op.equals("-=")||op.equals("<<=")||op.equals(">>=")
				||op.equals("&=")||op.equals("^=")||op.equals("|=")) {
			return EQUAL_OP;
		}else if (op.equals("&&") || op.equals("||")) {
			return LOGIC_OP;
		}else if (op.equals("==") || op.equals("!=")) {
			return LOGIC_EQUAL_OP;
		}else if (op.equals("<") || op.equals(">")|| op.equals("<=")|| op.equals(">=")) {
			return COMPARE_OP;
		}else if (op.equals("+") || op.equals("-")|| op.equals("*")|| op.equals("/") ||op.equals("%")) {
			return ALGO_OP;
		}else if (op.equals("<<") || op.equals(">>")|| op.equals("&")|| op.equals("|") ||op.equals("^")) {
			return BIT_OP;
		}else {
			return -1;
		}
	}

	private int getUnaryOpValue(String op) {
		if (op.equals("++") || op.equals("--")) {
			return INC_UANRY_OP;
		}else if (op.equals("sizeof")) {
			return SIZEOF_UANRY_OP;
		}else if (op.equals("&")) {
			return ADDR_UANRY_OP;
		}else if (op.equals("*")) {
			return POINTER_UANRY_OP;
		}else if (op.equals("+") || op.equals("-")|| op.equals("~")) {
			return ALGO_UANRY_OP;
		}else if (op.equals("!")) {
			return NEG_UANRY_OP;
		}else {
			return -1;
		}
	}
	// typeSpecifiers 是否合法
	private boolean dealWithSpecifiers(List<ASTToken> specifiers) {
		if(specifiers == null || specifiers.size() == 0)
			return false;
		return true;
	}
	// 这里是很粗糙的对specifiers的判断，返回specifiers列表的最后一个，作为类型
	public static NormalType getSpecifierType(List<ASTToken> specifiers) {
		String str = specifiers.get(specifiers.size()-1).value;
		if(str.equals("void")) {
			return GlobalSymbolTable.voidType;
		}else if(str.equals("int")) {
			return GlobalSymbolTable.intType;
		}else if(str.equals("long")) {
			return GlobalSymbolTable.longType;
		}else if(str.equals("short")) {
			return GlobalSymbolTable.shortType;
		}else if(str.equals("float")) {
			return GlobalSymbolTable.floatType;
		}else if(str.equals("double")) {
			return GlobalSymbolTable.doubleType;
		}else {
			return null;
		}
	}

}
