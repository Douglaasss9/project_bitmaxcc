package bit.minisys.minicc.symbol;

import java.util.ArrayList;

public class FunctionSymbol extends Symbol {
    public int paramCount;
    public ArrayList<String> paramTypes;
    public String returnType;

    public FunctionSymbol() {
        this.paramTypes = new ArrayList<>();
    }
}