package bit.minisys.minicc.symbol;

import java.util.ArrayList;
import java.util.HashMap;

public class SymbolTable {

    private final HashMap<String, Symbol> arrayNames;
    private final HashMap<String, Symbol> functionNames;
    private final HashMap<String, Symbol> labelNames;
    private final HashMap<String, Symbol> variableNames;

    public SymbolTable() {
        this.arrayNames = new HashMap<>();
        this.functionNames = new HashMap<>();
        this.labelNames = new HashMap<>();
        this.variableNames = new HashMap<>();
    }

    public boolean hasVariable(String name) {
        return variableNames.containsKey(name);
    }

    public boolean hasFunction(String name) {
        return functionNames.containsKey(name);
    }

    public boolean hasLabel(String name) {
        return labelNames.containsKey(name);
    }

    public boolean hasArray(String name) {
        return arrayNames.containsKey(name);
    }

    public void addFunction(String name, String returnType, ArrayList<String> paramsType) {
        FunctionSymbol item = new FunctionSymbol();
        item.paramCount = paramsType.size();
        item.paramTypes = paramsType;
        item.returnType = returnType;
        item.text = name;
        item.type = "function";

        functionNames.put(name, item);
    }

    public void addVariable(String name, String type, String flag, int offset) {
        VariableSymbol item = new VariableSymbol();
        item.text = name;
        item.type = "variable";
        item.varType = type;
        item.flag = flag;
        item.offset = offset;

        variableNames.put(name, item);
    }
/*
    public void addLabel(String name) {
        LabelSymbol item = new LabelSymbol();
        item.type = "label";
        item.text = name;

        labelNames.put(name, item);
    }

    public void addArray(String name, ArrayList<Integer> lens, String type) {
        ArraySymbol item = new ArraySymbol();
        item.type = "array";
        item.text = name;
        item.lens = lens;
        item.arrType = type;
        item.dim = lens.size();

        arrayNames.put(name, item);
    }
*/
    public VariableSymbol getVariable(String name) {
        return (VariableSymbol) variableNames.get(name);
    }

    public FunctionSymbol getFunction(String name) {
        return (FunctionSymbol) functionNames.get(name);
    }
    /*
    public ArraySymbol getArray(String name) {
        return (ArraySymbol) arrayNames.get(name);
    }
    */
}
